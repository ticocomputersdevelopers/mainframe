
module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        // watch: {
        //     js: {
        //       files: ['js/client/**/*.js','js/partner/*.js','js/login.js','js/contact_form.js','js/newsletter.js','js/forgot_password.js','js/main.js','js/admin/**/*.js'],
        //       tasks: ['uglify'],
        //     }
        // },
        uglify: {
            options: {
                manage: false
            },
            my_target: {
                files: [
                    {
                        'js/shop/themes/bsmodern/shop.min.js': ['js/shop/themes/bsmodern/**/*.js', '!js/shop/themes/bsmodern/shop.min.js']
                    }
                ]
            }
        },
        cssmin: {
            my_target: {
                files: [{
                    'css/themes/bsmodern/shop.min.css': ['css/themes/bsmodern/style.css','css/themes/bsmodern/custom.css']
                }]
            }
        }

    });


    // include pakages
    // grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.registerTask('default', ["uglify","cssmin"]);
};
