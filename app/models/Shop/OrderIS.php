<?php

class OrderIS {

    public static function orderLogik($nacin_isporuke,$nacin_placanja,$napomena=''){
        $mp_kupac_id_is = !is_null(Options::info_sys('logik')->mp_kupac_id_is) ? Options::info_sys('logik')->mp_kupac_id_is : '100';
        $partner_id = DB::table('partner')->where('id_is',$mp_kupac_id_is)->pluck('partner_id');
        $web_b2c_narudzbina_id = DB::select("SELECT last_value as web_b2c_narudzbina_id FROM web_b2c_narudzbina_web_b2c_narudzbina_id_seq")[0]->web_b2c_narudzbina_id;
        $current_order_id = DB::connection('logik')->select("SELECT MAX(sifra_narudzbine_logik) as current_order_id FROM narudzbine")[0]->current_order_id + 1;
        $sql = "INSERT INTO narudzbine (sifra_narudzbine_tico,sifra_kupca_logik,sifra_kupca_tico,vrsta_narudzbina,datum_narudzbine,sifra_nacin_isporuke,naziv_nacin_isporuke,sifra_nacin_placanja,naziv_nacin_placanja,narudzbina_prihvacena,narudzbina_stornirana,narudzbina_realizovana,roba_poslata,sluzba_slanje,broj_posiljke,napomena,semaphore) VALUES ('".strval($web_b2c_narudzbina_id)."',".$mp_kupac_id_is.",".($partner_id ? $partner_id : 'NULL').",0,'".date('Y-m-d')."',1,'".$nacin_isporuke."',1,'".$nacin_placanja."',0,0,0,0,'Nedefinisana',NULL,'".$napomena."',1)";
        DB::connection('logik')->statement($sql);
        return (object) array('id_is'=>$current_order_id,'web_b2c_narudzbina_id'=>$web_b2c_narudzbina_id);
    }
    public static function addOrderStavkaLogik($order_ids,$article,$item_number,$quantity){
        $sql = "INSERT INTO narudzbine_stavke (sifra_narudzbine_tico,sifra_artikla_logik,sifra_artikla_tico,redni_broj_stavke,kolicina,sifra_tarifne_grupe,nabavna_cena,dilerska_cena,end_cena,maloprodajna_cena,web_cena,prodajna_cena,semaphore) VALUES ('".strval($order_ids->web_b2c_narudzbina_id)."',".strval(($article->id_is && $article->id_is != '') ? $article->id_is : 'NULL').",'".strval($article->roba_id)."',".strval($item_number).",".strval($quantity).",".strval($article->tarifna_grupa_id).",".$article->racunska_cena_nc.",".$article->racunska_cena_a.",".$article->racunska_cena_a.",".$article->mpcena.",".$article->web_cena.",".$article->web_cena.",1)";
        DB::connection('logik')->statement($sql);
    }

    public static function createOrderLogik($orderDetails){
        $success = false;
        $order_id = 0;

        $customer = DB::table('web_kupac')->where('web_kupac_id',$orderDetails['web_kupac_id'])->first();
        if(!is_null($customer)){
            $delivery = DB::table('web_nacin_isporuke')->where('web_nacin_isporuke_id',$orderDetails['web_nacin_isporuke_id'])->pluck('naziv');
            $payment = DB::table('web_nacin_placanja')->where('web_nacin_placanja_id',$orderDetails['web_nacin_placanja_id'])->pluck('naziv');

            $note = ($customer->flag_vrsta_kupca == 0 ? ($customer->ime.' '.$customer->prezime) : ($customer->naziv.' '.$customer->pib)) .';'. ($customer->adresa ? $customer->adresa : '') .';'. ($customer->mesto ? $customer->mesto : '') .';'. ($customer->telefon ? $customer->telefon : '') .';'. ($customer->email ? $customer->email : '') .';'.$orderDetails['napomena'];

            $order_ids = self::orderLogik($delivery,$payment,$note);
            $cartItems = DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_id',$order_ids->web_b2c_narudzbina_id)->get();
            foreach($cartItems as $key => $stavka){
                $article = DB::table('roba')->where('roba_id',$stavka->roba_id)->first();
                self::addOrderStavkaLogik($order_ids,$article,($key+1),$stavka->kolicina);
            }

            $success = true;
        }
        return (object) array('success'=>$success, 'order_id'=>$order_id);
    }



    public static function orderInfograf($partner_id,$napomena=''){
        $sql = "INSERT INTO narudzbina (partner_id,napomena) VALUES (".strval($partner_id).",'".strval($napomena)."')";
        DB::connection('infograf')->statement($sql);
        $last_order_id = DB::connection('infograf')->select("SELECT MAX(id) as last_order_id FROM narudzbina")[0]->last_order_id;
        return $last_order_id;
    }
    public static function addOrderStavkaInfograf($web_order_id,$article_id,$quantity,$amount,$discount){
        $sql = "INSERT INTO narudzbina_stavka (narudzbina_id,artikal_id,kolicina,cena,rabat) VALUES (".strval($web_order_id).",".strval($article_id).",".strval($quantity).",".strval($amount).",".strval($discount).")";
        DB::connection('infograf')->statement($sql);
    }
    public static function orderConfirmInfograf($order_id){
        $sql = "CALL web_order(".strval($order_id).")";
        $result = DB::connection('infograf')->select($sql);
        if(isset($result[0])){
            return $result[0]->dok;
        }
        return 0;
    }
    public static function createOrderInfograf($orderDetails){
        $partner_id_is = !is_null(Options::info_sys('infograf')->mp_kupac_id_is) ? Options::info_sys('infograf')->mp_kupac_id_is : '100';
        $success = false;
        $order_id = 0;

        $kurs = DB::table('kursna_lista')->where('kursna_lista_id',DB::table('kursna_lista')->max('kursna_lista_id'))->pluck('web');

        $customer = DB::table('web_kupac')->where('web_kupac_id',$orderDetails['web_kupac_id'])->first();

        if(!is_null($customer)){
            $web_b2c_narudzbina_id = DB::select("SELECT (last_value) as web_b2c_narudzbina_id FROM web_b2c_narudzbina_web_b2c_narudzbina_id_seq")[0]->web_b2c_narudzbina_id;
            $delivery = DB::table('web_nacin_isporuke')->where('web_nacin_isporuke_id',$orderDetails['web_nacin_isporuke_id'])->pluck('naziv');
            $payment = DB::table('web_nacin_placanja')->where('web_nacin_placanja_id',$orderDetails['web_nacin_placanja_id'])->pluck('naziv');

            $note = strval($web_b2c_narudzbina_id) .';'. ($customer->flag_vrsta_kupca == 0 ? ($customer->ime.' '.$customer->prezime) : ($customer->naziv.' '.$customer->pib)) .';'. ($customer->adresa ? $customer->adresa : '') .';'. ($customer->mesto ? $customer->mesto : '') .';'. ($customer->telefon ? $customer->telefon : '') .';'. ($customer->email ? $customer->email : '') .';'. $delivery .';'. $payment .';'. $orderDetails['napomena'];

            $cartItems = DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->get();

            $order_id = self::orderInfograf($partner_id_is,$note);

            foreach($cartItems as $stavka){
                $roba = DB::table('roba')->where('roba_id',$stavka->roba_id)->first();
                self::addOrderStavkaInfograf($order_id,$roba->id_is,$stavka->kolicina,(($roba->web_cena / 1.2) / $kurs),0);
            }
            
            $order_id = self::orderConfirmInfograf($order_id);
            $success = true;
        }

        return (object) array('success'=>$success, 'order_id'=>$order_id);
    }

    public static function createOrderSbcs($orderDetails){
        $partner_id_is = !is_null(Options::info_sys('sbcs')->mp_kupac_id_is) ? Options::info_sys('sbcs')->mp_kupac_id_is : '100';

        $success = false;
        $customer = DB::table('web_kupac')->where('web_kupac_id',$orderDetails['web_kupac_id'])->first();

        if(!is_null($customer)){
            $web_b2c_narudzbina_id = DB::select("SELECT (last_value) as web_b2c_narudzbina_id FROM web_b2c_narudzbina_web_b2c_narudzbina_id_seq")[0]->web_b2c_narudzbina_id;
            $delivery = DB::table('web_nacin_isporuke')->where('web_nacin_isporuke_id',$orderDetails['web_nacin_isporuke_id'])->pluck('naziv');
            $payment = DB::table('web_nacin_placanja')->where('web_nacin_placanja_id',$orderDetails['web_nacin_placanja_id'])->pluck('naziv');

            $note = strval($web_b2c_narudzbina_id) .';'. ($customer->flag_vrsta_kupca == 0 ? ($customer->ime.' '.$customer->prezime) : ($customer->naziv.' '.$customer->pib)) .';'. ($customer->adresa ? $customer->adresa : '') .';'. ($customer->mesto ? $customer->mesto : '') .';'. ($customer->telefon ? $customer->telefon : '') .';'. ($customer->email ? $customer->email : '') .';'. $delivery .';'. $payment .';'. $orderDetails['napomena'];

            $cartItems = DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->get();

            //generate xml
            $xml = new DOMDocument("1.0","UTF-8");
            $narudzbina = $xml->createElement("narudzbina");
            $xml->appendChild($narudzbina);         
            
            B2bCommon::xml_node($xml,"kupac_id",$partner_id_is,$narudzbina);
            B2bCommon::xml_node($xml,"datum",date('Y-m-d'),$narudzbina);
            B2bCommon::xml_node($xml,"napomena",$note,$narudzbina);

            $narudzbina_stavke = $xml->createElement("narudzbina_stavke");

            foreach($cartItems as $item){
                $roba = DB::table('roba')->where('roba_id',$item->roba_id)->first();

                if(!is_null($roba)){
                    $stavka = $xml->createElement("narudzbina_stavka");

                    B2bCommon::xml_node($xml,"roba_id",(!is_null($roba->id_is) && $roba->id_is != '' ? $roba->id_is : '-1'),$stavka);
                    B2bCommon::xml_node($xml,"sifra",(!is_null($roba->sifra_is) && $roba->sifra_is != '' ? $roba->sifra_is : ''),$stavka);
                    B2bCommon::xml_node($xml,"kolicina",$item->kolicina,$stavka);
                    B2bCommon::xml_node($xml,"cena",$item->jm_cena,$stavka);
                    B2bCommon::xml_node($xml,"vpcena",$item->racunska_cena_nc,$stavka);
                    B2bCommon::xml_node($xml,"pdv",DB::table('tarifna_grupa')->where('tarifna_grupa_id',$roba->tarifna_grupa_id)->pluck('porez'),$stavka);
                    B2bCommon::xml_node($xml,"jedinica_mere",DB::table('jedinica_mere')->where('jedinica_mere_id',$roba->jedinica_mere_id)->pluck('naziv'),$stavka);
                    B2bCommon::xml_node($xml,"broj_stavke",$item->broj_stavke,$stavka);
                    B2bCommon::xml_node($xml,"opis",substr($roba->naziv,0,250),$stavka);

                    $narudzbina_stavke->appendChild($stavka);
                }

            }

            $narudzbina->appendChild($narudzbina_stavke);
            $store_path = 'files/IS/xml/narudzbine/b2c/narudzbina'.$web_b2c_narudzbina_id.'.xml';

            $xml->formatOutput = true;
            $xml->save($store_path) or die("Error");
            
            $success = true;
        }
        return $success;
    }

    // public static function createOrderXml($orderDetails){
    //     $partner_id_is = !is_null(Options::info_sys('sbcs')->mp_kupac_id_is) ? Options::info_sys('sbcs')->mp_kupac_id_is : '100';

    //     $success = false;
    //     $customer = DB::table('web_kupac')->where('web_kupac_id',$orderDetails['web_kupac_id'])->first();

    //     if(!is_null($customer)){
    //         $web_b2c_narudzbina_id = DB::select("SELECT (last_value) as web_b2c_narudzbina_id FROM web_b2c_narudzbina_web_b2c_narudzbina_id_seq")[0]->web_b2c_narudzbina_id;
    //         $delivery = DB::table('web_nacin_isporuke')->where('web_nacin_isporuke_id',$orderDetails['web_nacin_isporuke_id'])->pluck('naziv');
    //         $payment = DB::table('web_nacin_placanja')->where('web_nacin_placanja_id',$orderDetails['web_nacin_placanja_id'])->pluck('naziv');

    //         $note = strval($web_b2c_narudzbina_id) .';'. ($customer->flag_vrsta_kupca == 0 ? ($customer->ime.' '.$customer->prezime) : ($customer->naziv.' '.$customer->pib)) .';'. ($customer->adresa ? $customer->adresa : '') .';'. ($customer->mesto ? $customer->mesto : '') .';'. ($customer->telefon ? $customer->telefon : '') .';'. ($customer->email ? $customer->email : '') .';'. $delivery .';'. $payment .';'. $orderDetails['napomena'];

    //         $cartItems = DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->get();

    //         //generate xml
    //         $xml = new DOMDocument("1.0","UTF-8");
    //         $narudzbina = $xml->createElement("narudzbina");
    //         $xml->appendChild($narudzbina);         
            
    //         B2bCommon::xml_node($xml,"kupac_id",$partner_id_is,$narudzbina);
    //         B2bCommon::xml_node($xml,"datum",date('Y-m-d'),$narudzbina);
    //         B2bCommon::xml_node($xml,"napomena",$note,$narudzbina);

    //         $narudzbina_stavke = $xml->createElement("narudzbina_stavke");

    //         foreach($cartItems as $item){
    //             $roba = DB::table('roba')->where('roba_id',$item->roba_id)->first();

    //             if(!is_null($roba)){
    //                 $stavka = $xml->createElement("narudzbina_stavka");

    //                 B2bCommon::xml_node($xml,"roba_id",(!is_null($roba->id_is) && $roba->id_is != '' ? $roba->id_is : '-1'),$stavka);
    //                 B2bCommon::xml_node($xml,"sifra",(!is_null($roba->sifra_is) && $roba->sifra_is != '' ? $roba->sifra_is : ''),$stavka);
    //                 B2bCommon::xml_node($xml,"kolicina",$item->kolicina,$stavka);
    //                 B2bCommon::xml_node($xml,"cena",$item->jm_cena,$stavka);
    //                 B2bCommon::xml_node($xml,"vpcena",$item->racunska_cena_nc,$stavka);
    //                 B2bCommon::xml_node($xml,"pdv",DB::table('tarifna_grupa')->where('tarifna_grupa_id',$roba->tarifna_grupa_id)->pluck('porez'),$stavka);
    //                 B2bCommon::xml_node($xml,"jedinica_mere",DB::table('jedinica_mere')->where('jedinica_mere_id',$roba->jedinica_mere_id)->pluck('naziv'),$stavka);
    //                 B2bCommon::xml_node($xml,"broj_stavke",$item->broj_stavke,$stavka);
    //                 B2bCommon::xml_node($xml,"opis",substr($roba->naziv,0,250),$stavka);

    //                 $narudzbina_stavke->appendChild($stavka);
    //             }

    //         }

    //         $narudzbina->appendChild($narudzbina_stavke);
    //         $store_path = 'files/IS/xml/narudzbine/b2c/narudzbina'.$web_b2c_narudzbina_id.'.xml';

    //         $xml->formatOutput = true;
    //         $xml->save($store_path) or die("Error");
            
    //         $success = true;
    //     }
    //     return $success;
    // }

    public static function wingsAutorize($username,$password){

        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://portal.wings.rs/api/v1/".Options::info_sys('wings')->portal."/system.user.log?output=jsonapi",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => '{"aUn": "'.$username.'","aUp": "'.$password.'"}',
          CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json"
          ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
          $error = "cURL Error #:" . $err;
          return false;
        } else {
          $result = json_decode($response);
          return $result;
        }       
    }

    public static function wingsOrderRabate($access_token,$payload_body){
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://portal.wings.rs/api/v1/".Options::info_sys('wings')->portal."/local.proxy.narudzbenica?output=jsonapi",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => $payload_body,
          CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json",
            "Cookie: PHPSESSID=".$access_token.""
          ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
          $error = "cURL Error #:" . $err;
          return false;
        } else {
          $result = json_decode($response);
          return $result;
        }       
    }

    public static function wingsLogout($access_token){

        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://portal.wings.rs/api/v1/".Options::info_sys('wings')->portal."/system.user.log?logout=1",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "Cookie: PHPSESSID=".$access_token.""
          ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);  
    }

    public static function createOrderWings($orderDetails){
        $success = false;
        $order_id = 0;

        $customer = DB::table('web_kupac')->where('web_kupac_id',$orderDetails['web_kupac_id'])->first();
        if(!is_null($customer)){
            $delivery = DB::table('web_nacin_isporuke')->where('web_nacin_isporuke_id',$orderDetails['web_nacin_isporuke_id'])->pluck('naziv');
            $payment = DB::table('web_nacin_placanja')->where('web_nacin_placanja_id',$orderDetails['web_nacin_placanja_id'])->pluck('naziv');

            $note = ($customer->flag_vrsta_kupca == 0 ? ($customer->ime.' '.$customer->prezime) : ($customer->naziv.' '.$customer->pib)) .';'. ($customer->telefon ? $customer->telefon : '') .';'. ($customer->adresa ? $customer->adresa : '') .';'. ($customer->mesto ? $customer->mesto : '') .';'. ($customer->email ? $customer->email : '') .';'.$orderDetails['napomena'];

            $web_b2c_narudzbina_id = DB::select("SELECT (last_value) as web_b2c_narudzbina_id FROM web_b2c_narudzbina_web_b2c_narudzbina_id_seq")[0]->web_b2c_narudzbina_id;
            $cartItems = DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->get();

            $wingsItems = array(
                'partner'=> intval(Options::info_sys('wings')->mp_kupac_id_is),
                'status'=> 'K',
                'valuta'=> date('m.d.y'),
                'rok'=> 5,
                'isporuka'=> '',
                'objekat'=> '',
                'magacin'=> '',
                'napomena'=> substr($note,0,75),
                'artikal'=>array()
                );
            
            foreach($cartItems as $row){
                $roba = DB::table('roba')->where('roba_id',$row->roba_id)->first();
                if(isset($roba->id_is) && $roba->id_is != null){
                    $wingsItems['artikal'][]=[
                      'id'=> intval($roba->id_is),
                      'kolicina'=>intval($row->kolicina),
                      'rabat'=>0
                    ];
                }
            }

            if(count($wingsItems['artikal']) > 0){
                $payload = json_encode($wingsItems);
                $wings_data = Options::info_sys('wings');
                $username = $wings_data->username;
                $password = $wings_data->password;
                $result = self::wingsAutorize($username,$password);
                if(!($result == false || isset($result->errors))){
                    $token = $result->data[0]->attributes->token;
                    $response = self::wingsOrderRabate($token,$payload);
                    if(!isset($response->errors) && isset($response->data) && isset($response->data->id)){
                        $order_id = intval($response->data->id);
                        $success = true;
                    }
                    self::wingsLogout($token);
                }
            }
        }
        return (object) array('success'=>$success, 'order_id'=>$order_id);
    }
    public static function createOrderAbacus($order_id){
        $success = false;
        
        $order_details=DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$order_id)->first();
        $order_items=DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_id',$order_id)->get();

        $items=array();
        foreach ($order_items as $item) {
            $roba=DB::table('roba')->where('roba_id',$item->roba_id)->first();
            if(!empty($roba->id_is)){
                $items[]=array(
                    'id_item'=> $roba->id_is,
                    'quantity'=> $item->kolicina,
                    'price'=> $item->jm_cena
                );
            }
        }
        $abacusItems = array('order'=> 
            array(
                'id_partner'=> intval(Options::info_sys('abacus')->mp_kupac_id_is),
                'note'=> 'Order number:'.$order_details->broj_dokumenta.' Note:'.$order_details->napomena,
                'items'=>$items,
                ),
            "id_m"=> 8
            );

        $payload = json_encode($abacusItems);
        $curl = curl_init();

            curl_setopt_array($curl, array(
              CURLOPT_URL => 'http://api.bencomltd.com/TECH/Main/postSale?token=v8K5Um3jJC&protocol=2.0',
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => '',
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 0,
              CURLOPT_FOLLOWLOCATION => true,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => 'POST',
              CURLOPT_POSTFIELDS =>$payload,
              CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                'Cookie: ASP.NET_SessionId=3qgvm3nydfqoglzn2an1uezd'
              ),
            ));

            $response = curl_exec($curl);

            curl_close($curl);
            $response=json_decode($response);

            if(is_null($response->Error) ){
                $success = true;
            }

        return (object) array('success'=>$success, 'order_id'=>$order_id);
    }
    // public static function createOrderTimCode($orderDetails,$web_b2c_narudzbina_id=null){
    //     $success = false;
    //     $order_id = 0;

    //     $customer = DB::table('web_kupac')->where('web_kupac_id',$orderDetails['web_kupac_id'])->first();
    //     if(!is_null($customer)){
    //         $delivery = DB::table('web_nacin_isporuke')->where('web_nacin_isporuke_id',$orderDetails['web_nacin_isporuke_id'])->pluck('naziv');
    //         $payment = DB::table('web_nacin_placanja')->where('web_nacin_placanja_id',$orderDetails['web_nacin_placanja_id'])->pluck('naziv');

    //         $note = ($customer->flag_vrsta_kupca == 0 ? ($customer->ime.' '.$customer->prezime) : ($customer->naziv.' '.$customer->pib)) .';'. ($customer->telefon ? $customer->telefon : '') .';'. ($customer->adresa ? $customer->adresa : '') .';'. ($customer->mesto ? $customer->mesto : '') .';'. ($customer->email ? $customer->email : '') .';'.$orderDetails['napomena'];

    //         $web_b2c_narudzbina_id = !is_null($web_b2c_narudzbina_id) ? $web_b2c_narudzbina_id : DB::select("SELECT (last_value) as web_b2c_narudzbina_id FROM web_b2c_narudzbina_web_b2c_narudzbina_id_seq")[0]->web_b2c_narudzbina_id;
    //         $web_b2c_narudzbina = DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->first();
    //         $cartItems = DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->get();


    //         // $data['auth'] = array(
    //         //     'username' => "sinko.webservice",
    //         //     'password' => "vrf6832",
    //         //     'firmaNaziv' => "SINKO PLUS",
    //         //     'godina' => date('Y'),
    //         // );
    //         $infoSys = Options::info_sys('timcode');
    //         $data = array(
    //                     'auth' => array(
    //                         'username' => $infoSys->username,
    //                         'password' => $infoSys->password,
    //                         'firmaNaziv' => $infoSys->portal,
    //                         'godina' => date('Y')
    //                     )
    //                 );
    //         $data['arrProfak'] = array(
    //             'magacinID'=> intval($infoSys->b2c_magacin),
    //             'komitID' => intval($infoSys->mp_kupac_id_is),
    //             'brdok' => '',
    //             'datum' => date('Y-m-d'),
    //             'datumDpo' => date('Y-m-d'),
    //             'vrstaRezervacije' => 1,
    //             'opis' => $web_b2c_narudzbina->broj_dokumenta,
    //             'usloviPlacanja' => '',
    //             'napomena' => $note,
    //             'mestoIsporukeID' => '',
    //             'devValuta' => '',
    //             'devKurs' => '',
    //             'profakStav' => array()
    //         );

    //         foreach($cartItems as $row){
    //             $roba = DB::table('roba')->where('roba_id',$row->roba_id)->first();
    //             if(isset($roba->id_is) && $roba->id_is != null){
    //                 $data['arrProfak']['profakStav'][]=[
    //                     'robaID'=> intval($roba->id_is),
    //                     'kolIzlaz'=>floatval($row->kolicina),
    //                     'fakCena' => round($row->jm_cena,2),
    //                     'rabatProc' => 0,
    //                     'prodVred' => 0,
    //                     'devFakCena' => 0,
    //                     'devProdVred' => 0
    //                 ];
    //             }
    //         }

    //         $client = new SoapClient('https://tim-erp.com/ERPX_WEB/awws/ErpX.awws?wsdl');
    //         $response = $client->wsUplProfak($data);

    //         if($response){
    //             $success = true;
    //         }
    //     }
    //     return (object) array('success'=>$success, 'order_id'=>$order_id);
    // }

    // public static function createOrderSpSoft($orderId){
    //     $order = DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$orderId)->first();

    //     $cartItems=DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_id',$orderId)->orderBy('broj_stavke','asc')->get();
    //     $nacin_isporuke = DB::table('web_nacin_isporuke')->where('web_nacin_isporuke_id',$order->web_nacin_isporuke_id)->pluck('naziv');
    //     $nacin_placanja = DB::table('web_nacin_placanja')->where('web_nacin_placanja_id',$order->web_nacin_placanja_id)->pluck('naziv');
    //     $note=substr('Način isporuke: '.$nacin_isporuke.'. Način plaćanja: '.$nacin_placanja.'. Napomena: '.$order->napomena,0,255);
    //     // $avans = (isset($order->web_nacin_placanja_id) && $order->web_nacin_placanja_id != 9) ? 0 : -4.1667;

    //     $success = false;
    //     $partner = DB::table('partner')->where('id_is',intval(Options::info_sys('spsoft')->mp_kupac_id_is))->first();

    //     if(!is_null($partner) && !is_null($partner->id_is)){
    //         // $troskovi = B2bBasket::troskovi();
    //         self::createOrderDocumentSpSoft($partner,$order,$note);

    //         foreach($cartItems as $key => $stavka){
    //             $roba = DB::table('roba')->where('roba_id',$stavka->roba_id)->first();
    //             if(!empty($roba->id_is)){
    //                 self::addOrderItemSpSoft($order->web_b2c_narudzbina_id,intval($roba->id_is),$stavka->kolicina,$stavka->jm_cena,0,($key+1));
    //             }
    //         }
    //         $success = true;
    //     }
    //     return (object) array('success'=>$success, 'order_id'=>null);
    // }

    // public static function createOrderDocumentSpSoft($partner,$order,$note){
    //     $docKey = null;
    //     $db = DB::connection('ortopedija');

    //     $data = array(
    //         'id' => $order->web_b2c_narudzbina_id,
    //         'datDok' => $order->datum_dokumenta,
    //         'vrstaDok' => 'B2B',
    //         'idPP' => intval($partner->id_is),
    //         'statusDok' => 0,
    //         'komentar' => $note
    //     );
    //     $db->table('narucenowebz')->insert($data);
    // }

    // public static function addOrderItemSpSoft($orderId,$articleId,$quantity,$price,$rebate,$sortNumber){
    //     $itemOrder = null;
    //     $db = DB::connection('ortopedija');

    //     $data = array(
    //         'id' => $orderId,
    //         'rb' => $sortNumber,
    //         'idArt' => $articleId,
    //         'kolicina' => $quantity,
    //         'cena' => $price,
    //         'procRabat' => $rebate
    //     );
    //     $db->table('narucenowebs')->insert($data);
    // }

}
