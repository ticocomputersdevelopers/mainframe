<?php 
class Slider {

    public static function slajder($slajder_id){
        return DB::table('slajder')->select('slajder.*','slajder_tip.naziv as slajder_tip_naziv')->join('slajder_tip','slajder.slajder_tip_id','=','slajder_tip.slajder_tip_id')->where(['slajder.flag_aktivan'=>1, 'slajder_id'=>$slajder_id])->first();
    }

    public static function slajderStavke($slajder_id){
        $now = date('Y-m-d');
        return DB::table('slajder_stavka')->leftJoin('slajder_stavka_jezik','slajder_stavka.slajder_stavka_id','=','slajder_stavka_jezik.slajder_stavka_id')
        ->where(['flag_aktivan'=>1, 'slajder_id'=>$slajder_id])->where(function($q){ $q->where(['jezik_id'=>Language::lang_id()])->orWhereNull('jezik_id'); })
        ->where(function($q) use ($now){ $q->where('datum_od','<',$now)->orWhereNull('datum_od'); })
        ->where(function($q) use ($now){ $q->where('datum_do','>=',$now)->orWhereNull('datum_do'); })
        ->orderBy('rbr','asc')->get();
    }

    public static function getFirstSlider($tip='Slajder'){
        return DB::table('slajder')->where(['flag_aktivan'=>1, 'slajder_tip_id'=>DB::table('slajder_tip')->where('naziv',$tip)->pluck('slajder_tip_id')])->first();
    }

    // public static function slajd_data($baner_id)
    // {
    //     $lang_data = DB::table('baneri_jezik')->where(array('baneri_id'=>$baner_id,'jezik_id'=>Language::lang_id()))->first();
    //     if(is_null($lang_data)){
    //         $lang_data = (object) array('sadrzaj'=>'','naslov'=>'','nadnaslov'=>'','naslov_dugme'=>'','link'=>'','alt'=>'');
    //     }
    //     return $lang_data;
    // }

    // public static function trajanje_banera($id){ 
        
    // foreach(DB::table('baneri')->where('baneri_id',$id)->get() as $row){            
            
    //     if(isset($row->datum_do) && isset($row->datum_od)){
    //         $do = date_create($row->datum_do);
    //         $od = date_create($row->datum_od);
    //         $danasnji_dan = date_create();
        
    //         $trajanje_banera = date_diff($do,$danasnji_dan);        

    //         if($trajanje_banera->invert == 1 ){
    //         return 1;
    //         }else{
    //         return 0;     
    //         }
    //     } else{
    //         return 1;
    //     }        
    // }
    // }

    // public static function future_banners($id){ 
        
    //     foreach(DB::table('baneri')->where('baneri_id',$id)->get() as $row){            
            
    //         if(isset($row->datum_do) && isset($row->datum_od)){
    //         $trajanje = date_create($row->datum_od);
    //         $danasnji_dan = date_create(); 

    //         $trajanje_banera = date_diff($trajanje,$danasnji_dan);
    //         //var_dump($trajanje_banera->invert);die;
            
    //         if($trajanje_banera->invert == 0){
    //         return 1;
    //         }else{
    //         return 0;     
    //         }
    //     }else{
    //         return 1;
    //     }
    //     }         
    // }
    
    // public static function popup_banners(){         
    //    return DB::table('baneri')->where('tip_prikaza',9)->where('aktivan',1)->get();     
    // }

    // public static function fileExtension($image_path){
    //     $extension = 'jpg';
    //     $slash_parts = explode('/',$image_path);
    //     $old_name = $slash_parts[count($slash_parts)-1];
    //     $old_name_arr = explode('.',$old_name);
    //     $extension = $old_name_arr[count($old_name_arr)-1];
    //     return $extension;
    // }

}