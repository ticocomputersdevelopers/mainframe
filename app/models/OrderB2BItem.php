<?php

class OrderB2BItem extends Eloquent
{
    protected $table = 'web_b2b_narudzbina_stavka';

    protected $primaryKey = 'web_b2b_narudzbina_stavka_id';
    
    public $timestamps = false;

	protected $hidden = array(
        'web_b2b_narudzbina_stavka_id',
        'web_b2b_narudzbina_id',
        'broj_stavke',
        'roba_id',
        'kolicina',
        'jm_cena',
        'tarifna_grupa_id',
        'racunska_cena_nc'
    	);

	public function getIdAttribute()
	{
	    return isset($this->attributes['web_b2b_narudzbina_stavka_id']) ? $this->attributes['web_b2b_narudzbina_stavka_id'] : null;
	}
    public function getOrderIdAttribute()
    {
        return isset($this->attributes['web_b2b_narudzbina_id']) ? $this->attributes['web_b2b_narudzbina_id'] : null;
    }
    public function getSortAttribute()
    {
        return isset($this->attributes['broj_stavke']) ? $this->attributes['broj_stavke'] : null;
    }
    public function getArticleIdAttribute()
    {
        return isset($this->attributes['roba_id']) ? $this->attributes['roba_id'] : null;
    }
    public function getQuantityAttribute()
    {
        return isset($this->attributes['kolicina']) ? floatval($this->attributes['kolicina']) : 0;
    }
    public function getBasicPriceAttribute()
    {
        return isset($this->attributes['racunska_cena_nc']) ? floatval($this->attributes['racunska_cena_nc']) : 0;
    }
    public function getVatIdAttribute()
    {
        return isset($this->attributes['tarifna_grupa_id']) ? $this->attributes['tarifna_grupa_id'] : null;
    }
    public function getUnitPriceAttribute()
    {
        return isset($this->attributes['jm_cena']) ? floatval($this->attributes['jm_cena']) : null;
    }

	protected $appends = array(
    	'id',
        'order_id',
        'sort',
        'article_id',
        'quantity',
        'basic_price',
        'vat_id',
        'unit_price'
    	);

    public function order(){
        return $this->belongsTo('OrderB2B','web_b2b_narudzbina_id');
    }

    public function article(){
        return $this->belongsTo('Article','roba_id');
    }

    public function vat(){
        return $this->belongsTo('Vat','tarifna_grupa_id');
    }

}