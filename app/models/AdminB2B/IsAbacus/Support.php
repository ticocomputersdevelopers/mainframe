<?php
namespace IsAbacus;

use DB;
use Groups;
use Image;
use AdminImport;

class Support {


	public static function getPartnerId($sifra_kupca_logik){
		$partner = DB::table('partner')->where('id_is',$sifra_kupca_logik)->first();
		if(!is_null($partner)){
			return $partner->partner_id;
		}
		return null;
	}

	public static function getTarifnaGrupaId($naziv_tarifne_grupe,$vrednost_tarifne_grupe=20){
		$tg = DB::table('tarifna_grupa')->where(array('porez'=>$vrednost_tarifne_grupe))->first();

		if(is_null($tg)){
			$next_id = DB::table('tarifna_grupa')->max('tarifna_grupa_id')+1;
			DB::table('tarifna_grupa')->insert(array(
				'tarifna_grupa_id' => $next_id,
				'sifra' => $next_id,
				'naziv' => substr($naziv_tarifne_grupe,0,99),
				'porez' => $vrednost_tarifne_grupe,
				'active' => 1,
				'default_tarifna_grupa' => 0,
				'tip' => substr($vrednost_tarifne_grupe,0,19)
				));
			$tg = DB::table('tarifna_grupa')->where('porez',$vrednost_tarifne_grupe)->first();
		}
		return $tg->tarifna_grupa_id;
	}

	public static function getJedinicaMereId($jedinica_mere){
		$jm = DB::table('jedinica_mere')->where('naziv',$jedinica_mere)->first();

		if(is_null($jm)){
			DB::table('jedinica_mere')->insert(array(
				'jedinica_mere_id' => DB::table('jedinica_mere')->max('jedinica_mere_id')+1,
				'naziv' => $jedinica_mere
				));
			$jm = DB::table('jedinica_mere')->where('naziv',$jedinica_mere)->first();
		}
		return $jm->jedinica_mere_id;
	}

	public static function getProizvodjacId($proizvodjac){
		$pro = DB::table('proizvodjac')->where('naziv',$proizvodjac)->first();

		if(is_null($pro)){
			DB::table('proizvodjac')->insert(array(
				'naziv' => $proizvodjac,
				'sifra_connect' => 0
				));
			$pro = DB::table('proizvodjac')->where('naziv',$proizvodjac)->first();
		}
		return $pro->proizvodjac_id;
	}
	public static function getOrgjId($naziv){
		$orgj = DB::table('orgj')->where('naziv',$naziv)->first();
		$orgj_id = DB::select("SELECT MAX(orgj_id) + 1 AS max FROM orgj")[0]->max;
		if(is_null($orgj)){
			DB::table('orgj')->insert(array(
				'orgj_id' => $orgj_id,
				'preduzece_id' => 1,
				'parent_orgj_id' => 1,
				'mesto_id' => 757,
				'sifra' => '000'.$orgj_id,
				'tip_orgj_id' => -1,
				'level' => 2,
				'naziv' => $naziv
				));
			$orgj = DB::table('orgj')->where('naziv',$naziv)->first();
		}
		return $orgj->orgj_id;
	}
	public static function getGrupaIdByParent($parent_group_code,$group_code){
		$parent_group_code = trim($parent_group_code);
		$group_code = trim($group_code);
		$groups = DB::table('grupa_pr')->where('sifra_is',$group_code)->whereNotNull('sifra_is')->where('sifra_is','!=','')->get();
		$parent_groups = DB::table('grupa_pr')->where('sifra_is',$parent_group_code)->whereNotNull('sifra_is')->where('sifra_is','!=','')->get();

		$main_group_id = self::getNewGrupaId('PANTEON GRUPE OSTALO');

		$grupa_pr_id = $main_group_id;
		foreach($groups as $group){
			foreach($parent_groups as $parent_group){
				if($parent_group->grupa_pr_id == $group->parrent_grupa_pr_id){
					$grupa_pr_id = $group->grupa_pr_id;
					break;
				}
			}
			if($grupa_pr_id != $main_group_id){
				break;
			}
		}
		return $grupa_pr_id;
	}

	public static function getGrupaId($group_name_code){
		$group_name_code = trim($group_name_code);
		$group = DB::table('grupa_pr')->where('sifra_is',$group_name_code)->whereNotNull('sifra_is')->where('sifra_is','!=','')->first();

		if(is_null($group)){
			$grupa_pr_id = self::getNewGrupaId('MAINFRAME GRUPA');
		}else{
			$grupa_pr_id = $group->grupa_pr_id;
		}
		return $grupa_pr_id;
	}

	public static function DCgetCharsAndPicts($idIss,$mappedArticles){

		$robaIds = array_map(function($idIs) use ($mappedArticles) {
			if(isset($mappedArticles[$idIs])){
				return $mappedArticles[$idIs];
			}
		},$idIss);
		$dobavljac_cenovnik = DB::table('dobavljac_cenovnik')->select('roba_id','dobavljac_cenovnik_id','sifra_kod_dobavljaca','partner_id')->where('roba_id','<>',-1)->whereIn('roba_id',$robaIds)->get();

        foreach($dobavljac_cenovnik as $dc){
        	$roba_id = $dc->roba_id;
            $dobavljac_cenovnik_id = $dc->dobavljac_cenovnik_id;
            $sifra_kod_dobavljaca = $dc->sifra_kod_dobavljaca;
            $partner_id = $dc->partner_id;
            
            //opis
            DB::statement("UPDATE roba r SET web_karakteristike = dc.opis FROM dobavljac_cenovnik dc WHERE r.roba_id = ".$roba_id." AND dobavljac_cenovnik_id = ".$dobavljac_cenovnik_id." AND dc.opis IS NOT NULL AND dc.opis <> '' AND (r.web_karakteristike IS NULL OR r.web_karakteristike = '')");

            //dobavljac-karakteristike
            DB::table('dobavljac_cenovnik_karakteristike')->where(array('roba_id'=>-1,'sifra_kod_dobavljaca'=>$sifra_kod_dobavljaca))->update(array('roba_id'=>$roba_id));

            //slike
            $slike_query_dob = DB::table('dobavljac_cenovnik_slike')->where(array('sifra_kod_dobavljaca'=>$sifra_kod_dobavljaca, 'partner_id'=>$partner_id ));
            $slike_count_roba = DB::table('web_slika')->where('roba_id',$roba_id)->count();

            $diff_count = $slike_query_dob->count() - $slike_count_roba;
            if($diff_count > 0){
                $slike = $slike_query_dob->orderBy('dobavljac_cenovnik_slike_id','desc')->limit($diff_count)->get();
                AdminImport::saveImages($slike,$roba_id);
            }
        }		
	}

	public static function getNewGrupaId($group_name_code){
		$group_name_code = trim($group_name_code);
		$group = DB::table('grupa_pr')->where('sifra_is',$group_name_code)->whereNotNull('sifra_is')->where('sifra_is','!=','')->first();

		if(is_null($group)){
			$grupa_pr_id = DB::select("SELECT MAX(grupa_pr_id) + 1 AS max FROM grupa_pr")[0]->max;
			DB::table('grupa_pr')->insert(array(
				'grupa_pr_id' => $grupa_pr_id,
				'grupa' => $group_name_code,
				'parrent_grupa_pr_id' => 0,
				'sifra' => $grupa_pr_id,
				'sifra_is' => $group_name_code
				));
		}else{
			$grupa_pr_id = $group->grupa_pr_id;
		}
		return $grupa_pr_id;
	}

	public static function saveSingleGroup($mapp,$group_code,$group_parent_code=null,$update_parent=false){
		$group_code = trim($group_code);
		$group = DB::table('grupa_pr')->where('sifra_is',$group_code)->whereNotNull('sifra_is')->where('sifra_is','!=','')->first();

		$group_parent_id = self::getGrupaId('KATEGORIJE');
		if(!is_null($group_parent_code) && trim($group_parent_code) != ''){
			$group_parent_id = self::getGrupaId(trim($group_parent_code));
		}

		if(is_null($group)){
			$grupa_pr_id = DB::select("SELECT MAX(grupa_pr_id) + 1 AS max FROM grupa_pr")[0]->max;
			if(is_null($grupa_pr_id)){
				$grupa_pr_id = 1;
			}
			DB::table('grupa_pr')->insert(array(
				'grupa_pr_id' => $grupa_pr_id,
				'grupa' => isset($mapp[$group_code]) ? mb_convert_encoding($mapp[$group_code],"UTF-8") : mb_convert_encoding($group_code,"UTF-8"),
				'parrent_grupa_pr_id' => $group_parent_id,
				'sifra' => $grupa_pr_id,
				'sifra_is' => $group_code
				));
		}else{
			$grupa_pr_id = $group->grupa_pr_id;
			if($update_parent){
				DB::table('grupa_pr')->where('grupa_pr_id',$grupa_pr_id)->update(array('parrent_grupa_pr_id' => $group_parent_id));				
			}
		}
		return $grupa_pr_id;
	}

	public static function filteredGroups($articles){
		$groups = array();
		$mapp = array();
		foreach($articles as $articleObj) {
			
			if(isset($article->Artikal_Kategorija_ID) && $article->Artikal_Kategorija_ID != ''){
				$groups[mb_convert_encoding($article->Artikal_Kategorija_ID,mb_detect_encoding($article->Artikal_Kategorija_ID),"UTF-8")] = mb_convert_encoding($article->Artikal_Grupa_ID,mb_detect_encoding($article->Artikal_Grupa_ID),"UTF-8");

				$mapp[mb_convert_encoding($article->Artikal_Kategorija_ID,mb_detect_encoding($article->Artikal_Kategorija_ID),"UTF-8")] = Support::convert($article->Artikal_Kategorija_Naziv);
			}
			if(isset($article->Artikal_Grupa_ID) && $article->Artikal_Grupa_ID != ''){
				$mapp[mb_convert_encoding($article->Artikal_Grupa_ID,mb_detect_encoding($article->Artikal_Grupa_ID),"UTF-8")] = Support::convert($article->Artikal_Grupa_Naziv);
			}
		
		return (object) ['tree' => $groups, 'mapp' => $mapp];
		}
	}

	public static function parentGroups($allgroups,$group,&$tree){
		if(!is_null($group) && $group != ''){
			$tree[] = $group;
			if(isset($allgroups[$group]) && $allgroups[$group] != ''){
				self::parentGroups($allgroups,$allgroups[$group],$tree);
			}
		}
	}

	public static function saveGroups($groups){
		foreach($groups->tree as $grupa => $nadgrupa) {
			if($grupa != $nadgrupa){
				$tree = array($grupa);
				self::parentGroups($groups->tree,$nadgrupa,$tree);
				for ($i=(count($tree)-1);$i>=0;$i--) {
					self::saveSingleGroup($groups->mapp,$tree[$i],(isset($tree[$i+1]) ? $tree[$i+1] : null),true);
				}
			}
		}
	}

	public static function getPartnerCategoryId($category_name){
		$category_name = trim($category_name);
		$category = DB::table('partner_kategorija')->where('naziv',$category_name)->first();

		if(is_null($category)){
			$id_kategorije = DB::select("SELECT MAX(id_kategorije) + 1 AS max FROM partner_kategorija")[0]->max;
			if(is_null($id_kategorije)){
				$id_kategorije = 1;
			}
			DB::table('partner_kategorija')->insert(array(
				'id_kategorije' => $id_kategorije,
				'naziv' => $category_name,
				'rabat' => 0,
				'active' => 1
				));
		}else{
			$id_kategorije = $category->id_kategorije;
		}
		return $id_kategorije;
	}

	public static function getMappedArticles(){
		$mapped = array();
		$articles = DB::table('roba')->select('roba_id','id_is')->whereNotNull('id_is')->where('id_is','!=','')->get();
		foreach($articles as $article){
			$mapped[$article->id_is] = $article->roba_id;
		}
				//var_dump($mapped);die;
		return $mapped;
	}
	public static function getMappedOrgj(){
		$mapped = array();
		$orgj = DB::table('orgj')->whereNotNull('naziv')->where('orgj_id','>','0')->where('naziv','!=','')->get();
		foreach($orgj as $org){
			$mapped[$org->naziv] = $org->orgj_id;
		}
		return $mapped;
	}
	public static function getMappedPartners(){
		$mapped = array();

		$partners = DB::table('partner')->select('sifra','partner_id')->whereNotNull('sifra')->where('sifra','!=','')->get();
		foreach($partners as $partner){
			$mapped[trim($partner->sifra)] = $partner->partner_id;
		}

		return $mapped;
	}

	public static function getMappedGroups(){
		$mapped = array();
		$groups = DB::table('grupa_pr')->whereNotNull('sifra_is')->whereNotNull('sifra_is')->where('sifra_is','!=','')->get();
		foreach($groups as $group){
			$mapped[$group->sifra_is] = $group->grupa_pr_id;
		}
		return $mapped;
	}

	public static function convert($text){
    $encoding = mb_detect_encoding($text, mb_detect_order(), false);

    if($encoding == "UTF-8")
    {
        $text = mb_convert_encoding($text, 'UTF-8', 'UTF-8');    
    }


    $out = iconv(mb_detect_encoding($text, mb_detect_order(), false), "UTF-8//IGNORE", $text);


    return $out;
	}

	public static function postUpdate($table_temp_body){
		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='roba'"));
		$table_temp = "(VALUES ".$table_temp_body.") roba_temp(".implode(',',$columns).")";

		DB::statement("UPDATE roba t SET web_cena = roba_temp.web_cena, mpcena = roba_temp.mpcena FROM ".$table_temp." WHERE t.id_is=roba_temp.id_is::varchar AND t.id_is IS NOT NULL AND t.id_is <> '' AND t.flag_zakljucan='false' AND (select sum(kolicina) from lager l where t.roba_id = l.roba_id and orgj_id = 1 limit 1) = 0");	

        DB::statement("UPDATE roba r SET flag_prikazi_u_cenovniku = CASE
        						WHEN ( select sum(kolicina) from lager l where r.roba_id = l.roba_id ) < 1 
        							THEN 0
        							ELSE 1
        					    END WHERE r.sifra_is is not null");
       
	}

	public static function roba_cene($id_is){
		return DB::table('roba')->select('racunska_cena_end','stara_cena')->where('id_is',$id_is)->first();
	}

	public static function lager_magacin($roba_id,$orgj_id){
		return DB::table('lager')->select('kolicina','poc_kolicina')->where(array('roba_id'=>$roba_id,'orgj_id'=>$orgj_id))->first();
	}

	public static function groupedGroups($articles){
		$codes = array();
		$grouped = array();
		foreach($articles as $article) {
			if(isset($article->acClassif) && $article->acClassif != ''){
				if(!in_array((mb_convert_encoding($article->acClassif,mb_detect_encoding($article->acClassif),"UTF-8")), $codes)){
					$grouped[] = (object) ['code' => (mb_convert_encoding($article->acClassif,mb_detect_encoding($article->acClassif),"UTF-8")), 'name' => Support::convert($article->acClassifName), 'parent' => '0'];

					$codes[] = (mb_convert_encoding($article->acClassif,mb_detect_encoding($article->acClassif),"UTF-8"));
				}

				if(isset($article->acClassif2) && $article->acClassif2 != ''){
					if(!in_array((mb_convert_encoding($article->acClassif2,mb_detect_encoding($article->acClassif2),"UTF-8")), $codes)){
						$grouped[] = (object) ['code' => (mb_convert_encoding($article->acClassif2,mb_detect_encoding($article->acClassif2),"UTF-8")), 'name' => Support::convert($article->acClassif2Name), 'parent' => (mb_convert_encoding($article->acClassif,mb_detect_encoding($article->acClassif),"UTF-8"))];

						$codes[] = (mb_convert_encoding($article->acClassif2,mb_detect_encoding($article->acClassif2),"UTF-8"));
					}
				}
			}
		}
		return $grouped;
	}
	public static function updateGroupsParent($groups){
		foreach($groups as $group){
			$group_id = $group->code;

			if($group->parent != '0'){
				$parrent_grupa_pr = DB::table('grupa_pr')->where('sifra_is',$group->parent)->first();
				if(!is_null($parrent_grupa_pr)){
					$parrent_grupa_pr_id = $parrent_grupa_pr->grupa_pr_id;
					DB::table('grupa_pr')->where(array('sifra_is'=>$group->code))->update(array('parrent_grupa_pr_id'=>$parrent_grupa_pr_id));
				}else{
					// DB::table('grupa_pr')->where('id_is',$group->parent)->delete();
				}
			}
		}

	}

	public static function image_type($name){
		$name_arr = explode('.',$name);
		for ($i=(count($name_arr)-1); $i>0; $i--) {
			if(strtolower($name_arr[$i]) =='png'){
				return 'png';
			}elseif(strtolower($name_arr[$i]) =='jpg'){
				return 'jpg';
			}elseif(strtolower($name_arr[$i]) =='jpeg'){
				return 'jpeg';
			}
		}
		return null;
	}

	public static function saveImageFile($putanja,$content,$sirina_big=600){
		try {
			$img_file = fopen($putanja,"w");
			fwrite($img_file, $content);
			fclose($img_file);

			if(filesize($putanja) <= 1000000){
				Image::make($putanja)->resize($sirina_big, null, function ($constraint) { $constraint->aspectRatio(); })->save();
			}
			return true;
		} catch (Exception $e) {
			File::delete($putanja);
			return false;
		}
	}
	public static function save_image_files($images){
		$sirina_big = DB::table('options')->where('options_id',1331)->pluck('int_data');
		foreach($images as $article2) {
			foreach($article2 as $article) {
				$slika=$article->image;
				$sifra = $article->id_item;
				$roba_id = DB::table('roba')->where('id_is',$sifra)->pluck('roba_id');
                $count=DB::table('web_slika')->where('roba_id',$roba_id)->count();
                if($count<1){
				  	if(isset($slika) AND !empty($slika) AND !is_null($slika)){
					   	foreach ($slika as $key => $value) {
					   	
					   	$slika=$value;	
					   		
	                    $slika_id = DB::select("SELECT nextval('web_slika_web_slika_id_seq')")[0]->nextval;
	 
	                    $name = $slika_id.'.'.self::image_type($slika);
	                   
	                    $destination = 'images/products/big/'.$name;
	                    $url = $slika;
	                    $akcija = 1;
	                    $dobavljac = -1;
	                    $sifra= '';

                        $content = @file_get_contents($url);
                        file_put_contents($destination,$content);	                

	                    $insert_data = array(
	                        'web_slika_id'=>intval($slika_id),
	                        'roba_id'=>intval($roba_id),
	                        'akcija'=>$akcija, 
	                        'dobavljac_id'=>$dobavljac, 
	                        'sifra_kod_dobavljaca'=>$sifra,
	                        'flag_prikazi'=>1,
	                        'putanja'=>'images/products/big/'.$name
	                        );
	 					$sirina_slike = DB::table('options')->where('options_id',1331)->pluck('int_data');
	                    Image::make($destination)->resize($sirina_slike, null, function ($constraint){ $constraint->aspectRatio(); })->save();
	                //    Image::make($destination)->resize(DB::table('options')->where('options_id',1333)->pluck('int_data'), null, function ($constraint){ $constraint->aspectRatio(); })->save('images/products/small/'.$name);

	                    DB::table('web_slika')->insert($insert_data);
	 					}
	                }
					}
				}
			}
		}
	

}
