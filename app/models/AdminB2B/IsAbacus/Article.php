<?php
namespace IsAbacus;
use DB;
use Options;
use All;

class Article {

	public static function table_body($articles){
		$kurs = DB::table('kursna_lista')->where('kursna_lista_id',DB::table('kursna_lista')->max('kursna_lista_id'))->pluck('ziralni');
		$defaultGroupId = -1;

		$result_arr = array();

		$roba_id =  DB::table('roba')->max('roba_id')+1;
		$sifra_k = DB::table('roba')->max('sifra_k')+1;

		foreach($articles as $article) {			

			if(isset($article->id_item)){
				if(isset($article->price) AND is_numeric($article->price) AND $article->price > 0){
				$id_is = $article->id_item;
				$roba_id++;
				$sifra_k++;

				$sifra_is = $article->code;
				$sifra_d = isset($article->supplier_code) ? pg_escape_string($article->supplier_code) : '';
				$naziv =  pg_escape_string($article->name);
				$web_opis = '';
				$jedinica_mere_id = isset($article->Jedinica_Mere) && $article->Jedinica_Mere != '' ? Support::getJedinicaMereId(strtolower($article->Jedinica_Mere)) : 1;
				$proizvodjac_id = isset($article->manufacturer) && trim($article->manufacturer) != '' ? Support::getProizvodjacId($article->manufacturer) : -1;
				
				$vrednost_tarifne_grupe = isset($article->Tarifa_Stopa) && is_numeric($article->Tarifa_Stopa) ? $article->Tarifa_Stopa : 20;
				$tarifna_grupa_id = Support::getTarifnaGrupaId(strval($vrednost_tarifne_grupe),$vrednost_tarifne_grupe);
				$grupa_pr_id =  isset($article->group) && $article->group != '' ? Support::getGrupaId($article->group) : -1;

				$barkod = isset($article->ean) AND $article->ean != "'\'" ? $article->ean : "NULL";
				// $grupa_pr_id = Support::getGrupaId('NOVI ARTIKLI');
				// $kursValuta = trim($article->acCurrency) != 'EUR' ? 1 : $kurs;
				//$kursValuta = floatval($article->anFxRate);

				$racunska_cena_nc = 0.00;
				$mpcena = $article->price_rrp;
				$web_cena = $article->price; 
				$racunska_cena_a = 0.00;
				$racunska_cena_end = 0.00;

				$stara_cena = 0.00;

				$marza = 0;
				$flag_aktivan = '1';
				$flag_cenovnik = '1';
				$sku = trim($roba_id);
				$garancija = 0;
		
				$result_arr[] = "(".strval($roba_id).",'".$sifra_d."','".substr($naziv,0,500)."',NULL,NULL,NULL,".$grupa_pr_id.",".strval($tarifna_grupa_id).",".strval($jedinica_mere_id).",".strval($proizvodjac_id).",-1,".strval($sifra_k).",NULL,NULL,'".$naziv."',0,-1,0,0,0,0,9,0,0,0,".strval($garancija).",1,".$flag_cenovnik.",0,NULL,".$flag_aktivan.",".strval($racunska_cena_a).",".strval($racunska_cena_end).",".strval($racunska_cena_nc).",0,NULL,".strval($mpcena).",false,".strval($marza).",(NULL)::integer,'".Support::convert($naziv)."',".$flag_cenovnik.",NULL,('".date('Y-m-d H:i:s')."')::timestamp,(NULL)::integer,(NULL)::integer,0,0,0,-1,-1,".strval($web_cena).",1,0,'".$web_opis."',NULL,NULL,0,0,0,NULL,NULL,NULL,NULL,1,0,NULL,".strval($stara_cena).",0,1,1,-1,NULL,NULL,NULL,NULL,0,0.00,0.00,0.00,0,".trim($sku).",(NULL)::date,(NULL)::date,(NULL)::integer,NULL,'".strval($sifra_is)."','".strval($id_is)."',0,0,0,(NULL)::date,(NULL)::date,0.00,0.00,1,NULL)";			
				}
			}
		}

		return (object) array("body"=>implode(",",$result_arr));
	}

	public static function query_insert_update($table_temp_body,$upd_cols=array()) {
		if($table_temp_body == ''){
			return false;
		}

		$all_columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='roba'"));
		$columns = $all_columns;
		$columns_without_id = $all_columns;
		unset($columns_without_id[0]);
		
		$table_temp = "(VALUES ".$table_temp_body.") roba_temp(".implode(',',$columns).")";

		
		// update
		if(count($upd_cols)>0){
			$columns = $upd_cols;
		}
		$updated_columns = array();
		foreach($columns as $col){
			if($col!="roba_id" && $col!="id_is"){
		    	$updated_columns[] = "".$col." = roba_temp.".$col."";
			}
		}


		DB::statement("SET CLIENT_ENCODING TO 'UTF8'");
		DB::statement("UPDATE roba t SET id_is = roba_temp.id_is::varchar, sifra_is = roba_temp.sifra_is::varchar FROM ".$table_temp." WHERE t.dobavljac_id > 0 AND t.sifra_d=roba_temp.sifra_d::varchar AND roba_temp.sifra_d::varchar IS NOT NULL AND roba_temp.sifra_d::varchar <> '' AND (t.id_is IS NULL OR t.id_is = '')");
		// DB::statement("UPDATE roba t SET ".implode(',',$updated_columns)." FROM ".$table_temp." WHERE t.id_is=roba_temp.id_is::varchar AND t.id_is IS NOT NULL AND t.id_is <> '' AND t.flag_zakljucan='false'");	
		$id_iss = DB::select("SELECT id_is FROM ".$table_temp." WHERE id_is NOT IN (SELECT id_is FROM roba WHERE id_is IS NOT NULL AND id_is <> '')");
		//insert
		DB::statement("INSERT INTO roba(".implode(',',$columns_without_id).") SELECT ".implode(',',$columns_without_id)." FROM ".$table_temp." WHERE roba_temp.id_is NOT IN (SELECT id_is FROM roba WHERE id_is IS NOT NULL AND id_is <> '')");
		DB::statement("SET CLIENT_ENCODING TO 'WIN1250'");
		DB::statement("SELECT setval('roba_roba_id_seq', (SELECT MAX(roba_id) FROM roba) + 1, FALSE)");


		//LINKED WITH DC
		DB::statement("UPDATE dobavljac_cenovnik dc SET roba_id = r.roba_id, povezan = 1 FROM roba r WHERE dc.sifra_kod_dobavljaca = r.sifra_d AND povezan <> 1 AND dc.roba_id = -1 AND r.sifra_d IS NOT NULL AND r.sifra_d <> '' AND r.dobavljac_id > 0 AND r.id_is IS NOT NULL AND r.id_is <> ''");
		DB::statement("UPDATE dobavljac_cenovnik dc SET grupa_pr_id = r.grupa_pr_id, proizvodjac_id = r.proizvodjac_id, tarifna_grupa_id = r.tarifna_grupa_id FROM roba r WHERE dc.sifra_kod_dobavljaca = r.sifra_d AND dc.roba_id = r.roba_id AND r.id_is IS NOT NULL AND r.id_is <> ''");
		DB::statement("UPDATE dobavljac_cenovnik dc SET roba_id = -1, povezan = 0 WHERE roba_id <> -1 AND povezan = 1 AND roba_id NOT IN (SELECT roba_id FROM roba)");
		DB::statement("UPDATE roba SET sifra_d = null, dobavljac_id = -1 WHERE (sifra_d is not null OR dobavljac_id > 0) AND sifra_d NOT IN (SELECT sifra_kod_dobavljaca FROM dobavljac_cenovnik)");


		return array_map('current',$id_iss);

	}
	public static function query_update_unexists($table_temp_body) {

		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='roba'"));
		$table_temp = "(VALUES ".$table_temp_body.") roba_temp(".implode(',',$columns).")";

		DB::statement("SET CLIENT_ENCODING TO 'WIN1250'");
		//DB::statement("UPDATE roba t SET flag_aktivan = 0 WHERE t.id_is IS NOT NULL AND NOT EXISTS(SELECT * FROM ".$table_temp." WHERE t.id_is=roba_temp.id_is)");
		DB::statement("SET CLIENT_ENCODING TO 'UTF8'");
	}
	public static function query_delete_unexists($table_temp_body) {
		if($table_temp_body == ''){
			return false;
		}
		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='roba'"));
		$table_temp = "(VALUES ".$table_temp_body.") roba_temp(".implode(',',$columns).")";

		DB::statement("SET CLIENT_ENCODING TO 'WIN1250'");
		DB::statement("DELETE FROM roba WHERE id_is NOT IN (SELECT id_is FROM ".$table_temp.")");
		DB::statement("SET CLIENT_ENCODING TO 'UTF8'");
	}

}