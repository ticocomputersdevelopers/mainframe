<?php
namespace IsAbacus;
use Image;
use DB;

class Images {

	public static function table_body($articles,$mapped){

		$result_arr = array();
		$sirina_slike = DB::table('options')->where('options_id',1331)->pluck('int_data');

		foreach($articles as $article) {
			$images = $article->image;
    		if(isset($images)){
			//var_dump($images);die;
    			
    			$roba_id = isset($mapped[$article->id_item]) ? $mapped[$article->id_item] : null;
    			if(isset($roba_id)){
    			$sifra_d=isset($article->supplier_code) ? pg_escape_string($article->supplier_code) : '';
    			$img_count = DB::table('web_slika')->where('roba_id',$roba_id)->count();
    			
				foreach($images as $key => $image){
						if($img_count==0){
			                try { 
			                    $web_slika_id = DB::select("SELECT nextval('web_slika_web_slika_id_seq')")[0]->nextval;
			                    $putanja = $web_slika_id.'.jpg';
			                    $destination = 'images/products/big/'.$putanja;
		                        $content = @file_get_contents($image);
		                        file_put_contents($destination,$content);

			                    Image::make($destination)->resize($sirina_slike, null, function ($constraint){ $constraint->aspectRatio(); })->save();

								$result_arr[] = "(".strval($web_slika_id).",".strval($roba_id).",(NULL)::bytea,".strval($key==0?1:0).",-1,'".$sifra_d."',(NULL)::integer,1,'".$destination."',NULL,0,'".$article->id_item."',(NULL)::integer)";		 
			                }
			                catch (Exception $e) {
			                }
						}
					}
				}
				}
    		}
    		return (object) array("body"=>implode(",",$result_arr));
	}

	public static function query_insert_update($table_temp_body,$upd_cols=array()) {

		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='web_slika'"));
		$table_temp = "(VALUES ".$table_temp_body.") web_slika_temp(".implode(',',$columns).")";

		//insert
		DB::statement("INSERT INTO web_slika (SELECT * FROM ".$table_temp.")");
		DB::statement("SELECT setval('web_slika_web_slika_id_seq', (SELECT MAX(web_slika_id)+1 FROM web_slika), FALSE)");
	}

}