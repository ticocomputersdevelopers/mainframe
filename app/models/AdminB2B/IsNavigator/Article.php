<?php
namespace IsNavigator;
use DB;
use Options;

class Article {

	public static function table_body($articles){
		// $kurs = DB::table('kursna_lista')->where('kursna_lista_id',DB::table('kursna_lista')->max('kursna_lista_id'))->pluck('ziralni');

		$result_arr = array();

		$roba_id =  DB::table('roba')->max('roba_id')+1;
		$sifra_k = DB::table('roba')->max('sifra_k')+1;

		foreach($articles as $article) {
			if(isset($article->id)){
				$id_is = $article->id;
				$roba_id++;
				$sifra_k++;

				$sifra_is = isset($article->code) ? pg_escape_string($article->code) : '';
				$sku = isset($article->sku) ? pg_escape_string($article->sku) : '';
				$naziv = isset($article->name) ? Support::convert(substr($article->name,0,300)) : '';
				$web_opis = isset($article->description) ? Support::convert($article->description) : '';
				$jedinica_mere_id = isset($article->unit_of_measure) && $article->unit_of_measure != '' ? Support::getJedinicaMereId(strtolower($article->unit_of_measure)) : 1;
				$proizvodjac_id = -1;
				// $vrednost_tarifne_grupe = isset($article->anVat) && is_numeric($article->anVat) ? $article->anVat : 20;
				$tarifna_grupa_id = $article->vat_tax_code == 11 ? 0 : 1;
				// $grupa_pr_id = isset($article->acClassif) && $article->acClassif != '' && isset($article->acClassif2) && $article->acClassif2 != '' ? Support::getGrupaIdByParent($article->acClassif,$article->acClassif2) : -1;
				$grupa_pr_id = Support::getGrupaId('NOVI ARTIKLI');
				$barkod = isset($article->bar_code) ? pg_escape_string($article->bar_code) : '';
				$tezinki_faktor = isset($article->weight) ? floatval($article->weight)*1000 : 0;
				// $kursValuta = trim($article->acCurrency) != 'EUR' ? 1 : $kurs;

				// $racunska_cena_nc = isset($article->anRTPrice) && is_numeric(floatval($article->anRTPrice)) ? (floatval($article->anRTPrice) * $kursValuta) : 0;
				$racunska_cena_nc = 0;
				// $mpcena = isset($article->anSalePrice) && is_numeric(floatval($article->anSalePrice)) ? (floatval($article->anSalePrice) * $kursValuta) : 0;
				$mpcena = 0;
				$web_cena = $mpcena;
				$racunska_cena_a = $racunska_cena_nc;
				$racunska_cena_end = $racunska_cena_nc;

				$marza = 0;
				$flag_aktivan = $article->status == 'T' ? '1' : '0';
				// $flag_prikazi_u_cenovniku = ($racunska_cena_nc > 0 && $web_cena > 0) ? '1' : '0';
				$flag_prikazi_u_cenovniku = 0;

				$result_arr[] = "(".strval($roba_id).",NULL,'".$naziv."',NULL,NULL,NULL,".$grupa_pr_id.",".strval($tarifna_grupa_id).",".strval($jedinica_mere_id).",".strval($proizvodjac_id).",-1,".strval($sifra_k).",NULL,NULL,'".substr($naziv,0,20)."',0,-1,0,0,0,".strval($tezinki_faktor).",9,0,0,0,0,1,".$flag_prikazi_u_cenovniku.",0,NULL,".$flag_aktivan.",".strval($racunska_cena_a).",".strval($racunska_cena_end).",".strval($racunska_cena_nc).",0,NULL,".strval($mpcena).",false,".strval($marza).",(NULL)::integer,'".$naziv."',1,NULL,NULL,(NULL)::integer,(NULL)::integer,0,0,0,-1,-1,".strval($web_cena).",1,0,'".$web_opis."',NULL,NULL,0,0,0,NULL,NULL,NULL,NULL,1,0,'".$barkod."',0,0,1,1,-1,NULL,NULL,NULL,NULL,0,0.00,0.00,0.00,0,'".$sku."',(NULL)::date,(NULL)::date,(NULL)::integer,NULL,'".strval($sifra_is)."','".strval($id_is)."',0,(NULL)::integer,0,(NULL)::date,(NULL)::date,0.00,0.00,1,NULL)";
			}
		}

		return (object) array("body"=>implode(",",$result_arr));
	}

	public static function query_insert_update($table_temp_body,$upd_cols=array()) {
		if($table_temp_body == ''){
			return false;
		}

		$all_columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='roba'"));
		$columns = $all_columns;
		$columns_without_id = $all_columns;
		unset($columns_without_id[0]);
		
		$table_temp = "(VALUES ".$table_temp_body.") roba_temp(".implode(',',$columns).")";

		// DB::statement("SET CLIENT_ENCODING TO 'WIN1250'");
		
		// update
		if(count($upd_cols)>0){
			$columns = $upd_cols;
		}
		$updated_columns = array();
		foreach($columns as $col){
			if($col!="roba_id" && $col!="sifra_d" && $col!="sifra_is" && $col!="id_is" && $col!="racunska_cena_nc" && $col!="web_cena" && $col!="racunska_cena_a" && $col!="racunska_cena_end"&& $col!="flag_prikazi_u_cenovniku"){
		    	$updated_columns[] = "".$col." = roba_temp.".$col."";
			}
		}

		if(count($updated_columns) > 0){
			DB::statement("UPDATE roba t SET ".implode(',',$updated_columns)." FROM ".$table_temp." WHERE t.id_is=roba_temp.id_is::varchar AND t.id_is IS NOT NULL AND t.id_is <> ''");	
		}

		$id_iss = DB::select("SELECT id_is FROM ".$table_temp." WHERE id_is NOT IN (SELECT id_is FROM roba WHERE id_is IS NOT NULL AND id_is <> '')");

		//insert
		DB::statement("INSERT INTO roba(".implode(',',$columns_without_id).") SELECT ".implode(',',$columns_without_id)." FROM ".$table_temp." WHERE roba_temp.id_is NOT IN (SELECT id_is FROM roba WHERE id_is IS NOT NULL AND id_is <> '')");


		// DB::statement("SET CLIENT_ENCODING TO 'UTF8'");

		DB::statement("SELECT setval('roba_roba_id_seq', (SELECT MAX(roba_id) FROM roba) + 1, FALSE)");

		if(isset($updated_columns['naziv']) || isset($updated_columns['naziv_web'])){
			Support::entityDecode();
		}else{
			$id_iss = array_map(function($item){ return "'".strval($item->id_is)."'"; },$id_iss);
			Support::entityDecode($id_iss);
		}
	}

	public static function query_update_unexists($table_temp_body) {
		if($table_temp_body == ''){
			return false;
		}
		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='roba'"));
		$table_temp = "(VALUES ".$table_temp_body.") roba_temp(".implode(',',$columns).")";

		DB::statement("UPDATE roba t SET flag_aktivan = 0 WHERE t.id_is NOT IN (SELECT id_is FROM ".$table_temp.")");
	}

}