<?php

class AdminB2BCommon {
	public static function get_page_start(){
		
		return DB::table('web_b2c_seo')->where('web_b2c_seo_id',1)->pluck('naziv_stranice');
	}
	
	public static function grupa_title($grupa_pr_id){
		
		foreach(DB::table('grupa_pr')->where('grupa_pr_id',$grupa_pr_id)->get() as $row){
				
				return $row->grupa;
			}
		
	}

	public static function allGroups(&$niz,$grupa_pr_id)
	{		
		$niz[]=$grupa_pr_id;
		$check_parent=DB::table('grupa_pr')->where('parrent_grupa_pr_id',$grupa_pr_id)->get();
		if (count($check_parent)>0)
		{
			foreach ($check_parent as $row)
			{
				self::allGroups($niz,$row->grupa_pr_id);
			}				

		}
	}

    public static function saveLog($report){
        if(Session::has('b2c_admin'.Options::server())){
            
            $filename = "files/logs/users/administrators.txt";
            $user = DB::table('imenik')->where('imenik_id',Session::get('b2c_admin'.self::server()))->first();        
            $myfile = fopen($filename, "a");
            $line = date("d-m-Y H:i:s").' - Korisnik: '.$user->ime.' '.$user->prezime.' ('.$user->imenik_id.') - '.trim($report)."\r\n";
            fwrite($myfile, $line);
            fclose($myfile);        
        }
    }
    public static function get_manofacture_name($proizvodjac_id){
    	return DB::table("proizvodjac")->where('proizvodjac_id',$proizvodjac_id)->pluck('naziv');
    }


}
