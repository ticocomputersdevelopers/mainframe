<?php
namespace ISCustom;
use AdminB2BIS;
use PHPExcel;
use PHPExcel_IOFactory;
use DB;
use All;

class PartnersExcelNiansa {

	public static function table_body(){

		$products_file = "files/IS/excel/partner.xlsx";
		$excelReader = PHPExcel_IOFactory::createReaderForFile($products_file);
		$excelObj = $excelReader->load($products_file);
		$worksheet = $excelObj->getSheet(0);
		$lastRow = $worksheet->getHighestRow();

		$result_arr = array("(-1,'','Nedefinisan',NULL,-1,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,NULL,0,0,1,1,0,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,0,0,0,0,NULL)","(1,'A00127','".AdminB2BIS::encodeTo1250('Matična firma')."',NULL,-1,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,NULL,0,0,1,1,0,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,0,1,0,0,NULL)");
		$partner_codes = array('','A00127');

		$partner_id = DB::select("SELECT nextval('partner_partner_id_seq')")[0]->nextval;
		for ($row = 1; $row <= $lastRow; $row++) {
		    $A = $worksheet->getCell('A'.$row)->getValue();
		    $B = $worksheet->getCell('B'.$row)->getValue();
		    $C = $worksheet->getCell('C'.$row)->getValue();
		    $D = $worksheet->getCell('D'.$row)->getValue();
		    $E = $worksheet->getCell('E'.$row)->getValue();
		    $F = $worksheet->getCell('F'.$row)->getValue();

		    if(isset($A) && isset($B)){
		    	$partner_id++;
		    	$B = AdminB2BIS::encodeTo1250($B);
		    	if(!is_numeric($C) || !isset($C)){
		    		$C = '';
		    	}
		    	if(!is_numeric($E) || !isset($E)){
		    		$E = '0';
		    	}
		    	if(!is_numeric($F) || !isset($F)){
		    		$F = '0';
		    	}
		        $partner_codes[] = trim($A);
		        $result_arr[] = "(".strval($partner_id).",'".trim($A)."','".trim($B)."','".AdminB2BIS::encodeTo1250(trim($D))."',678,0,NULL,NULL,'".strval($C)."',NULL,NULL,NULL,NULL,NULL,".strval($E).",0,".strval($F).",'".trim($B)."',0,0,1,1,0,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,0,1,0,0,NULL)";
		    }

		}
		return (object) array("body"=>implode(",",$result_arr),"codes"=>$partner_codes);
	}

	public static function query_insert_update($table_temp_body,$upd_cols=array()) {

		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='partner'"));
		$table_temp = "(VALUES ".$table_temp_body.") partner_temp(".implode(',',$columns).")";

		DB::statement("SET CLIENT_ENCODING TO 'WIN1250'");

		// update
		$updated_columns=array();
		if(count($upd_cols)>0){
			$columns = $upd_cols;
		}
		$updated_columns = array();
		foreach($columns as $col){
			if($col!="partner_id" && $col!="sifra"){
		    	$updated_columns[] = "".$col." = partner_temp.".$col."";
			}
		}
		//DB::statement("UPDATE partner t SET ".implode(',',$updated_columns)." FROM ".$table_temp." WHERE t.sifra=partner_temp.sifra");

		//insert
		DB::statement("INSERT INTO partner (SELECT * FROM ".$table_temp." WHERE NOT EXISTS(SELECT * FROM partner t WHERE t.sifra=partner_temp.sifra))");

		DB::statement("SET CLIENT_ENCODING TO 'UTF8'");
		DB::statement("SELECT setval('partner_partner_id_seq', (SELECT MAX(partner_id) FROM partner) + 1, FALSE)");
	}
}