<?php
namespace ISCustom;
use AdminB2BIS;
use PHPExcel;
use PHPExcel_IOFactory;

class PoreskeStopeExcelNiansa {

	public static function table_body(){

		$products_file = "files/IS/excel/tarifna_grupa.xlsx";
		$excelReader = PHPExcel_IOFactory::createReaderForFile($products_file);
		$excelObj = $excelReader->load($products_file);
		$worksheet = $excelObj->getSheet(2);
		$lastRow = $worksheet->getHighestRow();

		$result_arr = array("(-1,'','Promet dobara sa pravom odbitka prethodnog poreza',0,0,0,'nema')","(0,'P00','Promet poreska stopa 0%',0,0,0,'bez')");
		$manufacturer_ids = array(-1,0);
		
		for ($row = 1; $row <= $lastRow; $row++) {
		    $A = $worksheet->getCell('A'.$row)->getValue();
		    $B = $worksheet->getCell('B'.$row)->getValue();

		    if(is_numeric($A) && is_numeric($B)){
				$poreska_stopa_ids[] = intval($A);
				$result_arr[] = "(".intval($A).",'P".$B."','Promet poreska stopa ".$B."%',".intval($B).",0,0,NULL)";
		    }

		}
		return (object) array("body"=>implode(",",$result_arr),"ids"=>$poreska_stopa_ids);
	}


}