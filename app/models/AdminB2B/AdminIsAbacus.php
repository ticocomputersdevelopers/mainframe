<?php
ini_set('memory_limit', '-1');
ini_set('max_execution_time', 21000);



use IsAbacus\APIData;
use IsAbacus\Article;
use IsAbacus\ArticleGroup;
use IsAbacus\Images;
use IsAbacus\Stock;
use IsAbacus\Partner;
use IsAbacus\Support;



class AdminIsAbacus {

    public static function execute(){

        // try { 

            //articles
            $articles = APIData::artikli();
            $articles_mp = APIData::artikli_mp();

            // artikli 
            $resultArticle = Article::table_body($articles);
            $newIdIss = Article::query_insert_update($resultArticle->body);
            $mappedArticles = Support::getMappedArticles();

            //lager VP
            $resultLager = Stock::table_body($articles,$mappedArticles,4);
            Stock::query_insert_update($resultLager->body,array('kolicina'));

            //lager MP
            $resultLager = Stock::table_body_mp($articles_mp,$mappedArticles,2);
            Stock::query_insert_update($resultLager->body,array('kolicina'));

            Support::postUpdate($resultArticle->body);
            
            //slike            
            $resultImage = Images::table_body($articles,$mappedArticles);
            if($resultImage->body!=''){
                Images::query_insert_update($resultImage->body);
            }

            Support::DCgetCharsAndPicts($newIdIss,$mappedArticles);

            AdminB2BIS::saveISLog('true');
            return (object) array('success'=>true);

        // }catch (Exception $e){
        //     AdminB2BIS::saveISLog('false');
        //     AdminB2BIS::saveISLogError($e->getMessage());
        //     AdminB2BIS::sendNotification(array(9,12,15,18),15,5);
        //     return (object) array('success'=>false,'message'=>$e->getMessage());
        // }
    }




}