<?php

class AdminCrm {
	public static function getCrm() {
		$ukupno = DB::table('crm_akcija')->where('datum', substr(date('Y-m-d H:m:s'),0,-9))->count();
		return $ukupno;
	}
	public static function getAkcijeDanas($date){
		
			$broj=DB::table('crm_akcija')->where('datum', substr(date('Y-m-d H:m:s'),0,-9))->count();
			return $broj;
			if($broj>0){
				return $broj;
			}
			else{
				return 0;
			}
		
	}

	public static function getPartnerID($crm_id){
		$crm = DB::table('crm')->where('crm_id', $crm_id)->pluck('partner_id');
		return $crm;
	}
	public static function getPartnerNaziv($partner_id){
		$partner = DB::table('partner')->where('partner_id', $partner_id)->pluck('naziv');
		return $partner;
	}
	public static function getCrmOpis($crm_id){
		$crm = DB::table('crm')->where('crm_id', $crm_id)->pluck('opis');
		return $crm;
	}
 

	//crm akcija
	public static function getAkcijaId($crm_id){
		$akcija_id = DB::table('crm_akcija')->where('crm_id', $crm_id)->pluck('crm_akcija_tip_id');
		return $akcija_id;
	}
	public static function getAkcijaI($crm_id){
		$akcija_id = DB::table('crm_akcija')->where('crm_id', $crm_id)->pluck('crm_akcija_id');
		return $akcija_id;
	}
	public static function getAkcijaOpis($crm_id){
		$akcija_opis = DB::table('crm_akcija')->where('crm_id', $crm_id)->pluck('opis');
		return $akcija_opis;
	}
	public static function getAkcijaRBR($crm_akcija_tip_id)
	{
		$rbr = DB::table('crm_akcija_tip')->where('crm_akcija_tip_id', $crm_akcija_tip_id)->pluck('rbr');
		return $rbr;
	}
	public static function getAkcijaActive($crm_akcija_tip_id)
	{
		$active = DB::table('crm_akcija_tip')->where('crm_akcija_tip_id', $crm_akcija_tip_id)->pluck('flag_aktivan');
		return $active;
	}

	public static function getAkcijaNaziv($crm_id){
		$akcija_naziv = DB::table('crm_akcija_tip')->orderBy('crm_akcija_tip_id','asc')->where('crm_akcija_tip_id',AdminCrm::getAkcijaId($crm_id))->pluck('naziv');
		return $akcija_naziv;
	}
	public static function getAkcija($crm_akcija_tip_id){
		$akcija_naziv = DB::table('crm_akcija_tip')->where('crm_akcija_tip_id',$crm_akcija_tip_id)->pluck('naziv');
		return $akcija_naziv;
	}
	public static function getAktivnaAkcija($crm_id){
		$aktivna = DB::table('crm_akcija')->where('crm_id', $crm_id)->pluck('flag_zavrseno');
		return $aktivna;
	}
//DB::table('crm_akcija')->orderBy('datum','asc')->where('crm_id', $crm_id)->select('datum')->get();
	public static function getDatumAkcije($crm_id){
		$datum=DB::table('crm_akcija')->orderBy('datum','dsc')->where('crm_id', $crm_id)->pluck('datum');
		return $datum;
	}
	public static function checkDateActionCrm($date) {
		$datum=DB::table('crm_akcija')->select('datum')->get();
		$check=false;
		foreach ($datum as $dd) {	
			if(substr($dd->datum, 0,-9) >= $date)
			{
				$check=true;
				//AdminCrm::crmSendMailNotification();			
			}
		}
		return $check;
		
	}
	public function managerSelect()
	{
		
		 return DB::table('imenik')->where('kvota','>',0)->orderBy('ime','asc')->get();
	}

	public static function getDatumZavrsetkaAkcije($crm_id){
		$datum=DB::table('crm_akcija')->where('crm_id', $crm_id)->pluck('datum_zavrsetka');
		return $datum;
	}
	public static function getZavrsenaAkcija($crm_id){
		$flag=DB::table('crm_akcija')->where('crm_id', $crm_id)->pluck('flag_zavrseno');
		return $flag;
	}
	public static function getBrojAkcija($crm_id){
		$flag=DB::table('crm_akcija')->where('crm_id', $crm_id)->count();
		return $flag;
	}
	//crm tip
	public static function getTipNaziv($crm_tip_id){
		$tip_naziv=DB::table('crm_tip')->where('crm_tip_id', $crm_tip_id)->pluck('naziv');
		return $tip_naziv;
	}
	//crm status
	public static function getStatusNaziv($crm_status_id){
		 $status=DB::table('crm_status')->where('crm_status_id', $crm_status_id)->pluck('naziv');
		return $status;
	}
	
	//ponuda
	public static function getCrmPonuda($partner_id){
		 $ponuda=DB::table('ponuda')->where('partner_id', $partner_id)->pluck('ponuda_id');
		return $ponuda;
	}

	//crm partneri
	
	public static function getVrstaKontakta($vrsta_kontakta_id){
		$vrsta_kontakta=DB::table('vrsta_kontakta')->where('vrsta_kontakta_id', $vrsta_kontakta_id)->pluck('naziv');
		return $vrsta_kontakta;
	}
	public static function getSviKontakti($partner_id){
		$sviKontakti=DB::table('partner_kontakt')->where('partner_id', $partner_id)->get();
		return $sviKontakti;
	}
	public static function getNisuCrm(){
		$nisuCrm=DB::table('partner')->join('partner_je', function($join)
        			{$join->on('partner.partner_id', '=', 'partner_je.partner_id');
        			})->get();
		return $nisuCrm;
	}


	public static function get_komercijalista_id(){
        $user = DB::table('imenik')->where('imenik_id',Session::get('b2c_admin'.AdminOptions::server()))->first();
        return $user->imenik_id;
    }

    public static function getBaterryTooltip($id){
        switch ($id) {
        	case '0':
        		echo "<span class='tooltipz' aria-label='Nemate aktivnih akcija'> 
					<i class='fa fa-battery-0' style='color:red;'></i> </span>";
        		break;
        	case '1':
        		echo "<span class='tooltipz' aria-label='Sve akcije su aktivne'> 
					<i class='fa fa-battery-1' style='color:red;'></i> </span>";
        		break;
        	case '2':
        		echo "<span class='tooltipz' aria-label='Imate nezavršene akcije'> 
					<i class='fa fa-battery-2' style='color:red;'></i> </span>";
        		break;
        	case '4':
        		echo "<span class='tooltipz' aria-label='Akcije završene'> 
					<i class='fa fa-battery-4' style='color:green;'></i> </span>";
        		break;      	
        	default:
        		echo '<span> </span>';
        		break;
        }
    }



	//filteri statusa
	public static function statusiSelect($status_ids=array()){
		$dodati=true;
		$query = DB::table('crm_status')->select('crm_status_id','naziv');
		if($dodati){
			$query = $query->whereIn('crm_status_id',array_map('current',DB::table('crm')->select('crm_status_id')->whereNotNull('crm_status_id')->get()));
		}
		$data = $query->orderBy('naziv','asc')->get();

		$render = '';
        foreach($data as $row){
            if(in_array($row->crm_status_id,$status_ids)){
                $render .= '<option value="'.$row->crm_status_id.'" selected>'.$row->naziv.'</option>';
            }else{
                $render .= '<option value="'.$row->crm_status_id.'">'.$row->naziv.'</option>';
            }
        }
        return $render;
	}

	//filteri tipova
	public static function tipoviSelect($tip_ids=array(),$dodati=true){
		$query = DB::table('crm_tip')->select('crm_tip_id','naziv');
		if($dodati){
			$query = $query->whereIn('crm_tip_id',array_map('current',DB::table('crm')->select('crm_tip_id')->whereNotNull('crm_tip_id')->get()));
		}
		$data = $query->orderBy('naziv','asc')->get();

		$render = '';
        foreach($data as $row){
            if(in_array($row->crm_tip_id,$tip_ids)){
                $render .= '<option value="'.$row->crm_tip_id.'" selected>'.$row->naziv.'</option>';
            }else{
                $render .= '<option value="'.$row->crm_tip_id.'">'.$row->naziv.'</option>';
            }
        }
        return $render;
	}
	//aktivno
	public static function aktivnoFilter($aktivniS=''){
		$query = DB::table('crm')->select('flag_zavrseno');
		
		$data = $query->get();

		$render = '';
        foreach($data as $row){
          
                $render .= '<option value="'.$row->flag_zavrseno.'" selected>'.'Zavrseni'.'</option>';
           
        }
        return $render;
	}

	public static function getCrmFilteri($aktivniS=null,$datum_pocetka=null,$datum_kraja=null,$search=null,$status_ids=array(),$tip_ids=array(),$sort_column='cr.datum_kreiranja',$sort_direction='DSC',$limit=30,$page=1){
		$date = date_create();
		$date=date_format($date, 'Y-m-d H:i:s');
	
	
		$query = "SELECT DISTINCT cr.crm_id,p.naziv,p.telefon,p.adresa,p.kontakt_osoba,p.broj_maticni,cr.opis,p.mail,st.naziv,tip.naziv,tip.crm_tip_id,st.crm_status_id,cr.datum_kreiranja,cr.datum_zavrsetka,cr.partner_id,cr.flag_zavrseno,cr.ponuda_id, cr.vrednost,cr.kursna_lista_id
		FROM crm cr 
		LEFT JOIN partner p ON p.partner_id = cr.partner_id 
		LEFT JOIN crm_status st ON st.crm_status_id = cr.crm_status_id 
		LEFT JOIN crm_tip tip ON tip.crm_tip_id = cr.crm_tip_id";
		$where = false;

		if(!is_null($search) && $search != ''){
			$query .= " WHERE";
			$where = true;

			$search = urldecode($search);
			$query .= " (";
			foreach(explode(' ',$search) as $word){
				$query .= " p.naziv ILIKE '%".$word."%' OR p.telefon ILIKE '%".$word."%' OR p.adresa ILIKE '%".$word."%' OR p.kontakt_osoba ILIKE '%".$word."%' OR p.broj_maticni ILIKE '%".$word."%' OR cr.opis ILIKE '%".$word."%' OR p.mail ILIKE '%".$word."%' OR st.naziv ILIKE '%".$word."%' OR tip.naziv ILIKE '%".$word."%' OR";
			}
			$query = substr($query,0,-3);
			$query .= ")";
		}



			if(count($tip_ids) > 0){
			if(!$where){
				$query .= " WHERE";
				$where = true;
			}else{
				$query .= " AND";
			}
			if(count($tip_ids) == 1)
			{
				$query .= " (";
				$query .= " tip.crm_tip_id = ".$tip_ids." OR";
				$query = substr($query,0,-3);
				$query .= ")";
			}
			else
			{
				$query .= " (";
			foreach($tip_ids as $crm_tip_id){
				$query .= " tip.crm_tip_id= ".$crm_tip_id." OR";
			}
				$query = substr($query,0,-3);
				$query .= ")";
			}
		}

		if(count($status_ids) > 0){
			if(!$where){
				$query .= " WHERE";
				$where = true;
			}else{
				$query .= " AND";
			}
			if(count($status_ids) == 1)
			{
				$query .= " (";
				$query .= " st.crm_status_id = ".$status_ids." OR";
				$query = substr($query,0,-3);
				$query .= ")";
			}
			else
			{
				$query .= " (";
			foreach($status_ids as $status_id){
				$query .= " st.crm_status_id = ".$status_id." OR";
			}
				$query = substr($query,0,-3);
				$query .= ")";
			}
		}


		if(!is_null($datum_pocetka) && $datum_pocetka != ''){
			if(!$where){
				$query .= " WHERE";
				$where = true;
			}else{
				$query .= " AND";
			}
			$query .= " DATE(cr.datum_kreiranja) > '".$datum_pocetka."'";
		}
		if(!is_null($datum_kraja) && $datum_kraja != ''){
			if(!$where){
				$query .= " WHERE";
				$where = true;
			}else{
				$query .= " AND";
			}
			$query .= " DATE(cr.datum_zavrsetka) < '".$datum_kraja."'";
		}

	
		

	if(AdminCrm::get_komercijalista_id()>1)
	{

			$kom=AdminCrm::get_komercijalista_id();
			$crmID=DB::table('imenik_crm')->select('crm_id')->where('imenik_id',$kom)->get();
		foreach($crmID as $id=>$val)
		{
			if(count($val->crm_id) > 0)
			{
				if(!$where){
					$query .= " WHERE";
					$where = true;
				}else{
					$query .= " OR";
				}
				if(count($val->crm_id) == 1)
				{
					$query .= " (";
					$query .= " cr.crm_id = ". $val->crm_id." OR";
					$query = substr($query,0,-3);
					$query .= ")";
				}
				else
				{
					$query .= " (";
				foreach($val->crm_id as $crm){
					$query .= " cr.crm_id = ". $val->crm_id." OR";
				}
					$query = substr($query,0,-3);
					$query .= ")";
				}
			}
		}
		//All::dd($query);
	}
	if(!is_null($aktivniS) && $aktivniS != '')

		{
			if(!$where){
				$query .= " WHERE";
				$where = true;
			}else{
				$query .= " AND";
			}
			$query .= " cr.flag_zavrseno = ".$aktivniS."";			
			}
		
		$all = DB::select($query);
		$count = count($all);

		 $query .= " ORDER BY ".$sort_column." ".$sort_direction."";
		// $query .= " LIMIT ".strval($limit)." OFFSET ".strval($limit*($page-1))."";

		return (object) array('rows'=>DB::select($query), 'all' => $all, 'count' => $count);
		}


		public static function getImenikId($crm_id){
			$imenik= array_map('current',DB::table('imenik_crm')
               ->select('imenik_id')
               ->where('crm_id', $crm_id)
               ->get());

			return $imenik;
		}

	

	public static function checkCrmMenadzer($imenik_id){
		var_dump($imenik_id);
		$imenik=DB::table('crm')
		->join('partner', 'crm.partner_id', '=', 'partner.partner_id')
		->join('crm_status', 'crm.crm_status_id', '=', 'crm_status.crm_status_id')
		->join('crm_tip', 'crm.crm_tip_id', '=', 'crm_tip.crm_tip_id')
		->join('imenik_crm', 'crm.crm_id', '=', 'imenik_crm.crm_id')
		->get();
		foreach($imenik as $ime)
		{
			if($ime->imenik_id == $imenik_id)
			{
			 $all = DB::select($imenik);
			$count = count($all);
			return (object) array('rows'=>DB::select($imenik), 'all' => $all, 'count' => $count);
			}

		}
	}

	// public static function crmSendMailNotification(){
	// 		$mailFrom='info@selltico.com';
	// 		$mailTo='milena@tico.rs';
	// 		$subject='danas imate aktivne akcije';
	// 		$body=DB::table('crm_akcija')->select('crm_akcija.opis','crm_akcija.datum','crm_akcija_tip.naziv','crm_akcija.flag_zavrseno','crm_akcija.crm_id', 'crm_akcija.datum_zavrsetka','partner.naziv as partner_ime')
 //                ->where('datum','>=',date('Y-m-d'))
 //                ->join('crm', 'crm_akcija.crm_id', '=', 'crm.crm_id')
 //                ->join('partner', 'crm.partner_id', '=', 'partner.partner_id')
 //                ->join('crm_akcija_tip', 'crm_akcija.crm_akcija_tip_id', '=', 'crm_akcija_tip.crm_akcija_tip_id')->orderBy('datum','asc')->get();
	// 		Mailer::send($mailFrom,$mailTo,$subject,$body);
	// 	}

		

	
	public static function crmFajlovi($crm_id){
        return DB::select("SELECT DISTINCT vrsta_fajla_id, (SELECT ekstenzija FROM vrsta_fajla WHERE vrsta_fajla_id = cd.vrsta_fajla_id) AS ext FROM crm_dokumenta cd WHERE crm_id = ".$crm_id."");
    }
	public static function checkPartnerCrm($partner_id=null){
		
	$crm_partner=DB::table('partner_je')->where('partner_id', $partner_id)->pluck('partner_id');
		return $crm_partner;

	}
	public static function checkFilesCrm($crm_id){
		
	$filesCrm=DB::table('crm_dokumenta')->where('crm_id', $crm_id)->get();
		return $filesCrm;

	}
	public static function getNazivJednogKomercijaliste($imenik_id){
		$ime=DB::table('imenik')->where('imenik_id', $imenik_id)->pluck('ime');
		return $ime;

	}
	public static function getNaziviKomercijaliste($crm_id){

		$imena=DB::table('imenik_crm')->select('imenik.ime')->where('imenik_crm.crm_id', $crm_id)->join('imenik', 'imenik.imenik_id', '=', 'imenik_crm.imenik_id')->get();
		$kom=array();
		foreach ($imena as $ime) {
			$kom=$ime;
		} 
		$oms=implode("",(array)$kom);
		return $oms;
	
	}
	public static function getStatusAkcija($crm_id)
	{
		$akcije= DB::table('crm_akcija')->where('crm_id', $crm_id)->count();
		$akcije_sve=DB::table('crm_akcija')->where('crm_id', $crm_id)->select('crm_akcija_id','flag_zavrseno');
		$flag_zavrseno=DB::table('crm_akcija')->where('crm_id', $crm_id)->where('flag_zavrseno',1)->count();
		if($flag_zavrseno==0 && $akcije==0)
		{
			return 0;
		}
		if($flag_zavrseno==0 && $akcije > 0)
		{
			return 1;
		}
		if($akcije>$flag_zavrseno)
		{
			return 2;
		}
		
		if($flag_zavrseno==$akcije)
		{
			return 4;
		}
		
		//var_dump($avg);
	}
	//analitika
	public static function getUkupnoLeadova() {
		$ukupno = DB::table('crm')->where('crm_id', '!=', -1)->count();
		return $ukupno;
	}
	public static function getUkupnoAkcija() {
		$ukupno = DB::table('crm_akcija')->where('crm_akcija_id', '!=', -1)->count();
		return $ukupno;
	}
	public static function getZavrsenLead() {
		$ukupno = DB::table('crm')->where('flag_zavrseno', 1)->count();
		return $ukupno;
	}
	public static function getZavrseneAkcije() {
		$ukupno = DB::table('crm_akcija')->where('flag_zavrseno', 1)->count();
		return $ukupno;
	}
	public static function getUspesnoZavrsenLead() {
		$search='sklopljen';
		$id_statusa= DB::table('crm_status')->where('naziv','LIKE', '%'.$search.'%')->pluck('crm_status_id');
		
		$ukupno = DB::table('crm')->where('crm_status_id', $id_statusa)->count();
		return $ukupno;
	}
	public static function jucerasnjiPrihodCrm() {
		$juce = date('Y-m-d', strtotime(date('Y-m-d')." -1 day"));
		
		$query = DB::table('crm')->where('datum_zavrsetka', $juce)->where('kursna_lista_id', 1)->get();

		$ukupno = 0;
		foreach($query as $q){
			$ukupno += $q->vrednost;
		}
		return $ukupno;
	}
	public static function mesecniPrihodCrmDin() {
		$mesec = date('Y-m-d', strtotime(date('Y-m-d') . " -1 month"));
		
		$query = DB::table('crm')->whereBetween('datum_zavrsetka', array($mesec, date('Y-m-d')))->select('vrednost', 'kursna_lista_id')->where('kursna_lista_id',1)->get();
		  // try {  
    //       $content = file_get_contents('https://nbs.rs/kursnaListaModul/zaDevize.faces');
    //     } catch (Exception $e) {
    //         return Redirect::to(AdminOptions::base_url().'admin/crm_analitika/')->with('alert','Došlo je do greške, kurs nije preuzet!');
    //     }
    //     $exploded = explode('<td class="tableCell" style="text-align:right;">', $content);
    //     $kursevi = array(
    //         'EUR' => array('kupovni' => '','kupovni' => floatval(substr($exploded[2], 0, 6)),'srednji' => (floatval(substr($exploded[1], 0, 6))+floatval(substr($exploded[2], 0, 6)))/2,'prodajni' => floatval(substr($exploded[1], 0, 6)))
    //         );
    //     $evro = $kursevi['EUR']['prodajni'];
      

		$ukupno = 0;
		foreach($query as $q){
			if($q->kursna_lista_id==1)
			{
				$ukupno += $q->vrednost;
			}
			else{
				
				$ukupno=round($q->vrednost*118);
			}
			
		}
		return $ukupno;

	}
	public static function evroDanas()
	{
		  // try {  
    //       $content = file_get_contents('https://nbs.rs/kursnaListaModul/zaDevize.faces');
    //     } catch (Exception $e) {
    //         return Redirect::to(AdminOptions::base_url().'admin/crm_analitika/')->with('alert','Došlo je do greške, kurs nije preuzet!');
    //     }
    //     $exploded = explode('<td class="tableCell" style="text-align:right;">', $content);
    //     $kursevi = array(
    //         'EUR' => array('kupovni' => '','kupovni' => floatval(substr($exploded[2], 0, 6)),'srednji' => (floatval(substr($exploded[1], 0, 6))+floatval(substr($exploded[2], 0, 6)))/2,'prodajni' => floatval(substr($exploded[1], 0, 6)))
    //         );
        $evro = 118;
      return $evro;

	}
	public static function mesecniPrihodCrmEvr() {
		$mesec = date('Y-m-d', strtotime(date('Y-m-d') . " -1 month"));
		
		$query = DB::table('crm')->whereBetween('datum_zavrsetka', array($mesec, date('Y-m-d')))->select('vrednost', 'kursna_lista_id')->where('kursna_lista_id',null)->get();
		$ukupno = 0;
		foreach($query as $q){
				$ukupno += $q->vrednost;
		}
		return $ukupno;

	}
	public static function ukupanPrihodCrmDin() {
		
		$query = DB::table('crm')->leftJoin('partner', 'partner.partner_id', '=', 'crm.partner_id')->where('kursna_lista_id',1)->get();

		$ukupno = 0;
		foreach($query as $q){
			$ukupno += $q->vrednost;
		}
		return $ukupno;
	}
	public static function ukupanPrihodCrmEvr() {
		
		$query = DB::table('crm')->leftJoin('partner', 'partner.partner_id', '=', 'crm.partner_id')->where('kursna_lista_id',null)->get();

		$ukupno = 0;
		foreach($query as $q){
			$ukupno += $q->vrednost;
		}
		return $ukupno;
	}
	public static function nazivCrmPartnera($crm_id)
	{
		$partner=DB::table('crm')->join('partner', 'crm.partner_id', '=', 'partner.partner_id')->where('crm.crm_id','=',$crm_id)->pluck('partner.naziv');
		return $partner;
	}
	public static function nazivKomercijaliste($crm_id){
		$komercijalista=DB::table('imenik_crm')->where('crm_id',$crm_id)->join('imenik', 'imenik.imenik_id', '=', 'imenik_crm.imenik_id')->pluck('imenik.ime');
		return $komercijalista;
	}
	 public static function check_administrator(){
      // return true;
        $imenik_id = Session::get('b2c_admin'.AdminOptions::server());
        $kvota = intval(DB::table('imenik')->where('imenik_id',$imenik_id)->pluck('kvota'));
        if($kvota == 0){
            return true;
        }
    
    }
    public static function getCrmFakture($search=null,$tip_ids=array(),$datum_pocetka=null,$datum_kraja=null,$sort_column='p.naziv',$sort_direction='ASC'){
    	$query = "SELECT p.partner_id, p.mail, p.kontakt_osoba, p.naziv AS ime, tip.crm_tip_id, tip.naziv, cf.datum_fakturisanja, cf.crm_fakture_id, cf.opis, cf.kursna_lista_id, cf.vrednost,cf.popust,cf.flag_poslato FROM crm_fakture cf LEFT JOIN partner p ON p.partner_id = cf.partner_id LEFT JOIN crm_tip tip ON tip.crm_tip_id = cf.crm_tip_id";
		$where = false;
		
		if(!is_null($search) && $search != ''){
			$query .= " WHERE";
			$where = true;

			$search = urldecode($search);
			$query .= " (";
			foreach(explode(' ',$search) as $word){
				$query .= " p.naziv ILIKE '%".$word."%' OR cf.opis ILIKE '%".$word."%' OR tip.naziv ILIKE '%".$word."%' OR";
			}
			$query = substr($query,0,-3);
			$query .= ")";
		}
		if(count($tip_ids) > 0){
			if(!$where){
				$query .= " WHERE";
				$where = true;
			}else{
				$query .= " AND";
			}
			if(count($tip_ids) == 1)
			{
				$query .= " (";
				$query .= " cf.crm_tip_id = ".$tip_ids." OR";
				$query = substr($query,0,-3);
				$query .= ")";
			}
			else
			{
				$query .= " (";
			foreach($tip_ids as $tip_id){
				$query .= " cf.crm_tip_id = ".$tip_id." OR";
			}
				$query = substr($query,0,-3);
				$query .= ")";
			}
		}
		// if(!is_null($datum_pocetka) && $datum_pocetka != ''){
		// 	if(!$where){
		// 		$query .= " WHERE";
		// 		$where = true;
		// 	}else{
		// 		$query .= " AND";
		// 	}
		// 	$query .= " DATE(cf.datum_fakturisanja) > '".$datum_pocetka."'";
		// }
		// if(!is_null($datum_kraja) && $datum_kraja != ''){
		// 	if(!$where){
		// 		$query .= " WHERE";
		// 		$where = true;
		// 	}else{
		// 		$query .= " AND";
		// 	}
		// 	$query .= " DATE(cf.datum_fakturisanja) < '".$datum_kraja."'";
		// }
		$all = DB::select($query);
		$count = count($all);

		 $query .= " ORDER BY ".$sort_column." ".$sort_direction."";
		// $query .= " LIMIT ".strval($limit)." OFFSET ".strval($limit*($page-1))."";

		return (object) array('rows'=>DB::select($query), 'all' => $all, 'count' => $count);
		}

		public static function trebaFakturisati($crm_fakture_id){
			$mesecNazad = date('Y-m-d', strtotime(date('Y-m-d') . " -1 month"));
			$danas = date('Y-m-d');
			$isprobati = DB::table('crm_fakture')
					->where('crm_fakture_id', $crm_fakture_id)
                    ->whereBetween('datum_fakturisanja', [$mesecNazad, $danas])->get();
			if($isprobati)
			{
				return true;
			}
			else 
			{
				return false;
			}

		}
		public static function mailZaFakturisanje($partner_id){
			$vrednost='fakt';
			$mail= DB::table('partner_kontakt')
			->join('vrsta_kontakta', 'vrsta_kontakta.vrsta_kontakta_id', '=', 'partner_kontakt.vrsta_kontakta_id')
			->where('vrsta_kontakta.naziv','ILIKE', '%'.$vrednost.'%')
			->where('partner_kontakt.partner_id', $partner_id)
			->pluck('partner_kontakt.vrednost');
			return $mail;

		}
		public static function getLogoviFiltered($search=null,$crm_logovi_akcija=null,$korisnik=null,$sort_column='cl.datum_izmene',$sort_direction=null,$datum_pocetka=null,$datum_kraja=null,$limit=null,$page=1){
		$query = "SELECT DISTINCT cl.crm_log_id,cl.datum_izmene,cl.akcija,cl.stara_vrednost,cl.nova_vrednost,i.imenik_id,i.ime,i.prezime
		FROM crm_log cl 
		LEFT JOIN imenik i ON i.imenik_id = cl.imenik_id";
		$where = false;

		if(!is_null($search) && $search != ''){
			$query .= " WHERE";
			$where = true;

			$search = urldecode($search);
			$query .= " (";
			foreach(explode(' ',$search) as $word){
				$query .= " cl.akcija ILIKE '%".$word."%' OR cl.stara_vrednost ILIKE '%".$word."%' OR cl.nova_vrednost ILIKE '%".$word."%' OR i.ime ILIKE '%".$word."%' OR i.prezime ILIKE '%".$word."%' OR ";
			}
			$query = substr($query,0,-3);
			$query .= ")";
		}
		if(!is_null($crm_logovi_akcija) && $crm_logovi_akcija != ''){
			$query .= " WHERE";
			$where = true;
			$crm_logovi_akcija = urldecode(strtolower($crm_logovi_akcija));
			$query .= " (";
			foreach(explode(' ',$crm_logovi_akcija) as $word){
				$query .= " cl.akcija ILIKE '%".$word."%' OR ";
			}
			$query = substr($query,0,-3);
			$query .= ")";
		}
		if(!is_null($datum_pocetka) && $datum_pocetka != ''){
			if(!$where){
				$query .= " WHERE";
				$where = true;
			}else{
				$query .= " AND";
			}
			$query .= " DATE(cl.datum_izmene) > '".$datum_pocetka."'";
		}
		if(!is_null($datum_kraja) && $datum_kraja != ''){
			if(!$where){
				$query .= " WHERE";
				$where = true;
			}else{
				$query .= " AND";
			}
			$query .= " DATE(cl.datum_izmene) < '".$datum_kraja."'";
		}
		if(count($korisnik) > 0){
			if(!$where){
				$query .= " WHERE";
				$where = true;
			}else{
				$query .= " AND";
			}			
				$query .= " (";
				$query .= " i.imenik_id = ".$korisnik." OR";
				$query = substr($query,0,-3);
				$query .= ")";
		}
		
		$all = DB::select($query);
		$count = count($all);

		 $query .= " ORDER BY ".$sort_column." ".$sort_direction."";
		// $query .= " LIMIT ".strval($limit)." OFFSET ".strval($limit*($page-1))."";

		return (object) array('rows'=>DB::select($query), 'all' => $all, 'count' => $count);

	

}


    

}