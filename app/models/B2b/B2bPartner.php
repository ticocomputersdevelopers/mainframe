<?php 


class B2bPartner extends Eloquent {
    
    protected $table = 'partner';
    
    public function isPartner(){
        return Auth::user()->partner_id>0;    
    }

    public static function getPartnerName(){
        $partner = self::where('partner_id',Session::get('b2b_user_'.B2bOptions::server()))->first();
        return $partner->naziv;
    }

    public static function forgotPassword($mail){
        $newPassword = B2bCommon::user_password();
        DB::table('partner')->where('mail',$mail)->update(array('password' => $newPassword));
        $body="Vi ili neko sa Vašom e-mail adresom ".$mail." je uputio zahtev sa <a href='".B2bOptions::base_url()."b2b'>".B2bOptions::base_url()."b2b</a> u vezi sa zaboravljenom lozinkom.Vaša lozinka je resetovana i glasi ovako:<br />".$newPassword;
        $subject="Promena lozinke";
        B2bCommon::send_email_to_client($body,$mail,$subject);

    }

    public static function komercijalista(){
        return DB::table('partner')->select('imenik.*')->join('partner_komercijalista','partner_komercijalista.partner_id','=','partner.partner_id')->join('imenik','imenik.imenik_id','=','partner_komercijalista.imenik_id')->where('kvota','32.00')->where('partner.partner_id',Session::get('b2b_user_'.B2bOptions::server()))->first();

    }
    
    public static function getPartnerObject(){
        return self::where('partner_id',Session::get('b2b_user_'.B2bOptions::server()))->first();
    }

    public static function getPartnerUserObject(){
        return DB::table('partner_korisnik')->where('aktivan',1)->where('partner_korisnik_id',Session::get('b2b_partner_user_'.B2bOptions::server()))->first();
    }

    public static function dokumentiUser($kind=null){
        $user = null;
        $org_user = null;

        if(Session::has('dokumenti_user_'.Options::server())){
            $userKind = Session::get('dokumenti_user_kind_'.Options::server());
            $userId = Session::get('dokumenti_user_'.Options::server());

            if($userKind == 'admin'){
                $org_user = DB::table('imenik')->where('imenik_id',$userId)->first();
                $user = (object) array('kind'=>'admin', 'id'=>$org_user->imenik_id);
            }else if($userKind == 'saradnik'){
                $org_user = DB::table('partner')->where('partner_id',$userId)->first();
                $user = (object) array('kind'=>'saradnik', 'id'=>$org_user->partner_id);
            }

        }

        if(!is_null($kind)){
            if($kind=='admin' && $user->kind != 'admin'){
                return null;
            }else if($kind=='saradnik' && $user->kind != 'saradnik'){
                return null;
            }
        }

        if(!is_null($org_user)){
            return (object) array_merge((array) $user,(array) $org_user);
        }else{
            return $user;
        }
    }
} 