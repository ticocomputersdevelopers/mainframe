<?php
namespace Import;
use Import\Support;
use DB;
use File;
use PHPExcel; 
use PHPExcel_IOFactory;

class Mainframe {

	public static function execute($dobavljac_id,$kurs=null,$extension=null){
		if($extension==null){
			$products_file = "files/mainframe/mainframe_excel/mainframe.xlsx";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;			
		}

//var_dump($continue);die;
		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}

	        $excelReader = PHPExcel_IOFactory::createReaderForFile($products_file);
	        $excelObj = $excelReader->load($products_file);
	        $worksheet = $excelObj->getSheet(0);
	        $lastRow = $worksheet->getHighestRow();
	        
	        for ($row = 2; $row <= $lastRow; $row++) {
                $sifra = $worksheet->getCell('A'.$row)->getValue();
                $proizvodjac = $worksheet->getCell('C'.$row)->getValue();
                $naziv = $worksheet->getCell('D'.$row)->getValue();
                $kategorija = $worksheet->getCell('E'.$row)->getValue();
                $cena_nc = (float) $worksheet->getCell('F'.$row)->getValue();
                $slika1 = $worksheet->getCell('G'.$row)->getValue();
                $slika2 = $worksheet->getCell('H'.$row)->getValue();

                $grupe = explode("|",$kategorija);
                if (isset($grupe[sizeof($grupe) - 2])) {
               	 	$grupa = $grupe[sizeof($grupe) - 2];
                }
                if (isset($grupe[sizeof($grupe) - 1])) {
                	$podgrupa = $grupe[sizeof($grupe) - 1];
 				}
               
              
                
				
				if(isset($sifra) && isset($naziv) && isset($cena_nc) && is_numeric($cena_nc) && $cena_nc > 0){

					$sPolja = '';
					$sVrednosti = '';
					$sPolja .= " partner_id,";				$sVrednosti .= " " . $dobavljac_id . ",";
					$sPolja .= " sifra_kod_dobavljaca,";	$sVrednosti .= " '" . addslashes(Support::encodeTo1250($sifra)) . "',";
					$sPolja .= " proizvodjac,";					$sVrednosti .= " '" . addslashes(Support::encodeTo1250($proizvodjac)) . "',";
					$sPolja .= " naziv,";					$sVrednosti .= " '" . str_replace("\'","\"",addslashes(Support::encodeTo1250($naziv))) . "',";
					$sPolja .= " grupa,";					$sVrednosti .= " '" . addslashes(Support::encodeTo1250($grupa)) . "',";
					$sPolja .= " podgrupa,";					$sVrednosti .= " '" . addslashes(Support::encodeTo1250($podgrupa)) . "',";
					$sPolja .= " kolicina,";				$sVrednosti .= "20,";
					$sPolja .= " cena_nc";					$sVrednosti .= " " . number_format(Support::replace_empty_numeric($cena_nc,2,$kurs,$valuta_id_nc),2, '.', '') . "";

					DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");

					// if (isset($slika1) && !empty($slika1)) {
					// 	DB::statement("INSERT INTO dobavljac_cenovnik_slike_temp(partner_id,sifra_kod_dobavljaca,putanja,akcija)VALUES(".$dobavljac_id.",".addslashes(Support::encodeTo1250($sifra)).",'".Support::encodeTo1250($slika1)."',1 )");
					// }

					// if (isset($slika2) && !empty($slika2)) {
					// 	DB::statement("INSERT INTO dobavljac_cenovnik_slike_temp(partner_id,sifra_kod_dobavljaca,putanja,akcija)VALUES(".$dobavljac_id.",".addslashes(Support::encodeTo1250($sifra)).",'".Support::encodeTo1250($slika2)."',0 )");
					// }		
				}
			}
			Support::queryExecute($dobavljac_id,array('i','u'),array('i'),array());
			
			//Brisemo fajl
			if($extension!=null){
				//File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    //File::delete($products_file);
                }				
			}
		}

	}

	public static function executeShort($dobavljac_id,$kurs=null,$extension=null){

		if($extension==null){
			$products_file = "files/mainframe/mainframe_excel/mainframe.xlsx";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;			
		}

		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}

	        $excelReader = PHPExcel_IOFactory::createReaderForFile($products_file);
	        $excelObj = $excelReader->load($products_file);
	        $worksheet = $excelObj->getSheet(0);
	        $lastRow = $worksheet->getHighestRow();

	        for ($row = 2; $row <= $lastRow; $row++) {
	        	$sifra = $worksheet->getCell('A'.$row)->getValue();
				$cena_nc = (float) $worksheet->getCell('F'.$row)->getValue();

				if(isset($sifra) && isset($naziv) && isset($cena_nc) && is_numeric($cena_nc) && $cena_nc > 0){


					$sPolja = '';
					$sVrednosti = '';
					$sPolja .= " partner_id,";				$sVrednosti .= " " . $dobavljac_id . ",";
					$sPolja .= " sifra_kod_dobavljaca,";	$sVrednosti .= " '" . addslashes(Support::encodeTo1250($sifra)) . "',";
					$sPolja .= " cena_nc";					$sVrednosti .= " " . number_format(Support::replace_empty_numeric($cena_nc,2,$kurs,$valuta_id_nc),2, '.', '') . "";


					DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");		
				}
			}

			//Support::queryShortExecute($dobavljac_id, $type);
			//Brisemo fajl
			if($extension!=null){
				//File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                   // File::delete($products_file);
                }				
			}
		}
	}


}