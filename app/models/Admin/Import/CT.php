<?php
namespace Import;
use Import\Support as Support;
use DB;
use AdminOptions;
use SOAPClient;
use DOMDocument;
use Export\Support as ExportSupport;

class CT {

	public static function execute($dobavljac_id,$kurs=null){
		Support::initIniSetup();
		Support::initQueryExecute();
		if($kurs==null){
			$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
		}

		$functionGroups = "GetCTProductGroups"; 
		$functionArticles = "GetCTProducts"; 
		$functionGroupsWithAttributes = "GetCTProductGroups_WithAttributes";

		$user_data =array("username" => Support::serviceUsername($dobavljac_id), "password" => Support::servicePassword($dobavljac_id));
		
			$client = new SOAPClient( 
			'https://www.ct4partners.me/WebServices/CTProductsInStock.asmx?WSDL', 
			array( 
				'location' => 'https://www.ct4partners.me/WebServices/CTProductsInStock.asmx?WSDL', 
				'trace' => 1, 
				'style' => SOAP_RPC, 
				'use' => SOAP_ENCODED, 
			) 
			);
			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;

			$groups = $client->__soapCall($functionGroups, array("parameters"=>$user_data));
			$groups_with_attributes = $client->__soapCall($functionGroupsWithAttributes, array("parameters"=>$user_data));

			$xml = new DOMDocument("1.0","UTF-8");
			$root = $xml->createElement("grupe");
			$xml->appendChild($root);

			foreach ($groups->GetCTProductGroupsResult->ProductGroup as $group) {
				$grupa   = $xml->createElement("grupa");

				ExportSupport::xml_node($xml,"Code",$group->Code,$grupa);
				ExportSupport::xml_node($xml,"GroupDescription",$group->GroupDescription,$grupa);

				$root->appendChild($grupa);
			}


			$xml->formatOutput = true;
			$xml->save("files/ct/ct_xml/groups.xml");

			$groups = simplexml_load_file("files/ct/ct_xml/groups.xml", 'SimpleXMLElement', LIBXML_NOCDATA);


			$grupe = array();
			foreach ($groups as $group) {
				$grupe[] = (string) $group->Code;
			}
			

			


			$functionArticlesAttributes = "GetCTProducts_WithAttributes"; 
			
			foreach ($grupe as $group) {
 				
				$user_data2 = $user_data;
				$user_data2['productGroupCode'] = $group;
				
				

				$products_with_attributes = $client->__soapCall($functionArticlesAttributes, array("parameters"=>$user_data2));
				
				if(count((array) $products_with_attributes->GetCTProducts_WithAttributesResult) == 0){
					continue;
				}
				//var_dump($products_with_attributes); die;
				foreach ($products_with_attributes->GetCTProducts_WithAttributesResult->CTPRODUCT as $artikal) {
					$product_array = array();
					$slike_array = array();
					$karakteristike_array = array();
					//var_dump($artikal['CODE']); die;
					$artikal = (array) $artikal;

					if (isset($artikal['CODE']) && !empty($artikal['CODE'])) {
						$product_array['sifra_kod_dobavljaca'] = $artikal['CODE'];
					}

					if (isset($artikal['PRODUCTGROUPCODE']) && !empty($artikal['PRODUCTGROUPCODE'])) {
						$product_array['grupa'] = str_replace("'","",Support::encodeTo1250(Support::quotedStr($artikal['PRODUCTGROUPCODE'])));
					}

					if (isset($artikal['NAME']) && !empty($artikal['NAME'])) {
						$product_array['naziv'] = str_replace("'","",Support::encodeTo1250(Support::quotedStr($artikal['NAME'])));
					}

					if (isset($artikal['MANUFACTURER']) && !empty($artikal['MANUFACTURER'])) {
						$product_array['proizvodjac'] = str_replace("'","",Support::encodeTo1250(Support::quotedStr($artikal['MANUFACTURER'])));
					}

					if (isset($artikal['QTTYINSTOCK']) && !empty($artikal['QTTYINSTOCK'])) {
						$kol = str_replace(array('>','&gt;'),array('',''),$artikal['QTTYINSTOCK']);
							if($kol==0){
								$product_array['kolicina'] = 0;
							}else{
								$product_array['kolicina'] = intval($kol);
							}
					}

					if (isset($artikal['TAX']) && !empty($artikal['TAX'])) {
						$product_array['pdv'] = $artikal['TAX'];
					}

					if (isset($artikal['EUR_ExchangeRate']) && !empty($artikal['EUR_ExchangeRate'])) {
						$kurs_eura = number_format((double)str_replace(',', '.', $artikal['EUR_ExchangeRate']),2,'.',''); 
					}

					if(isset($artikal['PRICE']) && !empty($artikal['PRICE'])) {
						$cena_nc = str_replace(',', '.', $artikal['PRICE']);
						$product_array['cena_nc'] = number_format((double) Support::replace_empty_numeric($cena_nc,2,$kurs_eura,$valuta_id_nc),2, '.', '');
					}

					if (isset($artikal['RETAILPRICE']) && !empty($artikal['RETAILPRICE'])) {
						$pmp_cena = str_replace(',', '.', $vrednost);
						$product_array['pmp_cena'] = number_format((double) Support::replace_empty_numeric($pmp_cena,2,$kurs,$valuta_id_nc),2, '.', '');
					}

					if (isset($artikal['SHORT_DESCRIPTION']) && !empty($artikal['SHORT_DESCRIPTION'])) {
						$product_array['opis'] = Support::encodeTo1250(Support::quotedStr($artikal['SHORT_DESCRIPTION']));
						$product_array['flag_opis_postoji'] = 1;
					}

					if (isset($artikal['BARCODE']) && !empty($artikal['BARCODE'])) {
						$product_array['barkod'] = Support::encodeTo1250(Support::quotedStr($artikal['BARCODE']));
					}

					if (isset($artikal['IMAGE_URLS']) && !empty($artikal['IMAGE_URLS'])) {
						//var_dump($artikal['IMAGE_URLS']); die;
							$vrednost_slike = (array) $artikal['IMAGE_URLS'];
							foreach ($vrednost_slike as $slika) {
								if (!empty($slika)) {
									$slike_array[] = $slika;
								}
							}
					}

						if (!empty($product_array) && isset($product_array['naziv']) && !empty($product_array['naziv']) && isset($product_array['grupa']) && !empty($product_array['grupa']) && isset($product_array['cena_nc']) && !empty($product_array['cena_nc']) && $product_array['cena_nc'] > 0) {
							//upis u bazu karakteristike

							if (isset($artikal['ATTRIBUTES']) && !empty($artikal['ATTRIBUTES'])) {
										$vrednost_karak = (array) $artikal['ATTRIBUTES'];
										
										foreach ($vrednost_karak as  $atributi) {
												$atributi = (array) $atributi;
											foreach ($atributi as $atribut) {
													unset($karakteristika_naziv);
													unset($karakteristika_vrednost);
													$atribut = (array) $atribut;
												foreach ($atribut as $key_atribut => $karakteristika) {
															if ($key_atribut == 'AttributeValue') {
																$karakteristika_vrednost = $karakteristika;
															}
															if ($key_atribut == 'AttributeCode') {
																$karakteristika_naziv = $karakteristika;
															}
																
													}

												if (isset($karakteristika_naziv) && !empty($karakteristika_naziv) && isset($karakteristika_vrednost) && !empty($karakteristika_vrednost)) {
														
																	
							
												$karakteristike_array['partner_id'] = $dobavljac_id;
												$karakteristike_array['sifra_kod_dobavljaca'] = $product_array['sifra_kod_dobavljaca'];
												$karakteristike_array['karakteristika_naziv'] =  $karakteristika_naziv;
												$karakteristike_array['karakteristika_vrednost'] = $karakteristika_vrednost;

												DB::table('dobavljac_cenovnik_karakteristike_temp')->insert($karakteristike_array);

												}
													
											}
										}
										
							
								
							}
					


						if (count($slike_array) > 0) {
							$product_array['flag_slika_postoji'] = 1;
						}

							// upis artikala u bazu
							$product_array['partner_id'] = $dobavljac_id;
							DB::table('dobavljac_cenovnik_temp')->insert($product_array);

						//upis slika u bazu
						foreach ($slike_array as $picture) {
							$picture = (array) $picture;
							foreach ($picture as $slika_key => $slika) {
								
								if ($slika_key == 0) {
									DB::statement("INSERT INTO dobavljac_cenovnik_slike_temp(partner_id,sifra_kod_dobavljaca,putanja,akcija)VALUES(".$dobavljac_id.",'".$product_array['sifra_kod_dobavljaca']."','".(string) $slika."',1 )");
								} else {
									DB::statement("INSERT INTO dobavljac_cenovnik_slike_temp(partner_id,sifra_kod_dobavljaca,putanja,akcija)VALUES(".$dobavljac_id.",'".$product_array['sifra_kod_dobavljaca']."','".(string) $slika."',0 )");
								}
							}
						}

						}


				}
				


			}
						
				
			Support::queryExecute($dobavljac_id,array('i','u'),array('i'),array('i','u'));
	}

	public static function executeShort($dobavljac_id,$kurs=null){
		Support::initIniSetup();
		Support::initQueryExecute();
		if($kurs==null){
			$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
		}

		$functionGroups = "GetCTProductGroups"; 
		$functionArticles = "GetCTProducts"; 
		$functionGroupsWithAttributes = "GetCTProductGroups_WithAttributes";

		$user_data =array("username" => Support::serviceUsername($dobavljac_id), "password" => Support::servicePassword($dobavljac_id));
		
			$client = new SOAPClient( 
			'https://www.ct4partners.me/WebServices/CTProductsInStock.asmx?WSDL', 
			array( 
				'location' => 'https://www.ct4partners.me/WebServices/CTProductsInStock.asmx?WSDL', 
				'trace' => 1, 
				'style' => SOAP_RPC, 
				'use' => SOAP_ENCODED, 
			) 
			);
			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;

			$groups = $client->__soapCall($functionGroups, array("parameters"=>$user_data));
			$groups_with_attributes = $client->__soapCall($functionGroupsWithAttributes, array("parameters"=>$user_data));

			$xml = new DOMDocument("1.0","UTF-8");
			$root = $xml->createElement("grupe");
			$xml->appendChild($root);

			foreach ($groups->GetCTProductGroupsResult->ProductGroup as $group) {
				$grupa   = $xml->createElement("grupa");

				ExportSupport::xml_node($xml,"Code",$group->Code,$grupa);
				ExportSupport::xml_node($xml,"GroupDescription",$group->GroupDescription,$grupa);

				$root->appendChild($grupa);
			}


			$xml->formatOutput = true;
			$xml->save("files/ct/ct_xml/groups.xml");

			$groups = simplexml_load_file("files/ct/ct_xml/groups.xml", 'SimpleXMLElement', LIBXML_NOCDATA);


			$grupe = array();
			foreach ($groups as $group) {
				$grupe[] = (string) $group->Code;
			}
			

			


			$functionArticlesAttributes = "GetCTProducts_WithAttributes";

			foreach ($grupe as $group) {
 				
				$user_data2 = $user_data;
				$user_data2['productGroupCode'] = $group;
				
				

				$products_with_attributes = $client->__soapCall($functionArticlesAttributes, array("parameters"=>$user_data2));
				
				if(count((array) $products_with_attributes->GetCTProducts_WithAttributesResult) == 0){
					continue;
				}
				
				foreach ($products_with_attributes->GetCTProducts_WithAttributesResult->CTPRODUCT as $artikal) {
					$product_array = array();
					
					$artikal = (array) $artikal;

					if (isset($artikal['CODE']) && !empty($artikal['CODE'])) {
						$product_array['sifra_kod_dobavljaca'] = $artikal['CODE'];
					}

					if (isset($artikal['QTTYINSTOCK']) && !empty($artikal['QTTYINSTOCK'])) {
						$kol = str_replace(array('>','&gt;'),array('',''),$artikal['QTTYINSTOCK']);
							if($kol==0){
								$product_array['kolicina'] = 0;
							}else{
								$product_array['kolicina'] = intval($kol);
							}
					}

					if (isset($artikal['EUR_ExchangeRate']) && !empty($artikal['EUR_ExchangeRate'])) {
						$kurs_eura = number_format((double)str_replace(',', '.', $artikal['EUR_ExchangeRate']),2,'.',''); 
					}

					if(isset($artikal['PRICE']) && !empty($artikal['PRICE'])) {
						$cena_nc = str_replace(',', '.', $artikal['PRICE']);
						$product_array['cena_nc'] = number_format((double) Support::replace_empty_numeric($cena_nc,2,$kurs_eura,$valuta_id_nc),2, '.', '');
					}

					if (isset($artikal['RETAILPRICE']) && !empty($artikal['RETAILPRICE'])) {
						$pmp_cena = str_replace(',', '.', $vrednost);
						$product_array['pmp_cena'] = number_format((double) Support::replace_empty_numeric($pmp_cena,2,$kurs,$valuta_id_nc),2, '.', '');
					}

					if (!empty($product_array) && isset($product_array['cena_nc']) && $product_array['cena_nc'] > 0) {
							$product_array['partner_id'] = $dobavljac_id;
							DB::table('dobavljac_cenovnik_temp')->insert($product_array);
					}


				}
			}
	}

}

?>