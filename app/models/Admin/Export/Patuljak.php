<?php
namespace Export;

use Export\Support;
use DB;
use AdminOptions;
use AdminCommon;
use DOMDocument;
use Response;
use View;

class Patuljak {

	public static function execute($export_id,$kind,$short=false){

		$export_products = DB::select("SELECT roba_id, web_cena, naziv_web, web_flag_karakteristike, web_karakteristike, web_opis, (SELECT naziv FROM proizvodjac WHERE proizvodjac_id = roba.proizvodjac_id) AS proizvodjac, (SELECT grupa FROM grupa_pr WHERE grupa_pr_id = roba.grupa_pr_id) AS grupa, (SELECT number from export_patuljak where export_patuljak_id = (SELECT parent_id from export_grupe where export_id = ".$export_id ." and grupa_id = roba.grupa_pr_id)) AS patuljak_grupa,model, (SELECT ROUND(kolicina) FROM lager WHERE roba_id = roba.roba_id AND poslovna_godina_id = (SELECT poslovna_godina_id FROM poslovna_godina WHERE status=0) AND orgj_id = (SELECT orgj_id FROM imenik_magacin WHERE izabrani=1)) AS kolicina, akcija_flag_primeni, akcijska_cena, datum_akcije_od, datum_akcije_do, barkod, napomena, garancija, tezinski_faktor FROM roba WHERE roba_id IN (SELECT roba_id FROM roba_export WHERE export_id=".$export_id.") AND proizvodjac_id <> -1 AND flag_aktivan = 1 AND flag_prikazi_u_cenovniku = 1 AND web_cena > 0 and grupa_pr_id in (select grupa_id from export_grupe where export_id = ".$export_id.")");

		if($kind=='xml'){
			return self::xml_exe($export_products);
		}else{
			echo '<h2>Dati format nije podržan!</h2>';
		}

	}

	public static function xml_exe($products){
		$xml = new DOMDocument("1.0","UTF-8");
		$root = $xml->createElement("artikli");
		$xml->appendChild($root);

		foreach($products as $article){

			$product   = $xml->createElement("artikal");

			Support::xml_node_cdata($xml,"number",$article->roba_id,$product);
		    Support::xml_node_cdata($xml,"title",$article->naziv_web,$product);
			Support::xml_node_cdata($xml,"lager",$article->kolicina,$product);
			Support::xml_node_cdata($xml,"brands",$article->proizvodjac,$product);
			Support::xml_node_cdata($xml,"description",$article->web_opis."\n".Support::characteristics($article->roba_id,$article->web_flag_karakteristike,$article->web_karakteristike),$product);
			Support::xml_node_cdata($xml,"categ",$article->patuljak_grupa,$product);
			Support::xml_node_cdata($xml,"retail",($article->akcija_flag_primeni==1 && (is_null($article->datum_akcije_od) || $article->datum_akcije_od <= date('Y-m-d')) && is_null(is_null($article->datum_akcije_do) || $article->datum_akcije_do >= date('Y-m-d')) ? $article->akcijska_cena:$article->web_cena),$product);
			foreach(DB::table('web_slika')->where(array('roba_id'=>$article->roba_id, 'flag_prikazi'=>1))->get() as $slika){
				Support::xml_node_cdata($xml,"photo", AdminOptions::base_url().$slika->putanja,$product);
			}


			$root->appendChild($product);
		}

		$xml->formatOutput = true;
		$store_path = 'files/exporti/patuljak/patuljak.xml';
		$xml->save($store_path) or die("Error");

		// header('Content-type: text/xml');
		// //header('Content-Disposition: attachment; filename="CeneRS.xml"');
		// echo file_get_contents($store_path); die;	
		return $store_path;
	}

}