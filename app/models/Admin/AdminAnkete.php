<?php

class AdminAnkete {

	public static function find($anketa_id, $column)
	{
		return DB::table('anketa')->select($column)->where('anketa_id', $anketa_id)->pluck($column);

	}
	public static function question($anketa_pitanje_id, $column)
	{
		return DB::table('anketa_pitanje')->select($column)->where('anketa_pitanje_id', $anketa_pitanje_id)->pluck($column);

	}
	
	public static function ankete($flag_aktivan=null)
	{	
		if(!is_null($flag_aktivan) && is_numeric($flag_aktivan) && in_array($flag_aktivan,[0,1])){
			return DB::table('anketa')->where('flag_aktivan',$flag_aktivan)->get();
		}else{
			return DB::table('anketa')->get();
		}

	}

	public static function anketaPitanjaCheckAnalitika($anketaId,$webKupacId=0,$datumOd=null,$datumDo=null,$narudzbinaId=0)
	{	
		return DB::select("select ap.anketa_pitanje_id, ap.pitanje, ao.odgovor, count(aok.anketa_odgovor_id) from anketa_odgovor_korisnik aok left join anketa_odgovor ao on ao.anketa_odgovor_id = aok.anketa_odgovor_id left join anketa_pitanje ap on ap.anketa_pitanje_id = aok.anketa_pitanje_id 
			where ap.tip = 'checkbox' and ap.anketa_id = ".$anketaId." 
			".($webKupacId>0 ? " and aok.web_kupac_id = ".strval($webKupacId) : "")."
			".($narudzbinaId>0 ? " and aok.web_b2c_narudzbina_id = ".strval($narudzbinaId) : "")."
			".(!empty($datumOd) ? " and aok.datum >= '".$datumOd."'" : "")."
			".(!empty($datumDo) ? " and aok.datum < '".$datumDo."'" : "")."
			group by ap.anketa_pitanje_id, ao.anketa_odgovor_id order by ap.rbr asc, ao.rbr asc");

	}
	public static function anketaOdgovoriUkupno($anketa_pitanje_id,$webKupacId=0,$datumOd=null,$datumDo=null,$narudzbinaId=0)
	{	
		return DB::select("select count(aok.anketa_odgovor_id) from anketa_odgovor_korisnik aok left join anketa_pitanje ap on ap.anketa_pitanje_id = aok.anketa_pitanje_id where ap.tip = 'checkbox' and ap.anketa_pitanje_id = ".$anketa_pitanje_id." 
			".($webKupacId>0 ? " and aok.web_kupac_id = ".strval($webKupacId) : "")."
			".($narudzbinaId>0 ? " and aok.web_b2c_narudzbina_id = ".strval($narudzbinaId) : "")."
			".(!empty($datumOd) ? " and aok.datum >= '".$datumOd."'" : "")."
			".(!empty($datumDo) ? " and aok.datum < '".$datumDo."'" : "")."
			")[0]->count;

	}
	public static function anketaPitanjaTextAnalitika($anketaId,$webKupacId=0,$datumOd=null,$datumDo=null,$narudzbinaId=0)
	{	
		return DB::select("select ap.anketa_pitanje_id, ip, (select (CASE WHEN flag_vrsta_kupca = 0 THEN ime || ' ' || prezime WHEN flag_vrsta_kupca = 1  THEN naziv END) from web_kupac where web_kupac_id = aok.web_kupac_id) as kupac, ap.pitanje, anketa_odgovor_text from anketa_odgovor_korisnik aok left join anketa_pitanje ap on ap.anketa_pitanje_id = aok.anketa_pitanje_id 
			where ap.tip = 'text' and ap.anketa_id = ".$anketaId." 
			".($webKupacId>0 ? " and aok.web_kupac_id = ".strval($webKupacId) : "")."
			".($narudzbinaId>0 ? " and aok.web_b2c_narudzbina_id = ".strval($narudzbinaId) : "")."
			".(!empty($datumOd) ? " and aok.datum >= '".$datumOd."'" : "")."
			".(!empty($datumDo) ? " and aok.datum < '".$datumDo."'" : "")."
			order by ap.anketa_pitanje_id ASC");

	}
	public static function anketaKupci($anketaId)
	{	
		return DB::select("select distinct wk.web_kupac_id, (select (CASE WHEN flag_vrsta_kupca = 0 THEN ime || ' ' || prezime WHEN flag_vrsta_kupca = 1  THEN naziv END)) as kupac from anketa_odgovor_korisnik aok left join web_kupac wk on wk.web_kupac_id = aok.web_kupac_id where aok.web_kupac_id is not null and aok.anketa_pitanje_id in (select anketa_pitanje_id from anketa_pitanje where anketa_id=".$anketaId.")");

	}
	public static function anketaKupciDatum($anketaId)
	{	
		return DB::select("select distinct wk.web_kupac_id, aok.datum, (select (CASE WHEN flag_vrsta_kupca = 0 THEN ime || ' ' || prezime WHEN flag_vrsta_kupca = 1  THEN naziv END)) as kupac from anketa_odgovor_korisnik aok left join web_kupac wk on wk.web_kupac_id = aok.web_kupac_id where aok.web_kupac_id is not null and aok.anketa_pitanje_id in (select anketa_pitanje_id from anketa_pitanje where anketa_id=".$anketaId.") order by aok.datum desc");

	}

	public static function anketaNarudzbine($anketaId,$webKupacId)
	{	
		return DB::select("select aok.web_b2c_narudzbina_id, aok.datum, (select broj_dokumenta from web_b2c_narudzbina where web_b2c_narudzbina_id=aok.web_b2c_narudzbina_id) from anketa_odgovor_korisnik aok where aok.web_b2c_narudzbina_id is not null and aok.anketa_pitanje_id in (select anketa_pitanje_id from anketa_pitanje where anketa_id=".$anketaId.($webKupacId > 0 ? " and aok.web_kupac_id=".$webKupacId."":"").") group by aok.web_b2c_narudzbina_id, aok.datum order by aok.datum desc");

	}

}
