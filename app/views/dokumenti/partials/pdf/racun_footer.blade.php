
<div class="row"> 
	<div class="napomena">
		<p>Kod plaćanja - poziv na broj: {{$racun->broj_dokumenta}}</p>
		@if(AdminOptions::web_options(311)==0)
		<p><strong>Napomena o poreskom oslobađanju: "{{AdminOptions::company_name()}}" nije u sistemu pdv-a </strong></p>
		@else
		<p><strong>Napomena o poreskom oslobađanju: Nema </strong></p>
		@endif
		<p></p>
	</div>
</div>

<div class="row"> 
	<table class="signature">
		<tr>  
			<td>&nbsp;</td>
			<td class="text-right">
				<span class="robu_izdao" style=" vertical-align: middle;">Račun izdao</span> 
				<img style="max-width: 220px; max-height: 90px;" src="{{ AdminSupport::potpis() }}">
			</td>
		</tr>

		<tr>
			<td style='width: 60%;'>
				<p style="border-top: 1px solid #666; "> Račun je izrađen na računaru i punovažan je bez pečata i potpisa</p>
			</td> 
		</tr>
	</table>
</div>