<div id="main-content" >
	@if(Session::has('message'))
	<script>
		alertify.success('{{ Session::get('message') }}');
	</script>
	@endif

	@include('admin/partials/tabs')
	
	<div class="row">
		<div class="medium-3 small-12 columns ">
			<div class="flat-box">
				<h3 class="title-med">{{ AdminLanguage::transAdmin('Izaberi katalog') }}  <i class="fa fa-newspaper-o"></i></h3>
				<div class="manufacturer"> 
					<select class="JSeditSupport search_select">
						<option value="{{ AdminOptions::base_url() }}admin/katalog/0">{{ AdminLanguage::transAdmin('Dodaj novi') }}</option>				
						@foreach(AdminKatalog::getKatalog(true) as $row)
						<option value="{{ AdminOptions::base_url() }}admin/katalog/{{ $row->katalog_id }}" @if($row->katalog_id == $katalog_id) {{ 'selected' }} @endif>{{ $row->naziv }} </option>
						@endforeach
					</select>
				</div>
			</div>
		</div>

		<div class="small-12 medium-6 columns">
			<div class="flat-box">

				<h1 class="title-med">{{ $title }}</h1> 

				<form method="POST" action="{{ AdminOptions::base_url() }}admin/katalog-edit" enctype="multipart/form-data">
					<input type="hidden" name="katalog_id" value="{{ $katalog_id }}">

					<div class="row">
						<div class="columns medium-12 {{ $errors->first('naziv') ? ' error' : '' }}">
							<label for="naziv">{{ AdminLanguage::transAdmin('Naziv kataloga') }}</label>
							<input type="text" name="naziv" value="{{ Input::old('naziv') ? Input::old('naziv') : $naziv }}" autofocus="autofocus" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'readonly' : '' }}>
						</div>
					</div>

					<div class="row">
						<div class="columns medium-3 small-6">
							<label>{{ AdminLanguage::transAdmin('Aktivno') }}</label>
							<select name="aktivan" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'disabled' : '' }}>
								@if(Input::old('aktivan') == '1')
								@if(Input::old('aktivan'))
								<option value="1" selected>{{ AdminLanguage::transAdmin('DA') }}</option>
								<option value="0" >{{ AdminLanguage::transAdmin('NE') }}</option>
								@else
								<option value="1" >{{ AdminLanguage::transAdmin('DA') }}</option>
								<option value="0" selected>{{ AdminLanguage::transAdmin('NE') }}</option>
								@endif
								@else
								@if($aktivan == '1')
								<option value="1" selected>{{ AdminLanguage::transAdmin('DA') }}</option>
								<option value="0" >{{ AdminLanguage::transAdmin('NE') }}</option>
								@else
								<option value="1" >{{ AdminLanguage::transAdmin('DA') }}</option>
								<option value="0" selected>{{ AdminLanguage::transAdmin('NE') }}</option>
								@endif
								@endif									
							</select>
						</div>
						
						<div class="columns medium-3 small-6">
							@if(AdminOptions::checkB2C())
							<div class="inline-block"> 
								<label>{{ AdminLanguage::transAdmin('B2C') }}</label>
								<input type="checkbox" name="b2c_aktivno" {{ (count(Input::old())>0 ? (Input::old('b2c_aktivno')=='on') : ($b2c_aktivno==1)) ? 'checked' : '' }}>
							</div>
							&nbsp;&nbsp;&nbsp;
							@endif

							@if(AdminOptions::checkB2B()) 
							<div class="inline-block">  
								<label>{{ AdminLanguage::transAdmin('B2B') }}</label>
								<input type="checkbox" name="b2b_aktivno" {{ (count(Input::old())>0 ? (Input::old('b2b_aktivno')=='on') : ($b2b_aktivno==1)) ? 'checked' : '' }}>
							</div>
							@endif
						</div>
						
						<div class="columns medium-6">
							<label>{{ AdminLanguage::transAdmin('Sortiranje') }}</label>
							<select name="sort" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'disabled' : '' }}> 
								<option value="web_cena-ASC" {{ $sort=='web_cena-ASC' ? 'selected' : '' }}>{{ AdminLanguage::transAdmin('Ceni rastuce') }}</option>
								<option value="web_cena-DESC" {{ $sort=='web_cena-DESC' ? 'selected' : '' }}>{{ AdminLanguage::transAdmin('Ceni opadajuce') }}</option>
								<option value="naziv_web-ASC" {{ $sort=='naziv_web-ASC' ? 'selected' : '' }}>{{ AdminLanguage::transAdmin('Nazivu') }}</option> 
							</select>
						</div>
					</div>

					<div class="row">
						<div class="column medium-6">
							<label>{{ AdminLanguage::transAdmin('Vazi od') }}</label>
							<div class="relative"> 
								<input class="akcija-input" id="datum_od" name="vazi_od" autocomplete="off" type="text" value="{{ Input::old('vazi_od') ? Input::old('vazi_od') : $vazi_od }}">
								<span id="datum_od_delete" class="text-red absolute-right"><i class="fa fa-times" aria-hidden="true"></i></span>
							</div>				
						</div>

						<div class="columns medium-6">
							<label>{{ AdminLanguage::transAdmin('Vrsta') }}</label>
							<select name="katalog_vrsta_id" id="JSKatalogVrsta">														
								@foreach($katalog_vrste as $row)
								<option value="{{ $row->katalog_vrsta_id }}" @if($row->katalog_vrsta_id == $katalog_vrsta_id) {{ 'selected' }} @endif>{{ $row->naziv }} </option>
								@endforeach
							</select>
						</div>
					</div>

						<div class="row" {{((Input::old('katalog_vrsta_id') ? Input::old('katalog_vrsta_id') : $katalog_vrsta_id ) == 3) ? '' : 'hidden="hidden"'}} id="JSKarakteristikeSort">
							<div class="column medium-6">
								<label>{{ AdminLanguage::transAdmin('Grupe') }}</label>
								<select name="grupa_pr_id" id="JSGrupaID">
									{{ AdminSupport::selectGroups((Input::old('grupa_pr_id') ? Input::old('grupa_pr_id') : null),true) }}
								</select>							
							</div>

							<div class="columns medium-6">
								<label>{{ AdminLanguage::transAdmin('Sortiranje po') }}</label>
								<div name="grupa_pr_naziv_id" id="JSGrupaNazivID">
									<!-- ajax content -->
								</div>
							</div>

							<div class="columns medium-6">
								<label>{{ AdminLanguage::transAdmin('Izabrane grupe sa sort karakteristikom') }}</label>
								<ul>
									@foreach(AdminKatalog::getKatalogGrupeKarakteristikeSort($katalog_id) as $grupaKarakteristika)
									<li>{{ $grupaKarakteristika->grupa_naziv }} ({{ $grupaKarakteristika->karakteristika_naziv }}) <span class="JSGrupaKarakNazivID" data-katalog_grupe_sort='{"katalog_id":"{{$grupaKarakteristika->katalog_id}}","grupa_pr_id":"{{$grupaKarakteristika->grupa_pr_id}}","grupa_pr_naziv_id":"{{$grupaKarakteristika->grupa_pr_naziv_id}}"}'>Ukloni</span></li>
									@endforeach
								</ul>
							</div>
						</div>

						@if(Admin_model::check_admin(array('SIFARNICI_AZURIRANJE'))) 
						<div class="btn-container center">
							<button type="submit" class="btn btn-primary save-it-btn">{{ AdminLanguage::transAdmin('Sačuvaj') }}</button>
							@if($katalog_id != 0)
							<button class="btn btn-danger JSbtn-delete" data-link="{{ AdminOptions::base_url() }}admin/katalog-delete/{{ $katalog_id }}">{{ AdminLanguage::transAdmin('Obriši') }}</button>
							@endif
						</div> 
						@endif 
					</form>
				</div>
			</div>

			<div class="medium-3 small-12 columns">
				<div class="flat-box">
					<h3 class="title-med">{{ AdminLanguage::transAdmin('Polja') }}</h3>
					<ul class="ktlg-list lista JSFildsSortable" data-katalog_id="{{$katalog_id}}">
						@foreach($katalog_polja as $row)
						<li class="ui-state-default relative" id="{{$row->katalog_polja_id}}">
							<span>{{$row->title}}</span>
							<a class="absolute-right" href="{{ AdminOptions::base_url() }}/admin/katalog-polje/{{$row->katalog_polja_id}}/obrisi"><i class="fa fa-times text-red"></i></a>
						</li>
						@endforeach
					</ul>
					
					@if(count($katalog_dostupna_polja) > 0 AND $katalog_id > 0)
					<div class="manufacturer">
						<label>Dodaj novo polje</label>
						<select id="JSKatalogPoleSelect" class="search_select">
							@foreach($katalog_dostupna_polja as $key => $value)
							<option value="{{ $key }}" title="{{ $value }}">{{ $value }} </option>
							@endforeach
						</select>
						<div class="center"> 
							<button id="JSKatalogPoleAdd" class="btn btn-primary" data-katalog_id="{{$katalog_id}}">Dodaj</button>
						</div>
					</div>
					@endif		
				</div>
			</div>
		</div>
	</div>
