<div id="main-content" >
@if(Session::has('message'))
	<script>
		alertify.success('{{ Session::get('message') }}');
	</script>
@endif
	@include('admin/partials/tabs')

	<div class="row">
		<section class="small-12 medium-5 large-centered medium-centered columns">
			<div class="flat-box">
				<h3 class="title-med">Kupovina na rate</h3>
				<table>
					<thead>
						<tr> 
							<th>Broj mesečnih rata</th>
							<th>&nbsp;&nbsp;&nbsp;</th>
							<th>Kamata %</th>
							<th></th> 
						</tr>
					</thead>
					<tbody>
						@foreach($broj_rata as $row)
						<tr>
							<form method="POST" action="{{AdminOptions::base_url()}}admin/rata_edit">
								<input type="hidden" name="rata_id" value="{{ $row->rata_id }}">
								<input type="hidden" name="br_meseca" value="{{ $row->br_meseca }}">
								<input type="hidden" name="kamata" value="{{ $row->kamata }}">				

								<td><input type="text" name="br_meseca" value="{{ $row->br_meseca }}">	
						    	<td>&nbsp;&nbsp;&nbsp;</td>
								<td><input type="text" name="kamata" value="{{ $row->kamata }}"></td>
								<td><input type="submit" value="Sačuvaj"></td>			 
								<td><button class="btn btn-danger JSbtn-delete" data-link="{{ AdminOptions::base_url() }}admin/rata_delete/{{ $row->rata_id }}"><i class="fa fa-times" style="color:red;" aria-hidden="true"></i></button></td>
							</form>
						</tr>
						@endforeach					
					</tbody>
				</table>

			</div>
		</section>
	</div>


</div>