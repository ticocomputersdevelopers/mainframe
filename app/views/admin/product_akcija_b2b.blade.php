<div id="main-content">
	@include('admin/partials/product-tabs')

	<div class="m-tabs clearfix"> 
		@if(AdminOptions::checkB2C() && Admin_model::check_admin(array('ARTIKLI')) && Admin_model::check_admin(array('ARTIKLI_PREGLED')))
		<div class="m-tabs__tab{{ $strana=='product_akcija' ? ' m-tabs__tab--active' : '' }}"><a href="{{AdminOptions::base_url()}}admin/product_akcija/{{ $roba_id }}">{{ AdminLanguage::transAdmin('B2C AKCIJA') }} </a></div>
		@endif

		@if(AdminOptions::checkB2B() && Admin_model::check_admin(array('ARTIKLI')) && Admin_model::check_admin(array('ARTIKLI_PREGLED')))
		<div class="m-tabs__tab{{ $strana=='product_akcija_b2b' ? ' m-tabs__tab--active' : '' }}"><a href="{{AdminOptions::base_url()}}admin/product_akcija_b2b/{{ $roba_id }}">{{ AdminLanguage::transAdmin('B2B AKCIJA') }}</a></div>
		@endif 
	</div>

	<div class="columns medium-7 medium-centered">	
		<form method="POST" action="{{AdminOptions::base_url()}}admin/b2b_akcija_edit">
			<input type="hidden" name="roba_id" value="{{ $roba_id ? $roba_id : 0 }}">

			<div class="flat-box">
				<div class="row">
					<div class="column medium-3 field-group">
						<label>{{ AdminLanguage::transAdmin('B2B akcija aktivna') }}</label>
						<select name="b2b_akcija_flag_primeni" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'disabled' : '' }}>
							@if(Input::old('b2b_akcija_flag_primeni') ? Input::old('b2b_akcija_flag_primeni') : $b2b_akcija_flag_primeni)
							<option value="1" selected>{{ AdminLanguage::transAdmin('DA') }}</option>
							<option value="0" >{{ AdminLanguage::transAdmin('NE') }}</option>
							@else
							<option value="1" >{{ AdminLanguage::transAdmin('DA') }}</option>
							<option value="0" selected>{{ AdminLanguage::transAdmin('NE') }}</option>
							@endif
						</select>
					</div>

					<div class="column medium-3 field-group">
						<label>{{ AdminLanguage::transAdmin('Redni broj') }}</label>
						<input type="text" name="b2b_akcija_redni_broj" class="{{ $errors->first('b2b_akcija_redni_broj') ? 'error' : '' }}" value="{{ Input::old('b2b_akcija_redni_broj') ? Input::old('b2b_akcija_redni_broj') : $b2b_akcija_redni_broj }}" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'readonly' : '' }}>
					</div>

					<div class="column medium-3 field-group">
						<label>{{ AdminLanguage::transAdmin('Datum od') }}</label>
						<div class="relative"> 
							<input class="akcija-input" id="b2b_datum_akcije_od" name="b2b_datum_akcije_od" autocomplete="off" type="text" value="{{ Input::old('b2b_datum_akcije_od') ? Input::old('b2b_datum_akcije_od') : $b2b_datum_akcije_od }}" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'disabled' : '' }}>
							
							@if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
							<span id="datum_od_delete" class="absolute-right text-red"><i class="fa fa-times" aria-hidden="true"></i></span>
							@endif
						</div>
					</div>

					<div class="column medium-3 field-group">
						<label>{{ AdminLanguage::transAdmin('Datum do') }}</label>
						<div class="relative">
							<input class="akcija-input" id="b2b_datum_akcije_do" name="b2b_datum_akcije_do" autocomplete="off" type="text" value="{{ Input::old('b2b_datum_akcije_do') ? Input::old('b2b_datum_akcije_do') : $b2b_datum_akcije_do }}" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'readonly' : '' }}>
							
							@if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
							<span id="datum_do_delete" class="absolute-right text-red"><i class="fa fa-times" aria-hidden="true"></i></span>
							@endif
						</div>
					</div>
				</div>
			</div>

			<div class="flat-box"> 
				<div class="row">
					<div class="column medium-4 field-group">
						<label>{{ AdminLanguage::transAdmin('Cena') }}</label>
						<input class="<?php if($errors->first('racunska_cena_end')){ echo 'error'; }else{ if(Session::get('message')){ echo 'error'; } } ?>" type="text" name="racunska_cena_end" value="{{ Input::old('racunska_cena_end') ? Input::old('racunska_cena_end') : $racunska_cena_end }}" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'readonly' : '' }}>
					</div>

					<div class="column medium-4 field-group">
						<label>{{ AdminLanguage::transAdmin('Akcijski popust (%)') }}</label>
						<input class="<?php if($errors->first('b2b_akcija_popust')){ echo 'error'; }else{ if(Session::get('message')){ echo 'error'; } } ?>" type="text" name="b2b_akcija_popust" value="{{ Input::old('b2b_akcija_popust') ? Input::old('b2b_akcija_popust') : $b2b_akcija_popust }}" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'readonly' : '' }}>
					</div>

					<div class="column medium-4 field-group">
						<label>{{ AdminLanguage::transAdmin('Akcijska cena') }}</label>
						<input class="<?php if($errors->first('b2b_akcijska_cena')){ echo 'error'; }else{ if(Session::get('message')){ echo 'error'; } } ?>" type="text" name="b2b_akcijska_cena" value="{{ Input::old('b2b_akcijska_cena') ? Input::old('b2b_akcijska_cena') : $b2b_akcijska_cena }}" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'readonly' : '' }}>
					</div>
				</div> 

				@if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE'))) 
					<div class="btn-container center">
						<button type="submit" class="setting-button btn btn-primary save-it-btn">{{ AdminLanguage::transAdmin('Sačuvaj') }}</button>
					</div>			
				@endif
			</div> 
		</form>	
	</div>	
</div> 
