<!DOCTYPE html>
<html>
	<head>
		<title>{{ AdminOptions::company_name()}} Admin</title>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="msapplication-tap-highlight" content="no"/>
		<link rel="icon" type="image/png" href="{{ AdminOptions::base_url()}}favicon.ico">
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,400,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
		<link href="{{ AdminOptions::base_url()}}css/foundation.min.css" rel="stylesheet" type="text/css" />
		<link href="{{ AdminOptions::base_url()}}css/admin.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
		
		<script src="{{ AdminOptions::base_url()}}js/jquery-1.11.2.min.js" type="text/javascript"></script>
<!-- 		<script src="{{ AdminOptions::base_url()}}js/foundation.min.js" type="text/javascript" ></script>
<script src="{{ AdminOptions::base_url()}}js/admin_funkcije.js" type="text/javascript"></script>
<script src="{{ AdminOptions::base_url()}}js/admin.js" type="text/javascript"></script> -->
		
	</head>
	<body class="body-login">
		<div class="color-overlay-image"></div> 
		<div class="login-form-wrapper"> 
			<div class="login-form-wrapper__box"> 
				<div class="form-avatar">
					<a class="logo" href="{{ AdminOptions::base_url()}}" title="{{ AdminOptions::company_name()}}"><img src="{{ AdminOptions::base_url()}}images/admin/logo-selltico-white.png" alt="Selltico"></a>
				</div> 
				<form action="{{ AdminOptions::base_url()}}admin-login-store" method="post" class="" autocomplete="off"> 
					<div class="field-group">
						<input class="login-form__input input_placeholder" placeholder="{{ AdminLanguage::transAdmin('Korisnik') }}" name="username" type="text" value="{{ Input::old('username') ? Input::old('username') : '' }}" >
					</div> 
					<div class="field-group">
						<input class="login-form__input input_placeholder" id="login-pass1" placeholder="{{ AdminLanguage::transAdmin('Lozinka') }}" name="password" type="password" value="{{ Input::old('password') ? Input::old('password') : '' }}">
					</div>
					<button type="submit" class="login-form-button admin-login">{{ AdminLanguage::transAdmin('Prijavi se') }}</button>
				</form>
				<div class="field-group error-login">
				<?php if($errors->first('username')){ echo $errors->first('username'); }elseif($errors->first('password')){ echo $errors->first('password'); } ?>
				</div>
			</div>  
		</div>  
		<input type="hidden" id="base_url" value="{{AdminOptions::base_url()}}" />

	<script type="text/javascript">
		$('#login-pass2').hide();
		$("#show-pass").click(function(){
			$('#login-pass2').val($('#login-pass1').val()); 
			$('#login-pass2').toggle();
			$('#login-pass1').toggle();
		}); 
	</script> 
	</body>
</html>