<section id="main-content"> 

	@if(Session::has('message'))
	<script>
		alertify.success('{{ Session::get('message') }}');
	</script>
	@endif

	@include('admin/partials/tabs')
	
	<div class="row">
		<section class="medium-3 columns">
			<div class="flat-box">	
				<h3 class="title-med">{{ AdminLanguage::transAdmin('Način isporuke') }} <i class="fa fa-truck"></i></h3>
				<div class="row"> 
					<div class="columns medium-12 after-select-margin"> 
						<select name="" id="JSnacinIsporuke">
							<option data-id="0">{{ AdminLanguage::transAdmin('Dodaj novi') }}</option>
							@foreach($nacini_isporuke as $row)
							<option data-id="{{ $row->web_nacin_isporuke_id }}" @if($row->web_nacin_isporuke_id == $web_nacin_isporuke_id) selected @endif>{{ $row->naziv }}</option>
							@endforeach
						</select>
					</div>
				</div>
			</div>
		</section>

		<section class="medium-5 columns">
			<div class="flat-box">
				@if($web_nacin_isporuke_id > 0)		
				@foreach($nacin_isporuke as $row)
				<form action="{{ AdminOptions::base_url() }}admin/nacin_isporuke/{{$web_nacin_isporuke_id}}" method="POST">
					<h3 class="title-med">{{ AdminLanguage::transAdmin('Izmeni') }}</h3>

					<div class="row"> 
						<div class="columns medium-12 field-group {{ $errors->first('naziv') ? ' error' : '' }}">
							<input type="text" name="naziv" value="{{ $row->naziv }}" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'readonly' : '' }}>
						</div>

						<div class="columns"> 
							<label> 
								<input type="checkbox" name="aktivno" @if($row->selected == 1) : checked @endif {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'disabled' : '' }}> {{ AdminLanguage::transAdmin('Aktivno') }}
							</label>
						</div>
					</div>

					@if(Admin_model::check_admin(array('SIFARNICI_AZURIRANJE'))) 
					<div class="btn-container text-center"> 
						<button type="submit" class="btn btn-primary save-it-btn">{{ AdminLanguage::transAdmin('Sačuvaj') }}</button>
					</div> 
					@endif 
				</form>

				<form action="{{ AdminOptions::base_url() }}admin/nacin_isporuke/{{$web_nacin_isporuke_id}}/delete" method="POST">
					@if(Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')))
					<div class="text-center"> 
						<input type="submit" value="Obriši" class="btn btn-danger">
					</div> 
					@endif
				</form>
				@endforeach

				@elseif($web_nacin_isporuke_id == 0)

				<form action="{{ AdminOptions::base_url() }}admin/nacin_isporuke/{{$web_nacin_isporuke_id}}" method="POST">
					<h3 class="title-med">{{ AdminLanguage::transAdmin('Novo') }}</h3>
					
					<div class="row"> 
						<div class="columns field-group {{ $errors->first('naziv') ? ' error' : '' }}">
							<input type="text" name="naziv" value="" autofocus="autofocus" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'readonly' : '' }}>
						</div>

						<div class="columns"> 
							<label> 
								<input type="checkbox" name="aktivno" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'disabled' : '' }}> {{ AdminLanguage::transAdmin('Aktivno') }}
							</label>
						</div>
					</div>

					@if(Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')))

					<div class="btn-container text-center"> 
						<button type="submit" class="btn btn-primary save-it-btn">{{ AdminLanguage::transAdmin('Sačuvaj') }}</button>
					</div> 
					@endif
				</form>
				@endif
			</div>
		</section>
	</div> 
</section>