
<html>

	<head>

		<title>{{ AdminLanguage::transAdmin('Export Patuljak') }}</title>
		<link href="{{ AdminOptions::base_url()}}css/normalize.css" rel="stylesheet" type="text/css" />
		<link href="{{ AdminOptions::base_url()}}css/foundation.min.css" rel="stylesheet" type="text/css" />


		<style>

body {
	background-color: #f2f2f2;
}
* {
	box-sizing: border-box;
}
.row::after {
    content: "";
    clear: both;
    display: table;
}
[class*="col-"] {
    float: left;
}
.col-1 {width: 8.33%;}
.col-2 {width: 16.66%;}
.col-3 {width: 25%;}
.col-4 {width: 33.33%;}
.col-5 {width: 41.66%;}
.col-6 {width: 50%;}
.col-7 {width: 58.33%;}
.col-8 {width: 66.66%;}
.col-9 {width: 75%;}
.col-10 {width: 83.33%;}
.col-11 {width: 91.66%;}
.col-12 {width: 100%;}

.pad20 {
	padding: 20px;
}

#padding-right20 {
	padding-right: 20px;
}

.logo{
	display: block;
    padding: 10px;
    text-align: center;
    margin: auto;
}	
header{
	background-color: #4e0e7c;
}
.text-center {text-align: center;}
.button {
    display: inline-block;
    padding: 5px 10px;
    background-color: #f80;
    margin: 0;
    border-radius: 5px;
}
.button:hover{
	background-color: #f60;
	transition: .1s ease-in-out;
}
.input-div input{
	border-radius: 5px;
}
.select-div span ul li input{
    margin: 5px;
    height: auto;
    width: auto !important;
}
.select2-container--default .select2-selection--multiple .select2-selection__choice{
    margin: 4px 2px;
    font-size: 13px;
}
.select2-results__option{
	padding: 0 5px !important;
    font-size: 13px;
}
</style>


	</head><body>
		


		<header> 
			<div class="row"> 
				<h1><img class="logo" src="/files/exporti/patuljak/patuljak.webp" alt="Patuljak Logo"></h1>
			</div>
		</header>


			<div class="text-center">
				
				<div>
					<br>
				 	<h2 class="text-center">{{ AdminLanguage::transAdmin('Export - Patuljak') }}</h2>

					<div class="text-center input-div pad20">
						<a href="{{ AdminOptions::base_url()}}direct-export/patuljak/{{$kind}}/{{$key}}" target="_blank" class="button">{{ AdminLanguage::transAdmin('Vidi export') }}</a>
					</div>

					<form  method="POST" action="{{AdminOptions::base_url()}}export/patuljak-config/{{ $key }}">
						<input type="hidden" name="export_id" value="{{ $export_id }}">
						<input type="hidden" name="kind" value="{{ $kind }}">
						<input type="hidden" name="key" value="{{ $key }}">
						<div class="row">
							<div class="col-6 text-center select-div" id="padding-right20">	
								<span>{{ AdminLanguage::transAdmin('Grupe u prodavnici') }}</span>	
								<select name="grupa_pr_ids[]" multiple="multiple">
								{{ AdminSupport::unrelatedGroupsFromRobaPatuljak($export_id) }}
								</select> 
							</div>

							<div class="col-6 text-center select-div">
								<span>{{ AdminLanguage::transAdmin('Patuljak grupe') }}</span>		
								<select name="export_patuljak_id">
								{{ AdminSupport::patuljakGroups() }}
								</select> 
							</div>
						</div>
						<div class="text-center input-div">  
							<div class="col-12 text-center input-div pad20">
								<input class="button" type="submit" value="{{ AdminLanguage::transAdmin('Upari grupe') }}">
							</div>
						</div>
					</form>
				</div>

					<br>

					<div class="row" >
						<form method="POST" action={{AdminOptions::base_url()}}export/patuljak-config-delete/{{ $key }}>
							<input type="hidden" name="export_id" value="{{ $export_id }}">
							<input type="hidden" name="kind" value="{{ $kind }}">
							<input type="hidden" name="key" value="{{ $key }}">


							<div class="row">
								<div class="col-9 text-center select-div">
										<span>{{ AdminLanguage::transAdmin('Povezane grupe (Grupa u prodavici --- Patuljak grupa)') }}</span>
										<select name="grupa_pr_ids_inserted[]" multiple="multiple">
										{{ AdminSupport::relatedGroupsFromRobaPatuljak(null,$export_id) }}
									</select>
								</div>

								
								<div class="col-3 text-center input-div">
									<input class="button" type="submit" value="{{ AdminLanguage::transAdmin('Razveži grupe') }}">
							</div>
							</div>
						</form>
					</div>
			</div>


		









	</body></head>
</html>

