@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')
 
<div class="row"> 
	<div class="col-md-4 col-sm-12 col-xs-12">
	 	 
	 	<br>

	 	<h2><span class="page-title">{{ Language::trans('Kontakt informacije') }}</span></h2>
	 	
	 	<ul class="row">
	 	@if(Options::company_name() != '')
			<li class="col-md-4 col-sm-5 col-xs-5">{{ Language::trans('Firma') }}:</li>
			<li class="col-md-8  col-sm-7 col-xs-7"> {{ Options::company_name() }} &nbsp;</li>
		@endif
		@if(Options::company_adress() != '')
		    <li class="col-md-4 col-sm-5 col-xs-5">{{ Language::trans('Adresa') }}:</li>
		    <li class="col-md-8 col-sm-7 col-xs-7"> {{ Options::company_adress() }} &nbsp;</li>
		@endif
		@if(Options::company_city() != '')
			<li class="col-md-4 col-sm-5 col-xs-5">{{ Language::trans('Grad') }}:</li>
			<li class="col-md-8 col-sm-7 col-xs-7"> {{ Options::company_city() }} &nbsp;</li>
		@endif
		@if(Options::company_phone() != '')
			<li class="col-md-4 col-sm-5 col-xs-5">{{ Language::trans('Telefon') }}:</li>
			<li class="col-md-8 col-sm-7 col-xs-7"> {{ Options::company_phone() }} &nbsp;</li>
		@endif
		@if(Options::company_fax() != '')
			<li class="col-md-4 col-sm-5 col-xs-5">{{ Language::trans('Fax') }}:</li>
			<li class="col-md-8 col-sm-7 col-xs-7"> {{ Options::company_fax() }} &nbsp;</li>
		@endif
		@if(Options::company_pib() != '')
			<li class="col-md-4 col-sm-5 col-xs-5">{{ Language::trans('PIB') }}:</li>
			<li class="col-md-8 col-sm-7 col-xs-7"> {{ Options::company_pib() }} &nbsp;</li>
		@endif
		@if(Options::company_maticni() != '')
			<li class="col-md-4 col-sm-5 col-xs-5">{{ Language::trans('Matični broj') }}:</li>
			<li class="col-md-8 col-sm-7 col-xs-7"> {{ Options::company_maticni() }} &nbsp;</li>
		@endif
		@if( Options::company_email() != '')
			<li class="col-md-4 col-sm-5 col-xs-5">{{ Language::trans('E-mail') }}:</li>
			<li class="col-md-8 col-sm-7 col-xs-7">
				<a class="mailto" href="mailto:{{ Options::company_email() }}">{{ Options::company_email() }}</a>
			</li>
		@endif
		</ul>
	</div> 


	<div class="col-md-8 col-sm-12 col-xs-12 clearfix"> 
	
		<br>
		
		<h2><span class="page-title">{{ Language::trans('Pošaljite poruku') }}</span></h2>

		<form method="POST" action="{{ Options::base_url() }}contact-message-send">
			<div>
				<label id="label_name">{{ Language::trans('Vaše ime') }} *</label>
				<input class="contact-name" name="contact-name" id="JScontact_name" type="text" value="{{ Input::old('contact-name') }}">
				<div class="error red-dot-error">{{ $errors->first('contact-name') ? $errors->first('contact-name') : "" }}</div>
			</div> 

			<div>
				<label id="label_email">{{ Language::trans('Vaša e-mail adresa') }} *</label>
				<input class="contact-email" name="contact-email" id="JScontact_email" type="text" value="{{ Input::old('contact-email') }}" >
				<div class="error red-dot-error">{{ $errors->first('contact-email') ? $errors->first('contact-email') : "" }}</div>
			</div>		

			<div>	
				<label id="label_message">{{ Language::trans('Vaša poruka') }} </label>
				<textarea class="contact-message" name="contact-message" rows="5" id="message">{{ Input::old('contact-message') }}</textarea>
				<div class="error red-dot-error">{{ $errors->first('contact-message') ? $errors->first('contact-message') : "" }}</div>
			</div> 

			<div class="capcha text-center"> 
				{{ Captcha::img(5, 160, 50) }}<br>
				<span>{{ Language::trans('Unesite kod sa slike') }}</span>
				<input type="text" name="captcha-string" tabindex="10" autocomplete="off">
				<div class="error red-dot-error">{{ $errors->first('captcha') ? $errors->first('captcha') : "" }}</div>
			</div>

			<div class="text-right"> 
				<button type="submit" class="button">{{ Language::trans('Pošalji') }}</button>
			</div>
		</form>
	</div>
</div>

@if(Options::company_map() != '' && Options::company_map() != ';')
<div class="map-frame relative">
	
	<div class="map-info">
		<h5>{{ Options::company_name() }}</h5>
		<h6>{{ Options::company_adress() }}, {{ Options::company_city() }}</h6> 
	</div>

	<iframe src="https://maps.google.com/maps?q={{ All::lat_long()[0] }},{{ All::lat_long()[1] }}&output=embed" width="100%" height="100%" frameborder="0" aria-hidden="false" tabindex="0"></iframe> 

</div> 
@endif
 
@if(Session::get('message'))
<script>
	$(document).ready(function(){    
	    $('.JSinfo-popup').fadeIn().delay(1000).fadeOut();
	    $('.JSinfo-popup .JSpopup-inner').html("<p class='p-info'>{{ Session::get('message') }}</p>");
	});
</script>
@endif
@endsection     