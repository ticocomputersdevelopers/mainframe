<div class="col-md-3 col-sm-3 col-xs-12 text-center">
	
	<div class="header-cart-container inline-block relative">  
		
		<a class="header-cart inline-block text-center relative" href="{{ Options::base_url() }}{{ Url_mod::slug_trans('korpa') }}">
			
			<span class="relative inline-block"> 

				<i class="fas fa-shopping-basket"></i>		
				
				<span class="JScart_num badge"> {{ Cart::broj_cart() }} </span> 

			</span> 
		  
			<span class="JScart_total text-bold total">{{ Cart::cena(Cart::cart_ukupno()) }}</span>		

			<input type="hidden" id="h_br_c" value="{{ Cart::broj_cart() }}" />	
		</a>

		<div class="JSheader-cart-content text-left hidden-sm hidden-xs">
			@include('shop/themes/'.Support::theme_path().'partials/mini_cart_list') 
		</div>
	</div>
</div> 