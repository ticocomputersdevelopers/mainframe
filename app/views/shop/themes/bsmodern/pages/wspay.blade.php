@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')

	<div class="col-md-12 col-sm-12 col-xs-12 text-center">
		<br><br>
		<h3>{{ Language::trans('Nakon potvrde vrši se preusmeravanje na sajt WSPay-a, gde se na zaštićenoj stranici realizuje proces plaćanja') }}.</h3></br>

		<h4>{{ Language::trans('Podaci o korisniku') }}:</h4>
		<ul>
			@if($web_kupac->flag_vrsta_kupca == 1)
				<li>{{ Language::trans('Firma') }}: {{ $web_kupac->naziv }}</li>
			@else
				<li>{{ Language::trans('Ime') }}: {{ $web_kupac->ime.' '.$web_kupac->prezime }}</li>
			@endif
			<li>{{ Language::trans('Adresa') }}: {{ $web_kupac->adresa }}, {{ $web_kupac->mesto }}</li>
		</ul>
		</br>
		<h4>{{ Language::trans('Podaci o trgovcu') }}:</h4>
		<ul>
			<li>{{ $preduzece->naziv }}</li>
			<li>{{ Language::trans('Kontakt osoba') }}: {{ $preduzece->kontakt_osoba }}</li>
			@if($preduzece->matbr_registra != '')
			<li>{{ Language::trans('MBR') }}: {{ $preduzece->matbr_registra }}</li>
			@endif
			@if($preduzece->pib != '')
			<li>VAT Reg No: {{ $preduzece->pib }}</li>
			@endif
			<li>{{ $preduzece->adresa }}, {{ $preduzece->mesto }}</li>
			<li>{{ Language::trans('Telefon') }}: {{ $preduzece->telefon }}</li>
			<li><a href="mailto:{{ $preduzece->email }}">{{ $preduzece->email }}</a></li>
		</ul>
		</br>

		<input type="checkbox" id="JSConditions" onclick="clickCheckbox()"> <a href="{{ Options::base_url() }}uslovi-kupovine" target="_blank">
		{{ Language::trans('Uslovi korišćenja') }}</a>

		<!-- <form method="post" action="https://form.wspay.biz/authorization.aspx"> -->
		<form name="pay" method="post" action="https://formtest.wspay.biz/authorization.aspx">
 				<input type="hidden" name="ShopID" value="{{ $ShopID }}">
				<input type="hidden" name="ShoppingCartID" value="{{ $ShoppingCartID }}">
				<input type="hidden" name="Version" value="2.0">
				<input type="hidden" name="TotalAmount" value="{{ $TotalAmount }}">
				<input type="hidden" name="Signature" value="{{ $Signature }}">
				<input type='hidden' name='ReturnURL' value="{{ $ReturnURL }}">
				<input type="hidden" name="CancelURL" value="{{ $CancelURL }}">
				<input type="hidden" name="ReturnErrorURL" value="{{ $ReturnErrorURL }}">
				<input type="hidden" name="Lang" value="EN">
				<input type="hidden" name="CustomerFirstName" value="{{ $web_kupac->ime }}">
				<input type="hidden" name="CustomerLastName" value="{{ $web_kupac->prezime }}">
				<input type="hidden" name="CustomerEmail" value="{{ $web_kupac->email }}">
				<input type="hidden" name="CustomerAddress" value="{{ $web_kupac->adresa }}">
				<input type="hidden" name="CustomerCity" value="{{ $web_kupac->mesto }}">
				<input type="hidden" name="CustomerZIP" value="">
				<input type="hidden" name="CustomerCountry" value="">
				<input type="hidden" name="CustomerPhone" value="{{ $web_kupac->telefon }}">
				<input type="hidden" name="PaymentPlan" value="0000">
				<input type="hidden" name="CreditCardName" value="">
				<input type="hidden" name="IntAmount" value="">
				<input type="hidden" name="IntCurrency" value="EUR">
				<input type="submit" value="Završi plaćanje karticom" id="JSWsPaySubmit" disabled>
			
		</form>
		<script type="text/javascript">
			function clickCheckbox(){
				if(document.getElementById("JSConditions").checked){
					document.getElementById("JSWsPaySubmit").removeAttribute('disabled');
				}else{
					document.getElementById("JSWsPaySubmit").setAttribute('disabled','disabled');
				}
			}
		</script>
	</div>
@endsection