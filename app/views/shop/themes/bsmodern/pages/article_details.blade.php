@extends('shop/themes/'.Support::theme_path().'templates/article')

@section('article_details')

<!-- ARTICLE_DETAILS.blade -->

<div id="fb-root"></div> 
<script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.12';
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

@if(Session::has('success_add_to_cart'))
    $(document).ready(function(){     
     
        bootboxDialog({ message: "<p>" + trans('Artikal je dodat u korpu') + ".</p>" }, 2200); 

    });
@endif

@if(Session::has('success_comment_message'))
    $(document).ready(function(){     
     
        bootboxDialog({ message: "<p>" + trans('Vaš komentar je poslat') + ".</p>" }, 2200); 

    });
@endif
</script> 


<main class="d-content JSmain relative"> 

    <div class="container">
     
        <ul class="breadcrumb hidden"> 
            {{ Product::product_bredacrumps(DB::table('roba')->where('roba_id',$roba_id)->pluck('grupa_pr_id')) }}
        </ul>
     
        <div class="row"> 
            
            <div class="JSproduct-preview-image col-md-4 col-sm-12 col-xs-12">
                <div class="row"> 
                    <div class="col-md-12 col-sm-12 col-xs-12 disableZoomer relative product-main-image-wrap no-padding"> 

                        <a class="fancybox" href="{{ Options::domain() }}{{ $slika_big }}">

                            @if(!empty(Product::design_id($roba_id)) AND Options::pitchprint_aktiv() == 1)

                                <span id="zoom_03" class="JSzoom_03" style="display: block;" data-zoom-image="https://s3-eu-west-1.amazonaws.com/pitchprint.io/previews/{{Product::design_id($roba_id)}}_1.jpg?k=0.4491390433183766">
                                    
                                    <img src="https://s3-eu-west-1.amazonaws.com/pitchprint.io/previews/{{Product::design_id($roba_id)}}_1.jpg?k=0.4491390433183766" id="art-img" class="JSzoom_03 img-responsive" alt="{{ Product::seo_title($roba_id)}}" />
                                
                                </span>

                            @else
                                <span id="zoom_03" class="JSzoom_03" style="display: block;" data-zoom-image="{{ Options::domain() }}{{ $slika_big }}">
                                   
                                    <img class="JSzoom_03 img-responsive" id="art-img" src="{{ Options::domain() }}{{ $slika_big }}" alt="{{ Product::seo_title($roba_id)}}" />

                                </span>
                            @endif
                        </a>

                        <div class="product-sticker flex">
                            @if( B2bArticle::stiker_levo($roba_id) != null )
                                <a class="article-sticker-img">
                                    <img class="img-responsive" src="{{ Options::domain() }}{{B2bArticle::stiker_levo($roba_id) }}" alt="sticker-label"  />
                                </a>
                            @endif 
                            
                            @if( B2bArticle::stiker_desno($roba_id) != null )
                                <a class="article-sticker-img clearfix">
                                    <img class="img-responsive pull-right" src="{{ Options::domain() }}{{B2bArticle::stiker_desno($roba_id) }}" alt="sticker-label"  />
                                </a>
                            @endif   
                        </div>
                    </div> 

                    <div id="gallery_01" class="col-md-12 col-sm-12 col-xs-12 text-center sm-no-padd">

                        @if(!empty(Product::design_id($roba_id)) AND Options::pitchprint_aktiv() == 1)                             
                            @if(Product::pitchPrintImageExist($roba_id) == TRUE)                          
                                <a class="elevatezoom-gallery JSp_print_small_img" 
                                href="javascript:void(0)" 
                                data-image="https://s3-eu-west-1.amazonaws.com/pitchprint.io/previews/{{Product::design_id($roba_id)}}_2.jpg?k=0.4491390433183766" 
                                data-zoom-image="https://s3-eu-west-1.amazonaws.com/pitchprint.io/previews/{{Product::design_id($roba_id)}}_2.jpg?k=0.4491390433183766">
                               
                                <img src="https://s3-eu-west-1.amazonaws.com/pitchprint.io/previews/{{Product::design_id($roba_id)}}_2.jpg?k=0.4491390433183766" 
                                alt="https://s3-eu-west-1.amazonaws.com/pitchprint.io/previews/{{Product::design_id($roba_id)}}_2.jpg?k=0.4491390433183766"/> 

                                </a>
                            @endif   
                                <a class="elevatezoom-gallery JSp_print_small_img" 
                                href="javascript:void(0)" 
                                data-image="https://s3-eu-west-1.amazonaws.com/pitchprint.io/previews/{{Product::design_id($roba_id)}}_1.jpg?k=0.4491390433183766" 
                                data-zoom-image="https://s3-eu-west-1.amazonaws.com/pitchprint.io/previews/{{Product::design_id($roba_id)}}_1.jpg?k=0.4491390433183766">
                               
                                <img src="https://s3-eu-west-1.amazonaws.com/pitchprint.io/previews/{{Product::design_id($roba_id)}}_1.jpg?k=0.4491390433183766" 
                                alt="https://s3-eu-west-1.amazonaws.com/pitchprint.io/previews/{{Product::design_id($roba_id)}}_1.jpg?k=0.4491390433183766"/> 

                            </a>   

                        @else
                            <div class="JSgallery_slick row">
                                @foreach($slike as $image)
                                <div class="col-md-3 col-sm-6 col-xs-4 gallery-slick-card">
                                    <a class="elevatezoom-gallery" href="javascript:void(0)" data-image="/{{$image->putanja}}" data-zoom-image="{{ Options::domain().$image->putanja }}">

                                        <img alt="{{ Options::domain().$image->putanja }}" id="{{ Options::domain().$image->web_slika_id }}" src="{{ Options::domain().$image->putanja }}"/>

                                    </a>
                                </div>
                                @endforeach
                            </div>
                        @endif
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12 additional_img">    
                        @foreach($glavne_slike as $slika)
                        <a class="inline-block text-center" href="{{Options::base_url()}}{{Url_mod::slug_trans('proizvod')}}/{{Url_mod::slugify(Product::seo_title($roba_id))}}/{{$slika->web_slika_id}}"> 
                            <img src="{{ Options::domain().$slika->putanja }}" alt=" {{ Options::domain().$slika->putanja }}" class="img-responsive inline-block"> 
                        </a>
                        @endforeach
                    </div>
                </div>
               
            </div>

            <div class="product-preview-info col-md-5 col-sm-12 col-xs-12">

                <h1 class="article-heading">{{ Product::seo_title($roba_id) }}</h1>

                <div class="review flex">{{ Product::getRating($roba_id) }}<span class="JSocenaCount hidden">1</span><span class="JSocenaLabel hidden">Ocena</span> <span>|</span> <a href="#product_preview_tabs">{{Language::trans('Dodaj svoj komentar')}}</a></div>

                @if($proizvodjac_id != -1)
                    @if( Product::slikabrenda($roba_id) != null )
                   
                    <a class="article-brand-img inline-block hidden" href="{{Options::base_url()}}{{Url_mod::slug_trans('proizvodjac')}}/{{Url_mod::slug_trans(Product::get_proizvodjac($roba_id)) }}">
                        <img src="{{ Options::domain() }}{{product::slikabrenda($roba_id) }}" alt="{{ product::get_proizvodjac($roba_id) }}" />
                    </a>

                    @else

                    <a href="{{Options::base_url()}}{{Url_mod::slug_trans('proizvodjac')}}/{{Url_mod::slug_trans(Product::get_proizvodjac($roba_id)) }}" class="artical-brand-text inline-block">{{ product::get_proizvodjac($roba_id) }}</a>

                    @endif                                   
                @endif     

                <!-- ARTICLE PASSWORD -->
                @if(AdminOptions::sifra_view_web()==1)
                <div>{{Language::trans('Šifra') }}: {{Product::sifra($roba_id)}}</div>
                @elseif(AdminOptions::sifra_view_web()==4)                       
                <div>{{Language::trans('Šifra') }}: {{Product::sifra_d($roba_id)}}</div>
                @elseif(AdminOptions::sifra_view_web()==3)                       
                <div>{{Language::trans('Šifra') }}: {{Product::sku($roba_id)}}</div>
                @elseif(AdminOptions::sifra_view_web()==2)                       
                <div>{{Language::trans('Šifra') }}: {{Product::sifra_is($roba_id)}}</div>
                @endif


                <ul>
                    @if($grupa_pr_id != -1)
                    <li>{{Language::trans('Proizvod iz grupe')}}:{{ Product::get_grupa($roba_id) }}</li>
                    @endif

                    @if($proizvodjac_id != -1) 
                    <li>{{Language::trans('Proizvođač')}}: 

                        @if(Support::checkBrand($roba_id))
                        <a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('proizvodjac')}}/{{ Url_mod::slug_trans(Product::get_proizvodjac($roba_id)) }}">{{Product::get_proizvodjac($roba_id)}}</a>
                        @else
                        <span>{{Product::get_proizvodjac($roba_id)}}</span>
                        @endif
                    </li>
                    @endif 

                    @if(Options::vodjenje_lagera() == 1)
                      <li>{{Language::trans('Dostupna količina')}}: {{Cart::check_avaliable($roba_id)}}</li>  
                    @endif  

                    @if(Options::checkTezina() == 1 AND Product::tezina_proizvoda($roba_id)>0)
                    <li>{{Language::trans('Težina artikla')}}: {{Product::tezina_proizvoda($roba_id)/1000}} kg</li>
                    @endif 
                </ul>

                <!-- PRICE -->
                <div class="product-preview-price">
                    @if(Product::getStatusArticlePrice($roba_id) == 1)

    <!--                         @if(Product::pakovanje($roba_id)) 
                        <div> 
                            <span class="price-label">{{ Language::trans('Pakovanje') }}:</span>
                            <span class="price-num">{{ Product::ambalaza($roba_id) }}</span> 
                        </div>
                        @endif  -->                                

                        @if(All::provera_akcija($roba_id))  
                        
                        <div>
                            <span class="text-uppercase text-bold">Akcija</span>
                        </div>

                        @if(Product::get_mpcena($roba_id) != 0) 
                        <div class="">
                            <span class="price-label ">{{ Language::trans('Maloprodajna cijena') }}:</span>
                            <span class="price-num mp-price product-old-price">{{ Cart::cena(Product::get_mpcena($roba_id)) }}</span> 
                        </div>
                        <br>
                        @endif  

                        <div class=""> 
                            <span class="price-label ">{{ Language::trans('Akcijska cijena')}}:</span> 
                            <span class="JSaction_price price-num main-price" data-action_price="{{Product::get_price($roba_id)}}">
                                {{ Cart::cena(Product::get_price($roba_id,true,false,(!is_null(Input::old('kamata')) ? Input::old('kamata') : 0 ))) }}
                            </span>
                        </div>

                        @if(Product::getPopust_akc($roba_id)>0)
                        <div class=""> 
                            <span class="price-label ">{{ Language::trans('Popust') }}: </span>
                            <span class="price-num discount">{{ Cart::cena_popust(Product::getPopustAkc($roba_id)) }}</span>
                        </div>
                        @endif

                        @else

                        @if(Product::get_mpcena($roba_id) != 0) 
                        <div class=" "> 
                            <span class="price-label ">{{ Language::trans('Maloprodajna cijena') }}:</span>
                            <span class="price-num mp-price">{{ Cart::cena(Product::get_mpcena($roba_id)) }}</span> 
                        </div>
                        @endif

                        <div class=""> 
                            <span class="price-label ">{{ Language::trans('Cijena sa uračunatim popustom za gotovinsko plaćanje') }}:</span>
                            <span class="JSweb_price price-num main-price" data-cena="{{Product::get_price($roba_id)}}">
                               {{ Cart::cena(Product::get_price($roba_id,true,false,(!is_null(Input::old('kamata')) ? Input::old('kamata') : 0 ))) }}
                           </span>
                       </div>

                       @if(Product::getPopust($roba_id)>0)
                           @if(AdminOptions::web_options(132)==1)
                           <div class="">  
                            <span class="price-label ">{{ Language::trans('Popust') }}:</span>
                            <span class="price-num discount">{{ Cart::cena_popust(Product::getPopust($roba_id)) }}</span>
                            </div>
                            @endif
                        @endif

                    @endif
                @endif 
            </div>



            <div class="add-to-cart-area clearfix">    

             @if(Product::getStatusArticle($roba_id) == 1)
                 @if(Cart::check_avaliable($roba_id) > 0)
                 <form method="POST" action="{{ Options::base_url() }}product-cart-add" id="JSAddCartForm" class="flex"> 
                  
                    <!-- PITCHPRINT -->
                    @if(!empty(Product::design_id($roba_id) AND !empty(Options::pitchprint() AND Options::pitchprint_aktiv() == 1)))
                        @include('shop/themes/'.Support::theme_path().'partials/pitchprint')
                    @endif
                    <!-- PITCHPRINT END -->

                @if(Product::check_osobine($roba_id))  
                    @foreach(Product::osobine_nazivi($roba_id) as $osobina_naziv_id)

                    <div class="attributes text-bold">

                        <div>{{ Product::find_osobina_naziv($osobina_naziv_id,'naziv') }}</div>

                        @foreach(Product::osobine_vrednosti($roba_id,$osobina_naziv_id) as $osobina_vrednost_id)
                        <label class="relative no-margin" style="background-color: {{ Product::find_osobina_vrednost($osobina_vrednost_id, 'boja_css') }}">

                            <input type="radio" name="osobine{{ $osobina_naziv_id }}" value="{{ $osobina_vrednost_id }}" {{ Product::check_osobina_vrednost($roba_id,$osobina_naziv_id,$osobina_vrednost_id,Input::old('osobine'.$osobina_naziv_id)) }}>

                            <span class="inline-block">{{ Product::find_osobina_vrednost($osobina_vrednost_id, 'vrednost') }}</span>
                            
                        </label>
                        @endforeach

                    </div>

                    @endforeach 
                @endif

                @if(AdminOptions::web_options(313)==1) 
                <div class="num-rates"> 
                    <div> 
                        <div class="inline-block lorem-1">{{ Language::trans('Broj rata') }}</div>
                    </div>
                    <select class="JSinterest" name="kamata">
                        {{ Product::broj_rata(Input::old('kamata')) }}
                    </select>
                </div>
                @endif

                <input type="hidden" name="roba_id" value="{{ $roba_id }}">
                <span class="hidden">&nbsp;{{Language::trans('Količina')}}&nbsp;</span>
            @if(Options::web_options(320) == 1 AND (Product::jedinica_mere($roba_id)->jedinica_mere_id) == 3 AND Product::pakovanje($roba_id) ) 
                <input type="number" name="kolicina" class="cart-amount weight" min="0" step="0.1" value="{{ Input::old('kolicina') ? Input::old('kolicina') : '1' }}"><span>&nbsp;{{Language::trans(' kg')}}&nbsp;</span>
            @else
                <button onClick="var result = document.getElementById('productAmount'); var productAmount = result.value; if( !isNaN( productAmount ) &amp;&amp; productAmount &gt; 0 ) result.value--;return false;" class="product-count" type="button"><i class="fa fa-minus"></i></button>
               
                <input id="productAmount" type="text" name="kolicina" class="cart-amount" value="{{ Input::old('kolicina') ? Input::old('kolicina') : '1' }}">

                <button onClick="var result = document.getElementById('productAmount'); var productAmount = result.value; if( !isNaN( productAmount )) result.value++;return false;" class="product-count" type="button"><i class="fa fa-plus"></i></button>
            @endif

                <button type="submit" id="JSAddCartSubmit" class="add-to-cart button relative"><span class="sprite sprite-cart"></span>{{Language::trans('Dodaj u Korpu')}}</button>
                <input type="hidden" name="projectId" value=""> 

                <div class="red-dot-error">{{ $errors->first('kolicina') ? $errors->first('kolicina') : '' }}</div>  

                <div class="printer inline-block" title="{{ Language::trans('Štampaj') }}">  
                    <a href="{{Options::base_url()}}stampanje/{{ $roba_id }}" target="_blank" rel="nofollow"><i class="fas fa-print"></i></a>
                </div>

                @if(Cart::kupac_id() > 0)
                <button class="like-it JSadd-to-wish" type="button" data-roba_id="{{$roba_id}}" title="{{ Language::trans('Dodaj na listu želja') }}"><i class="far fa-heart"></i></button>
                @else
                <button class="like-it JSnot_logged" type="button" data-roba_id="{{$roba_id}}" title="{{ Language::trans('Dodavanje artikla na listu želja moguće je samo registrovanim korisnicima') }}"><i class="far fa-heart"></i></button> 
                @endif


                </form>

                @else

                <button class="not-available button">{{Language::trans('Nije dostupno')}}</button>       
                @endif

                @else
                <button class="button" data-roba-id="{{$roba_id}}">
                    {{ Product::find_flag_cene(Product::getStatusArticle($roba_id),'naziv') }}
                </button>
                @endif
            </div>


        @if(!empty(Product::get_labela($roba_id)))
        <div class="custom-label inline-block relative">
            <i class="fa fa-info-circle"></i>
            {{Product::get_labela($roba_id)}} 
        </div>
        @endif

        <div class="genererated-chars relative">
            {{ Product::get_generisane_samo($roba_id) }}
        <div class="unleash-chars"><i class="fas fa-angle-double-down"></i></div>
        </div>

        <div class="mf-social-wrap clearfix">
            <div class="mf-social-icons">
                <ul class="link">
                    <li class="fb"><a class="fab fa-facebook-f" href="https://www.facebook.com/sharer.php?u={{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($roba_id))}}" target="_blank" title="Facebook share" onclick="javascript:window.open(this.href,
                    '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"></a></li>
                    <li class="tw"><a class="fab fa-twitter" href="https://twitter.com/share?url={{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($roba_id))}}" onclick="javascript:window.open(this.href,
                    '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"></a></li>
                    <li class="googleplus hidden"><a class="fab fa-google-plus-g" href="#"></a></li>
                    <li class="rss"><a class="fas fa-mail-bulk" href="mailto:?subject={{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($roba_id))}}&body={{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($roba_id))}}"></a></li>
                    <li class="pintrest"><a class="fab fa-pinterest-p" href="https://www.pinterest.com/share?url={{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($roba_id))}}" onclick="javascript:window.open(this.href,
                    '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"></a></li>
                    <li class="linkedin"><a class="fab fa-linkedin-in" href="https://www.linkedin.com/shareArticle?mini=true&url={{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($roba_id))}}" onclick="javascript:window.open(this.href,
                    '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"></a></li>
                    <li class="youtube hidden"><a class="fas fa-play" href="#"></a></li>
                </ul>
            </div>
        </div>

        <div class="how_to_buy">
            <div> <span> {{Language::trans('Kako kupiti?')}} </span> <span> {{Language::trans('Pogledajte video uputstvo.')}} </span> </div>

            <div class="JSopen_video inline-block"> <img class="img-responsive" src="../images/mainframe_img_for_video.jpg" alt="mainframe_img_for_video"> </div>
            <!-- HOW TO BUY VIDEO -->
            <div class="JSvideo_how_to_buy hidden"> 
                <div class="text-right"> <span class="fa fa-times"></span> </div>
                <div class="iframe-wrap">
                    <iframe src="https://www.youtube.com/embed/NlYd0TT5mXA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>  
            </div>
        </div>

        <div class="facebook-btn-share flex hidden">

            <div class="soc-network inline-block"> 
                <div class="fb-like" data-href="{{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($roba_id))}}" data-layout="button_count" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
            </div>

            <div class="soc-network"> 
                <a href="https://twitter.com/share" class="twitter-share-button">Tweet</a> <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
            </div>  
        </div>


        <!-- ADMIN BUTTON-->
        @if(Session::has('b2c_admin'.Options::server()) AND Admin_model::check_admin(array('ARTIKLI')) AND Admin_model::check_admin(array('ARTIKLI_PREGLED')))
        <div class="admin-article"> 
            @if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
            <a class="JSFAProductModalCall article-level-edit-btn" data-roba_id="{{$roba_id}}" href="javascript:void(0)" rel="nofollow">{{ Language::trans('IZMENI ARTIKAL') }}</a> 
            @endif
            <span class="supplier"> {{ Product::get_dobavljac($roba_id) }}</span> 
            <span class="supplier">{{ Language::trans('NCP') }}: {{ Product::get_ncena($roba_id) }}</span>
        </div>
        @endif
    </div>
    </div>

    @if(count($srodni_artikli) > 0)
        <div class="related-custom JSproducts_slick row"> 
        @foreach($srodni_artikli as $srodni_artikl)
        <div class="JSproduct col-md-4 col-sm-4 col-xs-12">  
            <div class="card flex row">  
                <div class="col-xs-3 no-padding">
                    <div class="img-wrap"> 
                        <a href="{{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($srodni_artikl->srodni_roba_id))}}">
                            <img class="img-responsive" src="{{ Options::domain() }}{{ Product::web_slika_big($srodni_artikl->srodni_roba_id) }}" alt="{{ Product::seo_title($srodni_artikl->srodni_roba_id)}}" />
                        </a>
                    </div>
                </div>

                <div class="col-xs-9">
                    <a href="{{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($srodni_artikl->srodni_roba_id))}}">
                        <h2 class="title">{{ Product::seo_title($srodni_artikl->srodni_roba_id) }}</h2>
                    </a>

                    <div>{{ Product::get_karakteristika_srodni($srodni_artikl->grupa_pr_vrednost_id) }}</div>

                    <div class="price"> 
                        <span>{{ Cart::cena(Product::get_price($srodni_artikl->srodni_roba_id)) }}</span>
                    </div>  
                </div>   
            </div>
        </div>
        @endforeach 
        </div>
    @endif

    @if(Options::checkTags() == 1)
        @if(Product::tags($roba_id) != '')
        <div class="product-tags">  
            <div>{{ Language::trans('Tagovi') }}:</div>

            <h6 class="text-white inline-block">
                {{ Product::tags($roba_id) }} 
            </h6>
        </div>       
        @endif
    @endif    

    <!-- PRODUCT PREVIEW TABS-->
    <div id="product_preview_tabs" class="product-preview-tabs row">
    <div class="col-xs-12"> 
        <ul class="nav nav-tabs tab-titles">
            <li class="{{ !Session::has('contactError') ? 'active' : '' }}"><a data-toggle="tab" href="#description-tab" rel="nofollow">{{Language::trans('Specifikacija')}}</a></li>
            <li><a data-toggle="tab" href="#service_desc-tab" rel="nofollow">{{Language::trans('Opis')}}</a></li>
            <li><a data-toggle="tab" href="#technical-docs" rel="nofollow">{{Language::trans('Sadržaji')}}</a></li>
            <li class="{{ Session::has('contactError') ? 'active' : '' }}"><a data-toggle="tab" href="#the-comments" rel="nofollow">{{Language::trans('Komentari')}}</a></li>
        </ul>

        <div class="tab-content"> 

            <div id="description-tab" class="tab-pane fade{{ !Session::has('contactError') ? ' in active' : '' }}">
                  {{Product::get_karakteristike($roba_id)}}
            </div>


            <div id="service_desc-tab" class="tab-pane fade">
                {{ Product::get_opis($roba_id) }} 
                
                @if(Product::get_proizvodjac_name($roba_id)=='Bosch')
                <script type="text/javascript"></script>

                <div class="loadbeeTabContent" data-loadbee-apikey="{{Options::loadbee()}}" data-loadbee-gtin="{{Product::get_barkod($roba_id)}}" data-loadbee-locale="sr_RS"></div>

                <script src="https://cdn.loadbee.com/js/loadbee_integration.js" async=""></script>
                @endif
                <div id="flix-minisite"></div>
                <div id="flix-inpage"></div>
                <!-- fixmedia -->
                <script type="text/javascript" src="//media.flixfacts.com/js/loader.js" data-flix-distributor="{{Options::flixmedia()}}" data-flix-language="rs" data-flix-brand="{{Product::get_proizvodjac_name($roba_id)}}" data-flix-mpn="" data-flix-ean="{{Product::get_barkod($roba_id)}}" data-flix-sku="" data-flix-button="flix-minisite" data-flix-inpage="flix-inpage" data-flix-button-image="" data-flix-price="" data-flix-fallback-language="en" async>                                    
                </script>
            </div>


            <div id="technical-docs" class="tab-pane fade">
                @if(Options::web_options(120))
                    @if(count($fajlovi) > 0) 

                        @foreach($fajlovi as $row)
                        <div class="files-list-item">
                            <a class="files-link" href="{{ $row->putanja != null ? Options::domain().$row->putanja : $row->putanja_etaz }}" target="_blank">
                                <img src="{{ Options::domain() }}images/file-icon.png" alt="{{ $row->naziv }}">
                                <div class="files-list-item">
                                    <div class="files-name">{{ Language::trans($row->naziv) }}</div> <!-- {{ Product::getExtension($row->vrsta_fajla_id) }} --> 
                                </div>
                            </a>
                        </div>
                        @endforeach 
                    @endif
                @endif
            </div>


            <div id="the-comments" class="tab-pane fade{{ Session::has('contactError') ? ' in active' : '' }}">
                <div class="row"> 
                    <?php $query_komentary=DB::table('web_b2c_komentari')->where(array('roba_id'=>$roba_id,'komentar_odobren'=>1));
                    if($query_komentary->count() > 0){?>
                        <div class="col-md-6 col-sm-12 col-xs-12"> 
                            <ul class="comments">
                                <?php foreach($query_komentary->orderBy('web_b2c_komentar_id', 'DESC')->get() as $row)
                                { ?>
                                    <li class="comment">
                                        <ul class="comment-content relative">
                                            <li class="comment-name">{{$row->ime_osobe}}</li>
                                            <li class="comment-date">{{$row->datum}}</li>
                                            <li class="comment-rating">{{Product::getRatingStars($row->ocena)}}</li>
                                            <li class="comment-text">{{ $row->pitanje }}</li>
                                        </ul>
                                        <!-- REPLIES -->
                                        @if($row->odgovoreno == 1)
                                        <ul class="replies">
                                            <li class="comment">
                                                <ul class="comment-content relative">
                                                    <li class="comment-name">{{ Options::company_name() }}</li>
                                                    <li class="comment-text">{{ $row->odgovor }}</li>
                                                </ul>
                                            </li>
                                        </ul>
                                        @endif
                                    </li>
                                <?php }?>
                            </ul>
                        </div>
                    <?php } ?>
                    <div class="col-md-6 col-sm-12 col-xs-12 pull-right">  
                        <form method="POST" action="{{ Options::base_url() }}comment-add">
                    
                            <label for="JScomment_message">{{Language::trans('Komentar')}}</label>
                            <textarea class="comment-message" name="comment-message" rows="5"  {{ $errors->first('comment-message') ? 'style="border: 1px solid red;"' : '' }}>{{ Input::old('comment-message') }}</textarea>
                            <input type="hidden" value="{{ $roba_id }}" name="comment-roba_id" />
                            <!-- JSrev-star -->
                            <span class="review"> 
                                <span>{{Language::trans('Ocena')}}:</span>
                                <a id="JSstar1" class="JSrev-star-group star-review-empty">
                                <input class="JSstars-checkbox" type="checkbox" value='1' name="ocena"/>
                                    <i class="fas fa-star review-star" aria-hidden="true"></i>
                                </a>
                                <a id="JSstar2" class="JSrev-star-group star-review-empty">
                                <input class="JSstars-checkbox" type="checkbox" value='2' name="ocena"/>
                                    <i class="fas fa-star review-star" aria-hidden="true"></i>
                                    <i class="fas fa-star review-star" aria-hidden="true"></i>
                                </a>
                                <a id="JSstar3" class="JSrev-star-group star-review-empty">
                                <input class="JSstars-checkbox" type="checkbox" value='3' name="ocena"/>
                                    <i class="fas fa-star review-star" aria-hidden="true"></i>
                                    <i class="fas fa-star review-star" aria-hidden="true"></i>
                                    <i class="fas fa-star review-star" aria-hidden="true"></i>
                                </a>
                                <a id="JSstar4" class="JSrev-star-group star-review-empty">
                                <input class="JSstars-checkbox" type="checkbox" value='4' name="ocena"/>
                                    <i class="fas fa-star review-star" aria-hidden="true"></i>
                                    <i class="fas fa-star review-star" aria-hidden="true"></i>
                                    <i class="fas fa-star review-star" aria-hidden="true"></i>
                                    <i class="fas fa-star review-star" aria-hidden="true"></i>
                                </a>
                                <a id="JSstar5" class="JSrev-star-group star-review-empty">
                                <input class="JSstars-checkbox" type="checkbox" value='5' name="ocena"/>
                                    <i class="fas fa-star review-star" aria-hidden="true"></i>
                                    <i class="fas fa-star review-star" aria-hidden="true"></i>
                                    <i class="fas fa-star review-star" aria-hidden="true"></i>
                                    <i class="fas fa-star review-star" aria-hidden="true"></i>
                                    <i class="fas fa-star review-star" aria-hidden="true"></i>
                                </a>
                           
                            </span>
                            <div class="capcha text-center"> 
                                {{ Captcha::img(5, 160, 50) }}<br>
                                <span>{{ Language::trans('Unesite kod sa slike') }}</span>
                                <input type="text" name="captcha-string" tabindex="10" autocomplete="off" {{ $errors->first('captcha') ? 'style="border: 1px solid red;"' : '' }}>
                            </div>
                            <button class="pull-right button">{{Language::trans('Pošalji')}}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- BRENDOVI SLAJDER -->
                <!--  <div class="row">
                     <div class="col-md-12">
                        <div class="dragg JSBrandSlider"> 
                               <?php //foreach(DB::table('proizvodjac')->where('brend_prikazi',1)->get() as $row){ ?>
                            <div class="col-md-12 col-sm-6 end sub_cats_item_brend">
                                <a class="brand-link" href="{{Options::base_url()}}{{ Url_mod::slug_trans('proizvodjac') }}/<?php //echo $row->naziv; ?> ">
                                     <img src="{{ Options::domain() }}<?php //echo $row->slika; ?>" />
                                 </a>
                            </div>
                            <?php //} ?>
                        </div>
                     </div>
                 </div> -->

                 @if(Options::web_options(118))
                 <br>
                     @if(count($vezani_artikli))
                     <h2><span class="section-title">{{Language::trans('Vezani artikli')}}</span></h2>
                     
                     <div class="JSproducts_slick row">

                        @foreach($vezani_artikli as $vezani_artikl)
                        @if(Product::checkView($vezani_artikl->roba_id))
                        <div class="JSproduct col-md-3 col-sm-4 col-xs-12 no-padding">
                            <div class="shop-product-card relative"> 
                                <!-- PRODUCT IMAGE -->
                                <div class="product-image-wrapper flex relative">

                                    <a class="margin-auto" href="{{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($vezani_artikl->vezani_roba_id))}}">
                                        <img class="product-image img-responsive JSlazy_load" src="{{Options::domain()}}images/quick_view_loader.gif" data-src="{{ Options::domain() }}{{ Product::web_slika($vezani_artikl->vezani_roba_id) }}" alt="{{ Product::seo_title($vezani_artikl->vezani_roba_id) }}" />
                                    </a>

                                    <!-- <a href="{{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($vezani_artikl->vezani_roba_id))}}" class="article-details hidden-xs">
                                        <i class="fas fa-search-plus"></i> {{Language::trans('Detaljnije')}}</a> -->
                                    </div>

                                    <div class="product-meta text-center"> 
                                        <h2 class="product-category">
                                            <a href="{{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($vezani_artikl->roba_id))}}">
                                                {{ Product::get_grupa_title($vezani_artikl->roba_id) }}  
                                            </a>
                                        </h2>

                                        <h2 class="product-name">
                                            <a href="{{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($vezani_artikl->roba_id))}}">
                                                {{ Product::short_title($vezani_artikl->roba_id) }}  
                                            </a>
                                        </h2>

                                        <div> 
                                            <span class="review">
                                                {{ Product::getRating($vezani_artikl->roba_id) }}
                                            </span>
                                        </div>

                                        @if(Product::getStatusArticlePrice($vezani_artikl->roba_id))
                                        <div class="price-holder flex">
                                            @if(All::provera_akcija($vezani_artikl->roba_id))
                                            <span class="product-old-price">{{ Cart::cena(Product::old_price($vezani_artikl->roba_id)) }}</span>
                                            @endif     
                                            <div class="@if(All::provera_akcija($vezani_artikl->roba_id)) action-price @endif"> {{ Cart::cena(Product::get_price($vezani_artikl->roba_id)) }} </div>
                                        </div>  
                                        @endif 


                                        @if(AdminOptions::web_options(153)==1)
                                        <div class="generic_car hidden">
                                            {{ Product::get_web_roba_karakteristike_short($vezani_artikl->roba_id) }}
                                        </div>
                                        @endif

                                        <div class="add-to-cart-container text-center">  

                                            <div class="add-to-cart-side-buttons inline-block">
                                                @if(Cart::kupac_id() > 0)
                                                <button class="like-it JSadd-to-wish" data-roba_id="{{$vezani_artikl->roba_id}}" title="{{ Language::trans('Dodaj na listu želja') }}"><i class="far fa-heart"></i></button> 
                                                @else
                                                <button class="like-it JSnot_logged" data-roba_id="{{$vezani_artikl->roba_id}}" title="{{ Language::trans('Dodavanje artikla na listu želja moguće je samo registrovanim korisnicima') }}"><i class="far fa-heart"></i></button> 
                                                @endif                          
                                            </div>

                                            <div class="add-to-cart-main-button inline-block">
                                                @if(Product::getStatusArticle($vezani_artikl->roba_id) == 1)  
                                                @if(Cart::check_avaliable($vezani_artikl->roba_id) > 0)

                                                    @if(!Product::check_osobine($vezani_artikl->roba_id)) 

                                                    <button class="buy-btn button JSadd-to-cart" data-roba_id="{{$vezani_artikl->roba_id}}">{{ Language::trans('U korpu') }}</button>

                                                    @else

                                                    <a href="{{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($vezani_artikl->roba_id))}}" class="buy-btn button">
                                                        {{ Language::trans('Vidi artikal') }}
                                                    </a>     
                                                    
                                                    @endif

                                                    <button class="hidden like-it JScompare {{ All::check_compare($vezani_artikl->roba_id) ? 'active' : '' }}" data-id="{{$vezani_artikl->roba_id}}" title="{{ Language::trans('Uporedi') }}">
                                                        <i class="fas fa-exchange-alt" aria-hidden="true"></i>
                                                    </button>
                                                    

                                                    @else   

                                                    <button class="not-available button">{{ Language::trans('Nije dostupno') }}</button>    

                                                    @if(Options::compare()==1 AND isset($filter_prikazi) AND $filter_prikazi == 1)
                                                    <button class="hidden like-it JScompare {{ All::check_compare($vezani_artikl->roba_id) ? 'active' : '' }}" data-id="{{$vezani_artikl->roba_id}}" title="{{ Language::trans('Uporedi') }}">
                                                        <i class="fas fa-exchange-alt" aria-hidden="true"></i>
                                                    </button>
                                                    @endif  

                                                @endif

                                                @else  
                                                    <button class="buy-btn button">{{ Product::find_flag_cene(Product::getStatusArticle($vezani_artikl->roba_id),'naziv') }}</button> 

                                                    @if(Options::compare()==1 AND isset($filter_prikazi) AND $filter_prikazi == 1)
                                                    <button class="hidden like-it JScompare {{ All::check_compare($vezani_artikl->roba_id) ? 'active' : '' }}" data-id="{{$vezani_artikl->roba_id}}" title="{{ Language::trans('Uporedi') }}">
                                                        <i class="fas fa-exchange-alt" aria-hidden="true"></i>
                                                    </button>
                                                    @endif   

                                                @endif 
                                            </div>

                                            <div class="add-to-cart-side-buttons inline-block">
                                                <button class="like-it JScompare {{ All::check_compare($vezani_artikl->roba_id) ? 'active' : '' }}" data-id="{{$vezani_artikl->roba_id}}" title="{{ Language::trans('Uporedi') }}">
                                                    <i class="fas fa-exchange-alt" aria-hidden="true"></i>
                                                </button>
                                            </div>  
                                        </div>             
                                    </div>
                                    
                                </div>
                                <!-- ADMIN BUTTON -->
                                @if(Session::has('b2c_admin'.Options::server()) AND Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
                                <a class="article-edit-btn JSFAProductModalCall" data-roba_id="{{$vezani_artikl->vezani_roba_id}}" href="#">{{Language::trans('IZMENI ARTIKAL')}}</a>
                                @endif
                            </div>
                        
                        @endif
                        @endforeach 
                    </div> 
                    @endif
                @endif

                @if(count(Product::get_related($roba_id)))
                    <!-- RELATED PRODUCTS --> 
                    <br>
                    <h2><span class="section-title">{{Language::trans('Srodni proizvodi')}}</span></h2>
                    <div class="JSproducts_slick row">
                        @foreach(Product::get_related($roba_id) as $row)
                            @include('shop/themes/'.Support::theme_path().'partials/products/product_on_grid')
                        @endforeach
                    </div>
                @endif

            </div>
        </div> 
    </div> 

    <!-- PRODUCT SIDE ICONS -->
    <div class="side-product-icons">
        <div class="side-icon-box flex flex-end">
            <div class="icon-content">
                <h3>{{ Language::trans('Besplatna dostava') }}</h3>
                <div class="icon-seperator">&nbsp;</div>
                <span>{{ Language::trans('na teritoriji Crne Gore iznad 20,00e') }}</span>
            </div>
            <div class="side-icon">
                <i class="fas fa-shipping-fast"></i>
            </div>
        </div>

        <div class="side-icon-box flex flex-end">
            <div class="icon-content">
                <h3>{{ Language::trans('14 dana') }}</h3>
                <div class="icon-seperator">&nbsp;</div>
                <span>{{ Language::trans('pravo povrata robe') }}</span>
            </div>
            <div class="side-icon">
                <i class="far fa-calendar-check"></i>
            </div>
        </div>

        <div class="side-icon-box flex flex-end">
            <div class="icon-content">
                <h3>{{ Language::trans('Podrška svakog radnog dana') }}</h3>
                <div class="icon-seperator">&nbsp;</div>
                <span>{{ Language::trans('Putem callcentra na broj') }} <a href="tel:020686795">020686795</a></span>
            </div>
            <div class="side-icon">
                <i class="fas fa-headphones-alt"></i>
            </div>
        </div>

        <div class="side-icon-box flex flex-end">
            <div class="icon-content">
                <h3>{{ Language::trans('Garancija na sve') }}</h3>
                <div class="icon-seperator">&nbsp;</div>
                <span>{{ Language::trans('proizvode iz ponude') }}</span>
            </div>
            <div class="side-icon">
                <i class="  fas fa-tag"></i>
            </div>
        </div>
    </div>

</main>

<!-- ARTICLE_DETAILS.blade -->

@endsection