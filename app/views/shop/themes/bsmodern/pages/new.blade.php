@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page') 

<!-- NEW.blade -->


<div class="col-md-3 p-left side-content sm-order-1">
	@include('shop/themes/'.Support::theme_path().'partials/side_content_news')
</div>

<div class="col-md-9 col-sm-9 col-xs-12 pull-right sm-order-0">
	<div class="single-news">
	 
	    @if(in_array(Support::fileExtension($slika),array('jpg','png','jpeg','gif')))
		
			<img class="max-width" src="{{ $slika }}" alt="{{ $naslov }}">
	    
	    @else
	    
	    	<iframe src="{{ $slika }}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>                   
	    
	    @endif
	     

		<h2 class="news-title">{{ $naslov }}</h2>

		<div class="news-meta">
			<span><i class="fa fa-user"></i> {{ Language::trans('Postavio') }} admin</span>
			<span><i class="fa fa-comments"></i> {{ count($web_vest_b2c_komentar) }}</span>
			<span><i class="far fa-clock"></i> {{ date('d-m-y', strtotime($datum)) }}</span>
		</div>

		{{ $sadrzaj }} 

	 
	</div>  

	<div class="news-comments-section">
		<h3>Komentari</h3>		

		<!-- comment 1 -->
		<div class="news-single-comment row">
		
	        @foreach($web_vest_b2c_komentar as $komentar)

			@if(!empty($komentar->web_kupac_id))
			<div class="col-md-2 news-profile-image no-padding"><img src="{{Options::base_url()}}{{WebKupac::get_user_profile_image($komentar->web_kupac_id) }}" alt=""></div>
			@else
			<div class="col-md-2 news-profile-image no-padding"><img src="{{Options::base_url()}} '/images/avatar-a.jpg'}}" alt=""></div>
			@endif
			
	            <span class="news-comments-name">
	            	{{$komentar->ime_osobe}}
	            	
	            	<i> - </i>
	            	<time>{{$komentar->datum}}</time>
	        	</span>

	        	<span class="news-comment-content"><p>{{$komentar->komentar}}</p>
	        	</span>

			@endforeach
			</div>
		</div>
		

	@if(Session::has('b2c_kupac'))
		<!-- ADD COMMNET HARDCODED -->
		<div class="news-add-comment">
			<h3>{{ Language::trans('Ostavite svoj komentar') }}</h3>
			
			<span class="news-comment-note">{{ Language::trans('Vaša email adresa neće biti objavljena. Obavezna polja su označena *') }}</span>

			<form class="news-comment-form" method="post" id="postComment" action="{{ Options::base_url() }}comment-add-news"> 
		        <input type="hidden" value="{{ $web_vest_b2c_id }}" name="web_vest_b2c_id" />
		         <input type="hidden" value="{{ Session::get('b2c_kupac') }}" name="web_kupac_id" />
		       
                <!-- JSrev-star -->
				<span class="review"> 
                    <span>{{Language::trans('Ocena')}}:</span>
                    <a id="JSstar1" class="JSrev-star-group star-review-empty">
                    <input class="JSstars-checkbox" type="checkbox" value='1' name="ocena"/>
                        <i class="fas fa-star review-star" aria-hidden="true"></i>
                    </a>
                    <a id="JSstar2" class="JSrev-star-group star-review-empty">
                    <input class="JSstars-checkbox" type="checkbox" value='2' name="ocena"/>
                        <i class="fas fa-star review-star" aria-hidden="true"></i>
                        <i class="fas fa-star review-star" aria-hidden="true"></i>
                    </a>
                    <a id="JSstar3" class="JSrev-star-group star-review-empty">
                    <input class="JSstars-checkbox" type="checkbox" value='3' name="ocena"/>
                        <i class="fas fa-star review-star" aria-hidden="true"></i>
                        <i class="fas fa-star review-star" aria-hidden="true"></i>
                        <i class="fas fa-star review-star" aria-hidden="true"></i>
                    </a>
                    <a id="JSstar4" class="JSrev-star-group star-review-empty">
                    <input class="JSstars-checkbox" type="checkbox" value='4' name="ocena"/>
                        <i class="fas fa-star review-star" aria-hidden="true"></i>
                        <i class="fas fa-star review-star" aria-hidden="true"></i>
                        <i class="fas fa-star review-star" aria-hidden="true"></i>
                        <i class="fas fa-star review-star" aria-hidden="true"></i>
                    </a>
                    <a id="JSstar5" class="JSrev-star-group star-review-empty">
                    <input class="JSstars-checkbox" type="checkbox" value='5' name="ocena"/>
                        <i class="fas fa-star review-star" aria-hidden="true"></i>
                        <i class="fas fa-star review-star" aria-hidden="true"></i>
                        <i class="fas fa-star review-star" aria-hidden="true"></i>
                        <i class="fas fa-star review-star" aria-hidden="true"></i>
                        <i class="fas fa-star review-star" aria-hidden="true"></i>
                    </a>
               
                </span>

                 <div>
		        	<!-- <label for="name">{{ Language::trans('Ime') }}<em class="required">*</em></label> -->
		          	<input type="text" class="input-text" title="Name" value="" id="user" name="ime_osobe" placeholder="Ime">
		        </div>


		        <div>
		        	<!-- <label for="email">{{ Language::trans('E-mail') }}<em class="required">*</em></label> -->
		          	<input type="text" class="input-text validate-email" title="Email" value="{{WebKupac::get_user_email()}}" id="email" name="email" placeholder="{{WebKupac::get_user_email()}}" >
		        </div>

		         <div>
		        	<!-- <label for="website">{{ Language::trans('Website') }}<em class="required">*</em></label> -->
		          	<input type="text" class="input-text validate-website" title="web_site" value="" id="website" name="web_site" placeholder="Website">
		        </div>

		        <div>
		        	<!-- <label for="comment">{{ Language::trans('Vaš komentar') }}<em class="required">*</em></label> -->
		        	<textarea rows="5" cols="50" class="input-text" title="Comment" id="comment" name="komentar" placeholder="Komentar"></textarea>
		        </div>

		        <button type="submit" class="button">{{ Language::trans('Postavi') }}</button>

		     </form>
		</div>
	@endif
	</div>
</div>

 

<!-- NEW.blade END -->

@endsection