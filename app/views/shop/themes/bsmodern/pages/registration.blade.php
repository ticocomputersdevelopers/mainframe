@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')

<!-- REGISTRATION.blade -->

<div class="row login-and-registration">
	<div class="col-md-12">
		<h2>Prijava i registracija</h2>
	</div>

	<div class="col-md-6">
		<h3><span class="page-title text-bold center-block">{{ Language::trans('Registrujte se') }}</span></h3> 

		<div class="registartion-message hidden">
			Lorem ipsum dolor sit amet, consectetur adipiscing elit. In malesuada lacus urna, in euismod eros sagittis vitae. Pellentesque auctor sapien vitae risus auctor, non porta magna facilisis.
		</div>

		<div class="reg-form-wrap">
			<div class="login-title">
				@if(Input::old('flag_vrsta_kupca') == 1)
					<span class="inline-block pointer private-user">{{ Language::trans('Fizičko lice') }}</span>
					<span class="inline-block pointer active-user company-user">{{ Language::trans('Pravno lice') }}</span>
				@else
					<span class="inline-block pointer active-user private-user">{{ Language::trans('Fizičko lice') }}</span>
					<span class="inline-block pointer company-user">{{ Language::trans('Pravno lice') }}</span>
				@endif
			</div> 


			<form action="{{ Options::base_url() }}registracija-post" method="post" class="registration-form" enctype="multipart/form-data" autocomplete="off"> 

				<input type="hidden" name="flag_vrsta_kupca" value="0"> 
				<div>
			        <label for="profilna_slika">{{ Language::trans('Profilna slika') }}</label>
			        <input id="profilna_slika" name="profilna_slika" type="file" >
			    </div>
			     
				<div class="field-group">
					<label for="ime">{{ Language::trans('Ime') }}</label>
					<input id="ime" name="ime" type="text" value="{{ htmlentities(Input::old('ime') ? Input::old('ime') : '') }}" >
					<div class="error red-dot-error">{{ $errors->first('ime') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('ime') : "" }}
					</div>
				</div>

				<div class="field-group">
					<label for="prezime">{{ Language::trans('Prezime') }}</label>
					<input id="prezime" name="prezime" type="text" value="{{ Input::old('prezime') ? Input::old('prezime') : '' }}" >
					<div class="error red-dot-error">{{ $errors->first('prezime') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('prezime') : "" }}</div>
				</div>

				<div class="field-group">
					<label for="naziv">{{ Language::trans('Naziv firme') }}:</label>
					<input id="naziv" name="naziv" type="text" value="{{ htmlentities(Input::old('naziv') ? Input::old('naziv') : '') }}" >
					<div class="error red-dot-error">{{ $errors->first('naziv') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('naziv') : "" }}
					</div>
				</div>

				<div class="field-group">
					<label for="pib">{{ Language::trans('PIB') }}</label>
					<input id="pib" name="pib" type="text" value="{{ Input::old('pib') ? Input::old('pib') : '' }}" >
					<div class="error red-dot-error">{{ $errors->first('pib') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('pib') : "" }}</div>
				</div>	

				<div class="field-group">
					<label for="maticni_br">{{ Language::trans('Matični broj') }}</label>
					<input id="maticni_br" name="maticni_br" type="text" value="{{ Input::old('maticni_br') ? Input::old('maticni_br') : '' }}" >
					<div class="error red-dot-error">{{ $errors->first('maticni_br') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('maticni_br') : "" }}</div>
				</div>	  

				<div class="field-group">
					<label for="email">{{ Language::trans('E-mail') }}</label>
					<input id="email" autocomplete="off" name="email" type="text" value="{{ Input::old('email') ? Input::old('email') : '' }}" >
					<div class="error red-dot-error">{{ $errors->first('email') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('email') : "" }}
					</div>
				</div>	

				<div class="field-group">
					<label for="lozinka">{{ Language::trans('Lozinka') }}</label>
					<input id="lozinka" name="lozinka" type="password" value="{{ htmlentities(Input::old('lozinka') ? Input::old('lozinka') : '') }}">
					<div class="error red-dot-error">{{ $errors->first('lozinka') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('lozinka') : "" }}</div>
				</div>

				<div class="field-group">
					<label for="telefon">{{ Language::trans('Telefon') }}</label>
					<input id="telefon" name="telefon" type="text" value="{{ Input::old('telefon') ? Input::old('telefon') : '' }}" >
					<div class="error red-dot-error">{{ $errors->first('telefon') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('telefon') : "" }}</div>
				</div>

				<div class="field-group">
					<label for="adresa">{{ Language::trans('Adresa') }}</label>
					<input id="adresa" name="adresa" type="text" value="{{ htmlentities(Input::old('adresa') ? Input::old('adresa') : '') }}" >
					<div class="error red-dot-error">{{ $errors->first('adresa') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('adresa') : "" }}</div>
				</div>

				<div class="field-group">
					<label for="mesto"><span class="red-dot"></span> {{ Language::trans('Mesto') }}/{{ Language::trans('Grad') }} </label>
					<input id="mesto" type="text" name="mesto" value="{{ htmlentities(Input::old('mesto') ? Input::old('mesto') : '') }}" >
					<div class="error red-dot-error">{{ $errors->first('mesto') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('mesto') : "" }}</div>
				</div> 

				<div class="capcha text-center"> 
					{{ Captcha::img(5, 160, 50) }}<br>
					<span>{{ Language::trans('Unesite kod sa slike') }}</span>
					<input type="text" name="captcha-string" autocomplete="off">
					<div class="error red-dot-error">{{ $errors->first('captcha') ? $errors->first('captcha') : "" }}</div>
				</div>

				<div class="registration-button">   
					<button type="submit" class="button">{{ Language::trans('Registruj se') }}</button>
				</div> 
			</form>
		</div>
	</div>

	<div class="col-md-6">
		
		<h3><span class="page-title text-bold center-block">{{ Language::trans('Prijavite se') }}</span></h3> 

		<div class="login-form-wrapper">
			<form action="{{ Options::base_url()}}login" method="post" class="login-form" autocomplete="off">
				<span class="welcome-login"><span>Dobrodošli!<br></span>Za pristup Vašem nalogu unesite E-mail i lozinku.</span>

				<div class="field-group">
					<label for="email_login">E-mail</label> 

					<?php if(Input::old('email_login')){ $old_mail = Input::old('email_login'); }else{ if(Input::old('email_fg')){ $old_mail = Input::old('email_fg'); }else{$old_mail ='';} } ?>
					<input class="login-form__input" placeholder="E-mail adresa" name="email_login" type="text" value="{{ $old_mail }}" autocomplete="off">
				</div>
				
				<div class="field-group">
					<label for="lozinka_login">Lozinka</label>

				    <input class="login-form__input" placeholder="Lozinka" autocomplete="off" name="lozinka_login" type="password" value="{{ Input::old('lozinka_login') ? Input::old('lozinka_login') : '' }}">
				 </div>

				 <div class="field-group flex"> 
					<button type="submit" class="login-form-button admin-login">Login</button>
					<form class="forgot_pass" action="{{ Options::base_url()}}zaboravljena-lozinka" method="post" autocomplete="off">
						<div class="error">{{ $errors->first('email_fg') ? $errors->first('email_fg') : "" }}</div>
						<?php if(Input::old('email_fg')){ $old_mail_fg = Input::old('email_login'); }else{ if(Input::old('email_login')){ $old_mail_fg = Input::old('email_login'); }else{$old_mail_fg ='';} } ?>
						<input name="email_fg" type="hidden" value="{{ $old_mail_fg }}" >
						<button class="btn-forgot-pass" type="submit">Zaboravili ste lozinku?</button>
					</form>
				</div>
			</form>

			<div class="field-group error-login">
				<div class="row"> 
					<div class="col-md-9 wrong-email"> 
				<?php if($errors->first('email_login')){ echo $errors->first('email_login'); }elseif($errors->first('lozinka_login')){ echo $errors->first('lozinka_login'); } ?>

				@if(Session::get('confirm'))
					Niste potvrdili registraciju.<br>Posle registracije dobili ste potvrdu na vašu e-mail adresu!
				@endif

				@if(Session::get('message'))
					Novu lozinku za logovanje dobili ste na navedenu e-mail adresu.
				@endif
					</div>
				</div>
			</div>

		</div> 
	</div>
</div>


<script>
@if(Session::get('message'))
	$(document).ready(function(){     
 	
	bootboxDialog({ message: "<p>" + trans('Poslali smo Vam potvrdu o registraciji na e-mail koji ste uneli. Molimo Vas da potvrdite Vašu e-mail adresu klikom na link iz e-mail poruke') + "!</p>", size: 'large', closeButton: true }, 6000); 
 
	});
@endif
</script>

<br>

<!-- REGISTRATION.blade -->

@endsection 