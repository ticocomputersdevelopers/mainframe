@extends('shop/themes/'. Support::theme_path().'templates/main')

@section('page')
    <div class="compare-page-table"> 
        <h1 class="padding-v-20"> {{ Language::trans('Upoređivanje proizvoda iz grupe') }}: {{ $grupa->grupa }}</h1>

        @if(count($ids)>0)
        <div class="table-responsive text-center">

            <table class="table table-bordered table-striped">
                <tbody> 
           
                    <tr>
                        @foreach($ids as $id)
                        <td class="relative">
                            <div class="product-image-wrapper flex">
                                <a class="inline-block" href="{{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($id))}}">
                                    <img src="{{ Options::base_url().Product::web_slika($id) }}" class="margin-auto img-responsive">
                                </a>
                            </div>

                            <a class="inline-block" href="{{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($id))}}">
                                <h2 class="product-name"> {{ Language::trans(All::getCompare($id)[0]->naziv_web) }} </h2>
                            </a>
                          
                            <span class="delete-absolute-btn JSclearCompare" data-id="{{ $id }}">
                                <i class="fa fa-times"> </i>
                            </span>
                        </td>
                        @endforeach
                    </tr>

                    <tr class="table-row-charact">
                        <!-- <td class="compare-table-info">{{ Language::trans('proizvođač') }}</td> -->
                        @foreach($ids as $id)
                            <td>
                                <div> {{ Language::trans('Proizvođač') }} </div>
                                <div> {{ All::getCompare($id)[0]->proizvodjac }} </div>
                            </td>
                        @endforeach
                    </tr>

                    @foreach(All::getCompare($ids) as $karak)
                        <tr class="table-row-charact">
                            <!-- <td>{{ Language::trans($karak->naziv) }}</td> -->
                        
                            @foreach($ids as $id)
                                <td>
                                    <div> {{ Language::trans($karak->naziv) }} </div>
                                    @if(All::getVrednost($id,$karak->naziv))
                                        <div> {{ All::getVrednost($id,$karak->naziv) }} </div>
                                    @else
                                        <div> --- </div> 
                                    @endif
                                </td>
                            @endforeach
                        </tr>
                    @endforeach

                  
                    <tr class="table-row-charact">
                        <!-- <td class="compare-table-info">{{ Language::trans('cena') }}</td> -->
                        @foreach($ids as $id)
                        <?php
                            $akcijska_cena = Cart::cena(All::getCompare($id)[0]->akcijska_cena);
                            $web_cena = Cart::cena(Options::checkCena()=='web_cena'?All::getCompare($id)[0]->web_cena:All::getCompare($id)[0]->mpcena);
                        ?>
                            @if(All::getCompare($id)[0]->akcija_flag_primeni==1 && $akcijska_cena<$web_cena && $akcijska_cena!=0)
                                <td>
                                    <div> {{ Language::trans('Cijena') }} </div>
                                    <div class="price-holder"> {{ $akcijska_cena }} </div>
                                </td>
                            @else
                                <td>
                                    <div> {{ Language::trans('Cijena') }} </div>
                                    <div class="price-holder"> {{ $web_cena }} </div>
                                </td>
                            @endif
                        @endforeach    
                    </tr>  
                </tbody> 
            </table>
        </div>

        <div class="text-right sm-text-center btn-compare-background">
            <a href="{{Options::base_url()}}clear-compare/{{ $grupa->grupa_pr_id }}" class="button inline-block">
                {{ Language::trans('Poništi sve') }}
            </a>
        </div>
        
    @else
        {{ Language::trans('Nemate proizvode za upoređivanje').'!' }}
    @endif
 </div>
@endsection