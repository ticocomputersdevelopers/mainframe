@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page') 


<div class="row">
	<div class="col-md-3 p-left">
		@include('shop/themes/'.Support::theme_path().'partials/side_content_news')
	</div>

	<div class="col-md-9">
	
		<div class="news-blogs-wrap">
			@foreach($news as $row) 

			<div class="single-news pull-right relative">
				    @if(in_array(Support::fileExtension($row->slika),array('jpg','png','jpeg','gif')))
					
						<img class="max-width" src="{{ $row->slika }}" alt="{{ $row->naslov }}">
						
				    @else
				    
				    	<iframe src="{{ $row->slika }}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>                   
				    
				    @endif

				<h2 class="news-title">{{ $row->naslov }}</h2>

				<div class="news-meta">
					<span><i class="fa fa-user"></i> {{ Language::trans('Postavio admin') }}</span>
					<span><i class="fa fa-comments"></i> {{ Language::trans('8 komentara') }}</span>
					<span><i class="far fa-clock"></i> {{ Language::trans('22. Maj') }}</span>
				</div>

				<div class="news-desc overflow-hdn">{{ All::shortNewDesc($row->tekst) }}</div>

				<button class="button relative news-read-button">
					{{ Language::trans('Pročitaј') }}<a class="overlink" href="{{ Options::base_url() }}{{ Url_mod::slug_trans('blog') }}/{{ Url_mod::blog_link($row->web_vest_b2c_id) }}"></a>
				</button>
			 
			</div> 

				<!-- <div>
					{{ All::getNews()->links() }}
				</div>   -->
			@endforeach
		</div> 

	</div>
</div>
@endsection



