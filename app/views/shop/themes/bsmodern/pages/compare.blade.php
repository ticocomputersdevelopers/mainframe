@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')

<div class="compare-page">
	<h1 class="padding-v-20"> {{ Language::trans('Uporedi proizvode') }}: </h1>

	<ul>	
		@foreach($compareArticles as $key => $row)
			@if((!isset($grupa) OR $row->grupa != $grupa))
				@if(!empty($grupa = $row->grupa))
				<li>
				<h3>{{ $grupa }}</h3>
				<div class="row list-on-mobile">
				@endif
			@endif

			@include('shop/themes/'.Support::theme_path().'partials/products/product_on_grid')

			@if(!isset($compareArticles[($key+1)]) OR $row->grupa != $compareArticles[($key+1)]->grupa)
			</div>
			<div class="margin-v-20 text-right sm-text-center">
				<a class="button inline-block" href="{{Options::base_url()}}clear-compare/{{ $row->grupa_pr_id }}">{{ Language::trans('Poništi') }}</a>
				<a class="button-compare inline-block" href="{{Options::base_url()}}{{ Url_mod::slug_trans('uporedi') }}/{{ $row->grupa_pr_id }}">{{ Language::trans('Uporedi') }} <i class="fas fa-exchange-alt"></i></a>
			</div>
			</li>
			@endif
		@endforeach

		@if(count($compareArticles) == 0)
			<div class="text-center"> 
				<br>
				<img class="img-responsive margin-auto" src="{{ Options::domain() }}/images/smiley-no-articles.png">
				<p class="section-title text-uppercase padding-v-20">{{ Language::trans('Nemate proizvode za upoređivanje').'!' }}</p>
			</div>
		@endif
	</ul>
</div>

@endsection