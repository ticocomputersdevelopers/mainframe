<?php
$web_b2c_seo_id = Seo::get_page_id($strana); 
$stranica=DB::table('web_b2c_seo')->where(array('web_b2c_seo_id'=>$web_b2c_seo_id))->first();
?>

@if($strana=='blog')
<?php $slajder_id = !is_null($stranica->slajder_id) ? $stranica->slajder_id : 0; ?>
@endif
<!-- news banner --> 
@if($slajder_id > 0 AND $baner = Slider::slajder($slajder_id) AND count($banerStavke = Slider::slajderStavke($baner->slajder_id)) > 0)
		<div class="bg-img side-content-box relative" style="background-image: url( '/{{ $banerStavke[0]->image_path }}' )">
			<a href="{{ $banerStavke[0]->link }}" class="slider-link"> </a> 
		</div>
@endif
	
<!-- last 4 comments -->
@if(!empty(All::getLatestComments()))
	<div class="side-content-box">   
		<h2>{{ Language::trans('Poslednji komentari') }}</h2>
		<div class="side-content-inner">
		@foreach(All::getLatestComments() as $row) 
			<div class="side-comment-wrap">
				<div class="">
					{{ $row->ime_osobe }} - <span class="side-comment">{{ $row->komentar }}</span>
				</div>

				<div class="side-comment-date">
					{{ Support::date_convert($row->datum) }}
				</div>
			</div>
		@endforeach 
		</div>
	</div>  
@endif

<!-- filter za kategorije vesti -->
@if(!empty(All::getNewsCategories()))
	<div class="side-content-box">   
		<h2>{{ Language::trans('Kategorije') }}</h2>
		<div class="side-content-inner">
		
			<div class="side-comment-wrap">
				<div class="">
					<a href="javascript:void(0)" rel=”nofollow”>
					<span class="JSfilter-categories"  data-element="{{'novo'}}">{{ 'Novo' }}</span>
					</a>
				</div>

				<div class="side-comment-date">
					<a href="javascript:void(0)" rel=”nofollow”>
					<span class="JSfilter-categories" data-element="{{'tehnika'}}">{{ 'Tehnika' }}</span>
					</a>
				</div>
			</div>
		
		
		</div>
		<input type="hidden" id="JScharacteristic-url" value="{{$url}}" />
	</div>  
@endif

<!-- most popular news -->
<div class="side-content-box">
	<h2>{{ Language::trans('Popularne vesti') }}</h2>
	<div class="side-content-inner">	 
		@foreach(All::getMostPopularNews() as $row) 
		<div class="side-blog row">
			<div class="col-xs-6 p-left">
		        <div class="side-blog-image-wrap flex">
			 		<a class="margin-auto" href="{{ Options::base_url() }}{{ Url_mod::slug_trans('blog') }}/{{ Url_mod::blog_link($row->web_vest_b2c_id) }}">
			        @if(in_array(Support::fileExtension($row->slika),array('jpg','png','jpeg','gif')))
			        	<img class="side-blog-image" href="{{ Options::base_url() }}{{ Url_mod::slug_trans('blog') }}/{{ Url_mod::blog_link($row->web_vest_b2c_id) }}" src="{{ $row->slika }}"></img>
			    	</a>
			        @endif
	        	</div>
	        </div>

	        <div class="col-xs-6 no-padding flex">
	        	<div class="side-blog-title">{{ $row->naslov }}</div>

				<div class="side-blog-date">
					{{ Support::date_convert($row->datum) }}
				</div>
	        </div>

			<div class="hidden">{{ All::shortNewDesc($row->sadrzaj) }}</div>	
		</div>

		<div class="text-center hidden">
			<a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('blog') }}/{{ Url_mod::blog_link($row->web_vest_b2c_id) }}" class="button inline-block relative z-index-1">{{ Language::trans('Pročitaj članak') }}</a>
		</div> 
	@endforeach
	</div>
</div> 