
<?php $roba = DB::table('roba')->get(); ?>

<div class="side-content-box">
	@if(Session::has('b2c_korpa') and Cart::broj_cart() >= 1 )
	<h2>{{ Language::trans('Korpa') }}</h2>
	<div class="side-content-inner">
		<span>{{ Language::trans('Imate') }}</span><a href="{{ Options::base_url() }}{{ Seo::get_korpa() }}"> {{Cart::broj_cart()}} <span>{{ Language::trans('artikala') }}</span></a><span> {{ Language::trans('u korpi') }}.</span>

		<div>{{ Language::trans('Total korpe') }}: <span class="side-price">{{ Cart::cena(Cart::cart_ukupno()) }}</span></div>

		<div class="side-cart-button button"> 
			<a href="{{ Options::base_url() }}{{ Seo::get_korpa() }}" rel="nofollow"><i class="fas fa-check"></i>{{Language::trans('Završi kupovinu')}}</a>
		</div>

		<div class="side-content-label">{{ Language::trans('Sadržaj korpe') }}:</div>

		@foreach(DB::table('web_b2c_korpa_stavka')->where('web_b2c_korpa_id',Cart::korpa_id())->orderBy('web_b2c_korpa_stavka_id','asc')->get() as $row)
		<div class="row mini-cart-item"> 

			<div class="col-xs-3 no-padding"> 
				@if(!empty(Product::design_id($row->roba_id)) AND Options::pitchprint_aktiv() == 1)  
				@if(!empty($row->project_id))			
				<img class="mini-cart-img img-responsive" src="https://s3-eu-west-1.amazonaws.com/pitchprint.io/previews/{{$row->project_id}}_1.jpg" alt="{{Product::short_title($row->roba_id)}}"/>
				@else
				<img class="mini-cart-img img-responsive" src="https://s3-eu-west-1.amazonaws.com/pitchprint.io/previews/{{Product::design_id($row->roba_id)}}_1.jpg" alt="{{Product::short_title($row->roba_id)}}"/>
				@endif
				@else
				<img class="mini-cart-img img-responsive" src="{{ Options::domain() }}{{Product::web_slika($row->roba_id)}}" alt="{{Product::short_title($row->roba_id)}}"/>
				@endif
			</div>

			<div class="col-xs-9 text-left"> 

				<div class="mini-cart-count"> 
					@if(AdminOptions::web_options(320)==1)
						@if(Product::jedinica_mere($row->roba_id)->jedinica_mere_id == 3 AND Product::pakovanje($row->roba_id))
						<span>{{$row->kolicina}}kg</span> x
						@else
						<span>{{round($row->kolicina)}}</span> x
						@endif
					@else
						<span>{{round($row->kolicina)}}</span> x
					@endif
					@if(AdminOptions::web_options(152)==1)
					<span class="side-price">{{Cart::cena(ceil(($row->jm_cena)/10)*10 )}}</span>
					@else
					<span class="side-price">{{Cart::cena($row->jm_cena)}}</span>
					@endif
				</div>

				<a class="mini-cart-title inline-block line-h" href="{{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($row->roba_id))}}">{{Product::short_title($row->roba_id)}}
				</a> 

				<a href="javascript:void(0)" data-stavka_id="{{ $row->web_b2c_korpa_stavka_id }}" title="{{ Language::trans('Ukloni artikal') }}" class="remove-cart-item JSdelete_cart_item" rel=”nofollow”><i class="fas fa-times"></i></a>

			</div>
		</div>
		@endforeach	
	</div>
	@endif
</div>

<div class="side-content-box">
	<h2>{{ Language::trans('Uporedjeni proizvodi') }}</h2>	
	<div class="side-content-inner" id="JSMiniCompareList">
		<!-- CONTENT HERE -->
		@include('shop/themes/'.Support::theme_path().'partials/mini_compare_list')
	</div>
</div>

<div class="side-content-box">
	<h2>{{ Language::trans('Popularni tagovi') }}</h2>
	<div class="side-content-inner JSside-tags flex">
	@foreach($roba as $row)
	        <h6 class="">
	        	{{ Product::tags($row->roba_id) }} 
		    </h6>
	@endforeach 
	</div>
</div>