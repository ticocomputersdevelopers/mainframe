@if(Cart::broj_cart()>0)
 
	@foreach(DB::table('web_b2c_korpa_stavka')->where('web_b2c_korpa_id',Cart::korpa_id())->orderBy('web_b2c_korpa_stavka_id','asc')->get() as $row)
	<div class="row mini-cart-item"> 

		<div class="col-xs-3 no-padding"> 
			@if(!empty(Product::design_id($row->roba_id)) AND Options::pitchprint_aktiv() == 1)  
			@if(!empty($row->project_id))			
			<img class="mini-cart-img img-responsive" src="https://s3-eu-west-1.amazonaws.com/pitchprint.io/previews/{{$row->project_id}}_1.jpg" alt="{{Product::short_title($row->roba_id)}}"/>
			@else
			<img class="mini-cart-img img-responsive" src="https://s3-eu-west-1.amazonaws.com/pitchprint.io/previews/{{Product::design_id($row->roba_id)}}_1.jpg" alt="{{Product::short_title($row->roba_id)}}"/>
			@endif
			@else
			<img class="mini-cart-img img-responsive" src="{{ Options::domain() }}{{Product::web_slika($row->roba_id)}}" alt="{{Product::short_title($row->roba_id)}}"/>
			@endif
		</div>

		<div class="col-xs-9 text-left"> 

			<div class="mini-cart-count"> 
				@if(AdminOptions::web_options(320)==1)
					@if(Product::jedinica_mere($row->roba_id)->jedinica_mere_id == 3 AND Product::pakovanje($row->roba_id))
					<span>{{$row->kolicina}}kg</span> x
					@else
					<span>{{round($row->kolicina)}}</span> x
					@endif
				@else
					<span>{{round($row->kolicina)}}</span> x
				@endif
				@if(AdminOptions::web_options(152)==1)
				<span>{{Cart::cena(ceil(($row->jm_cena)/10)*10 )}}</span>
				@else
				<span>{{Cart::cena($row->jm_cena)}}</span>
				@endif
			</div>

			<a class="mini-cart-title inline-block line-h" href="{{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($row->roba_id))}}">{{Product::short_title($row->roba_id)}}
			</a> 

			<a href="javascript:void(0)" data-stavka_id="{{ $row->web_b2c_korpa_stavka_id }}" title="{{ Language::trans('Ukloni artikal') }}" class="remove-cart-item JSdelete_cart_item" rel=”nofollow”><i class="fas fa-times"></i></a>

		</div>
	</div>
	@endforeach	


	<ul class="mini-cart-sum hidden"> 
		@if(Options::checkTroskoskovi_isporuke() == 1 AND Cart::cart_ukupno() < Cart::cena_do()) 

			<li>{{ Language::trans('Cijena artikala') }}: <i>{{Cart::cena(Cart::cart_ukupno())}}</i></li> 
			<li>{{ Language::trans('Troškovi isporuke') }}: <i>{{Cart::cena(Cart::cena_dostave())}}</i></li> 
			<li>{{ Language::trans('Ukupno') }}: <i>{{Cart::cena(Cart::cart_ukupno()+Cart::cena_dostave())}}</i></li> 

		@else
			
	 		<li>{{ Language::trans('Ukupno') }}: <i>{{ Cart::cena(Cart::cart_ukupno()) }}</i></li> 

		@endif  
	</ul>

	<div class="mini-cart-button"> 
		<a class="inline-block" href="{{ Options::base_url() }}{{ Seo::get_korpa() }}" rel="nofollow">{{Language::trans('Završi kupovinu')}}</a>
	</div>

@endif
