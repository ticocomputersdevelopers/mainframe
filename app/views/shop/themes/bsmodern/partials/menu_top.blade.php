<!-- MENU_TOP.blade -->
@if(Session::has('b2c_admin'.Options::server()) AND Admin_model::check_admin(array('ARTIKLI')) AND Admin_model::check_admin(array('ARTIKLI_PREGLED')))
<div id="admin-menu"> 
    <!-- <div class="container text-right"> -->
    <div class="text-right">
        <a href="#!" data-toggle="modal" data-target="#FAProductsModal" rel="nofollow"> <i class="fas fa-clipboard-list"></i> {{ Language::trans('Artikli') }}</a>
        |
        @if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
        <a href="#!" id="JSShortAdminSave" rel="nofollow"> <i class="fas fa-save"></i> {{ Language::trans('Sačuvaj izmene') }}</a>
        @endif
        | 
        <span class="ms admin-links"><a target="_blank" href="{{ Options::domain() }}admin" rel="nofollow"><i class="fas fa-cogs"></i> {{ Language::trans('Admin Panel') }}</a></span> |
        <span class="ms admin-links"><a href="{{ Options::domain() }}admin-logout" rel="nofollow">{{ Language::trans('Odjavi se') }}</a></span>
    </div>
</div> 

@include('shop/front_admin/modals/products')
@include('shop/front_admin/modals/product')
@endif

<div class="preheader">

    <div class="social-icons hidden hidden-sm hidden-xs">  
        {{Options::social_icon()}} 
    </div>

    <div class="container-fluid"> 
        <div class="row top-menu">

            <div class="col-md-1 col-sm-1 col-xs-2"> 
                @if(Options::checkB2B())
                <a href="{{Options::domain()}}b2b/login" class="center-block" rel="nofollow">B2B</a> 
                @endif 
            </div>  

            <div class="col-md-3 col-sm-12 col-xs-12 p-left hidden top-note">
                <a class="center-block un-link">Default welcome msg!</a>
            </div>

            <div class="col-md-12 col-sm-12 col-xs-12 text-right p-right JS-site-links resp-nav-order-4">     
                    
                @if(Options::stranice_count() > 0)
                <span class="JStoggle-btn inline-block text-white hidden">
                   <i class="fas fa-bars"></i>                 
                </span>
                @endif 

                <ul class="JStoggle-content inline-block">
                    @foreach(All::menu_top_pages() as $row)
                    <li><a class="center-block" href="{{ Options::base_url().Url_mod::page_slug($row->naziv_stranice)->slug }}">{{ Url_mod::page_slug($row->naziv_stranice)->naziv }}</a></li>
                    @endforeach
                    <li><a class="center-block" href="tel:{{Options::company_phone()}}">Pozovite nas</a></li>
                </ul>  

                <!-- LOGIN -->

                <div class="top-menu-login inline-block">
                    @if(Session::has('b2c_kupac'))
                    <div class="top-menu-login inline-block">
                        <a href="{{Options::base_url()}}{{Url_mod::slug_trans('korisnik')}}/{{Url_mod::slug_trans(WebKupac::get_user_name())}}" rel="nofollow">{{ WebKupac::get_user_name() }}</a>

                        <a href="{{Options::base_url()}}{{Url_mod::slug_trans('korisnik')}}/{{Url_mod::slug_trans(WebKupac::get_company_name())}}" rel="nofollow">{{ WebKupac::get_company_name() }}</a>
                    </div>

                        <a id="logout-button" class="inline-block" href="{{Options::base_url()}}logout" rel="nofollow">{{ Language::trans('Odjavi se') }}</a>

                    @else 
                        <div class="inline-block">
                            <button class="login-btn" type="button">
                                <span class="fas fa-sign-in-alt hidden"></span>  <a href="{{Options::base_url()}}{{ Url_mod::slug_trans('registracija') }}" rel="nofollow">{{ Language::trans('Moj nalog') }} </a>
                            </button>
                            <ul class="dropdown-menu login-dropdown hidden">
                                <!-- ====== LOGIN MODAL TRIGGER ========== -->
                                <li>
                                    <a href="#" data-toggle="modal" data-target="#loginModal" rel="nofollow">
                                    <i class="fas fa-user"></i> {{ Language::trans('Prijavi se') }}</a>
                                </li>
                                <li>
                                    <a href="{{Options::base_url()}}{{ Url_mod::slug_trans('registracija') }}" rel="nofollow"> 
                                    <i class="fas fa-user-plus"></i> {{ Language::trans('Registracija') }}</a>
                                </li>
                            </ul>
                        </div> 
                    @endif
                </div>
            </div>
 
        </div> 
    </div>
</div>
<!-- MENU_TOP.blade END -->



