<div class="col-md-3 col-sm-12 col-xs-12 sm-no-padd p-left resp-nav-order-3">

	<div class="JScategories relative"> 
		<?php $query_category_first=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>0,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc')->get(); ?>
		
		<h4 class="categories-title text-center text-white flex xs-hidden sm-hidden">
			<i class="fas fa-bars"></i>
			{{ Language::trans('Kategorije') }} 
			<span class="JSclose-nav hidden-md hidden-lg">&times;</span>
		</h4> 

		<!-- CATEGORIES LEVEL 1 -->

		<ul class="JSlevel-1">
			<!-- <li>
				<a href="javascript:void(0)">{{ Language::trans('Stranice') }}</a>

				<ul class="main-menu JSlevel-2">
                    @foreach(All::header_menu_pages() as $row)
                    <li>
                        @if(All::broj_cerki($row->web_b2c_seo_id) > 0)  
                        <a href="{{ Options::base_url().Url_mod::page_slug($row->naziv_stranice)->slug }}">{{Url_mod::page_slug($row->naziv_stranice)->naziv}}</a>
                        <ul class="drop-2">
                            @foreach(All::header_menu_pages($row->web_b2c_seo_id) as $row2)
                            <li> 
                                <a href="{{ Options::base_url().Url_mod::page_slug($row2->naziv_stranice)->slug }}">{{Url_mod::page_slug($row2->naziv_stranice)->naziv}}</a>
                                <ul class="drop-3">
                                    @foreach(All::header_menu_pages($row2->web_b2c_seo_id) as $row3)
                                    <li> 
                                        <a href="{{ Options::base_url().Url_mod::page_slug($row3->naziv_stranice)->slug }}">{{Url_mod::page_slug($row3->naziv_stranice)->naziv}}</a>
                                    </li>
                                    @endforeach
                                </ul>
                            </li>
                            @endforeach
                        </ul>
                        @else   
                        <a href="{{ Options::base_url().Url_mod::page_slug($row->naziv_stranice)->slug }}">{{Url_mod::page_slug($row->naziv_stranice)->naziv}}</a> 
                        @endif                    
                    </li>                     
                    @endforeach 

                    @if(Options::web_options(121)==1)
                    <?php $konfiguratori = All::getKonfiguratos(); ?>
                    @if(count($konfiguratori) > 0)
                    @if(count($konfiguratori) > 1)
                    <li>
                        
                        <a href="#!" rel="nofollow">{{Language::trans('Konfiguratori')}}</a>

                        <ul class="drop-2">
                            @foreach($konfiguratori as $row)
                            <li>
                                <a href="{{ Options::base_url() }}{{Url_mod::slug_trans('konfigurator')}}/{{ $row->konfigurator_id }}">{{ Language::trans($row->naziv) }}</a>
                            </li>
                            @endforeach
                        </ul>
                    </li>
                    @else
                    <li>
                        <a href="{{ Options::base_url() }}{{Url_mod::slug_trans('konfigurator')}}/{{ $konfiguratori[0]->konfigurator_id }}">
                            {{Language::trans('Konfigurator')}}
                        </a>
                    </li>
                    @endif
                    @endif
                    @endif
                </ul>   
			</li> -->

			@if(Options::category_type()==1) 
			@foreach ($query_category_first as $row1)
			@if(Groups::broj_cerki($row1->grupa_pr_id) >0)

			<li>
				<a href="{{ Options::base_url()}}{{ Url_mod::slug_trans($row1->grupa) }}" class="">
					@if(Groups::check_image_grupa($row1->putanja_slika))
					<span class="lvl-1-img-cont inline-block text-center hidden-sm hidden-xs"> 
						<img src="{{ Options::domain() }}{{$row1->putanja_slika}}" alt="{{ $row1->grupa }}" />
					</span>
					@endif
					{{ Language::trans($row1->grupa)  }} 
				</a>

				<?php $query_category_second=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row1->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc') ?>

				<ul class="JSlevel-2 row">
					<div class="row">
						<div class="col-xs-8 JSlevel-2-wrap">
							@foreach ($query_category_second->get() as $row2)
							<li>  
							<!-- <li class="col-md-6 col-sm-12 col-xs-12"> - MOVED TO JS  -->
		 
								<a href="{{ Options::base_url()}}{{ Url_mod::slug_trans($row1->grupa) }}/{{ Url_mod::slug_trans($row2->grupa) }}">
									@if(Groups::check_image_grupa($row2->putanja_slika))
									<span class="lvl-2-img-cont inline-block hidden-sm hidden-xs">
										<img src="{{ Options::domain() }}{{$row2->putanja_slika}}" alt="{{ $row2->grupa }}" />
									</span>
									@endif
									 
									{{ Language::trans($row2->grupa) }}
									 
								</a>
				   
								@if(Groups::broj_cerki($row2->grupa_pr_id) >0)
								<?php $query_category_third=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row2->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc')->get(); ?>
								
								<ul class="JSlevel-3">
									@foreach($query_category_third as $row3)
									
									<li>						 
										<a href="{{ Options::base_url()}}{{ Url_mod::slug_trans($row1->grupa) }}/{{ Url_mod::slug_trans($row2->grupa) }}/{{ Url_mod::slug_trans($row3->grupa) }}">{{ Language::trans($row3->grupa) }}</a>

		<!-- 								@if(Groups::broj_cerki($row3->grupa_pr_id) >0)
										<span class="JSCategoryLinkExpend hidden-sm hidden-xs">
											<i class="fas fa-chevron-down" aria-hidden="true"></i>
										</span>
										@endif   -->

										@if(Groups::broj_cerki($row3->grupa_pr_id) >0)
										<?php $query_category_forth=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row3->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc')->get(); ?>

										<ul class="JSlevel-4">
											@foreach($query_category_forth as $row4)
											<li>
												<a href="{{ Options::base_url()}}{{ Url_mod::slug_trans($row1->grupa) }}/{{ Url_mod::slug_trans($row2->grupa) }}/{{ Url_mod::slug_trans($row3->grupa) }}/{{ Url_mod::slug_trans($row4->grupa) }}">{{ Language::trans($row4->grupa) }}</a>
											</li>
											@endforeach
										</ul>
										@endif		
									</li>					 	
									@endforeach
								</ul>
								@endif
							</li>
							@endforeach
						</div>
				
							<div class="col-xs-4">
							<!-- HEADER BANNERS - category.blade-->
						    @if($baner = Slider::slajder($row1->baner_id) AND count($banerStavke = Slider::slajderStavke($row1->baner_id)) > 0)
							<div class="header-banner flex">
						    	@foreach(array_slice($banerStavke,0,2) as $banerStavka)
						    		<div class="header-banner-image-wrap">
										<div class="bg-img relative" style="background-image: url( '{{ Options::domain() }}{{ $banerStavka->image_path }}' )">
											<a href="{{ $banerStavka->link }}" class="slider-link"> </a> 
										</div>
									</div>
								@endforeach
							</div>
							@endif
						</div>
					</div>
				</ul>
			</li>

			@else

			<li>		 
				<a href="{{ Options::base_url()}}{{ Url_mod::slug_trans($row1->grupa) }}">		 
					@if(Groups::check_image_grupa($row1->putanja_slika))
					<span class="lvl-1-img-cont inline-block text-center hidden-sm hidden-xs"> 
						<img src="{{ Options::domain() }}{{$row1->putanja_slika}}" alt="{{ $row1->grupa }}" />
					</span>
					@endif
					{{ Language::trans($row1->grupa)  }}  				
				</a>			 
			</li>
			@endif
			@endforeach
			@else
			@foreach ($query_category_first as $row1)
			 

			@if(Groups::broj_cerki($row1->grupa_pr_id) >0)
			<a href="{{ Options::base_url()}}{{ Url_mod::slug_trans($row1->grupa) }}">{{ Language::trans($row1->grupa)  }}</a>
			<?php $query_category_second=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row1->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc') ?>

 
			@foreach ($query_category_second->get() as $row2)
			<li>
				<a href="{{ Options::base_url()}}{{ Url_mod::slug_trans($row1->grupa) }}/{{ Url_mod::slug_trans($row2->grupa) }}">
					{{ Language::trans($row2->grupa) }}
				</a>
				@if(Groups::broj_cerki($row2->grupa_pr_id) >0)
				<?php $query_category_third=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row2->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc')->get(); ?>
				<ul>
					@foreach($query_category_third as $row3)
					<a href="{{ Options::base_url()}}{{ Url_mod::slug_trans($row1->grupa) }}/{{ Url_mod::slug_trans($row2->grupa) }}/{{ Url_mod::slug_trans($row3->grupa) }}">{{ Language::trans($row3->grupa) }}
					</a>
					@if(Groups::broj_cerki($row3->grupa_pr_id) >0)
					<?php $query_category_forth=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row3->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc')->get(); ?>

					<ul>
						@foreach($query_category_forth as $row4)
						<a href="{{ Options::base_url()}}{{ Url_mod::slug_trans($row1->grupa) }}/{{ Url_mod::slug_trans($row2->grupa) }}/{{ Url_mod::slug_trans($row3->grupa) }}/{{ Url_mod::slug_trans($row4->grupa) }}">{{ Language::trans($row4->grupa) }}</a>
						@endforeach
					</ul>
					@endif	
					@endforeach
				</ul>
				@endif
			</li>
			@endforeach

			@else
				<li>
					<a href="{{ Options::base_url()}}{{ Url_mod::slug_trans($row1->grupa) }}">{{ Language::trans($row1->grupa) }}</a>
				</li>
			@endif 
			@endforeach				
			@endif

			@if(Options::all_category()==1)
			<li>
				<a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('sve-kategorije') }}">{{ Language::trans('Sve kategorije') }}</a>
			</li>
			@endif

		</ul>
	</div>
</div>
