<div class="col-md-3 col-sm-12 col-xs-12 resp-nav-order-2">
    <a class="responsive-header-cart-wrap relative" aria-label="cart button" href="{{ Options::base_url() }}{{ Url_mod::slug_trans('korpa') }}" rel="nofollow"></a>
	
	<div class="header-cart-container relative">  
		
		<a class="header-cart inline-block text-center relative" aria-label="cart button" href="{{ Options::base_url() }}{{ Url_mod::slug_trans('korpa') }}" rel="nofollow">
			
			<!-- <i class="fas fa-shopping-cart"></i>		 -->

			<span class="sprite sprite-cart"></span>
			
			<!-- <span class="JScart_num badge"> {{ Cart::broj_cart() }} </span> 		 -->
			
			<!-- <input type="hidden" id="h_br_c" value="{{ Cart::broj_cart() }}" />	 -->
		</a>

		<span class="top-cart-info"><div class="JSmini-cart-sum inline-block">{{ Cart::broj_cart() }}</div> {{ Language::trans('Artikla') }} / <div class="mini-cart-amount inline-block">{{ Cart::cena(Cart::cart_ukupno()) }}</div></span>

		<div class="JSheader-cart-content hidden-sm hidden-xs">
			@include('shop/themes/'.Support::theme_path().'partials/mini_cart_list') 
		</div>
	</div>
</div> 