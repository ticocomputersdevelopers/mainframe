<?php // echo $kara; ?>
<div class="side-content-box ">
	<h2>{{ Language::trans('Filteri') }}</h2>
@if(isset($niz_proiz))
	
	<div class="filters side-content-inner"> 
		<!-- SLAJDER ZA CENU -->
		<div class="filters-price-bar relative"> 
			<div class="filter-price-bar-label"><span>{{ Language::trans('Cijena') }}</span></div>
			<!-- <span class="">{{Articles::get_valuta()}}</span> -->
			<div class="flex filter-price-wrap">
				<em class="fas fa-chevron-right"></em>
				<span id="JSamount" class="filter-price inline-block"></span>
			</div>
			<div id="JSslider-range"></div><br>  
		</div>
		<!-- <span type="hidden" id="JSExcange" value= "{{Articles::get_valuta()}}"> </span> -->
	
		<div class="filters-selected-box">  
			<div class="clearfix JShidden-if-no-filters">
				<span>{{ Language::trans('Izabrani filteri') }}:</span>
				<a class="JSreset-filters-button inline-block pull-right" role="button" href="javascript:void(0)" rel=”nofollow”>{{ Language::trans('Poništi filtere') }} <span class="fas fa-times"></span></a>
			</div> 

			<ul class="selected-filters">
				<?php $br=0;
				foreach($niz_proiz as $row){
					$br+=1;
					if($row>0){
						?>
						<li>
							{{All::get_manofacture_name($row)}}     
							<a href="javascript:void(0)" rel=”nofollow”>
								<span class="JSfilter-close fas fa-times" data-type="1" data-rb="{{$br}}" data-element="{{$row}}"></span>
							</a>
						</li>
				<?php }}

				$br=0;
				foreach($niz_karakteristike as $row){
				$br+=1;
				if($row>0){
					?>
					<li>
						{{All::get_fitures_name($row)}}
						<a href="javascript:void(0)" rel=”nofollow”>
							<span class="JSfilter-close fas fa-times" data-type="2" data-rb="{{$br}}" data-element="{{$row}}"></span>
						</a>
					</li>
				<?php }}
				if($strana == 'pretraga'){
				foreach($niz_grupa as $row){
				$br+=1;
				if($row>0){
					?>
					<li>
						{{Groups::getGrupa($row)}}
						<a href="javascript:void(0)" rel=”nofollow”>
							<span class="JSfilter-close fas fa-times" data-type="2" data-rb="{{$br}}" data-element="{{$row}}"></span>
						</a>
					</li>
				<?php }}}?>
			</ul>
				<input type="hidden" id="JSmanufacturer_ids" value="{{$proizvodjac_ids}}"/>
				<input type="hidden" id="JSmanufacturer_attr" value="{{$kara}}"/>	
				@if($strana == 'pretraga')	
				<input type="hidden" id="JSGroups_ids" value="{{$grupa_ids}}"/>
				@endif

		</div>	 
	 		<ul class="">   
				@if(count($manufacturers)>0)
				<li>
					<a href="javascript:void(0)" class="filter-links center-block JSfilters-slide-toggle" rel=”nofollow”>{{ Language::trans('Proizvođač') }}
						<!-- <i class="fas fa-angle-down pull-right"></i> -->
					</a>

					<!-- JSfilters-slide-toggle-content -->
					<div class="">
						@foreach($manufacturers as $key => $value)
						@if(in_array($key,$niz_proiz))
							<label class="flex">
								<i class="fas fa-chevron-right"></i>
								<input class="filters-checkbox-main" name="proizvodjac[]" onchange="filters_send()" value="{{ $key }}" type="checkbox" checked>
								<span class="filter-text center-block">
									{{All::get_manofacture_name($key)}} 
								</span>
								<span class="filter-count">
									@if(Options::filters_type()) ({{ $value }}) @endif
								</span>
							</label>
						@else
							<label class="flex">
								<i class="fas fa-chevron-right"></i>
								<input class="filters-checkbox-main" name="proizvodjac[]" onchange="filters_send()" value="{{ $key }}" type="checkbox">
								<span class="filter-text center-block">
									{{All::get_manofacture_name($key)}} 
								</span>
								<span class="filter-count">
									@if(Options::filters_type()) ({{ $value }}) @endif
								</span>
							</label>
						@endif
						@endforeach
					</div>				 				 
				</li>
				@endif				
				@if($characteristics > 0)
					@foreach($characteristics as $keys => $values)				
					<li>
						<a href="javascript:void(0)" class="filter-links center-block JSfilters-slide-toggle" rel=”nofollow”>
							{{Language::trans($keys)}} 
							<!-- <i class="fas fa-angle-down pull-right"></i> -->  							
						</a>

						<!-- JSfilters-slide-toggle-content -->
						<div class="">
							@foreach($values as $key => $value)
								<?php if(in_array($key, $niz_karakteristike)){ ?>
									<label class="flex">
										<i class="fas fa-chevron-right"></i>
										<input class="filters-checkbox-main" type="checkbox" onchange="filters_send()" name="vrednost[]" value="{{ $key }}" checked>
										<span class="filter-text center-block">
											{{All::get_fitures_name($key)}}
										</span>
										<span class="filter-count">
											@if(Options::filters_type()) ({{ $value }}) @endif
										</span>
									</label>
								<?php }else {?>
									<label class="flex">
										<i class="fas fa-chevron-right"></i>
										<input class="filters-checkbox-main" type="checkbox" onchange="filters_send()" name="vrednost[]" value="{{ $key }}"> 
										<span class="filter-text center-block">
											{{All::get_fitures_name($key)}}
										</span>
										<span class="filter-count">
											@if(Options::filters_type()) ({{ $value }}) @endif
										</span>
									</label>
								<?php } ?>
							@endforeach
						</div>
					</li>
					@endforeach	
				@endif		
			</ul>	 

			<input type="hidden" id="JSfilters-url" value="{{$url}}" />


		@if($strana == 'pretraga')
			<div class="manufacturer-categories">  
				<ul>
					@foreach($grupe as $key => $value)
					<li>
						<a href="#" data-key="{{ $key }}" class="JSGrupa_id">{{ Language::trans(Groups::getGrupa($key) ) }}</a>
					</li>
					@endforeach
				</ul>
			</div>	
		@endif

			<script>
				var max_web_cena_init = {{ $max_web_cena }};
				var min_web_cena_init = {{ $min_web_cena }};
				var max_web_cena = {{ $cene!='0-0' ? explode('-',$cene)[1] : $max_web_cena }};
				var min_web_cena = {{ $cene!='0-0' ? explode('-',$cene)[0] : $min_web_cena }};
				var kurs = {{ Session::has('valuta') ? (Session::get('valuta')!="2" ? 1 : Options::kurs()) : 1 }};
			</script> 			 
				 
	</div>

			
@endif
			
</div>

