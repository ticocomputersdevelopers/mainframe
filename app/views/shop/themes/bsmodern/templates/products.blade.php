<!DOCTYPE html>
<html lang="sr">
<head>
	@include('shop/themes/'.Support::theme_path().'partials/head')

	<script type="application/ld+json">
		{
			"@context": "http://schema.org",
			"@type": "WebPage",
			"name": "<?php echo $title; ?>",
			"description": "<?php echo $description; ?>",
			"url" : "<?php echo Options::base_url().$url; ?>",
			"publisher": {
			"@type": "Organization",
			"name": "<?php echo Options::company_name(); ?>"
		}
	}
</script>
</head>
<body id="product-page" 
@if($strana == All::get_page_start()) id="start-page"  @endif 
@if(Support::getBGimg() != null) 
style="background-image: url({{Options::domain()}}{{Support::getBGimg()}}); background-size: cover; background-repeat: no-repeat; background-attachment: fixed; background-position: center;" 
@endif
>


@include('shop/themes/'.Support::theme_path().'partials/menu_top')
 
@include('shop/themes/'.Support::theme_path().'partials/header')


<!-- PRODUCTS.blade -->
<main class="d-content JSmain relative"> 
	<div class="container"> 
		<div class="row">  

			<div class="col-md-3 col-sm-12 col-xs-12 p-left side-content">  
				@if((!empty($baner_id) AND $banner = Slider::slajder($baner_id) AND count($bannerStavke = Slider::slajderStavke($banner->slajder_id)) > 0) AND !empty($bannerStavka = $bannerStavke[0]))
				<!-- BANNERS SECTION -->
				<div class="banners row wild-banners">
					<div class="col-md-12 col-sm-12 col-xs-12 no-padding">
						<a class="center-block" href="{{ $bannerStavka->link }}">
							<img class="img-responsive margin-auto" src="{{ Options::domain() }}{{ $bannerStavka->image_path }}" alt="{{$bannerStavka->alt}}" />
						</a>

						@if($bannerStavka->sadrzaj != '') 
						<div class="sliderText short-desc JSInlineFull content hidden-xs" data-target='{"action":"slide_content","id":"{{$bannerStavka->slajder_stavka_id}}"}'>
							{{ $bannerStavka->sadrzaj }}
						</div> 
						@endif
					</div>
				</div>
				@endif

				@if($filter_prikazi == 0)
					@if($strana == 'proizvodjac')
						@include('shop/themes/'.Support::theme_path().'partials/categories/manufacturer_category')
					@elseif($strana == 'tip')
						@include('shop/themes/'.Support::theme_path().'partials/categories/tip_category')
					@elseif($strana == 'akcija')
						@include('shop/themes/'.Support::theme_path().'partials/categories/akcija_category')
					@endif
				@endif

				@if(Options::enable_filters()==1 AND $filter_prikazi)
				<!-- ALERT THIS NEEDS TO BE FIXED -->
					@include('shop/themes/'.Support::theme_path().'partials/products/filters')
				@endif

				@include('shop/themes/'.Support::theme_path().'partials/side_content_main')
			</div>

 			<div class="col-md-9 col-sm-12 col-xs-12 product-page sm-order-0">
 				<h2></h2>

 				@if($strana!='akcija' and $strana!='tip')
				<ul class="breadcrumb">
					@if($strana!='pretraga' and $strana!='filteri' and $strana!='proizvodjac')
					{{ Url_mod::breadcrumbs2()}}
					@elseif($strana == 'proizvodjac')
					<li>
						<a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('brendovi') }}">{{ Language::trans('Brendovi') }}</a>  
						@if($grupa)
						<a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('proizvodjac') }}/{{ Url_mod::slug_trans($proizvodjac) }}">{{ All::get_manofacture_name($proizvodjac_id) }}</a>
						{{ Groups::get_grupa_title($grupa) }}
						@else
						{{ All::get_manofacture_name($proizvodjac_id) }} 
						@endif
					</li>
					@else					
					<li><a href="{{ Options::base_url() }}">{{ All::get_title_page_start() }}</a></li>
					<li>{{$title}}</li>
					@endif
				</ul>
				@endif 

 				<!-- SLIDER PRODUCT GRID/LIST - products.blade -->
				@if(!empty($slajder_id) AND $slajder = Slider::slajder($slajder_id) AND count($slajderStavke = Slider::slajderStavke($slajder->slajder_id)) > 0)
					<div class="bw JScategory-slider">
					    @foreach($slajderStavke as $slajderStavka)
					    <div class="relative">
							<a href="{{ $slajderStavka->link }}">
								<img class="img-responsive" src="{{ Options::domain() }}{{ $slajderStavka->image_path }}" alt="{{$slajderStavka->alt}}" />
							</a>

							<div class="sliderText"> 
								<!-- @if($slajderStavka->naslov != '') 
								<div class="main-desc JSInlineShort" data-target='{"action":"slide_title","id":"{{$slajderStavka->slajder_stavka_id}}"}'>
									{{ $slajderStavka->naslov }}
								</div> 
								@endif -->

								@if($slajderStavka->sadrzaj != '') 
								<div class="short-desc JSInlineFull" data-target='{"action":"slide_content","id":"{{$slajderStavka->slajder_stavka_id}}"}'>
									{{ $slajderStavka->sadrzaj }}
								</div> 
								@endif

								<!-- @if($slajderStavka->naslov_dugme != '') 
								<a href="{{ $slajderStavka->link }}" class="JSInlineShort" data-target='{"action":"slide_button","id":"{{$slajderStavka->slajder_stavka_id}}"}'>
									{{ $slajderStavka->naslov_dugme }}
								</a> 
								@endif -->
							</div>
					    </div>
					  	@endforeach
					</div>
				@endif

				@if($strana!='pretraga' and $strana!='filteri' and $strana!='proizvodjac' and $strana != 'tagovi')
					@if(isset($sub_cats) and !empty($sub_cats))
					<div class="row sub-group hidden">

						@foreach($sub_cats as $row)
						<div class="col-md-3 col-sm-3 col-xs-12 no-padding">
							<a class="flex" href="{{ Options::base_url()}}{{ $url }}/{{ Url_mod::slug_trans($row->grupa) }}">
								
								@if(isset($row->putanja_slika))
									<img src="{{ Options::domain() }}{{ $row->putanja_slika }}" alt="{{ $row->grupa }}" class=" img-responsive" />
						 		@endif
								
								<span>{{ $row->grupa }}</span>
							</a>
						</div>
						@endforeach

					</div>
					@endif
				@endif
				
				@yield('products_list')

			</div>

		</div>
	</div>
</main>
<!-- PRODUCTS.blade END -->

@include('shop/themes/'.Support::theme_path().'partials/footer')
 
@include('shop/themes/'.Support::theme_path().'partials/popups')


<!-- BASE REFACTORING -->
<input type="hidden" id="base_url" value="{{Options::base_url()}}" />
<input type="hidden" id="in_stock" value="{{Options::vodjenje_lagera()}}" />
<input type="hidden" id="elasticsearch" value="{{ Options::gnrl_options(3055) }}" />

<!-- js includes -->
@if(Session::has('b2c_admin'.Options::server()) OR (Options::enable_filters()==1 AND $filter_prikazi))
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
@endif
@if(Session::has('b2c_admin'.Options::server()))
<script src="{{Options::domain()}}js/tinymce_5.1.3/tinymce.min.js"></script> 
<script src="{{Options::domain()}}js/jquery-ui.min.js"></script>
<script src="{{Options::domain()}}js/shop/front_admin/main.js"></script>
<script src="{{Options::domain()}}js/shop/front_admin/products.js"></script>
<script src="{{Options::domain()}}js/shop/front_admin/product.js"></script>
@endif

<script src="{{Options::domain()}}js/slick.min.js"></script>
<script src="{{Options::domain()}}js/shop/translator.js"></script>

@if(Config::get('app.livemode'))
	<script src="{{Options::domain()}}js/shop/themes/{{Support::theme_path()}}shop.min.js"></script>
@else
	<script src="{{Options::domain()}}js/shop/themes/{{Support::theme_path()}}main.js"></script>
	<script src="{{Options::domain()}}js/shop/themes/{{Support::theme_path()}}main_function.js"></script>
	<script src="{{Options::domain()}}js/shop/themes/{{Support::theme_path()}}cart.js"></script>

	@if(Options::header_type()==1)
	<script src="{{Options::domain()}}js/shop/themes/{{Support::theme_path()}}fixed_header.js"></script>
	@endif
@endif

</body>
</html>
