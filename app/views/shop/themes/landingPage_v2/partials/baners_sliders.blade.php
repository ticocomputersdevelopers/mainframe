
@if($firstSlider = Slider::getFirstSlider() AND count($slajderStavke = Slider::slajderStavke($firstSlider->slajder_id)) > 0)
<div id="JSmain-slider" class="bw">
    @foreach($slajderStavke as $slajderStavka)
    <div class="relative">

        <a class="slider-link" href="{{ $slajderStavka->link }}"></a>

        <div class="bg-img" style="background-image: url( '{{ Options::domain() }}{{ $slajderStavka->image_path }}' )"></div>

        <div class="sliderText text-center"> 

            @if($slajderStavka->naslov != '') 
            <h2 class="JSInlineShort" data-target='{"action":"slide_title","id":"{{$slajderStavka->slajder_stavka_id}}"}'>
                {{ $slajderStavka->naslov }}
            </h2> 
            @endif

            @if($slajderStavka->sadrzaj != '') 
            <div class="JSInlineFull" data-target='{"action":"slide_content","id":"{{$slajderStavka->slajder_stavka_id}}"}'>
                {{ $slajderStavka->sadrzaj }}
            </div> 
            @endif

            @if($slajderStavka->naslov_dugme != '') 
                <a href="{{ $slajderStavka->link }}" class="slider-btn-link inline-block JSInlineShort" data-target='{"action":"slide_button","id":"{{$slajderStavka->slajder_stavka_id}}"}'>{{ $slajderStavka->naslov_dugme }}</a> 
            @endif
        </div>
    </div>
    @endforeach
</div>
@endif