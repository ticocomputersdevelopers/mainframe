
<section id="main-content">
	@include('crm/partials/crm_tabs')
	
		<div class="columns medium-4 anly-box">
			<div class="flat-box">
				<ul class="anly-ul">
					<div class="">
					<h2 class="anly-ul__li__count">{{ AdminLanguage::transAdmin('Opšta analitika') }}</h2>
					</div>

					@if(isset($ukupno))
					<li class="anly-ul__li">
						<span class="anly-ul__li__text">{{ AdminLanguage::transAdmin('Ukupno leadova') }}</span>
						<span class="anly-ul__li__count">{{$ukupno}}</span>	
					</li>
					@else
					<li class="anly-ul__li">
						<span class="anly-ul__li__text">{{ AdminLanguage::transAdmin('Ukupno leadova') }}</span>
						<span class="anly-ul__li__count">{{AdminCrm::getUkupnoLeadova()}}</span>
					</li>
					@endif
					@if(isset($ukupno_akcija))
					<li class="anly-ul__li">
						<span class="anly-ul__li__text">{{ AdminLanguage::transAdmin('Ukupno akcija') }}</span>
						<span class="anly-ul__li__count">{{$ukupno_akcija}}</span>	
					</li>
					@else
					<li class="anly-ul__li">
						<span class="anly-ul__li__text">{{ AdminLanguage::transAdmin('Ukupno akcija') }}</span>
						<span class="anly-ul__li__count">{{AdminCrm::getUkupnoAkcija()}}</span>
					</li>
					@endif

					@if(isset($zavrsen_lead))
					<li class="anly-ul__li">
						<span class="anly-ul__li__text">{{ AdminLanguage::transAdmin('Završen lead') }}</span>
						<span class="anly-ul__li__count">{{$zavrsen_lead}}</span>						
					</li>
					@else
					<li class="anly-ul__li">
						<span class="anly-ul__li__text">{{ AdminLanguage::transAdmin('Završen lead') }}</span>
						<span class="anly-ul__li__count">{{AdminCrm::getZavrsenLead()}}</span>
					</li>
					@endif
					@if(isset($uspesno_zavrsen))
					<li class="anly-ul__li">
						<span class="anly-ul__li__text">{{ AdminLanguage::transAdmin('Sklopljen ugovor') }}</span>
						<span class="anly-ul__li__count">{{$uspesno_zavrsen}}</span>	
					</li>
					@else
					<li class="anly-ul__li">
						<span class="anly-ul__li__text">{{ AdminLanguage::transAdmin('Sklopljen ugovor') }}</span>
						<span class="anly-ul__li__count">{{AdminCrm::getUspesnoZavrsenLead()}}</span>
					</li>
					@endif
					@if(isset($zavrsene_akcije))
					<li class="anly-ul__li">
						<span class="anly-ul__li__text">{{ AdminLanguage::transAdmin('Završene akcije') }}</span>
						<span class="anly-ul__li__count">{{$zavrsene_akcije}}</span>	
					</li>
					@else
					<li class="anly-ul__li">
						<span class="anly-ul__li__text">{{ AdminLanguage::transAdmin('Završene akcije') }}</span>
						<span class="anly-ul__li__count">{{AdminCrm::getZavrseneAkcije()}}</span>
					</li>
					@endif					
				</ul>
			</div>
		



	<div class="flat-box">
		<div class="anly-box"> 
				<div class="">
					<h2 class="anly-ul__li__count">{{ AdminLanguage::transAdmin('Prihodi u dinarima') }}</h2>
				</div>
				
					<ul class="anly-ul">
					<!-- <li class="anly-ul__li">
						<span class="anly-ul__li__text">{{ AdminLanguage::transAdmin('Jučerašnji prihod') }}</span>
						<span class="anly-ul__li__count">{{ number_format(AdminCrm::jucerasnjiPrihodCrm(), 2, ',', ' ')  }} din</span>
					</li> -->
					<li class="anly-ul__li">
						<span class="anly-ul__li__text">{{ AdminLanguage::transAdmin('Proteklih mesec dana') }}</span>
						<span class="anly-ul__li__count">{{ number_format(AdminCrm::mesecniPrihodCrmDin(), 2, ',', ' ')  }} din</span>
					</li>
					<li class="anly-ul__li">
						<span class="anly-ul__li__text">{{ AdminLanguage::transAdmin('Ukupan prihod u dinarima') }}</span>
						<span class="anly-ul__li__count">{{ 
						number_format(AdminCrm::ukupanPrihodCrmDin(), 2, ',', ' ')  }} din</span>
					</li>
					
				</ul>
			
		</div>
		<div class="anly-box">				
					<div class="">
						<h2 class="anly-ul__li__count">{{ AdminLanguage::transAdmin('Prihodi u evrima') }}</h2>
					</div>

			<div class="flat-box">
				<ul class="anly-ul">
					<!-- <li class="anly-ul__li">
						<span class="anly-ul__li__text">{{ AdminLanguage::transAdmin('Jučerašnji prihod') }}</span>
						<span class="anly-ul__li__count">{{ number_format(AdminCrm::jucerasnjiPrihodCrm(), 2, ',', ' ')  }} din</span>
					</li> -->
					<li class="anly-ul__li">
						<span class="anly-ul__li__text">{{ AdminLanguage::transAdmin('Proteklih mesec dana') }}</span>
						<span class="anly-ul__li__count">{{ number_format(AdminCrm::mesecniPrihodCrmEvr(), 2, ',', ' ')  }} €</span>
					</li>
					<li class="anly-ul__li">
						<span class="anly-ul__li__text">{{ AdminLanguage::transAdmin('Ukupan prihod u evrima') }}</span>
						<span class="anly-ul__li__count">{{ 
						number_format(AdminCrm::ukupanPrihodCrmEvr(), 2, ',', ' ')  }} €</span>
					</li>
				</ul>
			</div>
		</div>
			<div class="anly-box">
				<div class="flat-box h1-title">
					<ul class="anly-ul">	
						<li class="anly-ul__li">
							<span class="anly-ul__li__text"><h2 class="anly-ul__li__count">{{ AdminLanguage::transAdmin('Ukupno') }} </h2></span>
							<span class="anly-ul__li__count">{{ number_format(AdminCrm::ukupanPrihodCrmDin() + (AdminCrm::mesecniPrihodCrmEvr()*AdminCrm::evroDanas()))  }} din</span>
						</li>		
					</ul>
				</div>
			</div>
	
	</div>
</div>
		<div class="columns medium-4 anly-box">
			<div class="flat-box column-left">
				<ul class="anly-ul">
					<li class="anly-ul__li"> 
						<div class="column medium-5">
							<label for="">{{ AdminLanguage::transAdmin('Datum Od') }}</label>
							<input id="datum_od_crm" class="datum-val has-tooltip" name="datum_od_crm" type="text" value="{{$od}}">
						</div>
						<div class="column medium-5">
							<label for="">{{ AdminLanguage::transAdmin('Datum Do') }}</label>
							<input id="datum_do_crm" class="datum-val has-tooltip" name="datum_do_crm" type="text" value="{{$do}}">
						</div>
						<div class="column medium-2 no-padd">
							<label for="">&nbsp;</label>
							<a class="btn btn-danger fr" href="/admin/crm_analitika">{{ AdminLanguage::transAdmin('Poništi') }}</a>
						</div> 
					</li>
				</ul>
			</div>

			<div class="flat-box"> 
					<div class="column large-12">
						<h2 class="anly-ul__li__count">{{ AdminLanguage::transAdmin('Lead sa najvećim brojem akcija') }}</h2>

					</div>
					<div class="additional_details_second">
						<table class="analitics-table">
							<thead>
								<th>&nbsp;{{ AdminLanguage::transAdmin('Naziv') }}</th>
								<th></th>
								<th></th>
								<th></th>
								<th>{{ AdminLanguage::transAdmin('Broj akcija') }}</th>
							</thead>
							<tbody>

								@foreach($crm as $row)
								@if($row->count > 0)
								<tr>
									<td class="first-td">{{AdminCrm::nazivCrmPartnera($row->crm_id)}}</td>
									<td>{{'('}} {{ AdminCrm::getTipNaziv($row->crm_tip_id) }} {{')'}}</td>
									<td> </td>
									<td> </td>
									<td class="alertify-inner"> {{ $row->count + 0 }} </td> 
								</tr>
								@endif
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			
			<div class="flat-box">		
				<div class="column large-12">
					<h2 class="anly-ul__li__count">{{ AdminLanguage::transAdmin('Najčešce korišćene akcije') }}</h2>
					<div class="additional_details_second">
						<table class="analitics-table">
							<thead>
								<th>&nbsp;{{ AdminLanguage::transAdmin('Naziv') }}</th>
								<th></th>
								<th></th>
								<th>{{ AdminLanguage::transAdmin('Broj akcija') }}</th>
							</thead>
							<tbody>

								@foreach($akcije as $row)
								@if($row->count > 0)
								<tr>
									<td class="first-td">{{AdminCrm::getAkcija($row->crm_akcija_tip_id)}}</td>
									<td> </td>
									<td> </td>
									<td class="alertify-inner"> {{ $row->count + 0 }} </td> 
								</tr>
								@endif
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="flat-box">		
				<div class="column large-12">
					<h2 class="anly-ul__li__count">{{ AdminLanguage::transAdmin('Najčešci tipovi') }}</h2>
					<div class="additional_details_second">
						<table class="analitics-table">
							<thead>
								<th>&nbsp;{{ AdminLanguage::transAdmin('Naziv') }}</th>
								<th></th>
								<th></th>
								<th>{{ AdminLanguage::transAdmin('Broj tipova') }}</th>
							</thead>
							<tbody>

								@foreach($tipovi as $row)
								@if($row->count > 0)
								<tr>
									<td class="first-td">{{AdminCrm::getTipNaziv($row->crm_tip_id)}}</td>
									<td> </td>
									<td> </td>
									<td class="alertify-inner"> {{ $row->count + 0 }} </td> 
								</tr>
								@endif
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		

	


	<div class="columns medium-4">
	
		<div class="flat-box"> 
				<div class="anly-box">
					<h2 class="anly-ul__li__count">{{ AdminLanguage::transAdmin('Komercijaliste') }}</h2>
				</div>
				<table>
					<thead>
						<tr>
						<th>{{ AdminLanguage::transAdmin('Ime') }}</th>
						<th>{{ AdminLanguage::transAdmin('Naziv lead-a ') }}</th>
						<th>{{ AdminLanguage::transAdmin('Prihod') }}</th>
						</tr>
					</thead>
					<tbody>
						
						@foreach($komercijaliste as $row)
						<tr>
							<td>{{$row->ime}}</td>
							<td>{{$row->naziv}}{{'('}}{{ AdminCrm::getTipNaziv($row->crm_tip_id) }}{{')'}} </td>
							<td>{{$row->vrednost}} @if($row->kursna_lista_id == 1) {{ AdminLanguage::transAdmin('RSD') }} @else {{ AdminLanguage::transAdmin('€') }} @endif</td>
						</tr>
						@endforeach
					</tbody>
				</table>
		</div>

	
	<div class="flat-box">		
				<div class="anly-box">
					<h2 class="anly-ul__li__count">{{ AdminLanguage::transAdmin('Komercijaliste i broj leadova') }}</h2>
					<div class="">
						<table class="analitics-table">
							<thead>
								<th>&nbsp;{{ AdminLanguage::transAdmin('Ime') }}</th>
								<th></th>
								<th></th>
								<th>{{ AdminLanguage::transAdmin('Broj leadova') }}</th>
							</thead>
							<tbody>

								@foreach($brojKomercijalista as $row)
								@if($row->count > 0)
								<tr>
									<td class="first-td">{{AdminCrm::getNazivJednogKomercijaliste($row->imenik_id)}}</td>
									<td> </td>
									<td> </td>
									<td class="alertify-inner"> {{ $row->count + 0 }} </td> 
								</tr>
								@endif
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="flat-box">		
				<div class="anly-box">
					<h2 class="anly-ul__li__count">{{ AdminLanguage::transAdmin('Komercijaliste ukupan prihod') }}</h2>
					<div class="">
						<table class="analitics-table">
							<thead>
								<th>&nbsp;{{ AdminLanguage::transAdmin('Ime') }}</th>
								<th></th>
								<th></th>
								<th>{{ AdminLanguage::transAdmin('Broj leadova') }}</th>
							</thead>
							<tbody>
								<tr>
									
								@foreach($komercUkupanPrihod as $row)
									<td class="first-td">{{ $row->ime}}</td>
									<td> </td>
									<td> </td>
									<td class="alertify-inner"> {{ $row->vrednost}} </td> 
								</tr>	
								@endforeach
								

								
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="flat-box">		
				<div class="column large-12">
					<h2 class="anly-ul__li__count">{{ AdminLanguage::transAdmin('Najčešci statusi') }}</h2>
					<div class="additional_details_second">
						<!-- <select class="JSStatusIdValue search_select">
							<option value="0">{{ AdminLanguage::transAdmin('Izaberi status') }}</option>
						@foreach($status as $row)
							<option data-e-selected="{{ $row->crm_status_id }}" value="{{ $row->crm_status_id }}"> {{ $row->naziv }} </option>
						@endforeach
						</select> -->
					
						<table class="analitics-table">
							<thead>
								<th>&nbsp;{{ AdminLanguage::transAdmin('Naziv') }}</th>
								<th></th>
								<th></th>
								<th>{{ AdminLanguage::transAdmin('Broj statusa') }}</th>
							</thead>
							<tbody>

								@foreach($status as $row)
								@if($row->count > 0)
								<tr>
									<td class="first-td">{{AdminCrm::getStatusNaziv($row->crm_status_id)}}</td>
									<td> </td>
									<td> </td>
									<td class="alertify-inner"> {{ $row->count + 0 }} </td> 
								</tr>
								@endif
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		
</div>
</div>
		</section>
		