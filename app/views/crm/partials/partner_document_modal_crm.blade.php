

<div class="row">
			<div class="large-7 medium-7 small-7 columns large-centered medium-centered">
				<div class="flat-box">
				<h3 class="title-med">{{ AdminLanguage::transAdmin('Dodaj novi fajl') }}</h3>
				<form method="POST" action="{{AdminOptions::base_url()}}crm/partner_document_save" enctype="multipart/form-data">
					<input type="hidden" name="crm_id" value="{{ $crm_id }}">
					<input type="hidden" name="crm_dokumenta_id" value="">

					<div class="row"> 
						<div class="column medium-12 field-group">
							<label>{{ AdminLanguage::transAdmin('Naziv') }}</label>
							<input type="text" name="naziv" value="{{ Input::old('naziv') != null ?  Input::old('naziv') : null }}">
						</div>
					</div>

					<div class="row"> 
						
						 <div class="column medium-6 field-group">
							<label>{{ AdminLanguage::transAdmin('Vrsta fajla') }}</label>
							<select name="vrsta_fajla_id">
								@foreach(DB::table('vrsta_fajla')->orderBy('vrsta_fajla_id','asc')->get() as $row)
									@if($row->vrsta_fajla_id == Input::old('vrsta_fajla'))
									<option value="{{ $row->vrsta_fajla_id }}" selected>{{ $row->ekstenzija }}</option>
									@else
									<option value="{{ $row->vrsta_fajla_id }}">{{ $row->ekstenzija }}</option>
									@endif
								@endforeach
							</select>
						</div>
					</div>

					<div class="row"> 
						<div class="column"> 
							<div class="field-group" >
								<label>{{ AdminLanguage::transAdmin('Putanja (samo ukoliko je u pitanju link)') }}</label>
								<input type="text" name="putanja" value="">
							</div>

							<div  class="field-group">
								<div class="bg-image-file"> 
									<input type="file" name="upl_file"> 
								</div>
							</div>
							<?php $date=date('Y-m-d H:m:s');?>
							<input type="hidden" name="datum_kreiranja" value="{{$date}}">
						</div>
					</div>
					<div class="row"> 
						<div class="btn-container text-center"> 
							<button type="submit" class="btn btn-primary save-it-btn">{{ AdminLanguage::transAdmin('Sačuvaj') }}</button>
						</div>
					</div>
				</form>

				</div>
			</div>
		</div>