
@if(Session::has('message'))
	<script>
		alertify.success('{{ Session::get('message') }}');
	</script>
@endif
<br>
<div class="additional_details">
	<h3 class="title-med">{{ AdminLanguage::transAdmin('Osnovni kontakti')}}</h3>
	<table class='additional_table'>
		<tbody>
			<tr>
				<td><b>{{ AdminLanguage::transAdmin('Partner:')}}</b></td>
				<td>{{ $partner->naziv }}</td>
			</tr>
			<tr>
				<td><b>{{ AdminLanguage::transAdmin('Adresa:')}} </b></td>
				<td>{{ $partner->adresa }}</td>
			</tr>
			<tr>
				<td><b>{{ AdminLanguage::transAdmin('Kontakt osoba:')}} </b></td>
				<td>{{ $partner->kontakt_osoba }}</td>
			</tr>
			<tr>
				<td><b>{{ AdminLanguage::transAdmin('Mejl:')}}</b></td>
				<td><a href="mailto:{{$partner->mail}}?subject={{ 'ponuda' }}&body=Poštovani/na {{ $partner->kontakt_osoba }},">{{$partner->mail}}</a></td>
			</tr>
			<tr>
				<td><b>{{ AdminLanguage::transAdmin('Broj telefona:')}} </b></td>
				<td>{{ $partner->telefon }}</td>
			</tr>
		</tbody>
	</table>
</div> 


@if(count($dodatni_podaci)>0)
<div class="additional_contact_details"> 
	<h3 class="title-med">{{ AdminLanguage::transAdmin('Dodatni kontakti')}}</h3>
	<div class="table-scroll">
		<form method="POST" action="{{ AdminOptions::base_url() }}crm/crm_dodatni_kontakti" enctype="multipart/form-data">
		<table class='additional_table'>
			<table>								
				<tr>
					
					<th>{{ AdminLanguage::transAdmin('Osoba:')}}</th>
					<th>{{ AdminLanguage::transAdmin('Tip:')}}</th>  
					<th>{{ AdminLanguage::transAdmin('Vrednost:')}}</th> 
					<th>{{ AdminLanguage::transAdmin('Napomena:')}}</th>
					<th></th>
					<th></th> 					
				</tr>
							

				<tbody>
									
				@foreach($all as $row)
					<tr>
					@if($row->partner_id == $partnerid)
						<input type="hidden" value="{{$row->partner_kontakt_id}}" name= "partner_kontakt_id">
						<td><input type='text' value="{{Input::old('osoba') ? Input::old('osoba') : $row->osoba }}"  name='osoba'></td>
						<td>
						<select class="JSContactType" name='vrsta_kontakta_id'>
							@foreach(DB::table('vrsta_kontakta')->orderBy('vrsta_kontakta_id','asc')->get() as $vrsta)	
								<option  e-selected = "{{ $vrsta->vrsta_kontakta_id }}" value="{{ $vrsta->vrsta_kontakta_id }}" @if($row->vrsta_kontakta_id == $vrsta->vrsta_kontakta_id) {{ 'selected' }} @endif> {{ $vrsta->naziv }} </option>
							@endforeach
						</select></td>
						<td><input type='text' value="{{Input::old('vrednost') ? Input::old('vrednost') : $row->vrednost }}" name='vrednost'></td>
						<td><input type='text' value="{{Input::old('napomena') ? Input::old('napomena') : $row->napomena }}"  name='napomena'></td>
						<td><button type="submit" class="button-option tooltipz" aria-label="{{ AdminLanguage::transAdmin('Sačuvaj') }}"><i class="fa fa-save"></i></button></td>
						<td> 
							<a href="{{ AdminOptions::base_url() }}crm/crm_home/{{ $row->partner_kontakt_id }}/delete" onclick="return confirm('Jeste li sigurni da želite da obrišete kontakt?')" class="tooltipz inline-block" aria-label="{{ AdminLanguage::transAdmin('Obriši') }}"><i class="fa fa-times red"></i></a>
										</td>

					@endif				
				@endforeach
					</tr>
				</tbody>															
		</table>
		</form>
	</div>
</div> 
@endif


<div class="row"> 
	<h3 class="title-med">{{ AdminLanguage::transAdmin('Dodaj kontakte') }}</h3>

	<form method="POST" action="{{ AdminOptions::base_url() }}crm/crm_dodatni_kontakti" enctype="multipart/form-data">
		<input type='hidden' value="" name='partner_kontakt_id'>
		<input type='hidden' value="{{ $partner->partner_id }}" name='partner_id'>

		<select name='vrsta_kontakta_id'>
			@foreach(DB::table('vrsta_kontakta')->orderBy('vrsta_kontakta_id','asc')->get() as $row)	
			<option value="{{ $row->vrsta_kontakta_id }}"> {{ $row->naziv }} </option>
			@endforeach
		</select>


		<table class='additional_table'> 
			<tbody>
				<tr>
					<td><b>{{ AdminLanguage::transAdmin('Kontakt osoba') }}</b></td>
					<td><input type='text' value="" name='osoba'> </td>
				</tr>
				<tr>
					<td><b>{{ AdminLanguage::transAdmin('Vrednost') }}</b></td>
					<td><input type='text' value="" name='vrednost'></td>
				</tr>
				<tr>
					<td><b>{{ AdminLanguage::transAdmin('Napomena') }}</b></td>
					<td><input type='text' value="" name='napomena'></td>
				</tr>  
			</tbody>
		</table>

		<div class="text-center"> 
			<button type="submit" class="btn btn-primary">{{ AdminLanguage::transAdmin('Sačuvaj') }}</button> 
		</div>
	</form>
</div>
