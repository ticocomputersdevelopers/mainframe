@if(Session::has('message'))
	<script>
		alertify.success('{{ Session::get('message') }}');
	</script>
@endif
<section id="main-content">
@include('crm/partials/crm_tabs')
<!-- STATUS -->
	<div class="row">
		<div class="large-3 medium-3 small-12 columns ">
			<div class="flat-box">
				<h3 class="title-med">{{ AdminLanguage::transAdmin('Izaberi status') }}</h3>
				<div class="manufacturer"> 
					<select class="JSeditSupport search_select">
							<option value="{{ AdminOptions::base_url() }}crm/crm_status/0">{{ AdminLanguage::transAdmin('Dodaj novi status') }}</option>
						@foreach($crm_status as $row) 
							<option value="{{ AdminOptions::base_url() }}crm/crm_status/{{ $row->crm_status_id }}" @if($row->crm_status_id == $crm_status_id) {{ 'selected' }} @endif> ({{ $row->crm_status_id }}) {{ $row->naziv }} </option>
						@endforeach
					</select>
				</div>
			</div>
		</div>

		<section class="small-12 medium-12 large-5 large-centered columns">
			<div class="flat-box">

				<h1 class="title-med">{{ $title }}</h1>
			
				<!-- <h1 id="info"></h1> -->

				<form method="POST" action="{{ AdminOptions::base_url() }}crm/crm_status" enctype="multipart/form-data">
					  	<input type="hidden" name="crm_status_id" value="{{ $crm_status_id }}">
					  	<div class="row">
							<div class="columns medium-12 ">
								<label for="naziv">{{ AdminLanguage::transAdmin('Naziv statusa') }}</label>
								<input type="text" name="naziv" value="{{ Input::old('naziv') ? Input::old('naziv') : $naziv }}" autofocus="autofocus" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'readonly' : '' }}>
							</div>
						</div>

					    <div class="row">
							<div class="columns medium-4 field-group">
								<label>{{ AdminLanguage::transAdmin('Aktivan') }}</label>
								<select name="flag_aktivan" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'disabled' : '' }}>
									@if(Input::old('active'))
										@if(Input::old('active'))
										<option value="1" selected>{{ AdminLanguage::transAdmin('DA') }}</option>
										<option value="0" >{{ AdminLanguage::transAdmin('NE') }}</option>
										@else
										<option value="1" >{{ AdminLanguage::transAdmin('DA') }}</option>
										<option value="0" selected>{{ AdminLanguage::transAdmin('NE') }}</option>
										@endif
									@else
										@if($active)
										<option value="1" selected>{{ AdminLanguage::transAdmin('DA') }}</option>
										<option value="0" >{{ AdminLanguage::transAdmin('NE') }}</option>
										@else
										<option value="1" >{{ AdminLanguage::transAdmin('DA') }}</option>
										<option value="0" >{{ AdminLanguage::transAdmin('NE') }}</option>
										@endif
									@endif
								</select>
							</div>
				
						</div>
					
							<div class="row"> 
								<div class="btn-container center">
									<button type="submit" class="btn btn-primary save-it-btn">{{ AdminLanguage::transAdmin('Sačuvaj') }}</button>
									
									<button class="btn btn-danger JSbtn-delete" data-link="{{ AdminOptions::base_url() }}crm/crm_status/{{$crm_status_id}}/status_delete">{{ AdminLanguage::transAdmin('Obriši') }}</button>
				 					
								</div>
							</div>		 
				</form>
			</div>
	</section>
</div> <!-- end of .flat-box -->	