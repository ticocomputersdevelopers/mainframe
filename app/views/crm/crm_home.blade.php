
@if(Session::has('message'))
	<script>
		alertify.success('{{ Session::get('message') }}');
	</script>
@endif
<section id="main-content" class="crm_page">
	@include('crm/partials/crm_tabs')

	
	@if(AdminCrm::checkDateActionCrm(date('Y-m-d')))
	<div class="absolute_bg hide"> </div>
	@endif
	<div class="row">   

		<div class="article-edit-box"> 
			<div class="row flex">
				<div class="inline-block articles_list__item__link__cat">
					
					<label class="checkbox-label"> 
						<input name="aktivni" type="checkbox" class="JSactiveCrm" value="1"@if($aktivni == 1)  'checked' @endif  >
						<span class="label-text">{{ AdminLanguage::transAdmin('Zavrseni') }}</span>
					</label>
					
				</div>
				<div class="columns medium-3"> 
					<!-- SEARCH -->
					<div class="m-input-and-button">
						<input type="text" name="search" value="{{ urldecode($search) }}" placeholder={{ AdminLanguage::transAdmin('Pretraga...') }} >
						<button id="JSCRMSearch" class="btn btn-primary btn-small">{{ AdminLanguage::transAdmin('Pretraga') }}</button>
						<a class="btn btn-danger btn-small" href="{{AdminOptions::base_url()}}crm/crm_home">{{ AdminLanguage::transAdmin('Poništi') }}</a>
					</div>  
				</div> 

				<!-- filteri --> 
				<div class="columns medium-2">
					
						<select name="crm_tip_id">
							<option value="" selected>{{ AdminLanguage::transAdmin('Izaberi tip') }}</option>
							<option>{{ AdminCrm::tipoviSelect($tip_ids) }}</option>
						</select>
						<button id="JSTipoviSearch" class="btn btn-primary btn-abs-right">
							<i class="fa fa-check" area-hidden="true"></i>
						</button>
					</div>
						<button aria-label={{ AdminLanguage::transAdmin('Poništi') }} class="m-input-and-button__btn btn btn-secondary btn-xm tooltipz">
							<a href="{{AdminOptions::base_url()}}crm/crm_home"><i class="fa fa-times" aria-hidden="true"></i>
							</a>
						</button>
				

				<div class="columns medium-2">
					
						<select name="crm_status_id">
							<option value="" selected>{{ AdminLanguage::transAdmin('Izaberi status') }}</option>
							<option>{{ AdminCrm::statusiSelect($status_ids) }}</option>
						</select>
						<button id="JSStatusiSearch" class="btn btn-primary btn-abs-right">
							<i class="fa fa-check" area-hidden="true"></i>
						</button>
					</div>
						<button aria-label={{ AdminLanguage::transAdmin('Poništi') }} class="m-input-and-button__btn btn btn-secondary btn-xm tooltipz">
							<a href="{{AdminOptions::base_url()}}crm/crm_home">
								<i class="fa fa-times"  aria-hidden="true"></i>
							</a>
						</button>
				

				<div class="columns medium-1">
					<input name="datum_pocetka" type="text" value="{{ $datum_pocetka }}" autocomplete="on" placeholder="Datum od">
				</div>

				<div class="columns medium-1">
					<input name="datum_kraja" type="text" value="{{ $datum_kraja }}" autocomplete="on" placeholder="Datum do">
				</div>
				<button aria-label="Poništi" class="m-input-and-button__btn btn btn-secondary btn-xm tooltipz">
							<a href="{{AdminOptions::base_url()}}crm/crm_home">
								<i class="fa fa-times"  aria-hidden="true"></i>
							</a>
						</button>

				<div class="columns medium-1">  
					<a href="" class="full-width btn btn-primary btn-small" data-reveal-id="add-lead"> {{ AdminLanguage::transAdmin('Novi lead') }}</a>
				</div>
			</div>
		</div>


		<div class="article-edit-box">  
			<div class="row">
				<div class="columns medium-12">
					<div class="flex crm_label"> 
						<label>{{ AdminLanguage::transAdmin('Ukupno') }}: <b>{{count($crm)}}</b> </label> 
						
						@if(AdminCrm::checkDateActionCrm(date('Y-m-d')))
						<a href="#" class="open-today-actions tooltipz inline-block btn btn-sm"  aria-label="{{ AdminLanguage::transAdmin('') }}"> <i class="fa fa-calendar-check-o"> </i> {{ AdminLanguage::transAdmin(' Imate aktivne akcije') }}</a>
						@endif
					</div>

					<div class="table-scroll" style="max-height: 350"> 
						<table class="fixed-table-header" id="JSmyTable">	 				 
							<thead>  
								<th class="tooltipz" aria-label="{{ AdminLanguage::transAdmin('Broj akcija') }}">{{ AdminLanguage::transAdmin('br') }}</th>
								<th></th>
								<th class="pointer JSdate_time">{{ AdminLanguage::transAdmin('Datum') }}</th>
								<th></th>
								<th>{{ AdminLanguage::transAdmin('Partner') }}</th>
								<th></th>
								<th>{{ AdminLanguage::transAdmin('Crm tip') }}</th>								
								<th>{{ AdminLanguage::transAdmin('Status') }}</th>
								<!-- <th>{{ AdminLanguage::transAdmin('Roba') }}</th> -->
								<!-- <th>{{ AdminLanguage::transAdmin('Ponuda') }}</th>	 -->
								<th>{{ AdminLanguage::transAdmin('Opis') }}</th>
								<th  class="tooltipz btn btn-small" aria-label="{{ AdminLanguage::transAdmin('Kurs se preračunava po prodajnom kursu NBS-a na današnji dan')}}">{{ AdminLanguage::transAdmin('Vrednost lead-a') }}</th>
								<th></th>
								<th></th>
								<th></th>
								@if(AdminCrm::check_administrator())
								<th>{{ AdminLanguage::transAdmin('Komercijalisti') }}</th>
								@endif
							</thead>
							
							<tbody>

								@foreach($crm as $row)
												
								<tr @if($row->flag_zavrseno==1) class="end_flag" @endif  @if(date('Y-m-d',strtotime(AdminCrm::getDatumAkcije($row->crm_id))) == date('Y-m-d')) class="today-actions-pulseonly" @endif id='JSTableSelect'> 
									<form action="{{AdminOptions::base_url()}}crm/crm_home/{{$row->crm_id}}/edit" method="post"  autocomplete="false">

										<td>
											{{AdminCrm::getBaterryTooltip(AdminCrm::getStatusAkcija($row->crm_id))}}
										
										</td>

										@if(preg_match("/mail/i", AdminCrm::getAkcijaNaziv($row->crm_id)))			

										<td> 
											<a href="{{ AdminOptions::base_url() }}crm/crm_home/{{ $row->crm_id }}" data-e-selected='{{ $row->crm_id }}' class="JScheck_action tooltipz inline-block" aria-label="{{ AdminLanguage::transAdmin('Vidi detalje')}} "><i class="fa fa-eye"></i></a> &nbsp;&nbsp;
											
											<a href="{{ AdminOptions::base_url() }}crm/crm_home/crm_akcija/{{ $row->crm_id }}" class="tooltipz inline-block" aria-label="{{ AdminLanguage::transAdmin('Vidi/Dodaj akciju') }}"><i class="fa fa-plus-square"></i></a> &nbsp;&nbsp;
													
											<i class="tooltipz fa fa-envelope-o" style="color:#242118" aria-label="{{ AdminLanguage::transAdmin( AdminCrm::getAkcijaOpis($row->crm_id)) }}"></i> 
										</td>

										@elseif(preg_match("/telefon/i", AdminCrm::getAkcijaNaziv($row->crm_id)))			

										<td>
											
											<a href="{{ AdminOptions::base_url() }}crm/crm_home/{{ $row->crm_id }}" data-e-selected='{{ $row->crm_id }}' class="JScheck_action tooltipz inline-block" aria-label="{{ AdminLanguage::transAdmin('Vidi detalje')}} "><i class="fa fa-eye"></i></a> &nbsp;&nbsp;
											<a href="{{ AdminOptions::base_url() }}crm/crm_home/crm_akcija/{{ $row->crm_id }}" class="tooltipz inline-block" aria-label="{{ AdminLanguage::transAdmin('Vidi/Dodaj akciju') }}"><i class="fa fa-plus-square"></i></a>  &nbsp;&nbsp;
										
											
											<i aria-label="{{ AdminLanguage::transAdmin(AdminCrm::getAkcijaOpis($row->crm_id)) }}" class="tooltipz fa fa-phone" style="color:#242118"></i> 
										</td> 

										@elseif(preg_match("/sastanak/i", AdminCrm::getAkcijaNaziv($row->crm_id)))			

										<td>
											<a href="{{ AdminOptions::base_url() }}crm/crm_home/{{ $row->crm_id }}" data-e-selected='{{ $row->crm_id }}' class="JScheck_action tooltipz inline-block" aria-label="{{ AdminLanguage::transAdmin('Vidi detalje')}} "><i class="fa fa-eye"></i></a> &nbsp;&nbsp;
										
											<a href="{{ AdminOptions::base_url() }}crm/crm_home/crm_akcija/{{ $row->crm_id }}" class="tooltipz inline-block" aria-label="{{ AdminLanguage::transAdmin('Vidi/Dodaj akciju') }}"><i class="fa fa-plus-square"></i></a> &nbsp;&nbsp;
											

											<i class=" tooltipz fa fa-users"  aria-label="{{ AdminLanguage::transAdmin(AdminCrm::getAkcijaOpis($row->crm_id)) }}" style="color:#242118"></i> 
										</td>

										@elseif(preg_match("/prezentov/i", AdminCrm::getAkcijaNaziv($row->crm_id)))			

										<td>
											<a href="{{ AdminOptions::base_url() }}crm/crm_home/{{ $row->crm_id }}" data-e-selected='{{ $row->crm_id }}' class="JScheck_action tooltipz inline-block" aria-label="{{ AdminLanguage::transAdmin('Vidi detalje')}} "><i class="fa fa-eye"></i></a> &nbsp;&nbsp;
										
											<a href="{{ AdminOptions::base_url() }}crm/crm_home/crm_akcija/{{ $row->crm_id }}" class="tooltipz inline-block" aria-label="{{ AdminLanguage::transAdmin('Vidi/Dodaj akciju') }}"><i class="fa fa-plus-square"></i></a> &nbsp;&nbsp;
										

											<i class="tooltipz fa fa-bullhorn" aria-label="{{ AdminLanguage::transAdmin(AdminCrm::getAkcijaOpis($row->crm_id)) }}" style="color:#242118"></i> 
										</td>

										@elseif(preg_match("/video/i", AdminCrm::getAkcijaNaziv($row->crm_id)))			

										<td>
											<a href="{{ AdminOptions::base_url() }}crm/crm_home/{{ $row->crm_id }}" data-e-selected='{{ $row->crm_id }}' class="JScheck_action tooltipz inline-block" aria-label="{{ AdminLanguage::transAdmin('Vidi detalje')}} "><i class="fa fa-eye"></i></a> &nbsp;&nbsp;
											
											<a href="{{ AdminOptions::base_url() }}crm/crm_home/crm_akcija/{{ $row->crm_id }}" class="tooltipz inline-block" aria-label="{{ AdminLanguage::transAdmin('Vidi/Dodaj akciju') }}"><i class="fa fa-plus-square"></i></a> &nbsp;&nbsp;
											

											<i class="tooltipz fa fa-video-camera" aria-label="{{ AdminLanguage::transAdmin(AdminCrm::getAkcijaOpis($row->crm_id)) }}" style="color:#242118"></i> 
										</td>
										@elseif(preg_match("/fakt/i", AdminCrm::getAkcijaNaziv($row->crm_id)))			

										<td>
											<a href="{{ AdminOptions::base_url() }}crm/crm_home/{{ $row->crm_id }}" data-e-selected='{{ $row->crm_id }}' class="JScheck_action tooltipz inline-block" aria-label="{{ AdminLanguage::transAdmin('Vidi detalje')}} "><i class="fa fa-eye"></i></a> &nbsp;&nbsp;
											
											<a href="{{ AdminOptions::base_url() }}crm/crm_home/crm_akcija/{{ $row->crm_id }}" class="tooltipz inline-block" aria-label="{{ AdminLanguage::transAdmin('Vidi/Dodaj akciju') }}"><i class="fa fa-plus-square"></i></a> &nbsp;&nbsp;
										

											<i class="tooltipz fa fa-money" aria-label="{{ AdminLanguage::transAdmin(AdminCrm::getAkcijaOpis($row->crm_id)) }}" style="color:#242118"></i> 
										</td>


										@elseif(preg_match("/skype/i", AdminCrm::getAkcijaNaziv($row->crm_id)))			

										<td>
											<a href="{{ AdminOptions::base_url() }}crm/crm_home/{{ $row->crm_id }}" data-e-selected='{{ $row->crm_id }}' class="JScheck_action tooltipz inline-block" aria-label="{{ AdminLanguage::transAdmin('Vidi detalje')}} "><i class="fa fa-eye"></i></a> &nbsp;&nbsp;
										
											<a href="{{ AdminOptions::base_url() }}crm/crm_home/crm_akcija/{{ $row->crm_id }}" class="tooltipz inline-block" aria-label="{{ AdminLanguage::transAdmin('Vidi/Dodaj akciju') }}"><i class="fa fa-plus-square"></i></a> &nbsp;&nbsp;
											

											<i class="tooltipz fa fa-skype" style="color:#242118" aria-label="{{ AdminLanguage::transAdmin(AdminCrm::getAkcijaOpis($row->crm_id)) }}"></i>  
										</td>

										@elseif(preg_match("/zoom/i", AdminCrm::getAkcijaNaziv($row->crm_id)))			

										<td>
											<a href="{{ AdminOptions::base_url() }}crm/crm_home/{{ $row->crm_id }}" data-e-selected='{{ $row->crm_id }}' class="JScheck_action tooltipz inline-block" aria-label="{{ AdminLanguage::transAdmin('Vidi detalje')}} "><i class="fa fa-eye"></i></a> &nbsp;&nbsp;
											
											<a href="{{ AdminOptions::base_url() }}crm/crm_home/crm_akcija/{{ $row->crm_id }}" class="tooltipz inline-block" aria-label="{{ AdminLanguage::transAdmin('Vidi/Dodaj akciju') }}"><i class="fa fa-plus-square"></i></a> &nbsp;&nbsp;
										

											<i class="tooltipz fa fa-bullseye" style="color:#242118" aria-label="{{ AdminLanguage::transAdmin(AdminCrm::getAkcijaOpis($row->crm_id)) }}"></i>  
										</td>

										@else

										<td>
											<a href="{{ AdminOptions::base_url() }}crm/crm_home/{{ $row->crm_id }}" data-e-selected='{{ $row->crm_id }}' class="JScheck_action tooltipz inline-block" aria-label="{{ AdminLanguage::transAdmin('Vidi detalje')}} "><i class="fa fa-eye"></i></a> &nbsp;&nbsp;
											
											<a href="{{ AdminOptions::base_url() }}crm/crm_home/crm_akcija/{{ $row->crm_id }}" class="tooltipz inline-block" aria-label="{{ AdminLanguage::transAdmin('Vidi/Dodaj akciju') }}"><i class="fa fa-plus-square"></i></a> &nbsp;&nbsp;

											<i class="tooltipz"aria-label="{{ AdminLanguage::transAdmin(AdminCrm::getAkcijaOpis($row->crm_id)) }}">{{AdminCrm::getAkcijaNaziv($row->crm_id)}}</i>  
									
											 
										</td>

										@endif
										@if(AdminCrm::getDatumAkcije($row->crm_id))

											@if(date('Y-m-d',strtotime(AdminCrm::getDatumAkcije($row->crm_id))) == date('Y-m-d'))

											<td style="color:red"><i class="fa fa-bell" style="font-size:15px;color:red"></i> Danas</td> 

											@else 

											<td>{{date('d-m-Y, g:i a',strtotime(AdminCrm::getDatumAkcije($row->crm_id)))}}</td>
											 
											@endif
										@else
											<td> </td>
										@endif
										@if(AdminCrm::checkFilesCrm($row->crm_id))
										<td>
											<a class="JScheck_action" href="{{ AdminOptions::base_url() }}crm/crm_home/{{ $row->crm_id }}" data-e-selected='{{ $row->crm_id }}'><i class="fa fa-folder-o"></i>
										</td>
										@else
										<td></td>
										@endif

										<td> 
											<a style="color:#324032" href="#" data-id='{{ $row->crm_id }}' class="open-partnerDetailsModal tooltipz inline-block"  aria-label="{{ AdminLanguage::transAdmin('Vidi sve akcije') }}"><b>{{ AdminPartneri::partner_naziv($row->partner_id) }}</b>
											</a>	
										</td>
										<td>
											<a href="#" data-id='{{ $row->partner_id }}' class="open-add-contact tooltipz inline-block"  aria-label="{{ AdminLanguage::transAdmin('Vidi/izmeni kontakt') }}"><i class="fa fa-id-card-o"></i>
											</a>
										</td>

										<td>
											<select name="crm_tip_id" class="select-width">
												@foreach(DB::table('crm_tip')->orderBy('crm_tip_id','asc')->get() as $tip)	
												<option value="{{ $tip->crm_tip_id  }}"@if($row->crm_tip_id == $tip->crm_tip_id) selected @endif>{{ AdminCrm::getTipNaziv($tip->crm_tip_id) }}</option>
												@endforeach
											</select>
										</td>

										<td>
											<select name="crm_status_id" class="select-width">	
												@foreach(DB::table('crm_status')->orderBy('crm_status_id','asc')->get() as $status)
												<option value="{{ $status->crm_status_id  }}"@if($row->crm_status_id == $status->crm_status_id) selected @endif>{{  AdminCrm::getStatusNaziv($status->crm_status_id) }}</option> 
												@endforeach
											</select>
										</td>
<!-- 
										<td>
											
											<div class="articles min-width-300">
												<input class="" autocomplete="off" data-timer="" type="text" id="JSRobaSearch" data-id="" placeholder="Unesite id,sku ili ime artikla ili prozvođača ili krajnju grupu" />
												<div id="content_artikli"></div>
											</div>
										</td> -->

										<!-- <td>
											<div>
											<select name="ponuda_id" class="select-width">	
												@foreach(DB::table('ponuda')->orderBy('ponuda_id','asc')->get() as $ponuda)
												<option value="{{ $ponuda->ponuda_id  }}"@if($row->ponuda_id == $ponuda->ponuda_id) selected @endif>{{ $row->ponuda_id }}</option>
												@endforeach
											</select>

											</div>
										</td> -->
										
										<td>
											<input class="select-width tooltipz" type='text' value="{{Input::old('opis') ? Input::old('opis') : $row->opis }}" name='opis' aria-label="{{Input::old('opis') ? Input::old('opis') : $row->opis }}">
										</td>

										<td>
											<div class="relative">
											<input id='valueField' type='number' name='vrednost' value="{{Input::old('vrednost') ? Input::old('vrednost') : $row->vrednost }}">

											@if($row->kursna_lista_id)
											
											<a href="{{ AdminOptions::base_url() }}crm/crm_home/{{ $row->crm_id }}/dinari" class="tooltipz btn btn-abs-right" aria-label="{{ AdminLanguage::transAdmin('Preračunaj u evre')}}" style='background: #FFFFFF' id="dinary">{{ AdminLanguage::transAdmin('RSD')}}</a></td>
											@else
										
											<a href="{{ AdminOptions::base_url() }}crm/crm_home/{{ $row->crm_id }}/preracunaj" class="tooltipz btn btn-abs-right" aria-label="{{ AdminLanguage::transAdmin('Preračunaj u dinare')}}" style='background: #FFFFFF' id="evry">{{ AdminLanguage::transAdmin('EUR')}}</a>
											
											@endif
										</div>
										</td>
							
										<td>
											<button type="submit" class="button-option tooltipz" aria-label="{{ AdminLanguage::transAdmin('Sačuvaj') }}"><i class="fa fa-save"></i></button> 
										</td>	
									
									@if(AdminCrm::check_administrator())
									<td> 
										<a href="{{ AdminOptions::base_url() }}crm/crm_home/{{ $row->crm_id }}/crm_delete" onclick="return confirm('Jeste li sigurni da želite da obrišete lead i sve akcije vezane za njega')" class="tooltipz inline-block" aria-label="{{ AdminLanguage::transAdmin('Obriši lead') }}"><i class="fa fa-times red"></i></a>
									</td> 
									@endif
									<td> 
										@if($row->flag_zavrseno!=1)
										<a href="{{ AdminOptions::base_url() }}crm/crm_zavrseno/{{ $row->crm_id }}" class="tooltipz btn btn-small" aria-label="{{ AdminLanguage::transAdmin('Završi') }}"><i class="fa fa-power-off"></i></a> 
										@else 
										<a href="{{ AdminOptions::base_url() }}crm/crm_vrati/{{ $row->crm_id }}" class="tooltipz btn btn-small" aria-label="{{ AdminLanguage::transAdmin('Vrati lead') }}"><i class="fa fa-play"></i></a> 
										@endif
										@if(Admin_model::check_admin())
										<a href="{{ AdminOptions::base_url() }}crm/crm_fakturisi/{{ $row->partner_id }}" class="tooltipz btn btn-small" aria-label="{{ AdminLanguage::transAdmin('Fakturiši') }}"><i class="fa fa-table"></i></a>
										@endif 
									</td> 
								@if(AdminCrm::check_administrator())
								<td class="overflow-visible">
									<div class="custom-select managerS">
										
										<select type="text" name="imenik_id[]" class="JSManagerSelect" multiple="multiple">
											@foreach(AdminCRM::managerSelect() as $man)
												@if(in_array($man->imenik_id,(Input::old('imenik_id') ? Input::old('imenik_id') : AdminCrm::getImenikId($row->crm_id))))
												<option value="{{ $man->imenik_id }}" selected>{{ $man->ime }} {{ $man->prezime[0] }}</option>
												@else
												<option value="{{ $man->imenik_id }}">{{ $man->ime }} {{ $man->prezime[0] }}</option>
												@endif
											@endforeach
										</select>
									</div>
								</td>
								@endif
								</form>	
									<!-- <td class="overflow-visible"> 
										<div class="custom-select">
											<div class="row"> 
												<div class="columns medium-9 no-padd JScustom_select"> 
													<div class="relative">
														<select data-e-selected='{{ $row->crm_id }}' class= "crmIDi">
															<option>{{ AdminCrm::nazivKomercijaliste($row->crm_id) }}</option>
														</select>
														<div class="overSelect"></div>
													</div>
													@foreach(DB::table('imenik')->where('kvota','>',0)->orderBy('ime','asc')->get() as $manager)
													<div class="checkboxes">
														<label><input value="{{ $manager->imenik_id }}" type="checkbox" @if((DB::table('imenik_crm')->where('imenik_id',$manager->imenik_id)->where('imenik_id','>',0)->where('crm_id',$row->crm_id)->pluck('imenik_id'))!== null) checked @endif/>{{ $manager->ime }}</label>
													</div>
													@endforeach
												</div>
												<div class="columns medium-3 no-padd">  
													<button class="btn btn-primary no-margin xs-btn JScrm_manager"><i class="fa fa-check"></i></button>
												</div>
											</div>
										</div> 
									</td> -->
									
								</tr>
								@endforeach
								
								<!-- ALL ACTIONS-->
								<!-- <tr class='actions_heading hide'> -->

								</tbody>				
							</table>
			
							<!-- CONTENT ACTION TABLE -->
							@if(!empty($akcije))
							<table class="JSactions_content_table hide"> 
								<thead>
									<tr> 
										<th>{{ AdminLanguage::transAdmin('Akcija') }}</th>
										<th style="width: 10%">{{ AdminLanguage::transAdmin('Naziv akcije') }}</th>  
										<th style="width: 60%">{{ AdminLanguage::transAdmin('Komentar') }}</th> 
										<th style="width: 10%">{{ AdminLanguage::transAdmin('Datum pocetka') }}</th> 
										<th style="width: 10%">{{ AdminLanguage::transAdmin('Datum zavrsetka') }}</th> 
										<th></th> 
									</tr>
								</thead>

								<tbody>
									@foreach($akcije as $row) 
									<tr @if($row->flag_zavrseno==1) class="end_flag" @endif>
										<form method="POST" action="{{ AdminOptions::base_url() }}crm/crm_home/{{$row->crm_akcija_id}}" enctype="multipart/form-data"> 
											<input type="hidden" value="{{$row->crm_akcija_id }}" name= "crm_akcija_id">

											@if(preg_match("/mail/i", AdminCrm::getAkcija($row->crm_akcija_tip_id)))			

											<td class="tooltipz" aria-label="{{ AdminLanguage::transAdmin(AdminCrm::getAkcija($row->crm_akcija_tip_id)) }}"><i class="fa fa-envelope-o" style="color:#242118"></i></td>

											@elseif(preg_match("/telefon/i", AdminCrm::getAkcija($row->crm_akcija_tip_id)))			

											<td class="tooltipz" aria-label="{{ AdminLanguage::transAdmin(AdminCrm::getAkcija($row->crm_akcija_tip_id)) }}"><i class="fa fa-phone" style="color:#242118"></i>  </td>  
											@elseif(preg_match("/sastanak/i", AdminCrm::getAkcija($row->crm_akcija_tip_id)))			

											<td class="tooltipz" aria-label="{{ AdminLanguage::transAdmin(AdminCrm::getAkcija($row->crm_akcija_tip_id)) }}"><i class="fa fa-users" style="color:#242118"></i>  </td>

											@elseif(preg_match("/prezentov/i", AdminCrm::getAkcija($row->crm_akcija_tip_id)))			

											<td class="tooltipz" aria-label="{{ AdminLanguage::transAdmin(AdminCrm::getAkcija($row->crm_akcija_tip_id)) }}"><i class="fa fa-bullhorn" style="color:#242118"></i></td>

											@elseif(preg_match("/skype/i", AdminCrm::getAkcija($row->crm_akcija_tip_id)))			

											<td class="tooltipz" aria-label="{{ AdminLanguage::transAdmin(AdminCrm::getAkcija($row->crm_akcija_tip_id)) }}"><i class="fa fa-skype" style="color:#242118"></i>  </td>

											@elseif(preg_match("/video/i", AdminCrm::getAkcija($row->crm_akcija_tip_id)))			

											<td class="tooltipz" aria-label="{{ AdminLanguage::transAdmin(AdminCrm::getAkcija($row->crm_akcija_tip_id)) }}"><i class="fa fa-video-camera" style="color:#242118"></i></td>

											@elseif(preg_match("/zoom/i", AdminCrm::getAkcija($row->crm_akcija_tip_id)))			

											<td class="tooltipz" aria-label="{{ AdminLanguage::transAdmin(AdminCrm::getAkcija($row->crm_akcija_tip_id)) }}"><i class="fa fa-bullseye" style="color:#242118"></i></td>

											@else 
											<td>{{AdminCrm::getAkcija($row->crm_akcija_tip_id)}}</td> 
											@endif

											<td>
												<select class="select-width" name="crm_akcija_tip_id" id="JSNewTypeCheck" onchange="getSelectedValue();">
													@foreach(DB::table('crm_akcija_tip')->orderBy('crm_akcija_tip_id','asc')->get() as $akcija)
													<option value="{{ $akcija->crm_akcija_tip_id }}" @if($row->crm_akcija_tip_id == $akcija->crm_akcija_tip_id) {{ 'selected' }} @endif>{{ $akcija->naziv }}</option>	 			
													@endforeach
												</select>
											</td>

											<td>
												<input class="tooltipz" type='text' value="{{Input::old('opis') ? Input::old('opis') : $row->opis }}"  name='opis' aria-label="{{Input::old('opis') ? Input::old('opis') : $row->opis }}">
											</td>				
											<td>@if($row->datum != NULL)
												<input type='text' class="JSdatepickerAction" 
												value="{{date($row->datum )}}"  name='datum'>
												@else
												<input type='text' class="JSdatepickerAction"  name='datum'>
												@endif
											</td> 

											<td>@if($row->datum_zavrsetka != NULL)
												<input type='text' class="JSdatepickerAction" value="{{date($row->datum_zavrsetka)}}" name='datum_zavrsetka'>
												@else
												<input type='text' class="JSdatepickerAction" name='datum_zavrsetka'>
												@endif
											</td> 
											
											
											<td>
												<button type="submit" class="button-option tooltipz" aria-label="{{ AdminLanguage::transAdmin('Sačuvaj') }}"><i class="fa fa-save"></i></button>&nbsp;&nbsp;  

												<a href="{{ AdminOptions::base_url() }}crm/crm_home/{{$row->crm_akcija_id}}/akcija_delete" onclick="return confirm('Jeste li sigurni da želite da obrišete akciju?')" class="tooltipz inline-block" aria-label="{{ AdminLanguage::transAdmin('Obriši akciju') }}"><i class="fa fa-times red"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;

												@if($row->flag_zavrseno)  
												<a href="{{ AdminOptions::base_url() }}crm/crm_akcija_vrati/{{ $row->crm_akcija_id }}" class="tooltipz inline-block" aria-label="{{ AdminLanguage::transAdmin('Vrati akciju u aktivne') }}"><i class="fa fa-play"></i></a>&nbsp;&nbsp;  
												@else

												<a href="{{ AdminOptions::base_url() }}crm/crm_akcija_zavrsena/{{ $row->crm_akcija_id }}" class="tooltipz inline-block" aria-label="{{ AdminLanguage::transAdmin('Završi akciju') }}"><i class="fa fa-power-off"></i></a> 
											</td>

										</form>

										@endif	 
									</tr>
									@endforeach
								</tbody>
							</table>
							

						</div>
					</div>
				</div>
			</div>

			{{ Paginator::make($crm,$count,$limit)->links() }}
 

			<div class="article-edit-box all_actions">
				<div class="row">
					<div class="columns"> 

						<div class="relative"> 
							<h3 class="title-med">{{ AdminLanguage::transAdmin('Komentar CRMa:') }} </h3>

							<span class="btn-abs2">
								
								@if(isset($datum_pocetka_crma))
								
									{{ AdminLanguage::transAdmin('Pocetak CRMa:') }} 
									{{ date('d-m-Y',strtotime($datum_pocetka_crma)) }}
								
								@endif
								@if(isset($datum_zavrsetka_crma))
								<span class="active-sub">
									{{ AdminLanguage::transAdmin('Zavrsetak CRMa:') }} 
									{{ date('d-m-Y',strtotime($datum_zavrsetka_crma)) }}
								</span>
								@endif
							</span>
						</div>
  
						<div>{{ AdminLanguage::transAdmin($opis) }}</div>
					</div>
				</div>
			</div>
						<!-- dokumenta -->
				<div class="article-edit-box all_actions">
						<div class="row">
							<div class="medium-1 right">
								<a href="#" class="open-documentNew " data-id="{{$cerm_id}}"><i class="fa fa-file-text-o"  style="color:green"></i></a> &nbsp;&nbsp;
								{{ AdminLanguage::transAdmin('Novi dokument') }}
							
							</div>
							<div class="columns">
								<div class="relative"> 
								<h2 class="title-med">{{ AdminLanguage::transAdmin('Dokumenata:') }}</h2>
								<?php
        						$fajlovi = DB::select("SELECT * FROM crm_dokumenta WHERE crm_id = ".$cerm_id."ORDER BY vrsta_fajla_id DESC");
        						?>
								@foreach($fajlovi as $row)
								<div class="flat-box files-list">
									<div class="anly-ul__li__count">{{ AdminLanguage::transAdmin('Datum i vreme kreiranja:') }}
									{{date('d-m-Y H:m:s',strtotime($row->datum_kreiranja))}}</div>
									<b>{{ AdminLanguage::transAdmin('Naziv') }}:</b> {{ $row->naziv }} - {{ AdminSupport::getExtension($row->vrsta_fajla_id)}}
									<span class="files-list-items right">
										@if(AdminCrm::check_administrator())
										<a href="{{ AdminOptions::base_url() }}crm/partner_document_delete/{{$row->crm_dokumenta_id}}/delete" onclick="return confirm('Jeste li sigurni da želite da obrišete dokument?')">{{ AdminLanguage::transAdmin('Obriši') }}</a> |
										@endif 
										<a href="{{ $row->putanja != null ? AdminOptions::base_url().$row->putanja : $row->putanja_http }}" target="_blank">{{ AdminLanguage::transAdmin('Vidi') }}</a>
									</span>
								</div>
								@endforeach
							</div>
						</div>
				</div>
						
						
				@endif

						<!-- modal dokumenta --> 
						<div id="documentNew" class="reveal-modal medium" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
							<a class="close-reveal-modal" aria-label="Close">&#215;</a>
							<h4 class="modal-title-h4">{{ AdminLanguage::transAdmin('Dokumenta') }}</h4>
							<div id="JSDocumentNew"></div>
						</div>
						

						<!-- modal dodatni kontakti -->
						<div id="add-contact" class="reveal-modal medium" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
							<a class="close-reveal-modal" aria-label="Close">&#215;</a>

							<h4 class="modal-title-h4">{{ AdminLanguage::transAdmin('Dodatne informacije') }} </h4>

							<div id="JSPartnerDetailsModalContent"></div>
						</div>

						<!-- modal sve akcije jednog partnera -->
						<div id="partnerDetailsModal" class="reveal-modal medium" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
							<a class="close-reveal-modal" aria-label="Close">&#215;</a>

							<h4 class="modal-title-h4">{{ AdminLanguage::transAdmin('Ispis svih akcija jednog partnera') }} </h4>

							<div id="JSPartnerAllActions"></div>
						</div>

						<!-- modal za novi lead -->
						<div id="add-lead" class="reveal-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
							<a class="close-reveal-modal" aria-label="Close">&#215;</a>

							<br>

							<h4 class="modal-title-h4">{{ AdminLanguage::transAdmin('Dodaj novi lead') }}</h4>

							<form action="{{AdminOptions::base_url()}}crm/crm_lead/{crm_id}/lead_save" method="post"  autocomplete="false">
								<div class="table-scroll"> 
									<input type="hidden" name="crm_id" value="">
									<table> 
										<thead> 
											<tr>	 
												<th>{{ AdminLanguage::transAdmin('Partner') }}</th>
												<th>{{ AdminLanguage::transAdmin('Crm tip') }}</th>
												<th>{{ AdminLanguage::transAdmin('Crm status') }}</th>
												<!-- <th>{{ AdminLanguage::transAdmin('roba') }}</th> -->
											<!-- 	<th>{{ AdminLanguage::transAdmin('Ponuda') }}</th> -->
												<th>{{ AdminLanguage::transAdmin('Datum kreiranja') }}</th>
												<th>{{ AdminLanguage::transAdmin('Datum zavrsetka') }}</th>
												<th>{{ AdminLanguage::transAdmin('Vrednost RSD') }}</th>
												<th>{{ AdminLanguage::transAdmin('Napomene') }}</th>
												<th>{{ AdminLanguage::transAdmin('Flag završeno') }}</th>  
												<td></td>
											</tr>
										</thead>
										<tbody> 
											<tr> 
												<td>
													<select name="partner_id" class="select-width">
														<option  value=""> {{ AdminLanguage::transAdmin('Izaberi') }} </option>	
														@foreach($partneri as $partner)
														<option  value="{{ $partner->partner_id }}"selected>{{ $partner->naziv }}</option>
														@endforeach
													</select>
												</td>

												<td>
													<select name="crm_tip_id" class="select-width">
														<option value="">{{ AdminLanguage::transAdmin('Izaberi') }}</option>		
														@foreach(DB::table('crm_tip')->orderBy('crm_tip_id','asc')->get() as $tip)
														<option value="{{ $tip->crm_tip_id }}"selected>{{ $tip->naziv }}</option>
														@endforeach
													</select>	
												</td>

												<td> 
													<select name="crm_status_id" class="select-width">
														@foreach(DB::table('crm_status')->orderBy('crm_status_id','asc')->get() as $status)
														<option value="{{ $status->crm_status_id }}"selected>{{ $status->naziv }}</option>
														@endforeach
													</select>	
												</td>

												<!-- <td> 
													<select name="ponuda_id" class="select-width">  
														<option  value="">{{ AdminLanguage::transAdmin('Izaberi') }}</option>	
														@foreach(DB::table('ponuda_stavka')->orderBy('ponuda_id','asc')->get() as $ponuda)
														<option value="{{ $ponuda->ponuda_id }}"selected>{{ $ponuda->naziv_stavke }}</option>
														@endforeach
													</select>
												</td> -->

												<td><input type="date" name="datum_kreiranja" value="{{date('Y-m-d')}}"></td>
												<td><input type="date" name="datum_zavrsetka"></td>
												<td> 
													<input type="number" name="vrednost">
												</td>

											
												<td> 
													<div class="min-width-300">
														<textarea style="min-height: auto;" type="text" rows="3" name="opis"></textarea>
													</div>
												</td>
												
												<td class="text-center"><input type="checkbox" name="flag_zavrseno" value="1"></td>
											</tr>
										</tbody>
									</table>
								</div>

								<br>

								<div class="text-center">
									<input type="submit" value="{{ AdminLanguage::transAdmin('Sačuvaj') }}" name="submit" class="btn btn-primary">
								</div> 
							</form>
						</div>
						<br>
					</div> 
		
	
		<!-- modal danasnje akcije -->
		<div id="crmTodayActions" class="reveal-modal medium" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">	
			<a class="close-reveal-modal" aria-label="Close">&#215;</a>
			<h4 class="modal-title-h4">{{ AdminLanguage::transAdmin('Današnje i akcije u budućnosti') }} </h4>
			<br>
			<div>	
				<ul class=" anly-ul">
						@foreach($akcije_danas as $row)

						<li class="crm_li">
							<div class=" flex"> 
								<span> <i class="fa fa-calendar" style="font-size:18px;color:#2B3A41"></i> {{date('d-m-Y',strtotime($row->datum))}} </span>
								<span class="anly-ul__li__count">{{$row->partner_ime}}</span>
							</div>
							<ul class="hide">
								<li class="anly-ul__li">
									{{ AdminLanguage::transAdmin('Partner') }}
									<span class="anly-ul__li__count">{{AdminLanguage::transAdmin($row->partner_ime)}}</span>
								</li>
								@if(AdminCrm::check_administrator())
								<li class="text-green anly-ul__li">
									{{ AdminLanguage::transAdmin('Komercijalista') }}
									<b><span class="anly-ul__li__count">{{AdminCrm::nazivKomercijaliste($row->crm_id)}}</span></b>
								</li>
								@endif		
								
								<li class="anly-ul__li">{{ AdminLanguage::transAdmin('Tip akcije') }}
		  							<span class="anly-ul__li__count">{{$row->naziv}}</span>
		  						</li>

		  						<li class="anly-ul__li">
		  							{{ AdminLanguage::transAdmin('Datum početka') }}
		  							<span class="anly-ul__li__count">{{date('d-m-Y',strtotime($row->datum))}}</span>
		  						</li>

								<li class="anly-ul__li">
									{{ AdminLanguage::transAdmin('Opis akcije') }}{{'-'}}
									<span class="anly-ul__li__count">{{AdminLanguage::transAdmin($row->opis)}}</span>
								</li>
								
								<li class="anly-ul__li">
									{{ AdminLanguage::transAdmin('crm id') }}
									<span class="anly-ul__li__count">{{$row->crm_id}}</span>
								</li>

								@if($row->datum_zavrsetka != NULL)
									<li class="anly-ul__li">
										{{ AdminLanguage::transAdmin('Datum završetka') }}
										<span class="anly-ul__li__count">{{date('d-m-Y',strtotime($row->datum_zavrsetka))}}</span>
									</li>
								@else
									<li></li>
								@endif

								<li class="anly-ul__li">
								@if($row->flag_zavrseno == 1)
			  					<i class="fa fa-check-square-o tooltipz" style="font-size:30px;color:green"aria-label="{{ AdminLanguage::transAdmin('Završeno') }}"></i>
			  					@else
			  					@endif
			  					</li>
							</ul>
						</li> 
						@endforeach
					
				</ul>
			</div>
		</div>
	</section>
	
<!-- <script type="text/javascript">
         document.addEventListener("DOMContentLoaded", function(event) { 
            var scrollpos = localStorage.getItem('scrollpos');
            if (scrollpos) window.scrollTo(0, scrollpos);
        });

        window.onbeforeunload = function(e) {
            localStorage.setItem('scrollpos', window.scrollY);
        };
  </script>		 -->


