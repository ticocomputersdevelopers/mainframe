@extends('adminb2b.defaultlayout')
@section('content')

<section class="" id="main-content">
	@include('adminb2b.partials.partner_rabat_tabs')
	
	<div class="row art-row">
		<div class="row">
			<section class="medium-5 columns">
				<div class="flat-box">
					@include('adminb2b.pages.proizvodjaci')
				</div>
			</section>
		</div>
	</div>
</section>
@endsection