@extends('adminb2b.defaultlayout')
@section('content')

<div id="main-content">
	
	@include('adminb2b.partials.partner_rabat_tabs')

	<div class="row art-row">
		<div class="column medium-9">
			<div class="flat-box"> 
				<h3 class='title-med'>Rabat partnera</h3>
				<div class="btn-container clearfix"> 
					<div class="column medium-4">
						<form method="POST" action="{{AdminOptions::base_url()}}admin/b2b/partneri_search" class="m-input-and-button">
						<input type="text" name="search" value="{{ isset($word) ? $word : '' }}" placeholder="Pretraga..." class="m-input-and-button__input">
						<button class="btn btn-primary btn-small" type="submit">Traži</button>
						<a class="btn btn-danger btn-small discard" href="{{AdminOptions::base_url()}}admin/b2b/partneri"><i class="fa fa-close"></i></a>
						</form>	
					</div>

					@if(Admin_model::check_admin(array('B2B_RABAT_AZURIRANJE')))
						<div class="column medium-4 flex-me no-padd">
							<div class="columns medium-6 no-padd"> 
								<select name="kategorija_partner_edit" class="kategorija_partner_edit">
									<option>Bez kategorije</option>
									@foreach(AdminB2BSupport::getKategorije() as $row)
									<option>{{ $row->naziv }}</option>
									@endforeach
								</select>
							</div>
							<div class="columns medium-6 no-padd"> 
								<button class="btn btn-small btn-primary" id="kategorija_partner_edit-btn">Dodeli kategoriju</button>
							</div>
						</div>
	 

						<div class="column medium-3 flex-me no-padd">
							<div class="columns medium-3 no-padd"> 
								<input type="number" onkeydown="javascript: return event.keyCode === 8 || event.keyCode === 46 || event.keyCode === 190 || event.keyCode === 37 || event.keyCode === 39 ? true : !isNaN(Number(event.key))" name="rabat_partner_edit" class="rabat_partner_edit">
							</div>
							<div class="columns medium-9 no-padd"> 
								<button class="btn btn-primary btn-small" id="rabat_partner_edit-btn">Dodeli rabat</button> 							
							</div>
						</div>
					@endif
				</div>
			</div>
		</div>

		<div class="column medium-9">  
			<div class="table-scroll">
				<table class="article-artiacle-table articles-page-tab">
					<thead>
					<tr class="st order-list-titles row">
						<th class="table-head" data-coll="naziv" data-sort="DESC"><a href="#">NAZIV </a></th>
						<th class="table-head" data-coll="rabat" data-sort="DESC"><a href="#">RABAT </a></th>
						<th class="table-head" data-coll="kategorija" data-sort="DESC"><a href="#">KATEGORIJA </a></th>
					</tr>
					</thead>
					<tbody @if(Admin_model::check_admin(array('B2B_RABAT_AZURIRANJE'))) id="selectable" @endif>
						@foreach($partneri as $row)
						<tr class="admin-list order-list-item row ui-widget-content" data-id="{{ $row->partner_id }}">
						<td class="naziv-art"><span>{{ $row->naziv }}</span></td>
						<td class="rabat"><span>{{ $row->rabat }}</span></td>
						<td class="kategorija"><span>
						<?php
						 $sta=DB::table('partner_kategorija')->where('id_kategorije', $row->id_kategorije)->pluck('naziv');
						  if (!empty($sta)){
						 	echo $sta;
						 } else {
						 	echo "Bez kategorije";						 }
						 ?>
						</span></td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@endsection