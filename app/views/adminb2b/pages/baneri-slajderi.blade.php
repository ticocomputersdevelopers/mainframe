@extends('adminb2b.defaultlayout')
@section('content')
<section id="main-content" class="banners">
	@if(Session::has('message'))
	<script>
		alertify.success('{{ Session::get('message') }}');
	</script>
	@endif
	<section class="medium-12 large-3 columns">
		<div class="flat-box">
			<label class="text-center">Baneri</label>
			<ul @if(Admin_model::check_admin(array('B2B_BANERI_I_SLAJDER_AZURIRANJE'))) class="banner-list JSListaBanerB2B" id="banner-sortable" @endif data-table="2">
				<li id="0" class="new-banner new-elem relative">
					<a href="{{AdminB2BOptions::base_url()}}admin/b2b/baneri-slajdovi/0/1">Novi baner</a>
					<a href="{{AdminB2BOptions::base_url()}}admin/b2b/baneri-slajdovi/0/1"><span class="absolute-right padding-h-8 text-gray"><i class="fa fa-plus" aria-hidden="true"></i></span></a>
				</li>
				@foreach($banners as $banner)
				<li id="{{$banner->baneri_id}}" class="relative {{ ( (B2bOptions::trajanje_banera($banner->baneri_id) == 'NULL' ) && ( B2bOptions::trajanje_banera($banner->baneri_id) <= 1))  ? 'deadline' : '' }} ui-state-default @if($banner->baneri_id == $id) active @endif" id="{{$banner->baneri_id}}">
					<a href="{{AdminB2BOptions::base_url()}}admin/b2b/baneri-slajdovi/{{$banner->baneri_id}}/1">{{$banner->naziv}}</a>
					@if(Admin_model::check_admin(array('B2B_BANERI_I_SLAJDER_AZURIRANJE')))
					<a class="absolute-right padding-h-8 tooltipz JSbtn-delete" aria-label="Obriši" data-link="{{AdminB2BOptions::base_url()}}admin/b2b/baneri-delete/{{$banner->baneri_id}}"><i class="fa fa-times text-gray" aria-hidden="true"></i></a>
					@endif
				</li>
				@endforeach
			</ul>	
			<label class="text-center">Slajderi</label>
			<ul @if(Admin_model::check_admin(array('B2B_BANERI_I_SLAJDER_AZURIRANJE'))) class="banner-list JSListaSliderB2B" id="slide-sortable" @endif data-table="2">
				<li id="0" class="new-slide new-elem relative">
					<a href="{{AdminB2BOptions::base_url()}}admin/b2b/baneri-slajdovi/0/2">Novi slajd
					</a>
					<a href="{{AdminB2BOptions::base_url()}}admin/b2b/baneri-slajdovi/0/2"><span class="absolute-right padding-h-8 text-gray"><i class="fa fa-plus" aria-hidden="true"></i></span></a>
				</li>
				@foreach($sliders as $slide)
				<li id="{{$slide->baneri_id}}" class="relative {{ ( (B2bOptions::trajanje_banera($slide->baneri_id) == 'NULL' ) && ( B2bOptions::trajanje_banera($slide->baneri_id) <= 1))  ? 'deadline' : '' }} ui-state-default @if($slide->baneri_id == $id) active @endif" id="{{$slide->baneri_id}}">
					<a href="{{AdminB2BOptions::base_url()}}admin/b2b/baneri-slajdovi/{{$slide->baneri_id}}/2">{{$slide->naziv}}</a>
					@if(Admin_model::check_admin(array('B2B_BANERI_I_SLAJDER_AZURIRANJE')))
					<a class="absolute-right padding-h-8 tooltipz JSbtn-delete" aria-label="Obriši" data-link="{{AdminB2BOptions::base_url()}}admin/b2b/baneri-delete/{{$slide->baneri_id}}"><i class="fa fa-times text-gray" aria-hidden="true"></i></a>
					@endif
				</li>
				@endforeach
			</ul>
			<label class="text-center">Pop-Up baner</label>
			<ul @if(Admin_model::check_admin(array('B2B_BANERI_I_SLAJDER_AZURIRANJE'))) class="banner-list JSListaBaner" id="banner-sortable" @endif data-table="2">
				<li id="0" class="new-banner new-elem relative">
					<a href="{{AdminB2BOptions::base_url()}}admin/b2b/baneri-slajdovi/0/9">Novi pop-up baner</a>
					<a href="{{AdminB2BOptions::base_url()}}admin/b2b/baneri-slajdovi/0/9"><span class="absolute-right padding-h-8 text-gray"><i class="fa fa-plus" aria-hidden="true"></i></span></a>
				</li>
				@foreach($popup as $popbanner)
				<li id="{{$popbanner->baneri_id}}" class="relative {{ ( (B2bOptions::trajanje_banera($popbanner->baneri_id) == 'NULL' ) && ( B2bOptions::trajanje_banera($popbanner->baneri_id) <= 1))  ? 'deadline' : '' }} ui-state-default @if($popbanner->baneri_id == $id) active @endif" id="{{$popbanner->baneri_id}}">
					<a href="{{AdminB2BOptions::base_url()}}admin/b2b/baneri-slajdovi/{{$popbanner->baneri_id}}/9">{{$popbanner->naziv}}</a>
					@if(Admin_model::check_admin(array('B2B_BANERI_I_SLAJDER_AZURIRANJE')))
					<a class="absolute-right padding-h-8 tooltipz JSbtn-delete" aria-label="Obriši" data-link="{{AdminB2BOptions::base_url()}}admin/b2b/baneri-delete/{{$popbanner->baneri_id}}"><i class="fa fa-times text-gray" aria-hidden="true"></i></a>
					@endif
				</li>
				@endforeach
			</ul>
		</div>
		<!-- BACKGROUND IMAGE -->
		<div class="flat-box">
			<label class="text-center">Pozadinska slika</label>
			<ul class="">
				<form action="{{AdminB2BOptions::base_url()}}admin/b2b/bg-img-edit" method="POST" enctype="multipart/form-data">
					@if(Admin_model::check_admin(array('B2B_BANERI_I_SLAJDER_AZURIRANJE')))
					<input type="file" name="bgImg" value="Izaberi sliku">
					@endif
					
					@if(B2bOptions::getBGimg() == null)
					<img src="{{AdminB2BOptions::base_url()}}/images/no-image.jpg" alt="" class="bg-img">
					@else
					<img src="{{AdminB2BOptions::base_url()}}{{ B2bOptions::getBGimg() }}" alt="" class="bg-img">
					@endif
					<label class="text-center">Preporučene dimenzije slike: 1920x1080</label>
					<br>
					<!-- <label for="aktivna">
						<input type="checkbox" name="aktivna" value=""> Aktivna slika
					</label> -->

					<label class="text-center">Link leve strane</label>
					<input id="link" value="{{B2bOptions::getlink()}}" type="text" name="link" {{ Admin_model::check_admin(array('B2B_BANERI_I_SLAJDER_AZURIRANJE')) == false ? 'readonly' : '' }}>
					<label class="text-center">Link desne strane</label>
					<input id="link2" value="{{B2bOptions::getlink2()}}" type="text" name="link2" {{ Admin_model::check_admin(array('B2B_BANERI_I_SLAJDER_AZURIRANJE')) == false ? 'readonly' : '' }}>
					<input type="hidden" value="3" name="flag"><br>
					@if(Admin_model::check_admin(array('B2B_BANERI_I_SLAJDER_AZURIRANJE')))
					<div class="text-center btn-container">
						<button type="submit" class="btn btn-primary save-it-btn">Sačuvaj</button>
						<a class="btn btn-danger" href="{{AdminB2BOptions::base_url()}}admin/b2b/bg-img-delete">Obriši</a>
					</div>
					@endif
				</form>
			</ul>
		</div> 
	</section> 
	<section class="page-edit medium-12 large-9 columns">
		<div class="flat-box"> 
			<form action="{{AdminB2BOptions::base_url()}}admin/b2b/banner-edit" name="banner_upload" method="post" accept-charset="utf-8" enctype="multipart/form-data" class="logo-right">
				@if($type == 1)
				<label class="text-center">Izmeni baner</label>
				@elseif($type == 2)
				<label class="text-center">Izmeni slajder</label>
				@elseif($type == 9)
				<label class="text-center">Izmeni popup baner</label>
				@endif
				<section class="banner-edit-top row">
					<div class="medium-5 columns">
						@if($type == 1)
						<label>Naziv banera</label>
						@elseif($type == 2)
						<label>Naziv slajdera</label>
						@elseif($type == 9)
						<label>Naziv popup banera</label>
						@endif
						<div class="relative"> 
							<input id="naziv" value="{{ Input::old('naziv') ? Input::old('naziv') : trim($item->naziv) }}" type="text" name="naziv" {{ Admin_model::check_admin(array('B2B_BANERI_I_SLAJDER_AZURIRANJE')) == false ? 'readonly' : '' }}>
							@if(Admin_model::check_admin(array('B2B_BANERI_I_SLAJDER_AZURIRANJE')))
							<span class="banner-uploads has-tooltip"><input type="file" name="img"><span>Dodaj sliku</span></span>
							@endif
						</div>
						<div class="error">{{ $errors->first('naziv') ? $errors->first('naziv') : '' }}</div>
						<div class="error">{{ $errors->first('img') ? ($errors->first('img') == 'Niste popunili polje.' ? 'Niste izabrali sliku.' : $errors->first('img')) : '' }}</div>
					</div>
					<div class="medium-6 columns">
						<label>{{$type==1 ? 'Link banera' : 'Link slajdera'}}</label>
						<input id="link" value="{{ Input::old('link') ? Input::old('link') : trim($item->link) }}" type="text" name="link" {{ Admin_model::check_admin(array('B2B_BANERI_I_SLAJDER_AZURIRANJE')) == false ? 'readonly' : '' }}>
						<div class="error">{{ $errors->first('link') ? $errors->first('link') : '' }}</div>
					</div>
				</section>

				<div class="row">
					<div class="medium-2 columns"> 
						<label>Aktivan</label>
						<select name="aktivan" {{ Admin_model::check_admin(array('B2B_BANERI_I_SLAJDER_AZURIRANJE')) == false ? 'disabled' : '' }}>
							<option value="1">DA</option>
							<option value="0" {{ (Input::old('aktivan') == '0') ? 'selected' : (($item->aktivan == 0) ? 'selected' : '') }}>NE</option>
						</select>
					</div>
					@if($type == 1)
					<div class="column medium-3 akcija-column">
						<label>Baner postavljen:</label>
						<div class="relative"> 
							<input class="akcija-input" id="datum_od_b2b" name="datum_od" autocomplete="off" type="text" value="{{ Input::old('datum_od') ? Input::old('datum_od') : $datum_od }}" {{ Admin_model::check_admin(array('B2B_BANERI_I_SLAJDER_AZURIRANJE')) == false ? 'readonly' : '' }}>
							@if(Admin_model::check_admin(array('B2B_BANERI_I_SLAJDER_AZURIRANJE')))
							<span id="datum_od_delete" class="absolute-right text-red"><i class="fa fa-close tooltipz-left" aria-label="Ukloni"></i></span>
						</div>
						@endif
					</div>
					@elseif($type == 2)
					<div class="column medium-3 akcija-column">
						<label>Slajder postavljen:</label>
						<div class="relative"> 
							<input class="akcija-input" id="datum_od_b2b" name="datum_od" autocomplete="off" type="text" value="{{ Input::old('datum_od') ? Input::old('datum_od') : $datum_od }}" {{ Admin_model::check_admin(array('B2B_BANERI_I_SLAJDER_AZURIRANJE')) == false ? 'readonly' : '' }}>
							@if(Admin_model::check_admin(array('B2B_BANERI_I_SLAJDER_AZURIRANJE')))
							<span id="datum_od_delete" class="absolute-right text-red"><i class="fa fa-close tooltipz-left" aria-label="Ukloni"></i></span>
						</div>
						@endif
					</div>
					@elseif($type == 9)
					<div class="column medium-3 akcija-column">
						<label>Pop-Up baner postavljen:</label>
						<div class="relative"> 
							<input class="akcija-input" id="datum_od_b2b" name="datum_od" autocomplete="off" type="text" value="{{ Input::old('datum_od') ? Input::old('datum_od') : $datum_od }}" {{ Admin_model::check_admin(array('B2B_BANERI_I_SLAJDER_AZURIRANJE')) == false ? 'readonly' : '' }}>
							@if(Admin_model::check_admin(array('B2B_BANERI_I_SLAJDER_AZURIRANJE')))
							<span id="datum_od_delete" class="absolute-right text-red"><i class="fa fa-close tooltipz-left" aria-label="Ukloni"></i></span>
							@endif
						</div>
					</div>
					@endif
					<div class="column medium-3 akcija-column">
						<label>Traje Do:</label>
						<div class="relative"> 
							<input class="akcija-input" id="datum_do_b2b" name="datum_do" autocomplete="off" type="text" value="{{ Input::old('datum_do') ? Input::old('datum_do') : $datum_do }}" {{ Admin_model::check_admin(array('B2B_BANERI_I_SLAJDER_AZURIRANJE')) == false ? 'readonly' : '' }}>
							@if(Admin_model::check_admin(array('B2B_BANERI_I_SLAJDER_AZURIRANJE')))
							<span id="datum_do_delete" class="absolute-right text-red"><i class="fa fa-close tooltipz-left" aria-label="Ukloni"></i></span>
							@endif
						</div>
					</div>
				</div>				

				<section class="banner-preview-area">
					<img class="banner-preview" src="{{AdminB2BOptions::base_url()}}{{$item->img}}" alt="{{$item->naziv}}" />
				</section>		
				<div class="banner-display-pages">
					<input type="hidden" name="baneri_id" value="{{$item->baneri_id}}" />
					<input type="hidden" name="tip_prikaza" value="{{$type}}" />
					@if(Admin_model::check_admin(array('B2B_BANERI_I_SLAJDER_AZURIRANJE')))
					<div class="btn-container center">
						<button class="btn btn-primary save-it-btn">Sačuvaj</button>
					</div>
					@endif
				</div>
			</form>
		</div>
	</section>
</section>
@endsection

