@extends('b2b.templates.main')
@section('content')  
<div class="row">
	<div class="col-xs-12 single-news-container">
		
		<img class="img-responsive" src="{{ B2bOptions::base_url() . $vest->slika }}" alt="{{ $vest->naslov }}">
		
		<h2><span class="section-title"> {{ $vest->naslov }}</span></h2>
		
		{{ $vest->sadrzaj }}
	</div>
</div>    
@endsection