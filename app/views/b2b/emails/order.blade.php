<svg style="display: none;">
    <style>
 
        table tr td { padding: 3px 6px; border-bottom: 1px solid #ccc; }
        
        table tr th { padding: 3px 6px; }
        
        .table-title {
            background: #5ba7db;
            font-weight: 500;
            color: #fff; 
        }
        .table-regular {
            width: 100%;
            table-layout: fixed;
            border: 1px solid #ccc;
        } 
        @media screen and (max-width: 768px){
            .table-respons tr td, .table-respons tr th {
                white-space: nowrap;
            }
            table tr th , table tr td { 
                word-break: break-word; 
            }
        }
    </style>
</svg>

<!-- ************************ -->

<div class="order-page" style="max-width: 1170px; margin: auto; padding: 0 10px; font-size:14px;">
    <br>

    <h2 style="
        color: #222;
        margin: 10px 0 20px;
        font-size: 20px;
        text-transform: uppercase;
        font-weight: 600;
        display: block; 
        font-weight: 500;">
        Završena kupovina
    </h2> 
       
    <table class="table-regular">
        <thead>
            <tr>
                <th colspan="2" class="table-title">
                    Informacije o narudžbini:
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Broj porudžbine:</td>
                <td>{{$order->broj_dokumenta}}</td>
            </tr>
            <tr>
                <td>Datum porudžbine:</td>
                <td>{{ date('d.m.Y',strtotime($order->datum_dokumenta)) }}</td>
            </tr>
            <tr>
                <td>Način isporuke:</td>
                <td>{{ B2bBasket::getNameNacinIsporuke($order->web_nacin_isporuke_id) }}</td>
            </tr>
            <tr>
                <td>Način plaćanja:</td>
                <td>{{ B2bBasket::getNameNacinPlacanja($order->web_nacin_placanja_id) }}</td>
            </tr>
            <tr>
                <td>Napomena:</td>
                <td>{{$order->napomena}}</td>
            </tr>
        </tbody> 
    </table> 

<!-- ************************ -->
    <table class="table-regular">
        <thead>
            <tr>
                <th colspan="2" class="table-title">
                    Informacije o kupcu:
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Naziv:</td>
                @if(!is_null($partnerUser = B2bPartner::getPartnerUserObject()))
                <td>{{$partner->naziv}} - {{ $partnerUser->naziv }}</td>
                @else
                <td>{{$partner->naziv}}</td>
                @endif
            </tr>
            <tr>
                <td>PIB:</td>
                <td>{{$partner->pib}}</td>
            </tr>
            <tr>
                <td>Matični broj:</td>
                <td>{{$partner->broj_maticni}}</td>
            </tr>
            <tr>
                <td>Adresa:</td>
                <td>{{is_null($partnerKorisnik) ? $partner->adresa : $partnerKorisnik->adresa }}</td>
            </tr>
            <tr>
                <td>Mesto</td>
                <td>{{is_null($partnerKorisnik) ? $partner->mesto : $partnerKorisnik->mesto }}</td>
            </tr>
            <tr>
                <td>Telefon</td>
                <td>{{is_null($partnerKorisnik) ? $partner->telefon : $partnerKorisnik->telefon }}</td>
            </tr>
            <tr>
                <td>Email:</td>
                <td>{{(is_null($partnerKorisnik) OR empty($partnerKorisnik->email)) ? $partner->mail : $partnerKorisnik->email }}</td>
            </tr>
        </tbody>
    </table>

<!-- ************************ -->
 
    <table class="table-regular">
        <thead>
            <tr>
                <th colspan="2" class="table-title">
                    Informacije o prodavcu:
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Naziv prodavca:</td>
                <td>{{B2bOptions::company_name()}}</td>
            </tr>
            <tr>
                <td>Adresa:</td>
                <td>{{B2bOptions::company_adress()}}</td>
            </tr>
            <tr>
                <td>Telefon:</td>
                <td>{{B2bOptions::company_phone()}}</td>
            </tr>
            <tr>
                <td>Fax:</td>
                <td>{{B2bOptions::company_fax()}}</td>
            </tr>
            <tr>
                <td>PIB:</td>
                <td>{{B2bOptions::company_pib()}}</td>
            </tr>
            <tr>
                <td>Šifra delatnosti:</td>
                <td>{{B2bOptions::company_delatnost_sifra()}}</td>
            </tr>
            <tr>
                <td>Žiro račun:</td>
                <td>{{B2bOptions::company_ziro()}}</td>
            </tr>
            <tr>
                <td>E-mail:</td>
                <td>{{B2bOptions::company_email()}}</td>
            </tr>
        </tbody>
    </table>

<!-- ************************ -->
    <br>
    <br>

    <div style="overflow-x: auto;"> 
        <table style="width: 100%; border: 1px solid #ccc;" class="table-respons">
            <thead>
                <tr>
                    <th colspan="7" class="table-title">
                        Proizvodi:
                    </th>
                </tr>
                <tr>
                    <th>Naziv</th>
                    <th>Cena</th>
                    <th>Rabat</th>
                    <th>Cena sa rab.</th>
                    <th>PDV</th>
                    <th>Količina</th>
                    <th>Ukupno</th>
                </tr>
            </thead>
            <tbody>
                @foreach($orderItems as $row)
                <?php
                $cene = B2bBasket::b2bOrderStavkaCene($row->web_b2b_narudzbina_stavka_id);
                $robaObj = B2bArticle::getObject($row->roba_id);
                ?>
                <tr>
                    <td><span style="display: inline-block; max-width: 270px;">{{ B2bArticle::short_title($robaObj->roba_id,33) }}</span></td>
                   <!--  <td>{{$robaObj->id_is}} </td> -->
                    <td>{{number_format($cene->osnovna_cena,2)}} </td>
                    <td>{{number_format(($cene->ukupan_rabat))}} </td>
                    <td>{{number_format($cene->cena_sa_rabatom,2)}} </td>
                    <td>{{number_format($cene->porez)}} </td>
                    <td>{{(int)$row->kolicina }} </td>
                    <td>{{number_format($cene->ukupna_cena*$row->kolicina,2)}} </td>
                </tr>
                @endforeach
                <?php
                $orderTotal = B2bBasket::orderTotal($order->web_b2b_narudzbina_id);
                ?>
                <tr>
                    <th colspan="5"></th>
                    <th>Osnovica:</th>
                    <th>{{number_format($orderTotal->cena_sa_rabatom,2)}}</th>
                </tr>
                <tr>
                    <th colspan="5"></th>
                    <th>PDV:</th>
                    <th>{{number_format($orderTotal->porez_cena,2)}}</th>
                </tr>
                @if($orderTotal->avans > 0)
                <tr>
                    <th colspan="5"></th>
                    <th>Avans:</th>
                    <th>-{{number_format($orderTotal->avans,2)}}</th>
                </tr>
                @endif
                <?php $troskovi = B2bBasket::troskovi($order->web_b2b_narudzbina_id); ?>
                @if($troskovi > 0 AND $order->web_nacin_isporuke_id != 2)
                <tr>
                    <th colspan="5"></th>
                    <th>Troškovi isporuke:</th>
                    <th>{{number_format($troskovi,2)}}</th>
                </tr>
                @endif
                <tr>
                    <th colspan="5"></th>
                    <th>Ukupno:</th>
                    <th>{{number_format(($orderTotal->ukupna_cena+(($troskovi > 0 AND $order->web_nacin_isporuke_id != 2) ? $troskovi : 0)),2)}}</th>
                </tr>
            </tbody>
        </table>
    </div>
 
</div>