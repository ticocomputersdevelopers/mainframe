<div class="product-list table-responsive">
    <table class="table table-bordered table-striped"> 
        <thead> 
            <tr> 
                <th>Naziv</th>
                <th>Slika</th>
                <th>Cena</th>
                <th>Rabat</th> 
                <th>Cena sa rabatom</th>
                <th>PDV Stopa</th>
                <th>Cena sa PDV-om</th>
                <th>Lager</th>
                <th>Korpa</th>
            </tr>
        </thead>
        
        <tbody> 
            @foreach($query_products as $row)
            <tr> 
                <td class="relative">
                    <a class="product-title" href="{{B2bOptions::base_url()}}b2b/artikal/{{B2bUrl::slugify(B2bArticle::seo_title($row->roba_id))}}"><span class="xs-wrap">{{ B2bArticle::short_title($row->roba_id) }}</span></a>
                    <div class="addon-title">{{ B2bArticle::addon_title($row->roba_id) }}</div>
                    @if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
                        @if(Session::has('b2c_admin'.B2bOptions::server()))
                        <a class="article-edit-btn" target="_blank" href="{{ B2bOptions::base_url() }}admin/product/{{ $row->roba_id }}">IZMENI ARTIKAL</a>
                        @endif
                    @endif
                </td>
                <td>
                <a href="{{B2bOptions::base_url()}}b2b/artikal/{{B2bUrl::slugify(B2bArticle::seo_title($row->roba_id))}}" class="product-image-wrapper">
                    <img class="img-responsive margin-auto" src="{{B2bOptions::base_url()}}{{ B2bArticle::web_slika($row->roba_id) }}" alt="{{ B2bArticle::seo_title($row->roba_id) }}" />
                </a>
                </td>
                <?php
                $rabatCene = B2bArticle::b2bRabatCene($row->roba_id);
                $lager = B2bArticle::lagerObj($row->roba_id);
                $cartAmount = B2bBasket::getB2bQuantityItem($row->roba_id);
                $quantity = $lager->kolicina - ($lager->rezervisano + $cartAmount);
                $razlika = B2bArticle::price_diff($row->roba_id);               

                ?>
 
                <td>
                    @if(B2bArticle::getStatusArticle($row->roba_id) == 1)
                    <div data-toggle="tooltip" title="Cena"> {{B2bBasket::cena($rabatCene->osnovna_cena)}}</div>
                    @if($razlika > 0)<i class="price-change fa fa-arrow-up"></i>@elseif($razlika < 0)<i class="price-change fa fa-arrow-down">@endif</i>
                    @endif
                </td>
                <td>
                     
                    <div class="table-action">{{ B2bCommon::provera_akcija($row->roba_id) ? 'Akcija' : '' }} </div>
                    <span data-toggle="tooltip" title="Rabat">@if(B2bArticle::getStatusArticle($row->roba_id) == 1) {{number_format($rabatCene->ukupan_rabat,2)}} % @endif</span>
                </td>
 
                
                <td><span data-toggle="tooltip" title="Cena sa rabatom"> @if(B2bArticle::getStatusArticle($row->roba_id) == 1) {{B2bBasket::cena($rabatCene->cena_sa_rabatom)}} @endif</span></td>
               
                <td><span data-toggle="tooltip" title="PDV stopa"> @if(B2bArticle::getStatusArticle($row->roba_id) == 1){{number_format($rabatCene->porez,2)}} % @endif</span></td>
                
                <td><span data-toggle="tooltip" title="Cena sa PDV-om">@if(B2bArticle::getStatusArticle($row->roba_id) == 1) {{B2bBasket::cena($rabatCene->ukupna_cena,2)}} @endif</span></td>
                
                <td><span data-toggle="tooltip" title="Lager"> {{$quantity <= 10 ? $quantity :'> 10'}} </span></td>
                <td class="table-btn">
                @if(B2bArticle::getStatusArticle($row->roba_id) == 1)
                    @if($quantity>0)

                        <div class="inline-block quantity-change"> 
                            <a href="javascript:void(0)" class="JSProductListCartLess"><i class="fas fa-minus"></i></a>
                            <input class="JSProductListCartAmount add-amount" id="quantity-{{$row->roba_id}}" type="text" value="1">
                            <a href="javascript:void(0)" class="JSProductListCartMore"><i class="fas fa-plus"></i></a> 
                        </div>
                     
                        <button class="button add-to-cart-products" data-max-quantity="{{$quantity}}" data-roba-id="{{$row->roba_id}}"> 
                            U korpu
                        </button>
                        @if(!is_null(B2bPartner::dokumentiUser()))
                        <button class="button add-to-offer JSadd-to-offer-products" data-roba-id="{{$row->roba_id}}">Dodaj u ponudu</button>
                        @endif
                    @else
                        <button class="button">Nije dostupan</button>
                        @endif
                    @else
                    <button class="button">{{ B2bArticle::find_flag_cene(B2bArticle::getStatusArticle($row->roba_id),'naziv') }}</button>
                @endif  
                </td> 
            </tr>
            @endforeach
        </tbody> 
    </table>
</div>