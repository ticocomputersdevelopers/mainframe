<div class="header-cart-content hidden-sm hidden-xs" id="header-cart-content">
    @if((Route::current()->getName() != 'b2b.order'))
        @include('b2b/partials/ajax/header_cart')
    @endif
</div>