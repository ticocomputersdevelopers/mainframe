<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use Service\ElasticSearchService;


class ElasticReindexingCommand extends Command {
	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'elastic:reindexing';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Elastic Reindexing Command.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire(){

		$elastic = new ElasticSearchService();
		$elastic->updateGroups();
		$elastic->updateManufacturers();
		$elastic->updateArticles();
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			// array('mapped_all', InputArgument::OPTIONAL, 'Image path on root.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
		// 	array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}