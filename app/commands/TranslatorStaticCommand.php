<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use Service\TranslatorService;


class TranslatorStaticCommand extends Command {
	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'static:translate';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Translate.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	

	public function fire()
	{
		$izabraniJezik = DB::table('jezik')->where(array('aktivan'=>1,'izabrani'=>1))->first();
		$srJezik = DB::table('jezik')->where(array('kod'=>'sr'))->first();

		$defaultColumn = null;
		if($izabraniJezik->kod == 'sr'){
			$defaultColumn = 'B';
		}else if($izabraniJezik->kod == 'en'){
			$defaultColumn = 'C';
		}else if($izabraniJezik->kod == 'de'){
			$defaultColumn = 'D';
		}else if($izabraniJezik->kod == 'cir'){
			$column = '';
		}else{
			$this->info('Please choose language.');
			die;
		}

		$translator = new TranslatorService($srJezik->kod,$izabraniJezik->kod);

		$products_file = 'files/langs/Prevod.xlsx';
		PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);
        $excelReader = PHPExcel_IOFactory::createReaderForFile($products_file);
        $excelObj = $excelReader->load($products_file);
        $worksheet = $excelObj->getSheet(0);
        $lastRow = $worksheet->getHighestRow();
        
		//static
        foreach(DB::table('jezik')->where(array('aktivan'=>1))->where('jezik_id','!=',$izabraniJezik->jezik_id)->get() as $jezik){
			$column = null;
			if($jezik->kod == 'sr'){
				$column = 'B';
			}else if($jezik->kod == 'en'){
				$column = 'C';
			}else if($jezik->kod == 'de'){
				$column = 'D';
			}else if($jezik->kod == 'cir'){
				$column = '';
			}else{
				$this->info('Please choose language.');
				die;
			}

			for ($row = 2; $row <= $lastRow; $row++) {
				$check = trim($worksheet->getCell('A'.$row)->getValue());
				$word = $jezik->kod == 'cir' ? $translator->translate(trim($worksheet->getCell('B'.$row)->getValue())) : trim($worksheet->getCell($column.$row)->getValue());
				$defaultWord = $izabraniJezik->kod == 'cir' ? $translator->translate(trim($worksheet->getCell('B'.$row)->getValue())) : trim($worksheet->getCell($defaultColumn.$row)->getValue());
				
				if ($check == 'f'){		
					if(trim($word) && trim($word) != ''){
						if(DB::table('prevodilac')->where(array('izabrani_jezik_id'=>$izabraniJezik->jezik_id,'jezik_id'=>$jezik->jezik_id,'reci'=>trim($word)))->count() == 0){
							DB::table('prevodilac')->insert(array('izabrani_jezik_id'=>$izabraniJezik->jezik_id,'jezik_id'=>$jezik->jezik_id,'izabrani_jezik_reci'=>trim($defaultWord),'reci'=>trim($word),'is_js'=>1));
						}
					}
				}			
			}
        }
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			// array('langs', InputArgument::OPTIONAL, 'Languages.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
		// 	array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}