<?php
ini_set('memory_limit', '-1');

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;


class DSCCrawlerCommand extends Command {
	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'dsc:crawler';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'DSCCrawlerCommand.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	public static function fileExtension($image_path){
		$extension = 'jpg';
    	$slash_parts = explode('/',$image_path);
    	$old_name = $slash_parts[count($slash_parts)-1];
    	$old_name_arr = explode('.',$old_name);
    	$extension = $old_name_arr[count($old_name_arr)-1];
    	return $extension;
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$userAgent = 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.2309.372 Safari/537.36';
		$cookieFile = 'cookie.txt';
		$loginFormUrl = 'http://www.dsc.co.rs/panel/login.php?accesscheck=%2Fpanel%2Fpanelpocetna.php';
		$loginActionUrl = 'http://www.dsc.co.rs/panel/login.php';
		 
		$postValues = array(
		    'user' => 'Darko',
		    'pass' => 'Dare',
		    'button' => 'Uloguj+se'
		);

		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $loginActionUrl);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($postValues));
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_COOKIEJAR, $cookieFile);
		curl_setopt($curl, CURLOPT_USERAGENT, $userAgent);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_REFERER, $loginFormUrl);
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, false);
		curl_exec($curl);
		if(curl_errno($curl)){
		    throw new Exception(curl_error($curl));
		}

		curl_setopt($curl, CURLOPT_URL, 'http://www.dsc.co.rs/panel/artiklipregled.php');
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET');
		curl_setopt($curl, CURLOPT_COOKIEJAR, $cookieFile);
		curl_setopt($curl, CURLOPT_USERAGENT, $userAgent);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		// curl_setopt($curl, CURLOPT_FILE, fopen($file, "w"));
		$resultArticles = curl_exec($curl);



		preg_match_all('/<td>Y<\/td>\n      <td>.*?<\/td>    \n      <td class="c33"><span class="orange"><a href="artikliizmena\.php\?art_id=(.*?)"><img src="img\/update\.gif" width="16" height="16" alt="izmeni" border="0"\/><\/a><\/span><\/td>/i',$resultArticles,$matchResult);
		$ids = $matchResult[1];

		foreach(array_slice($ids,0,30) as $id){
			// $id=357;
			curl_setopt($curl, CURLOPT_URL, 'http://www.dsc.co.rs/panel/artikliizmena.php?art_id='.strval($id));
			curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET');
			curl_setopt($curl, CURLOPT_COOKIEJAR, $cookieFile);
			curl_setopt($curl, CURLOPT_USERAGENT, $userAgent);
			curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			$resultArticle = curl_exec($curl);

			preg_match_all('/<img src="\.\.\/files\/images\/artikli\/.*?_v\..*?" alt="" >\n<br>\n(.*?)\n<br>/i',$resultArticle,$matchResultImage);
			
			if(isset($matchResultImage[1]) && isset($matchResultImage[1][0])){
				$imageSource = $matchResultImage[1][0];
	            try { 
	                // $slika_id = DB::select("SELECT nextval('web_slika_web_slika_id_seq')")[0]->nextval;
	                // $name = $slika_id.'.'.self::fileExtension($imageSource);
	                $name = strval($id).'.'.self::fileExtension($imageSource);
	                $destination = 'images/products/big/'.$name;
	                $url = 'http://www.dsc.co.rs/'.$imageSource;
	                $akcija = 1;
	        	
	                $content = @file_get_contents($url);
	                file_put_contents($destination,$content);

	                // $insert_data = array(
	                //     'web_slika_id'=>intval($slika_id),
	                //     'roba_id'=> $article[0]->roba_id,
	                //     'akcija'=>$akcija, 
	                //     'flag_prikazi'=>1,
	                //     'putanja'=>'images/products/big/'.$name
	                //     );

	                // DB::table('web_slika')->insert($insert_data);
	            }
	            catch (Exception $e) {
	            }
	        }

	        preg_match_all('/<input type="hidden" id="art_opis" name="art_opis" value=["\']?([^"\'>]+)["\']? style="display:none" \/>/i',$resultArticle,$matchResultDesciption);
			if(isset($matchResultDesciption[1]) && isset($matchResultDesciption[1][0])){
				$articleDesciption = $matchResultDesciption[1][0];
				$articleDesciption = html_entity_decode($articleDesciption);
			}
	        
		}
		curl_close($curl);		
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			// array('mapped_all', InputArgument::OPTIONAL, 'Image path on root.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
		// 	array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}