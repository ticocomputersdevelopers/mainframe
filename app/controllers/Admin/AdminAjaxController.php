<?php
use Service\ElasticSearchService;

class AdminAjaxController extends Controller {

    public function ajax(){
        $action = Input::get('action');
        if($action == 'adminHeaderWidth') {
            if (Session::get('adminHeaderWidth')) {
                Session::put('adminHeaderWidth', '');
            } else {
                Session::put('adminHeaderWidth', 'small-width');
            }
        } elseif ($action == 'collapseAdminHeaderWidth') {
            Session::put('adminHeaderWidth', 'small-width');
        } elseif ($action == 'expendAdminHeaderWidth') {
            Session::put('adminHeaderWidth', '');
        }

        if($action == 'collapseCategories' ) {
            Session::put('adminCategories', 'wide-articles');
        } elseif ($action == 'openCategories' ) {
            Session::put('adminCategories', 'cats-show');    
        }

        if($action == 'getTemplate' ) {
            $roba_id = Input::get('roba_id');
            $sablon = AdminSupport::getTemplate($roba_id);
            return json_encode($sablon);
        }

    }
    
    public function ajaxGenerisane(){
        $action = Input::get('action');

        if($action == 'add_all_karak') {
            $roba_id = Input::get('roba_id');
            $grupa_pr_naziv_ids = AdminSupport::getGrupaKarak(AdminArticles::find($roba_id,'grupa_pr_id'));

            foreach($grupa_pr_naziv_ids as $row){
                $check_query = DB::table('web_roba_karakteristike')->where(array('roba_id'=>$roba_id, 'grupa_pr_naziv_id'=>$row->grupa_pr_naziv_id))->count();
                $grupa_pr_vrednost = DB::table('grupa_pr_vrednost')->where('grupa_pr_naziv_id',$row->grupa_pr_naziv_id)->orderBy('rbr','asc')->first();
                if($check_query == 0 && !is_null($grupa_pr_vrednost)){
                    DB::table('web_roba_karakteristike')->insert(array('roba_id'=>$roba_id, 'grupa_pr_naziv_id'=>$row->grupa_pr_naziv_id, 'grupa_pr_vrednost_id'=>$grupa_pr_vrednost->grupa_pr_vrednost_id, 'vrednost'=>$grupa_pr_vrednost->naziv, 'rbr'=>$row->rbr));
                }
            }
            AdminSupport::saveLog('KARAKTERISTIKE_DODAVANJE_GENERISANE', array(DB::table('web_roba_karakteristike')->max('web_roba_karakteristike_id')));
        }
        elseif($action == 'add_karak') {
            $roba_id = Input::get('roba_id');
            $grupa_pr_naziv_id = Input::get('grupa_pr_naziv_id');
            $rbr = DB::table('grupa_pr_naziv')->where('grupa_pr_naziv_id',$grupa_pr_naziv_id)->pluck('rbr');           

            $check_query = DB::table('web_roba_karakteristike')->where(array('roba_id'=>$roba_id, 'grupa_pr_naziv_id'=>$grupa_pr_naziv_id,'rbr'=>$rbr))->count();
            $grupa_pr_vrednost = DB::table('grupa_pr_vrednost')->where('grupa_pr_naziv_id',$grupa_pr_naziv_id)->orderBy('rbr','asc')->first();
            if($check_query == 0 && !is_null($grupa_pr_vrednost)){
                DB::table('web_roba_karakteristike')->insert(array('roba_id'=>$roba_id, 'grupa_pr_naziv_id'=>$grupa_pr_naziv_id, 'grupa_pr_vrednost_id'=>$grupa_pr_vrednost->grupa_pr_vrednost_id, 'vrednost'=>$grupa_pr_vrednost->naziv, 'rbr'=>$rbr));
                AdminSupport::saveLog('KARAKTERISTIKA_DODAVANJE', array(DB::table('web_roba_karakteristike')->max('web_roba_karakteristike_id')));
            }
        }
        elseif($action == 'save-gener') {
            $roba_id = Input::get('roba_id');
            $grupa_pr_naziv_id = Input::get('grupa_pr_naziv_id');
            $grupa_pr_vrednost_id = Input::get('grupa_pr_vrednost_id');
            $vrednost = DB::table('grupa_pr_vrednost')->where('grupa_pr_vrednost_id',$grupa_pr_vrednost_id)->pluck('naziv');
            $rbr = Input::get('rbr');

            $data = array('grupa_pr_vrednost_id'=>$grupa_pr_vrednost_id, 'vrednost'=>$vrednost, 'rbr'=>$rbr);
            if(!is_numeric($rbr)){
                unset($data['rbr']);
            }
            DB::table('web_roba_karakteristike')->where(array('roba_id'=>$roba_id, 'grupa_pr_naziv_id'=>$grupa_pr_naziv_id))->update($data);
            AdminSupport::saveLog('G_KARAKTERISTIKE_IZMENA', array($grupa_pr_vrednost_id));
        }
        elseif($action == 'save-gener-val') {
            $roba_id = Input::get('roba_id');           
            $grupa_pr_naziv_id = Input::get('grupa_pr_naziv_id');            
            $grupa_pr_vrednost_id = Input::get('grupa_pr_vrednost_id');
            $naziv = Input::get('naziv');
            $rbr = Input::get('rbr');

            $data = array(
                'naziv'=>$naziv,        
            );
            $rules = array(
                'naziv' => 'required|unique:grupa_pr_vrednost,naziv,'.$grupa_pr_vrednost_id.',grupa_pr_vrednost_id',
            );
            $validator = Validator::make($data, $rules);
            if($validator->fails()){
                $errors = $validator->messages();
                echo json_encode(
                    array(
                    'message'=>'error',
                    'errors'=>array(
                        'naziv' => $errors->first('naziv')
                    )
                    )
                ); 
            }else{
            if(!is_numeric($rbr)){
                unset($data['rbr']);
            }
            DB::table('grupa_pr_vrednost')->insert(array('naziv'=>$naziv,'rbr'=>$rbr,'grupa_pr_naziv_id'=>$grupa_pr_naziv_id,'active'=>1));
            $vrednost_id= DB::table('grupa_pr_vrednost')->where('naziv',$naziv)->where('grupa_pr_naziv_id',$grupa_pr_naziv_id)->pluck('grupa_pr_vrednost_id');
            
            DB::table('web_roba_karakteristike')->where(array('roba_id'=>$roba_id, 'grupa_pr_naziv_id'=>$grupa_pr_naziv_id))->update(array('vrednost'=>$naziv, 'grupa_pr_vrednost_id'=>$vrednost_id));
            AdminSupport::saveLog('ARTIKLI_G_KARAK_VREDNOST_DODAJ', array(DB::table('grupa_pr_vrednost')->max('grupa_pr_vrednost_id')));
            }
        }
        elseif($action == 'delete-gener') {
            $roba_id = Input::get('roba_id');
            $grupa_pr_naziv_id = Input::get('grupa_pr_naziv_id');

            AdminSupport::saveLog('G_KARAKTERISTIKE_OBRISI', array(DB::table('web_roba_karakteristike')->where(array('roba_id'=>$roba_id, 'grupa_pr_naziv_id'=>$grupa_pr_naziv_id))->pluck('web_roba_karakteristike_id')));
            DB::table('web_roba_karakteristike')->where(array('roba_id'=>$roba_id, 'grupa_pr_naziv_id'=>$grupa_pr_naziv_id))->delete();
            header("Refresh:0");
        }

    }

    public function ajaxOdDobavljaca(){
        $action = Input::get('action');

        if($action == 'save_naziv') {

            $karakteristika_id = Input::get('karakteristika_id');
            $naziv = Input::get('naziv');
            $vrednost = Input::get('vrednost');

            if($karakteristika_id == 'new'){
                $roba_id = Input::get('roba_id');
                $grupa = Input::get('grupa');
                $dobavljac_cenovnik_karakteristike_id = DB::select("SELECT MAX(dobavljac_cenovnik_karakteristike_id) AS max FROM dobavljac_cenovnik_karakteristike")[0]->max + 1;

                if(isset($grupa)){
                    $first = DB::table('dobavljac_cenovnik_karakteristike')->where(array('roba_id'=>$roba_id,'karakteristika_grupa'=>$grupa))->first();
                    $rbr = $first->redni_br_grupe;
                    $partner_id = $first->partner_id;
                    $sifra = $first->sifra_kod_dobavljaca;
                    DB::table('dobavljac_cenovnik_karakteristike')->insert(array('dobavljac_cenovnik_karakteristike_id'=>$dobavljac_cenovnik_karakteristike_id,'roba_id'=>$roba_id,'partner_id'=>$partner_id,'sifra_kod_dobavljaca'=>$sifra,'karakteristika_naziv'=>$naziv,'karakteristika_vrednost'=>$vrednost,'karakteristika_grupa'=>$grupa,'redni_br_grupe'=>$rbr));
                    $first = DB::table('dobavljac_cenovnik_karakteristike')->where('roba_id',$roba_id)->first();
                    $partner_id = $first->partner_id;
                    $sifra = $first->sifra_kod_dobavljaca;
                    DB::table('dobavljac_cenovnik_karakteristike')->insert(array('dobavljac_cenovnik_karakteristike_id'=>$dobavljac_cenovnik_karakteristike_id,'roba_id'=>$roba_id,'partner_id'=>$partner_id,'sifra_kod_dobavljaca'=>$sifra,'karakteristika_naziv'=>$naziv,'karakteristika_vrednost'=>$vrednost));
                AdminSupport::saveLog('DOBAVLJAC_KARAKTERISTIKE_DODAJ', array($dobavljac_cenovnik_karakteristike_id));
                } 

            }else{
                $roba_id = Input::get('roba_id');
                DB::table('dobavljac_cenovnik_karakteristike')->where('dobavljac_cenovnik_karakteristike_id',$karakteristika_id)->update(array('karakteristika_naziv'=>$naziv,'karakteristika_vrednost'=>$vrednost));
                AdminSupport::saveLog('DOBAVLJAC_KARAKTERISTIKE_IZMENI', array($karakteristika_id));
            }
        }
        elseif($action == 'delete_naziv') {
            $karakteristika_id = Input::get('karakteristika_id');
            AdminSupport::saveLog('DOBAVLJAC_KARAKTERISTIKE_OBRISI', array($karakteristika_id));
            DB::table('dobavljac_cenovnik_karakteristike')->where('dobavljac_cenovnik_karakteristike_id',$karakteristika_id)->delete();
        }

        elseif($action == 'save_grupa') {
            $roba_id = Input::get('roba_id');
            $grupa = Input::get('grupa');
            $grupa_old = Input::get('grupa_old');
            $check =  DB::table('dobavljac_cenovnik_karakteristike')->where(array('roba_id'=>$roba_id, 'karakteristika_grupa'=>$grupa))->count();

            if($check == 0){
                if(isset($grupa_old)){

                    $rbr = Input::get('rbr');
                    DB::table('dobavljac_cenovnik_karakteristike')->where(array('roba_id'=>$roba_id, 'karakteristika_grupa'=>$grupa_old))->update(array('karakteristika_grupa'=>$grupa,'redni_br_grupe'=>$rbr));
                    AdminSupport::saveLog('ARTIKLI_KARAKTERISTIKA_IZMENI', array(DB::table('dobavljac_cenovnik_karakteristike')->where(array('roba_id'=>$roba_id, 'karakteristika_grupa'=>$grupa_old,'karakteristika_grupa'=>$grupa,'redni_br_grupe'=>$rbr))->pluck('dobavljac_cenovnik_karakteristike_id')));
                }else{
                    $first = DB::table('dobavljac_cenovnik_karakteristike')->where('roba_id',$roba_id)->first();
                    $partner_id = $first->partner_id;
                    $sifra = $first->sifra_kod_dobavljaca;

                    $rbr = DB::select("SELECT MAX(redni_br_grupe) AS max FROM dobavljac_cenovnik_karakteristike WHERE roba_id=".$roba_id."")[0]->max + 1;
                    DB::table('dobavljac_cenovnik_karakteristike')->insert(array('roba_id'=>$roba_id,'partner_id'=>$partner_id,'sifra_kod_dobavljaca'=>$sifra,'karakteristika_naziv'=>'nedefinisan','karakteristika_vrednost'=>'nedefinisana','karakteristika_grupa'=>$grupa,'redni_br_grupe'=>$rbr));  

                    // $rbr = DB::select("SELECT MAX(redni_br_grupe) AS max FROM dobavljac_cenovnik_karakteristike WHERE roba_id=".$roba_id."")[0]->max + 1;
                    DB::table('dobavljac_cenovnik_karakteristike')->insert(array('roba_id'=>$roba_id,'partner_id'=>$partner_id,'sifra_kod_dobavljaca'=>$sifra,'karakteristika_naziv'=>'nedefinisan','karakteristika_vrednost'=>'nedefinisana','karakteristika_grupa'=>$grupa));                
                    AdminSupport::saveLog('ARTIKLI_KARAKTERISTIKA_DODAJ', array(DB::table('dobavljac_cenovnik_karakteristike')->where(array('roba_id'=>$roba_id,'partner_id'=>$partner_id,'sifra_kod_dobavljaca'=>$sifra,'karakteristika_naziv'=>'nedefinisan','karakteristika_vrednost'=>'nedefinisana','karakteristika_grupa'=>$grupa))->pluck('dobavljac_cenovnik_karakteristike_id')));              

                }
            }
        }
        elseif($action == 'delete_grupa') {
            $roba_id = Input::get('roba_id');
            $grupa_old = Input::get('grupa_old');

            AdminSupport::saveLog('ARTIKLI_KARAKTERISTIKA_OBRISI', array(DB::table('dobavljac_cenovnik_karakteristike')->where(array('roba_id'=>$roba_id, 'karakteristika_grupa'=>$grupa_old))->pluck('dobavljac_cenovnik_karakteristike_id')));
            DB::table('dobavljac_cenovnik_karakteristike')->where(array('roba_id'=>$roba_id, 'karakteristika_grupa'=>$grupa_old))->delete();
        }

    }
    public function ajaxPovezani(){
        $action = Input::get('action');
        if($action=='delete'){
            $store = Input::get('store');
            AdminSupport::saveLog('VEZANI_ARTIKLI_OBRISI', array($store['vezani_roba_id']));
            DB::table('vezani_artikli')->where($store)->delete();
        }
        if($action=='flag_cena'){
            $store = Input::get('store');
            $aktivan = Input::get('aktivan');
            DB::table('vezani_artikli')->where($store)->update(array('flag_cena'=>$aktivan)); 
            AdminSupport::saveLog('VEZANI_ARTIKLI_FLAG_CENA_IZMENI', array(DB::table('vezani_artikli')->where($store)->pluck('vezani_roba_id')));
        }
        if($action=='sacuvaj_cenu'){
            $store = Input::get('store');
            $cena = Input::get('cena');
            DB::table('vezani_artikli')->where($store)->update(array('cena'=>$cena));
            AdminSupport::saveLog('VEZANI_ARTIKLI_IZMENI_CENA', array('roba_id'=>DB::table('vezani_artikli')->where($store)->pluck('roba_id'), 'vezani_roba_id'=>DB::table('vezani_artikli')->where($store)->pluck('vezani_roba_id')));
        }
        if($action=='izmeni_vrednost'){
            $store = Input::get('store');
            $grupa_pr_vrednost_id = Input::get('grupa_pr_vrednost_id');
            DB::table('srodni_artikli')->where($store)->update(array('grupa_pr_vrednost_id'=>$grupa_pr_vrednost_id));
            AdminSupport::saveLog('SRODNI_ARTIKLI_IZMENI_KARAK', array('roba_id'=>DB::table('srodni_artikli')->where($store)->pluck('roba_id'), 'srodni_roba_id'=>DB::table('srodni_artikli')->where($store)->pluck('srodni_roba_id')));
        }
        if($action=='flag_cena_srodni'){
            $store = Input::get('store');
            $aktivan = Input::get('aktivan');
            DB::table('srodni_artikli')->where($store)->update(array('flag_cena'=>$aktivan)); 
            AdminSupport::saveLog('SRODNI_ARTIKLI_FLAG_CENA_IZMENI', array(DB::table('srodni_artikli')->where($store)->pluck('srodni_roba_id')));
        }
        if($action=='delete_srodni'){
            $store = Input::get('store');
            AdminSupport::saveLog('SRODNI_ARTIKLI_OBRISI', array($store['srodni_roba_id']));
            DB::table('srodni_artikli')->where($store)->delete();
        }
        echo $store['roba_id'];
    }

    function ajaxDcArticles(){
        $action = Input::get('action');
        $dobavljac_cenovnik_id = Input::get('dobavljac_cenovnik_id');
        
        if($action == 'fast_edit_quantity') {
            $dobavljac_cenovnik_id = Input::get('dobavljac_cenovnik_id');
            $val = Input::get('val');

            DB::table('dobavljac_cenovnik')->where('dobavljac_cenovnik_id',$dobavljac_cenovnik_id)->update(['kolicina' => $val]);

            AdminSupport::saveLog('WEB_IMPORT_KOLICINA_BRZA_IZMENA', array($dobavljac_cenovnik_id));
        } 
        if($action == 'promena_kolicine_artikal') {
            $art_id = Input::get('art_id');
            $val = Input::get('val');  
            $orgj_id = Input::get('orgj_id');

            if($orgj_id == 0){
                $default_orgj_id = DB::table('imenik_magacin')->where('izabrani',1)->pluck('orgj_id');
                DB::table('lager')->where(array('roba_id'=> $art_id, 'orgj_id'=> $default_orgj_id ))->update(['kolicina' => $val]);

                AdminSupport::saveLog('ARTIKLI_KOLICINA_BRZA_IZMENA', array($art_id));
                $godina_id = DB::table('poslovna_godina')->where('status',0)->pluck('poslovna_godina_id');              
                echo round(DB::select("select sum(kolicina) as ukupno from lager where roba_id = ".$art_id." and poslovna_godina_id = ".$godina_id)[0]->ukupno);
            }else{
                DB::table('lager')->where(array('roba_id'=> $art_id, 'orgj_id'=> $orgj_id ))->update(['kolicina' => $val]);
                AdminSupport::saveLog('ARTIKLI_KOLICINA_BRZA_IZMENA', array($art_id));
                echo $val;
            }

        } 
        if($action == 'promena_rbr') {
            $art_id = Input::get('art_id');
            $val = Input::get('val');            
            

            DB::table('web_roba_karakteristike')->where(array('web_roba_karakteristike_id'=> $art_id ))->update(['rbr' => $val]);
        
            AdminSupport::saveLog('ARTIKLI_KOLICINA_BRZA_IZMENA', array($art_id));
        } 
        elseif($action == 'fast_edit_name') {
            $art_id = Input::get('art_id');
            $val = Input::get('val');

            if($val != '' || $val != null) {
                DB::table('dobavljac_cenovnik')->where('dobavljac_cenovnik_id', $art_id)->update(['naziv' => $val]);
                echo $art_id;
            } else {
                echo 'empty val';
            }
            AdminSupport::saveLog('WEB_IMPORT_NAZIV_BRZA_IZMENA', array($art_id));
        } 
        //promena naziva na nivou artikla, kada se promeni naziv automatski da se menja i naziv u dobavljac cenovnik
        elseif($action == 'fast_edit_name1') {
            $art_id = Input::get('art_id');
            $val = Input::get('val');
      
            if($val != '' || $val != null) {
                DB::table('dobavljac_cenovnik')->where('roba_id', $art_id)->update(['naziv' => $val] );
                DB::table('roba')->where('roba_id', $art_id)->update(['naziv' => $val] );
                DB::table('roba')->where('roba_id', $art_id)->update(['naziv_web' => $val] );
                
                echo $art_id;
            } else {
                echo 'empty val';
            }
            AdminSupport::saveLog('ARTIKLI_NAZIV_BRZA_IZMENA', array($art_id));
        } 
        //edit web cene u admin panelu/artikli
        elseif($action == 'fast_edit_WebCena') {
            $art_id = Input::get('art_id');
            $val = Input::get('val');
            // var_dump($val);die;

            $nc = DB::table('roba')->where('roba_id', $art_id)->pluck('racunska_cena_nc');
            $tarifna_grupa = DB::table('roba')->where('roba_id', $art_id)->pluck('tarifna_grupa_id');
            $porez = DB::table('tarifna_grupa')->where('tarifna_grupa_id', $tarifna_grupa)->pluck('porez');
            $stopa = 1 + $porez / 100;
            if($nc != 0){
            $web_marza = (($val/($nc*$stopa))-1) * 100;
            }else{
            $web_marza = 0;    
            }
            if($val != '' || $val != null) {
                DB::table('roba')->where('roba_id', $art_id)->update(array('web_cena' => $val, 'web_marza' => $web_marza ));
                
                //echo $art_id;
                 echo number_format($web_marza,2);

            } else {
                echo 'empty val';
            }
            AdminSupport::saveLog('ARTIKLI_WEB_CENA_BRZA_IZMENA', array($art_id));
        }
        
        //FAST EDIT END CENA
        elseif($action == 'fast_edit_EndCena') {
            $art_id = Input::get('art_id');
            $val = Input::get('val');
            // var_dump($val);die;

            $nc = DB::table('roba')->where('roba_id', $art_id)->pluck('racunska_cena_nc');
            $tarifna_grupa = DB::table('roba')->where('roba_id', $art_id)->pluck('tarifna_grupa_id');
            $porez = DB::table('tarifna_grupa')->where('tarifna_grupa_id', $tarifna_grupa)->pluck('porez');
            $stopa = 1 + $porez / 100;
            if($nc != 0){
            $end_marza = (($val/($nc*$stopa))-1) * 100;
            }else{
            $end_marza = 0;    
            }
            if($val != '' || $val != null) {
                DB::table('roba')->where('roba_id', $art_id)->update(array('racunska_cena_end' => $val, 'end_marza' => $end_marza ));
                
                //echo $art_id;
                 echo number_format($end_marza,2);

            } else {
                echo 'empty val';
            }
            AdminSupport::saveLog('ARTIKLI_END_CENA_BRZA_IZMENA', array($art_id));
        }
        elseif($action == 'fast_edit_sifra') {
            $art_id = Input::get('art_id');
            $val = Input::get('val');

            if($val != '' || $val != null) {
                DB::table('dobavljac_cenovnik')->where('dobavljac_cenovnik_id', $art_id)->update(['sifra_kod_dobavljaca' => $val]);
                echo $art_id;
            } else {
                echo 'empty val';
            }
            AdminSupport::saveLog('WEB_IMPORT_SIFRA_BRZA_IZMENA', array($art_id));
        } 
        elseif ($action == 'execute') {

            $dc_ids = Input::get('dc_ids');
            $data = Input::get('data');

            foreach($data as $row){
                if($row['val'] == 'null'){
                    $row['val'] = null;
                }
                if($row['table'] != 'roba'){
                    if($row['column'] == 'flag_prikazi_u_cenovniku' || $row['column'] == 'flag_aktivan' || $row['column'] == 'flag_zakljucan'){
                        if($row['column'] == 'flag_aktivan'){
                            $query = DB::table('dobavljac_cenovnik')->whereIn('dobavljac_cenovnik_id', $dc_ids);
                            if($row['val'] == 1){
                                AdminSupport::saveLog('WEB_IMPORT_PREBACI_U_AKTIVNE', $dc_ids);
                            }else if($row['val'] == 0){
                                AdminSupport::saveLog('WEB_IMPORT_PREBACI_U_NEAKTIVNE', $dc_ids);
                            }
                        }else{
                            $query = DB::table('dobavljac_cenovnik')->where('roba_id','!=',-1)->whereIn('dobavljac_cenovnik_id', $dc_ids);
                        }
                        
                        $dcn_ids = array_map('current', $query->select('dobavljac_cenovnik_id')->get());
                        $roba_ids = array_map('current', $query->select('roba_id')->get());

                        if($row['column'] == 'flag_zakljucan'){
                            DB::table($row['table'])->whereIn('dobavljac_cenovnik_id', $dcn_ids)->update(array($row['column']=>$row['val']));
                            if($row['val']){
                                AdminSupport::saveLog('WEB_IMPORT_ZAKLJUCAJ', $dc_ids);
                            }else if (!$row['val']) {
                               AdminSupport::saveLog('WEB_IMPORT_OTKLJUCAJ', $dc_ids);
                            }
                        }else{
                            DB::table($row['table'])->whereIn('dobavljac_cenovnik_id', $dcn_ids)->where('flag_zakljucan',0)->update(array($row['column']=>$row['val']));
                            if($row['column'] == 'flag_prikazi_u_cenovniku' && $row['val'] == 0){
                                AdminSupport::saveLog('WEB_IMPORT_SKINI_SA_WEBA', $dc_ids);
                            }else if($row['column'] == 'flag_prikazi_u_cenovniku' && $row['val'] == 1){
                                AdminSupport::saveLog('WEB_IMPORT_STAVI_NA_WEB', $dc_ids);
                            }
                        }
                        echo json_encode($roba_ids);                    
                    }
                    elseif($row['column'] == 'grupa_pr_id' || $row['column'] == 'proizvodjac_id' || $row['column'] == 'tarifna_grupa_id'){
                        DB::table($row['table'])->whereIn('dobavljac_cenovnik_id', $dc_ids)->where(array('flag_zakljucan'=>0))->update(array($row['column']=>$row['val']));
                        if($row['column'] == 'grupa_pr_id'){
                            AdminSupport::saveLog('ARTIKLI_DOBAVLJAC_GRUPA_DODAJ', $dc_ids);
                        }elseif ($row['column'] == 'proizvodjac_id') {
                            AdminSupport::saveLog('ARTIKLI_DOBAVLJAC_PROIZVODJAC_DODAJ', $dc_ids);
                        }elseif ($row['column'] == 'tarifna_grupa_id') {
                            AdminSupport::saveLog('ARTIKLI_DOBAVLJAC_TARIFNA_GRUPA_DODAJ', $dc_ids);
                        }
                    }
                    else{
                        DB::table($row['table'])->where('flag_zakljucan',0)->whereIn('dobavljac_cenovnik_id', $dc_ids)->update(array($row['column']=>$row['val']));
                    }
                }else{
                    if($row['column'] == 'flag_zakljucan'){
                        $roba_ids = array_map('current', DB::table('dobavljac_cenovnik')->where('roba_id','!=',-1)->whereIn('dobavljac_cenovnik_id', $dc_ids)->select('roba_id')->get());
                    }else{
                        $roba_ids = array_map('current', DB::table('dobavljac_cenovnik')->where('flag_zakljucan',0)->where('roba_id','!=',-1)->whereIn('dobavljac_cenovnik_id', $dc_ids)->select('roba_id')->get());                        
                    }
                    DB::table($row['table'])->whereIn('roba_id', $roba_ids)->update(array($row['column']=>$row['val']));

                }
              
            }


            // custom for inputs
            if($data[0]['column'] == 'kolicina'){
                foreach ($dc_ids as $id) {
                    DB::statement("UPDATE dobavljac_cenovnik dc SET kolicina = ".$data[0]['val']." WHERE dobavljac_cenovnik_id = ".$id." AND flag_zakljucan = 0");
                    $response[$id] = $data[0]['val'];                   
                } 
                AdminSupport::saveLog('ARTIKLI_DOBAVLJAC_KOLICINA', $dc_ids);          
            }

            if($data[0]['column'] == 'grupa_pr_id'){
                echo DB::table('grupa_pr')->where('grupa_pr_id', $data[0]['val'])->select('grupa')->first()->grupa;
            }
            elseif($data[0]['column'] == 'tarifna_grupa_id'){
                echo DB::table('tarifna_grupa')->where('tarifna_grupa_id', $data[0]['val'])->select('porez')->first()->porez;
            }
            elseif($data[0]['column'] == 'proizvodjac_id'){
                echo DB::table('proizvodjac')->where('proizvodjac_id', $data[0]['val'])->select('naziv')->first()->naziv;
            }
            elseif($data[0]['column'] == 'a_marza' || $data[0]['column'] == 'mp_marza' || $data[0]['column'] == 'web_marza'){               
                if($data[0]['column'] == 'a_marza'){
                    $cena = 'cena_a';
                }
                elseif($data[0]['column'] == 'mp_marza'){
                    $cena = 'mpcena';
                }
                elseif($data[0]['column'] == 'web_marza'){
                    $cena = 'web_cena';
                }
                // $response = array();

                foreach ($dc_ids as $id) {
                    DB::statement("UPDATE dobavljac_cenovnik dc SET ".$cena." = ROUND((dc.cena_nc+(dc.cena_nc*dc.".$data[0]['column']."/100))*((select porez from tarifna_grupa tg where tg.tarifna_grupa_id=dc.tarifna_grupa_id)/100+1)) where dc.dobavljac_cenovnik_id=$id and dc.flag_zakljucan = 0");
                    $ncena = DB::table('dobavljac_cenovnik')->where('dobavljac_cenovnik_id', $id)->select($cena)->get();

                    if($data[0]['column'] == 'a_marza'){
                        $response[$id]=$ncena[0]->cena_a;
                    }
                    elseif($data[0]['column'] == 'mp_marza'){
                        $response[$id]=$ncena[0]->mpcena;
                    }
                    elseif($data[0]['column'] == 'web_marza'){
                        $response[$id]=$ncena[0]->web_cena;
                    }
                }
                if($data[0]['column'] == 'mp_marza'){
                    AdminSupport::saveLog('ARTIKLI_DOBAVLJAC_MP_MARZA', $dc_ids);
                }elseif ($data[0]['column'] == 'web_marza') {
                    AdminSupport::saveLog('ARTIKLI_DOBAVLJAC_WEB_MARZA', $dc_ids);
                }

                echo json_encode($response);
            }
        }

        elseif ($action == 'unesi_nove') {

            $dc_ids = Input::get('dc_ids');
            $fast_insert = Input::get('fast_insert');
            if($fast_insert==1 && AdminOptions:: gnrl_options(3030)==1){
                $dc_mapped_unmapped = AdminImport::dc_mapped($dc_ids);
                $dc_ids = $dc_mapped_unmapped->dc_ids;
            }

            if(count($dc_ids) == 0){
                return json_encode(array('data' => array(), 'roba_ids' => array())); 
            }
            $not_linked = DB::select("SELECT dobavljac_cenovnik_id FROM dobavljac_cenovnik WHERE dobavljac_cenovnik_id IN (".implode(',',$dc_ids).") AND (povezan = 1 OR flag_aktivan = 0 OR grupa_pr_id = -1 OR proizvodjac_id = -1 OR tarifna_grupa_id = -1 OR web_marza = -1)");
            if(count($not_linked) > 0){
                return json_encode(array('data' => array(), 'roba_ids' => array())); 
            }

            $flag = Input::get('flag');

            $min_roba_id = DB::select("SELECT MAX(roba_id) FROM roba")[0]->max +1;
            // $default_magacin_id = DB::table('imenik_magacin')->where('izabrani',1)->pluck('orgj_id');
            // $godina_id = DB::table('poslovna_godina')->where('status',0)->pluck('poslovna_godina_id');

            $response = array();
            $roba_ids = array();

            if(count($dc_ids) > 0){
                DB::statement("INSERT INTO roba(sifra_d,naziv,grupa_pr_id,tarifna_grupa_id,jedinica_mere_id,proizvodjac_id,klasa_pr_id, 
                     naziv_displej,flag_razlomljeno,cenovnik_jm_id,garancija,flag_prikazi_u_cenovniku,racunska_cena_nc,racunska_cena_a, 
                     a_marza,racunska_cena_end, end_marza,mpcena,web_cena,mp_marza,web_marza,naziv_web,rbr,dobavljac_id,web_opis,promena,web_flag_karakteristike,model,barkod,sku)
                     SELECT sifra_kod_dobavljaca,substr(naziv,0,140),grupa_pr_id,tarifna_grupa_id,(SELECT int_data FROM options WHERE options_id = 1336),proizvodjac_id,-1,
                     substr(naziv,0,18),0,9,0,".$flag.",cena_nc,cena_a,a_marza,cena_end,end_marza,mpcena,web_cena,mp_marza,web_marza,substr(naziv,0,140),-1,partner_id,opis,1,web_flag_karakteristike,model,barkod,roba_id
                     FROM dobavljac_cenovnik dc WHERE dc.dobavljac_cenovnik_id IN (".implode(',',$dc_ids).") AND dc.roba_id = -1");

                DB::statement("UPDATE dobavljac_cenovnik dc SET roba_id=r.roba_id, povezan = 1, flag_prikazi_u_cenovniku = r.flag_prikazi_u_cenovniku FROM roba r WHERE dc.sifra_kod_dobavljaca=r.sifra_d AND dc.partner_id=r.dobavljac_id AND dobavljac_cenovnik_id IN (".implode(',',$dc_ids).")"); 

                DB::statement("UPDATE dobavljac_cenovnik_karakteristike dck SET roba_id=dc.roba_id FROM dobavljac_cenovnik dc WHERE 
                     dck.sifra_kod_dobavljaca=dc.sifra_kod_dobavljaca AND dobavljac_cenovnik_id IN (".implode(',',$dc_ids).")");

                DB::statement("INSERT INTO lager (roba_id,orgj_id,poslovna_godina_id,kolicina) SELECT roba_id, (SELECT orgj_id FROM imenik_magacin WHERE izabrani = 1), (SELECT poslovna_godina_id FROM poslovna_godina WHERE status = 0), kolicina FROM dobavljac_cenovnik WHERE kolicina > 0 AND dobavljac_cenovnik_id IN (".implode(',',$dc_ids).")");


                foreach ($dc_ids as $id) {

                    if($fast_insert == 1){
                        $dc_new = DB::select("SELECT roba_id ,(SELECT grupa FROM grupa_pr WHERE grupa_pr_id = dc.grupa_pr_id) as nasa_grupa, (SELECT naziv FROM proizvodjac WHERE proizvodjac_id = dc.proizvodjac_id) as nas_proizvodjac, web_marza, mp_marza, (SELECT porez FROM tarifna_grupa WHERE tarifna_grupa_id = dc.tarifna_grupa_id) as porez, cena_nc, web_cena, mpcena FROM dobavljac_cenovnik dc WHERE dobavljac_cenovnik_id = ".$id."")[0];
                        $export_data['roba_id'] = $dc_new->roba_id;
                        $export_data['nasa_grupa'] = $dc_new->nasa_grupa;
                        $export_data['nas_proizvodjac'] = $dc_new->nas_proizvodjac;
                        $export_data['web_marza'] = $dc_new->web_marza;
                        $export_data['mp_marza'] = $dc_new->mp_marza;
                        $export_data['porez'] = $dc_new->porez;
                        $export_data['cena_nc'] = $dc_new->cena_nc;
                        $export_data['web_cena'] = $dc_new->web_cena;
                        $export_data['mpcena'] = $dc_new->mpcena;
                        $response["$id"]=$export_data;

                        $roba_ids[]=$dc_new->roba_id;
                    }else{
                        $dc_new = DB::select("SELECT roba_id FROM dobavljac_cenovnik dc WHERE dobavljac_cenovnik_id = ".$id."")[0];
                        $export_data['roba_id'] = $dc_new->roba_id;
                        $response["$id"]=$export_data;

                        $roba_ids[]=$dc_new->roba_id;
                    }
                }
            }

           // foreach ($dc_ids as $id) {
           //      $dobavljac_query = DB::table('dobavljac_cenovnik')->where('dobavljac_cenovnik_id', $id)->select('roba_id','sifra_kod_dobavljaca','kolicina','partner_id')->first();
           //      $roba_id = $dobavljac_query->roba_id;

           //      if ($roba_id === -1) {
           //          $dobavljac_kolicina = $dobavljac_query->kolicina;

           //          DB::statement("UPDATE dobavljac_cenovnik SET roba_id = nextval('roba_roba_id_seq'), povezan = 1, flag_prikazi_u_cenovniku = ".$flag." WHERE dobavljac_cenovnik_id = ".$id."");
           //          DB::statement("INSERT INTO roba(roba_id,sifra_d,naziv,grupa_pr_id,tarifna_grupa_id,jedinica_mere_id,proizvodjac_id,klasa_pr_id, 
           //               naziv_displej,flag_razlomljeno,cenovnik_jm_id,garancija,flag_prikazi_u_cenovniku,racunska_cena_nc,racunska_cena_a, 
           //               a_marza,racunska_cena_end, end_marza,mpcena,web_cena,mp_marza,web_marza,naziv_web,rbr,dobavljac_id,web_karakteristike,promena,web_flag_karakteristike,model,barkod)
           //               SELECT roba_id,sifra_kod_dobavljaca,substr(naziv,0,140),grupa_pr_id,tarifna_grupa_id,(SELECT int_data FROM options WHERE options_id = 1336),proizvodjac_id,-1,
           //               substr(naziv,0,18),0,9,0,flag_prikazi_u_cenovniku,cena_nc,cena_a,a_marza,cena_end,end_marza,mpcena,web_cena,mp_marza,web_marza,substr(naziv,0,140),-1,partner_id,opis,1,web_flag_karakteristike,model,barkod
           //               FROM dobavljac_cenovnik WHERE dobavljac_cenovnik_id = ".$id."");

           //          DB::statement("UPDATE roba r SET seo = d.naziv FROM dobavljac_cenovnik d WHERE 
           //              r.sifra_d = d.sifra_kod_dobavljaca AND r.dobavljac_id IN(SELECT partner_id FROM dobavljac_cenovnik WHERE dobavljac_cenovnik_id = ".$id.")");

           //          DB::statement("UPDATE dobavljac_cenovnik_karakteristike dck SET roba_id=dc.roba_id FROM dobavljac_cenovnik dc WHERE 
           //               dck.sifra_kod_dobavljaca=dc.sifra_kod_dobavljaca AND
           //               dck.partner_id IN(SELECT partner_id FROM dobavljac_cenovnik WHERE dobavljac_cenovnik_id = ".$id.")");

           //          // $roba_id_new = DB::table('dobavljac_cenovnik')->where('dobavljac_cenovnik_id', $id)->pluck('roba_id');
           //          $roba_id_new = DB::select("SELECT currval('roba_roba_id_seq') as new_id")[0]->new_id;
                    
           //          //PRIHVATI LAGER
           //          DB::statement("INSERT INTO lager (roba_id,orgj_id,poslovna_godina_id,kolicina) VALUES (".$roba_id_new.", ".$default_magacin_id.", ".$godina_id.",".$dobavljac_kolicina.")");
                    
           //          //PREUZMI SLIKE
           //          $slike = DB::table('dobavljac_cenovnik_slike')->select('partner_id','putanja','akcija','sifra_kod_dobavljaca')->where(array('sifra_kod_dobavljaca'=>$dobavljac_query->sifra_kod_dobavljaca, 'partner_id'=> $dobavljac_query->partner_id))->get();
           //          AdminImport::saveImages($slike,$roba_id_new);

           //          $export_data = array(
           //              'roba_id'=>$roba_id_new
           //              );
           //          if($fast_insert == 1){
           //              $dc_new = DB::select("SELECT (SELECT grupa FROM grupa_pr WHERE grupa_pr_id = dc.grupa_pr_id) as nasa_grupa, (SELECT naziv FROM proizvodjac WHERE proizvodjac_id = dc.proizvodjac_id) as nas_proizvodjac, web_marza, mp_marza, (SELECT porez FROM tarifna_grupa WHERE tarifna_grupa_id = dc.tarifna_grupa_id) as porez, cena_nc, web_cena, mpcena FROM dobavljac_cenovnik dc WHERE dobavljac_cenovnik_id = ".$id."")[0];
           //              $export_data['nasa_grupa'] = $dc_new->nasa_grupa;
           //              $export_data['nas_proizvodjac'] = $dc_new->nas_proizvodjac;
           //              $export_data['web_marza'] = $dc_new->web_marza;
           //              $export_data['mp_marza'] = $dc_new->mp_marza;
           //              $export_data['porez'] = $dc_new->porez;
           //              $export_data['cena_nc'] = $dc_new->cena_nc;
           //              $export_data['web_cena'] = $dc_new->web_cena;
           //              $export_data['mpcena'] = $dc_new->mpcena;
           //          }

           //          $response["$id"]=$export_data;
           //          $roba_ids[]=$roba_id_new;
           //      }  
           //      DB::statement("UPDATE roba  SET sku = roba_id WHERE roba_id = ".$roba_id_new." ");
           //  }

            $max_roba_id = DB::select("SELECT MAX(roba_id) FROM roba")[0]->max;
            AdminSupport::saveLog('DOBAVLJAC_ARTIKLI_UNOS_NOVIH', $dc_ids);

            if(Options::gnrl_options(3055) == 1 && Options::gnrl_options(3056) == 0){
                $elastic = new ElasticSearchService();
                $elastic->updateArticles();
            }

            return json_encode(array('data' => $response, 'roba_ids' => $roba_ids)); 
       
        } elseif ($action == 'prihvati_lager') {

           $dc_ids = Input::get('dc_ids');
           // $magacin_id = DB::table('imenik_magacin')->where('izabrani',1)->pluck('orgj_id');
           $magacin_id = Input::get('orgj_id');
           $godina_id = DB::table('poslovna_godina')->where('status',0)->pluck('poslovna_godina_id');
           $roba_ids = DB::table('dobavljac_cenovnik')->select('roba_id')->distinct()->whereIn('dobavljac_cenovnik_id',$dc_ids)->where('roba_id','!=',-1)->where('flag_zakljucan','!=',1)->get();
           $new_roba_ids = array();

           foreach ($roba_ids as $row) {
                $roba_id = $row->roba_id;
                $same_obj = DB::table('dobavljac_cenovnik')->select('dobavljac_cenovnik_id')->where('roba_id',$roba_id);
                $new_roba_ids[] = $roba_id;
                if($same_obj->count() > 0){
                  
                    if(DB::table('lager')->where(array('roba_id'=>$roba_id,'poslovna_godina_id'=>$godina_id,'orgj_id'=>$magacin_id))->count() > 0){
                        DB::statement("UPDATE lager l SET kolicina=dcn.kolicina FROM (SELECT roba_id, SUM(kolicina) AS kolicina FROM dobavljac_cenovnik dc WHERE dc.roba_id=".$roba_id." AND dc.flag_zakljucan=0 GROUP BY dc.roba_id) dcn WHERE l.roba_id=dcn.roba_id AND L.orgj_id=".$magacin_id."");
                    }else{
                        DB::statement("INSERT INTO lager (roba_id,orgj_id,poslovna_godina_id,kolicina) SELECT dcn.roba_id, ".$magacin_id.", ".$godina_id.",dcn.kolicina FROM (SELECT roba_id, SUM(kolicina) AS kolicina FROM dobavljac_cenovnik dc WHERE dc.roba_id=".$roba_id." AND dc.flag_zakljucan=0 GROUP BY dc.roba_id) dcn");
                    }

                //    DB::statement("UPDATE roba SET web_flag_obavezan_prikaz = 1 WHERE roba_id IN(SELECT roba_id FROM dobavljac_cenovnik WHERE kolicina > 0 AND partner_id = '+GetIndexOfComboData(cbDobavljac)+' );','lager',DM.connArhiva");
             
                }

            }

            $response = DB::table('lager')->where(array('poslovna_godina_id'=>$godina_id,'orgj_id'=>$magacin_id))->whereIn('roba_id',$new_roba_ids)->select('roba_id','kolicina')->get();
            AdminSupport::saveLog('WEB_IMPORT_PRIHVATI_LAGER', $dc_ids);          
            echo json_encode($response);
        }
        elseif ($action == 'prihvati_cene') {
            $check_naziv=Input::get('check_naziv');
            $is_cena=Input::get('is_cena');
            $is_kolicina=Input::get('is_kolicina');
            $partner_id=Input::get('partner_id');
           $dc_ids=Input::get('dc_ids');  
           $roba_ids = DB::table('dobavljac_cenovnik')->select('roba_id')->distinct()->where(array('flag_aktivan'=>1,'flag_zakljucan'=>0,'povezan'=>1))->whereIn('dobavljac_cenovnik_id',$dc_ids)->where('roba_id','!=',-1)->where('cena_nc','!=',0)->get();
           $dc_new_ids = array();

           foreach ($roba_ids as $row) {
                $roba_id = $row->roba_id;
                $same_obj = DB::table('dobavljac_cenovnik')->select('dobavljac_cenovnik_id')->where('roba_id',$roba_id)->where('cena_nc','!=',0);
                if($same_obj->count() > 0){
                    if($partner_id!=0){
                        if(DB::table('dobavljac_cenovnik')->where('roba_id',$roba_id)->where('cena_nc','!=',0)->where('partner_id',$partner_id)->count()>0){
                            $same_obj->where('partner_id',$partner_id);
                        }
                    }
                    if($is_kolicina==1){
                        if(DB::table('dobavljac_cenovnik')->where('roba_id',$roba_id)->where('cena_nc','!=',0)->where('kolicina','>',0)->count()>0){
                            $same_obj->where('kolicina','>',0);
                        }
                    }
                    if($is_cena==1){
                        $same_obj->orderBy('cena_nc','asc');
                    }

                    if(!is_null($same_obj->first())){
                        $dobavljac_cenovnik_id = $same_obj->first()->dobavljac_cenovnik_id;
                        $dc_new_ids[] = $dobavljac_cenovnik_id;

                        if($check_naziv==0){
                            DB::statement("UPDATE roba r SET racunska_cena_nc=dc.cena_nc,racunska_cena_a=dc.cena_a,racunska_cena_end=dc.cena_end,mpcena=dc.mpcena,web_cena=dc.web_cena,promena=1, a_marza=dc.a_marza, end_marza=dc.end_marza, mp_marza=dc.mp_marza, web_marza=dc.web_marza, dobavljac_id = dc.partner_id, sifra_d = dc.sifra_kod_dobavljaca FROM dobavljac_cenovnik dc WHERE dc.dobavljac_cenovnik_id = ".$dobavljac_cenovnik_id." AND r.roba_id = ".$roba_id."");
                        }else{
                            DB::statement("UPDATE roba r SET naziv=substr(dc.naziv,0,140), naziv_web=substr(dc.naziv,0,140), naziv_displej=substr(dc.naziv,0,18) FROM dobavljac_cenovnik dc WHERE dc.dobavljac_cenovnik_id = ".$dobavljac_cenovnik_id." AND r.roba_id = ".$roba_id."");
                        }
                    }

                }                                 
            }

            if(count($dc_new_ids)>0){            
                if($check_naziv==0){
                    $response = DB::select("SELECT roba_id, (SELECT naziv FROM partner WHERE partner_id=dc.partner_id) AS partner, cena_nc, web_cena, web_marza FROM dobavljac_cenovnik dc WHERE povezan=1 AND flag_aktivan=1 AND flag_zakljucan=0 AND dobavljac_cenovnik_id IN (".implode(',',$dc_new_ids).")");

                }else{
                    $response = DB::table('dobavljac_cenovnik')->select('roba_id','naziv')->where(array('povezan'=>1,'flag_aktivan'=>1,'flag_zakljucan'=>0))->whereIn('dobavljac_cenovnik_id',$dc_new_ids)->get();               
                }
            }else{
                $response = array();
            }
            AdminSupport::saveLog('WEB_IMPORT_PRIHVATI_CENE', $dc_ids);
            echo json_encode($response);
        }
        elseif ($action == 'prepisi_cenu') {

            $dc_ids=Input::get('dc_ids');
            
            $response = array();
            foreach ($dc_ids as $id) {

                $nc = DB::table('dobavljac_cenovnik')->where('dobavljac_cenovnik_id', $id)->pluck('cena_nc');
                if($nc>0){
                    DB::statement("UPDATE dobavljac_cenovnik SET web_cena = pmp_cena, web_marza = (((pmp_cena*100)/(cena_nc*1.2))-100) WHERE dobavljac_cenovnik_id = ".$id." AND flag_zakljucan = 0 AND pmp_cena > 0");
                    // $response[$id] = DB::table('dobavljac_cenovnik')->where('dobavljac_cenovnik_id',$id)->pluck('web_cena'); 
                    $cena_web = DB::select("SELECT ROUND(web_cena) AS web_cena, web_marza FROM dobavljac_cenovnik WHERE dobavljac_cenovnik_id = ".$id."")[0];
                    $response[$id]=array(
                      "web_cena"=>$cena_web->web_cena.'.00',  
                      "web_marza"=>$cena_web->web_marza  
                    );
                }else{
                     DB::statement("UPDATE dobavljac_cenovnik SET web_cena = pmp_cena WHERE dobavljac_cenovnik_id = ".$id." AND flag_zakljucan = 0 AND pmp_cena > 0");
                    // $response[$id] = DB::table('dobavljac_cenovnik')->where('dobavljac_cenovnik_id',$id)->pluck('web_cena'); 
                    $cena_web = DB::select("SELECT ROUND(web_cena) AS web_cena, web_marza FROM dobavljac_cenovnik WHERE dobavljac_cenovnik_id = ".$id."")[0];
                    $response[$id]=array(
                      "web_cena"=>$cena_web->web_cena.'.00', 
                    );
                }

            }
                AdminSupport::saveLog('WEB_IMPORT_PREPISI_CENU', $dc_ids); 

                echo json_encode($response);
        }
        elseif ($action == 'preracunaj_cene') {

            $dc_ids=Input::get('dc_ids');
            $response = array();
            foreach ($dc_ids as $id) {
                DB::statement("UPDATE dobavljac_cenovnik dc SET web_cena = cena_nc*(1+web_marza/100)*(SELECT 1+porez/100 FROM tarifna_grupa WHERE tarifna_grupa_id = dc.tarifna_grupa_id), mpcena = cena_nc*(1+mp_marza/100)*(SELECT 1+porez/100 FROM tarifna_grupa WHERE tarifna_grupa_id = dc.tarifna_grupa_id) WHERE dobavljac_cenovnik_id = ".$id." AND flag_zakljucan = 0");
                $cene_row = DB::select("SELECT ROUND(web_cena) AS web_cena, ROUND(mpcena) AS mpcena FROM dobavljac_cenovnik WHERE dobavljac_cenovnik_id = ".$id."")[0];
                $response[$id] = array(
                    "web_cena"=>$cene_row->web_cena.'.00',
                    "mpcena"=>$cene_row->mpcena.'.00'
                    ); 
            }

            AdminSupport::saveLog('WEB_IMPORT_PRERACUNAJ_CENU', $dc_ids);
            echo json_encode($response);
        }

        elseif ($action == 'preracunaj_cene_kurs') {

            $dc_ids = Input::get('dc_ids');
            $kurs = Input::get('kurs');
            if(empty($kurs) || $kurs < 1) {
                $kurs = 1;
            }

            $response = array();
            foreach ($dc_ids as $id) {
                DB::statement("UPDATE dobavljac_cenovnik dc SET web_cena = ".$kurs." * cena_nc*(1+web_marza/100)*(SELECT 1+porez/100 FROM tarifna_grupa WHERE tarifna_grupa_id = dc.tarifna_grupa_id), mpcena = ".$kurs." * cena_nc*(1+mp_marza/100)*(SELECT 1+porez/100 FROM tarifna_grupa WHERE tarifna_grupa_id = dc.tarifna_grupa_id) WHERE dobavljac_cenovnik_id = ".$id." AND flag_zakljucan = 0");
                $cene_row = DB::select("SELECT ROUND(web_cena) AS web_cena, ROUND(mpcena) AS mpcena FROM dobavljac_cenovnik WHERE dobavljac_cenovnik_id = ".$id."")[0];
                $response[$id] = array(
                    "web_cena"=>$cene_row->web_cena.'.00',
                    "mpcena"=>$cene_row->mpcena.'.00'
                    ); 
            }

            AdminSupport::saveLog('DOBAVLJAC_ARTIKLI_PRERACUNAJ_CENU_KURS', $dc_ids);
            echo json_encode($response);
        }
        elseif ($action == 'kolicina_null') {

            $dc_ids=Input::get('dc_ids');
            DB::statement("UPDATE dobavljac_cenovnik SET kolicina = 0 WHERE dobavljac_cenovnik_id IN (".implode(',',$dc_ids).") AND flag_zakljucan = 0");

            AdminSupport::saveLog('WEB_IMPORT_STORNIRAJ_KOLICINA_0', $dc_ids);
        }
        elseif ($action == 'obrisi') {

            $dc_ids=Input::get('dc_ids');
            AdminSupport::saveLog('WEB_IMPORT_OBRISI', $dc_ids);
            DB::statement("UPDATE roba SET sifra_d = NULL, dobavljac_id = NULL WHERE flag_zakljucan='false' AND roba_id IN (SELECT roba_id FROM dobavljac_cenovnik WHERE dobavljac_cenovnik_id IN (".implode(',',$dc_ids)."))");
            DB::statement("DELETE FROM dobavljac_cenovnik WHERE flag_zakljucan=0 AND dobavljac_cenovnik_id IN (".implode(',',$dc_ids).")");
            DB::statement("DELETE FROM dobavljac_cenovnik_karakteristike WHERE sifra_kod_dobavljaca IN (SELECT sifra_kod_dobavljaca FROM dobavljac_cenovnik WHERE flag_zakljucan=0 AND dobavljac_cenovnik_id IN (".implode(',',$dc_ids)."))");
            DB::statement("DELETE FROM dobavljac_cenovnik_slike WHERE sifra_kod_dobavljaca IN (SELECT sifra_kod_dobavljaca FROM dobavljac_cenovnik WHERE flag_zakljucan=0 AND dobavljac_cenovnik_id IN (".implode(',',$dc_ids)."))");
        }
        elseif ($action == 'karak_slike'){
            $dc_ids=Input::get('dc_ids');
            $partner_id=Input::get('partner_id');
            $roba_ids = DB::table('dobavljac_cenovnik')->select('roba_id')->distinct()->whereIn('dobavljac_cenovnik_id',$dc_ids)->where('roba_id','!=',-1)->get();

            foreach($roba_ids as $row){
                $roba_id = $row->roba_id;
                $same_roba_ids = DB::table('dobavljac_cenovnik')->select('dobavljac_cenovnik_id','sifra_kod_dobavljaca','partner_id')->where('roba_id',$roba_id);
                if($same_roba_ids->count() > 0){
                    if($partner_id!=0){
                        if(DB::table('dobavljac_cenovnik')->where('roba_id',$roba_id)->where('partner_id',$partner_id)->count()>0){
                            $same_roba_ids->where('partner_id',$partner_id);
                        }
                    }
                    $dobavljac_cenovnik_first = $same_roba_ids->first();
                    $dobavljac_cenovnik_id = $dobavljac_cenovnik_first->dobavljac_cenovnik_id;
                    $sifra_kod_dobavljaca = $dobavljac_cenovnik_first->sifra_kod_dobavljaca;
                    $partner_id = $dobavljac_cenovnik_first->partner_id;
                    
                    //opis
                    DB::statement("UPDATE roba r SET web_karakteristike = dc.opis FROM dobavljac_cenovnik dc WHERE r.roba_id = ".$roba_id." AND dobavljac_cenovnik_id = ".$dobavljac_cenovnik_id." AND dc.opis IS NOT NULL AND dc.opis <> ''");

                    //dobavljac-karakteristike
                    DB::table('dobavljac_cenovnik_karakteristike')->where(array('roba_id'=>-1,'sifra_kod_dobavljaca'=>$sifra_kod_dobavljaca))->update(array('roba_id'=>$roba_id));

                    //slike
                    $slike_query_dob = DB::table('dobavljac_cenovnik_slike')->where(array('sifra_kod_dobavljaca'=>$sifra_kod_dobavljaca, 'partner_id'=>$partner_id ));
                    $slike_count_roba = DB::table('web_slika')->where('roba_id',$roba_id)->count();

                    $diff_count = $slike_query_dob->count() - $slike_count_roba;
                    if($diff_count > 0){
                        $slike = $slike_query_dob->orderBy('dobavljac_cenovnik_slike_id','desc')->limit($diff_count)->get();
                        AdminImport::saveImages($slike,$roba_id);
                    }
                }
            }
            AdminSupport::saveLog('WEB_IMPORT_PREUZMI_KARAKTERISTIKE_SLIKE', $dc_ids);
        }
        elseif ($action == 'more') {
            $page=Input::get('page');
            $grupa_pr_id=Input::get('grupa_pr_id');
            $proizvodjac=Input::get('proizvodjac');
            $dobavljac=Input::get('dobavljac');
            $search=Input::get('search');

            $criteriaRoba = array(
                'grupa_pr_id' => intval($grupa_pr_id),
                'proizvodjac' => intval($proizvodjac),
                'dobavljac' => intval($dobavljac),
                'search' => $search
                );
            $limit = AdminOptions::limit_liste_robe();
            $roba = AdminArticles::fetchAll($criteriaRoba,array('limit' => $limit,'offset' => (intval($page)-1)*$limit),'r.roba_id-ASC');
            header('Content-type: text/plain; charset=utf-8');
            
            $string = '';
            foreach($roba as $row){
                if(isset($row->kolicina)){
                    $kolicina = intval($row->kolicina);
                }else{
                    $kolicina = '';
                }
                if($row->flag_aktivan == 1){
                    $flag_aktivan = 'DA';
                }else{
                    $flag_aktivan = 'NE';
                }
                if($row->flag_prikazi_u_cenovniku == 1){
                    $flag_prikazi_u_cenovniku = 'DA';
                }else{
                    $flag_prikazi_u_cenovniku = 'NE';
                }
                $string .= '
                <tr class="JSRobaRow" data-id="'.$row->roba_id.'">
                    <td>'.AdminSupport::dobavljac($row->dobavljac_id).'</td>
                    <td>'.$row->roba_id.'</td>
                    <td>'.$row->sifra_d.'</td>
                    <td class="flag_aktivan">'.$flag_aktivan.'</td>
                    <td class="flag_prikazi_u_cenovniku">'.$flag_prikazi_u_cenovniku.'</td>
                    <td> class="roba_naziv"'.$row->naziv.'</td>
                    <td class="kolicina">'.$kolicina.'</td>             
                    <td class="racunska_cena_nc">'.$row->racunska_cena_nc.'</td>
                    <td class="web_marza">'.$row->web_marza.'</td>
                    <td class="web_cena">'.$row->web_cena.'</td>
                    <td>'.AdminArticles::findGrupe($row->grupa_pr_id,'grupa').'</td>
                    <td>'.AdminSupport::find_proizvodjac($row->proizvodjac_id,'naziv').'</td>
                    <td class="">
                        <a target="_blank" class="" href="'.AdminOptions::base_url().'admin/product/'.$row->roba_id.'">
                            <button class="btn btn-secondary btn-xm edit-article-btn btn-circle small">
                                <i class="fa fa-pencil" aria-hidden="true"></i>
                            </button>
                        </a>
                    </td>
                </tr>';
            }
            echo $string;
        }
      elseif ($action == 'povezi_artikle') {

            $dc_ids=Input::get('dc_ids');
            $table_roba_id=Input::get('table_roba_id');

            $roba_first = DB::table('roba')->where('roba_id',$table_roba_id)->first();
            $check_dobavljac = DB::select("SELECT COUNT(dobavljac_cenovnik_id) AS cnt FROM dobavljac_cenovnik WHERE dobavljac_cenovnik_id IN (".implode(',',$dc_ids).") AND (flag_aktivan = 0 OR cena_nc = 0.00 )")[0]->cnt;
            $check_num_partners = count(DB::select("SELECT DISTINCT partner_id FROM dobavljac_cenovnik WHERE dobavljac_cenovnik_id IN (".implode(',',$dc_ids).")"));

            if($check_dobavljac > 0 || $roba_first->flag_aktivan == 0 || count($dc_ids) != $check_num_partners){
                echo 'break';
            }else{
                DB::table('dobavljac_cenovnik')->whereIn('dobavljac_cenovnik_id',$dc_ids)->update(array('roba_id' => $table_roba_id,'povezan' => 1,'tarifna_grupa_id'=>$roba_first->tarifna_grupa_id,'grupa_pr_id'=>$roba_first->grupa_pr_id,'flag_zakljucan'=>$roba_first->flag_zakljucan,'proizvodjac_id'=>$roba_first->proizvodjac_id,'web_marza'=>$roba_first->web_marza,'mp_marza'=>$roba_first->mp_marza));
                foreach ($dc_ids as $id) {
                    DB::statement("UPDATE dobavljac_cenovnik dc SET web_cena = cena_nc*(1+web_marza/100)*(SELECT 1+porez/100 FROM tarifna_grupa WHERE tarifna_grupa_id = dc.tarifna_grupa_id), mpcena = cena_nc*(1+mp_marza/100)*(SELECT 1+porez/100 FROM tarifna_grupa WHERE tarifna_grupa_id = dc.tarifna_grupa_id) WHERE dobavljac_cenovnik_id = ".$id." ");
                    $new_dob_info = DB::select("SELECT ROUND(web_cena) AS web_cena, ROUND(mpcena) AS mpcena, cena_nc, tarifna_grupa_id, grupa_pr_id, proizvodjac_id, web_marza, mp_marza,flag_zakljucan FROM dobavljac_cenovnik WHERE dobavljac_cenovnik_id = ".$id."")[0];
                    $response[$id] = array(
                        'cena_web_marza' => $new_dob_info->web_cena.'.00',
                        'cena_mp_marza' => $new_dob_info->mpcena.'.00',
                        'web_marza' => number_format($new_dob_info->web_marza,2),
                        'mp_marza' => number_format($new_dob_info->mp_marza,2),
                        'tarifna_grupa_id' => AdminSupport::find_tarifna_grupa($new_dob_info->tarifna_grupa_id,'porez'),
                        'cena_nc_pdv' => round(($new_dob_info->cena_nc*(1+AdminSupport::find_tarifna_grupa($new_dob_info->tarifna_grupa_id,'porez')/100))).'.00',
                        'grupa_pr_id' => AdminGroups::find($new_dob_info->grupa_pr_id,'grupa'),
                        'flag_zakljucan' =>$new_dob_info->flag_zakljucan,
                        'proizvodjac_id' => AdminSupport::find_proizvodjac($new_dob_info->proizvodjac_id,'naziv')
                        ); 
                }

                AdminSupport::saveLog('WEB_IMPORT_POVEZI_ARTIKLE', $dc_ids);
                echo json_encode($response);
            }

        }

        //Edit texta
        elseif ($action == 'text_edit') {
            $dc_ids = Input::get('dc_ids');
            $insert = Input::get('insert');
            $naziv_check = Input::get('naziv_check');
            $end_check = Input::get('end_check');

            $response = array('dc_ids'=>array());

            $logProvera = 0;
            foreach($dc_ids as $dc_id){

                $roba_id = DB::table('dobavljac_cenovnik')->where('dobavljac_cenovnik_id', $dc_id)->pluck('roba_id');
                    $logProvera = 1;
                    if($naziv_check && !$end_check){
                        DB::statement("UPDATE dobavljac_cenovnik set naziv = ('".$insert."' || ' '|| naziv) WHERE dobavljac_cenovnik_id = $dc_id");
                        if($roba_id != -1){
                            $naziv =DB::table('dobavljac_cenovnik')->where('dobavljac_cenovnik_id', $dc_id)->pluck('naziv');
                            DB::statement("UPDATE roba set naziv = '".$naziv."' WHERE roba_id = $roba_id  ");
                            DB::statement("UPDATE roba set naziv_web = '".$naziv."' WHERE roba_id = $roba_id");
                        }
                        $response['dc_ids'][$dc_id] = DB::table('dobavljac_cenovnik')->where('dobavljac_cenovnik_id', $dc_id)->pluck('naziv');

                    }
                    elseif($naziv_check && $end_check){
                        DB::statement("UPDATE dobavljac_cenovnik set naziv = (naziv || ' ' || '".$insert."') where dobavljac_cenovnik_id = $dc_id");
                        if($roba_id != -1){
                            $naziv =DB::table('dobavljac_cenovnik')->where('dobavljac_cenovnik_id', $dc_id)->pluck('naziv');
                            DB::statement("UPDATE roba set naziv = '".$naziv."'  WHERE roba_id = $roba_id ");
                            DB::statement("UPDATE roba set naziv_web = '".$naziv."'  WHERE roba_id = $roba_id ");
                        }
                        $response['dc_ids'][$dc_id] = DB::table('dobavljac_cenovnik')->where('dobavljac_cenovnik_id', $dc_id)->pluck('naziv');
                    }
                    elseif(!$naziv_check && !$end_check){
                        DB::statement("UPDATE dobavljac_cenovnik set opis = ('".$insert."' || ' ' || opis) where dobavljac_cenovnik_id = $dc_id");
                        if($roba_id != -1){
                            $opis =DB::table('dobavljac_cenovnik')->where('dobavljac_cenovnik_id', $dc_id)->pluck('opis');
                            DB::statement("UPDATE roba set opis = '".$opis."'  WHERE roba_id = $roba_id ");
                            DB::statement("UPDATE roba set web_opis = '".$opis."'  WHERE roba_id = $roba_id ");
                        }
                    }
                    elseif(!$naziv_check && $end_check){
                        DB::statement("UPDATE dobavljac_cenovnik set opis = (opis || ' ' || '".$insert."') where dobavljac_cenovnik_id = $dc_id");
                        if($roba_id != -1){
                            $opis =DB::table('dobavljac_cenovnik')->where('dobavljac_cenovnik_id', $dc_id)->pluck('opis');
                            DB::statement("UPDATE roba set opis = '".$opis."'  WHERE roba_id = $roba_id ");
                            DB::statement("UPDATE roba set web_opis = '".$opis."'  WHERE roba_id = $roba_id ");
                        }
                    }
                
            }
            if($logProvera){
                AdminSupport::saveLog('WEB_IMPORT_DODAJ_TEKST', $dc_ids);
            }

            return json_encode($response);

        }
        //Replace texta
        elseif ($action == 'text_replace') {
            $dc_ids = Input::get('dc_ids');
            $replaceFrom = Input::get('replaceFrom');
            $replaceTo = Input::get('replaceTo');
            $naziv_check = Input::get('naziv_check');
            $deoTeksta = Input::get('deoTeksta');

            $response = array('dc_ids'=>array(),'counter'=>0);


            if($deoTeksta){
                $logProvera = 0;
                foreach($dc_ids as $dc_id){

                    $roba_id = DB::table('dobavljac_cenovnik')->where('dobavljac_cenovnik_id', $dc_id)->pluck('roba_id');
                    if($roba_id == -1){
                        $logProvera = 1;
                        if($naziv_check){
                            $new_value = DB::table('dobavljac_cenovnik')->where('dobavljac_cenovnik_id', $dc_id)->pluck('naziv');
                        }elseif (!$naziv_check) {
                            $new_value = DB::table('dobavljac_cenovnik')->where('dobavljac_cenovnik_id', $dc_id)->pluck('opis');
                        }

                        $new_value = str_ireplace($replaceFrom, $replaceTo, $new_value, $count);

                        $response['counter'] = $response['counter'] + $count;

                        if($naziv_check){       
                            DB::statement("UPDATE dobavljac_cenovnik set naziv = '" .$new_value. "' where dobavljac_cenovnik_id = $dc_id ");
                            if($roba_id != -1){
                                $naziv =DB::table('dobavljac_cenovnik')->where('dobavljac_cenovnik_id', $dc_id)->pluck('naziv');
                                DB::statement("UPDATE roba set naziv = '".$naziv."'  WHERE roba_id = $roba_id ");
                                DB::statement("UPDATE roba set naziv_web = '".$naziv."'  WHERE roba_id = $roba_id ");
                            }
                            $response['dc_ids'][$dc_id] = $new_value;
                            // if($roba_id > 0){
                            //     DB::statement("UPDATE roba set naziv = '" .$new_value. "', naziv_web = '" .$new_value. "' where roba_id = $roba_id ");
                            // }
                        }elseif (!$naziv_check) {
                            DB::statement("UPDATE dobavljac_cenovnik set opis = '" .$new_value. "' where dobavljac_cenovnik_id = $dc_id ");
                            if($roba_id != -1){
                                $opis =DB::table('dobavljac_cenovnik')->where('dobavljac_cenovnik_id', $dc_id)->pluck('opis');
                                DB::statement("UPDATE roba set opis = '".$opis."'  WHERE roba_id = $roba_id ");
                                DB::statement("UPDATE roba set web_opis = '".$opis."'  WHERE roba_id = $roba_id ");
                            }
                            //  if($roba_id > 0){
                            // DB::statement("UPDATE roba set web_opis = '" .$new_value. "' where roba_id = $roba_id ");
                            // }
                        }
                    }
                }
            }elseif (!$deoTeksta) {


                $replace_from_arr = explode(' ',trim($replaceFrom));

                $logProvera = 0;

                foreach($dc_ids as $dc_id){

                    $roba_id = DB::table('dobavljac_cenovnik')->where('dobavljac_cenovnik_id', $dc_id)->pluck('roba_id');
                    if($roba_id == -1){
                        $logProvera = 1;
                        if($naziv_check){ 
                            $db_value = DB::table('dobavljac_cenovnik')->where('dobavljac_cenovnik_id', $dc_id)->pluck('naziv');
                        }elseif (!$naziv_check) {
                            $db_value = DB::table('dobavljac_cenovnik')->where('dobavljac_cenovnik_id', $dc_id)->pluck('opis');
                        }

                        $db_value_arr = explode(" ", trim($db_value));

                        for ($i=0; $i<count($db_value_arr); $i++){
                            if(strtoupper($db_value_arr[$i]) == strtoupper($replace_from_arr[0])){
                                $j=1;
                                $k=$i+1;
                                while ($j<count($replace_from_arr) && $k<count($db_value_arr) && strtoupper($db_value_arr[$k]) == strtoupper($replace_from_arr[$j])){
                                    $k++;
                                    $j++;
                                }
                                if($j == count($replace_from_arr)){
                                    $db_value_arr[$i] = $replaceTo;
                                    array_splice($db_value_arr, $i+1, count($replace_from_arr) - 1);
                                    $response['counter']++;                               
                                }                       
                            }
                        }

                        $new_value = implode(" ",$db_value_arr);

                        if($naziv_check){       
                            DB::statement("UPDATE dobavljac_cenovnik set naziv = '" .$new_value. "' where dobavljac_cenovnik_id = $dc_id ");
                            if($roba_id != -1){
                            $naziv =DB::table('dobavljac_cenovnik')->where('dobavljac_cenovnik_id', $dc_id)->pluck('naziv');
                            DB::statement("UPDATE roba set naziv = '".$naziv."'  WHERE roba_id = $roba_id ");
                            DB::statement("UPDATE roba set naziv_web = '".$naziv."'  WHERE roba_id = $roba_id ");
                            }
                            $response['dc_ids'][$dc_id] = $new_value;
                            // // if($roba_id > 0){
                            // DB::statement("UPDATE roba set naziv = '" .$new_value. "', naziv_web = '" .$new_value. "' where roba_id = $roba_id ");
                            // }
                        }
                        elseif (!$naziv_check) {
                            DB::statement("UPDATE dobavljac_cenovnik set opis = '" .$new_value. "' where dobavljac_cenovnik_id = $dc_id ");
                            if($roba_id != -1){
                                $opis =DB::table('dobavljac_cenovnik')->where('dobavljac_cenovnik_id', $dc_id)->pluck('opis');
                                DB::statement("UPDATE roba set opis = '".$opis."'  WHERE roba_id = $roba_id ");
                                DB::statement("UPDATE roba set web_opis = '".$opis."'  WHERE roba_id = $$roba_id ");
                            }
                            // if($roba_id > 0){
                            // DB::statement("UPDATE roba set web_opis = '" .$new_value. "' where roba_id = $roba_id ");
                            // }
                        }
                    }
                }
            }

            if($logProvera){
                AdminSupport::saveLog('WEB_IMPORT_IZMENI_TEKST', $dc_ids);  
            }        


            return json_encode($response);

        }
        elseif ($action == 'razvezi_artikle') {

            $dc_ids=Input::get('dc_ids');
            $dc_new_ids = array();
            $logs_roba_ids = '';

            foreach ($dc_ids as $id) {
                $first = DB::table('dobavljac_cenovnik')->select('roba_id','sifra_kod_dobavljaca','flag_zakljucan')->where('dobavljac_cenovnik_id',$id)->first();
                $roba_id = $first->roba_id;
                if($roba_id != -1){
                    $sifra_roba = DB::table('roba')->where('roba_id',$roba_id)->pluck('sifra_d');
                    if($sifra_roba != $first->sifra_kod_dobavljaca && $first->flag_zakljucan == 0){
                        DB::table('dobavljac_cenovnik')->where(array('dobavljac_cenovnik_id'=>$id))->update(array('roba_id'=>-1,'povezan'=>0,'flag_prikazi_u_cenovniku'=>0));
                        $dc_new_ids[] = $id;
                    }

                    $logs_roba_ids .= $roba_id.', ';
                }
            }

            AdminSupport::saveLog('WEB_IMPORT_RAZVEZI_ARTIKLE', $dc_ids);
            echo json_encode($dc_new_ids);

        }
        elseif ($action == 'product_data') {

            $dc_id=Input::get('dc_id');
            $sifra=Input::get('sifra');
            
            $first = DB::table('dobavljac_cenovnik')->select('naziv','kolicina','cena_nc','web_marza','web_cena','mp_marza','mpcena','tarifna_grupa_id','opis','partner_id')->where('dobavljac_cenovnik_id',$dc_id)->first();
            $slike = array_map('current',DB::table('dobavljac_cenovnik_slike')->select('putanja')->where('sifra_kod_dobavljaca',$sifra)->where('partner_id',$first->partner_id)->get()) ;  
                               
           
            $response = array(
                'naziv'=>$first->naziv,
                'kolicina'=>round($first->kolicina),
                'cena_nc'=>round($first->cena_nc).'.00',
                'web_marza'=>number_format($first->web_marza,2),
                'web_cena'=>round($first->web_cena).'.00',
                'mp_marza'=>number_format($first->mp_marza,2),
                'mpcena'=>round($first->mpcena).'.00',
                'opis'=>str_replace(array("![CDATA[", "]]"),array("",""), $first->opis),                
                'porez'=>AdminSupport::find_tarifna_grupa($first->tarifna_grupa_id,'porez'),
                'slike'=>$slike
                );

            echo json_encode($response);                           
         }
        elseif ($action == 'product_submit') {
            $data = Input::get();
            unset($data['action']);

            $rules = array(
                'naziv' => 'required|regex:'.AdminSupport::regex().'|between:1,200',
                'opis' => 'regex:'.AdminSupport::regex().'|between:0,8000',
                'kolicina' => 'numeric|digits_between:1,10',
                'cena_nc' => 'numeric|digits_between:1,20',
                'web_marza' => 'numeric|digits_between:1,10',
                'web_cena' => 'numeric|digits_between:1,20',
                'mp_marza' => 'numeric|digits_between:1,10',
                'mpcena' => 'numeric|digits_between:1,20'
            );

            $validator = Validator::make($data, $rules);
            if($validator->fails()){
                $errors = $validator->messages();
                echo json_encode(
                    array(
                    'message'=>'error',
                    'errors'=>array(
                        'naziv' => $errors->first('naziv'),
                        'kolicina' => $errors->first('kolicina'),
                        'cena_nc' => $errors->first('cena_nc'),
                        'opis' => $errors->first('opis'),
                        'web_marza' => $errors->first('web_marza'),
                        'web_cena' => $errors->first('web_cena'),
                        'mp_marza' => $errors->first('mp_marza'),
                        'mpcena' => $errors->first('mpcena')
                        )
                    )
                );                
            }else{
                $dobavljac_cenovnik_id = $data['dobavljac_cenovnik_id'];
                unset($data['dobavljac_cenovnik_id']);
                foreach($data as $key => $val){
                    if($val==null || $val==''){
                        unset($data[$key]);
                    }
                
                DB::table('dobavljac_cenovnik')->where('dobavljac_cenovnik_id',$dobavljac_cenovnik_id)->update($data);
                
                DB::table('dobavljac_cenovnik')->where('dobavljac_cenovnik_id',$dobavljac_cenovnik_id)->where('opis','=','')->update(array('flag_opis_postoji' => 0));
                DB::table('dobavljac_cenovnik')->where('dobavljac_cenovnik_id',$dobavljac_cenovnik_id)->where('opis','=',' ')->update(array('flag_opis_postoji' => 0));
                DB::table('dobavljac_cenovnik')->where('dobavljac_cenovnik_id',$dobavljac_cenovnik_id)->where('opis','=','![CDATA[]]')->update(array('flag_opis_postoji' => 0));

                DB::table('dobavljac_cenovnik')->where('dobavljac_cenovnik_id',$dobavljac_cenovnik_id)->where('opis', '!=', '')->where('opis', '!=', ' ')->where('opis', '!=', '![CDATA[]]')->update(array('flag_opis_postoji' => 1));

                }
                AdminSupport::saveLog('WEB_IMPORT_ARTIKAL_IZMENI', array($dobavljac_cenovnik_id));
                echo json_encode(
                    array(
                    'message'=>'success',
                    'data'=>$data
                    )
                );
            }
        }
        elseif ($action == 'take_images') {
            $roba_ids = Input::get('roba_ids');
            AdminImport::saveArticlesImages($roba_ids);
        }
        elseif ($action == 'definisane_marze_primeni') {

           $dc_ids = Input::get('dc_ids');

            DB::statement("update dobavljac_cenovnik set web_marza = definisana_web_marza(cena_nc), mp_marza = definisana_mp_marza(cena_nc) where dobavljac_cenovnik_id in (".implode(',',$dc_ids).") and flag_zakljucan = 0");

            DB::statement("UPDATE dobavljac_cenovnik r SET web_cena = cena_nc*(1+web_marza/100)*(1+(SELECT porez FROM tarifna_grupa WHERE tarifna_grupa_id=r.tarifna_grupa_id LIMIT 1)/100), mpcena = cena_nc*(1+mp_marza/100)*(1+(SELECT porez FROM tarifna_grupa WHERE tarifna_grupa_id=r.tarifna_grupa_id LIMIT 1)/100) WHERE dobavljac_cenovnik_id in (".implode(',',$dc_ids).") AND flag_zakljucan = 0");

            return json_encode(DB::table('dobavljac_cenovnik')->select('dobavljac_cenovnik_id','web_marza','mp_marza','web_cena','mpcena')->whereIn('dobavljac_cenovnik_id',$dc_ids)->get());

        }

    }
    
    public function articles_search() {
        $rec = trim(Input::get('articles'));
        $general_id = Input::get('general_id');

        $not_ids = '';
        $vezani = DB::table('vezani_artikli')->where('roba_id',$general_id)->get();
        if(count($vezani)>0){
            foreach($vezani as $row){
                $not_ids .= ','.$row->vezani_roba_id;
            }
            $not_ids = 'roba_id NOT IN ('.$general_id.','.substr($not_ids,1).') AND ';
        }else{
            $not_ids = 'roba_id <> '.$general_id.' AND ';
        }

        is_numeric($rec) ? $robaIdFormatiran = "r.roba_id = '" . $rec . "' OR " : $robaIdFormatiran = "";
        $skuFormatiran = "sku ILIKE '%" . $rec . "%' ";
        $nazivFormatiran = "naziv_web ILIKE '%" . $rec . "%' ";
        $grupaFormatiran = "grupa ILIKE '%" . $rec . "%' ";
        $proizvodjacFormatiran = "p.naziv ILIKE '%" . $rec . "%' ";

        $articles = DB::select("SELECT roba_id, sku, naziv_web, grupa FROM roba r INNER JOIN grupa_pr g ON r.grupa_pr_id = g.grupa_pr_id INNER JOIN proizvodjac p ON r.proizvodjac_id = p.proizvodjac_id WHERE flag_aktivan=1 AND flag_prikazi_u_cenovniku=1 AND ".$not_ids."(" . $robaIdFormatiran . "" . $nazivFormatiran . " OR " . $skuFormatiran . " OR " . $grupaFormatiran . " OR " . $proizvodjacFormatiran . ") ORDER BY grupa ASC");
        header('Content-type: text/plain; charset=utf-8');          
        $list = "<ul class='articles_list_new'>";
        foreach ($articles as $article) {
            $list .= "
                <li class='articles_list__item'>
                    <a class='articles_list__item__link' href='" . AdminOptions::base_url() . "admin/vezani_artikli/".$general_id."/" . $article->roba_id . "'>"
                    	."<span class='articles_list__item__link__small'>" . $article->roba_id . "</span>"
                        ."<span class='articles_list__item__link__small'>" . $article->sku . "</span>"
                        ."<span class='articles_list__item__link__text'>" . $article->naziv_web . "</span>"
                        ."<span class='articles_list__item__link__cat'>" . $article->grupa . "</span>
                    </a>
                </li>";
        }
        $list .= "</ul>";
        echo $list; 
    }


    public function srodni_search() {
        $rec = trim(Input::get('articles'));
        $general_id = Input::get('general_id');

        $not_ids = '';
        $srodni = DB::table('srodni_artikli')->where('roba_id',$general_id)->get();
        if(count($srodni)>0){
            foreach($srodni as $row){
                $not_ids .= ','.$row->srodni_roba_id;
            }
            $not_ids = 'roba_id NOT IN ('.$general_id.','.substr($not_ids,1).') AND ';
        }else{
            $not_ids = 'roba_id <> '.$general_id.' AND ';
        }

        is_numeric($rec) ? $robaIdFormatiran = "r.roba_id = '" . $rec . "' OR " : $robaIdFormatiran = "";
        $skuFormatiran = "sku ILIKE '%" . $rec . "%' ";
        $nazivFormatiran = "naziv_web ILIKE '%" . $rec . "%' ";
        $grupaFormatiran = "grupa ILIKE '%" . $rec . "%' ";
        $proizvodjacFormatiran = "p.naziv ILIKE '%" . $rec . "%' ";

        $articles = DB::select("SELECT roba_id, sku, naziv_web, grupa FROM roba r INNER JOIN grupa_pr g ON r.grupa_pr_id = g.grupa_pr_id INNER JOIN proizvodjac p ON r.proizvodjac_id = p.proizvodjac_id WHERE flag_aktivan=1 AND flag_prikazi_u_cenovniku=1 AND ".$not_ids."(" . $robaIdFormatiran . "" . $nazivFormatiran . " OR " . $skuFormatiran . " OR " . $grupaFormatiran . " OR " . $proizvodjacFormatiran . ") ORDER BY grupa ASC");
        header('Content-type: text/plain; charset=utf-8');          
        $list = "<ul class='articles_list_new'>";
        foreach ($articles as $article) {
            $list .= "
                <li class='articles_list__item'>
                    <a class='articles_list__item__link' href='" . AdminOptions::base_url() . "admin/srodni_artikli/".$general_id."/" . $article->roba_id . "'>"
                        ."<span class='articles_list__item__link__small'>" . $article->roba_id . "</span>"
                        ."<span class='articles_list__item__link__small'>" . $article->sku . "</span>"
                        ."<span class='articles_list__item__link__text'>" . $article->naziv_web . "</span>"
                        ."<span class='articles_list__item__link__cat'>" . $article->grupa . "</span>
                    </a>
                </li>";
        }
        $list .= "</ul>";
        echo $list; 
    }

    public function vrstaPartnera(){
        $partner_id = Input::get('partner_id');
        $vrsta = Input::get('vrsta');
        $crm_id=null;
        $faktura_id=null;
        $check = DB::table('partner_je')->where('partner_id', $partner_id)->where('partner_vrsta_id', $vrsta)->count();
        if($check == 0){
            DB::table('partner_je')->insert(['partner_id' => $partner_id,'partner_vrsta_id' => $vrsta]);
            if($vrsta == 117){                
                DB::table('dobavljac_cenovnik_kolone')->insert(['partner_id' => $partner_id]);
            }
             if($vrsta == 258){  
                $crm_tip=DB::table('crm_tip')->first();
                $crm_tip=$crm_tip->crm_tip_id;
                $crm_status=DB::table('crm_status')->first();  
                $crm_status=$crm_status->crm_status_id;           
                DB::table('crm')->insert(['partner_id' => $partner_id,'flag_zavrseno'=>0,'crm_status_id'=>$crm_status,'crm_tip_id'=>$crm_tip,'datum_kreiranja' => date('Y-m-d')]);
            }
            if($vrsta == 555){ 
                $partner = DB::table('crm_fakture')->where('partner_id', $partner_id)->first();
                if($partner === null)
                {
                     DB::table('crm_fakture')->insert(['partner_id' => $partner_id,'opis'=>'test','vrednost'=>0,'kursna_lista_id'=>1,'crm_tip_id'=>1,'datum_fakturisanja'=>date('Y-m-d')]);
                }  
               
                
            }
        } else {
            DB::table('partner_je')->where('partner_id', $partner_id)->where('partner_vrsta_id', $vrsta)->delete();
            if($vrsta == 117){                
                DB::table('dobavljac_cenovnik_kolone')->where('partner_id', $partner_id)->delete();
            }
        }

    }
    public function permissions() {
        $inputs = Input::get();
        $action = $inputs['action'];
        
        if($action == 'modul'){
            if($inputs['tip'] == 0){
                $sporedni_moduli = array_map('current',DB::table('ac_module')->select('ac_module_id')->where(array('aktivan'=>1,'parrent_ac_module_id'=>$inputs['module_id']))->get());
                DB::table('ac_group_module')->where('ac_group_id',$inputs['group_id'])->whereIn('ac_module_id',$sporedni_moduli)->update(array('alow'=>$inputs['execute']));
                AdminSupport::saveLog('GLAVNI_MODUL_IZMENA', array('modul' => $inputs['module_id'], 'nivo_pristupa' => $inputs['group_id']));
            }
            elseif($inputs['tip'] == 1){
                DB::table('ac_group_module')->where(array('ac_group_id'=>$inputs['group_id'],'ac_module_id'=>$inputs['module_id']))->update(array('alow'=>$inputs['execute']));
                AdminSupport::saveLog('SPOREDNI_MODUL_IZMENA', array('modul' => $inputs['module_id'], 'nivo_pristupa' => $inputs['group_id']));
            }
        }
    }

    public function permissionsAll(){
       $inputs = Input::get(); 
       DB::table('ac_group_module')->where('ac_group_id', $inputs['group_id'])->update(array('alow'=>$inputs['execute']));
       DB::table('ac_group_module')->where('ac_group_id', $inputs['group_id'])->update(array('alow'=>$inputs['execute']));
    }

    public function support() {
        $inputs = Input::get();
        $action = $inputs['action'];
        
        if($action == 'save_conf'){
            $konfigurator_id = $inputs['konfigurator_id'];
            $old_grupa_pr_id = $inputs['old_grupa_pr_id'];
            $grupa_pr_id = $inputs['grupa_pr_id'];
            $rbr = $inputs['rbr'];
            if($rbr == 'new'){
                $rbr = DB::select("SELECT MAX(rbr) AS max FROM konfigurator_grupe WHERE konfigurator_id = ".$konfigurator_id."")[0]->max + 1;
            }
            $check_exists = DB::table('konfigurator_grupe')->where(array('konfigurator_id'=>$konfigurator_id,'grupa_pr_id'=>$old_grupa_pr_id))->count();


            if($check_exists == 0){
                DB::table('konfigurator_grupe')->insert(array('konfigurator_id'=>$konfigurator_id,'grupa_pr_id'=>$grupa_pr_id,'rbr'=>$rbr));
                AdminSupport::saveLog('KONFIGURATOR_GRUPE_DODAJ', array('konfigurator' => $konfigurator_id, 'grupa' => $grupa_pr_id));
            }else{
                DB::table('konfigurator_grupe')->where(array('konfigurator_id'=>$konfigurator_id,'grupa_pr_id'=>$old_grupa_pr_id))->update(array('grupa_pr_id'=>$grupa_pr_id,'rbr'=>$rbr));
                AdminSupport::saveLog('KONFIGURATOR_GRUPE_IZMENI', array('konfigurator' => $konfigurator_id, 'grupa' => $grupa_pr_id));
            }
        }

        if($action == 'delete_conf'){
            $konfigurator_id = $inputs['konfigurator_id'];
            $grupa_pr_id = $inputs['grupa_pr_id'];

            DB::table('konfigurator_grupe')->where(array('konfigurator_id'=>$konfigurator_id,'grupa_pr_id'=>$grupa_pr_id))->delete();

            AdminSupport::saveLog('KONFIGURATOR_GRUPE_OBRISI', array('konfigurator' => $konfigurator_id, 'grupa' => $grupa_pr_id));
        }
    }

    public static function getLevelGroups($grupa_pr_id){
        return DB::table('grupa_pr')->select('grupa_pr_id', 'grupa', 'web_b2c_prikazi')->where('parrent_grupa_pr_id',$grupa_pr_id)->orderBy('redni_broj', 'asc')->get();
    }
    
    public function selectGroups($grupa_pr_id=null) {
        return AdminSupport::selectGroups($grupa_pr_id);
        // $render = '<option value="">Izaberite grupu</option><option value="-1">Nedefinisana</option>';
        // foreach(self::getLevelGroups(0) as $row_gr){
        //     if($row_gr->grupa_pr_id == $grupa_pr_id){
        //         $render .= '<option value="'.$row_gr->grupa_pr_id.'" selected>'.$row_gr->grupa.' - '.$row_gr->web_b2c_prikazi.'</option>';
        //     }else{
        //         $render .= '<option value="'.$row_gr->grupa_pr_id.'">'.$row_gr->grupa.' - '.$row_gr->web_b2c_prikazi.'</option>';
        //     }
        //     foreach(self::getLevelGroups($row_gr->grupa_pr_id) as $row_gr1){
        //         if($row_gr1->grupa_pr_id == $grupa_pr_id){
        //             $render .= '<option value="'.$row_gr1->grupa_pr_id.'" selected >|&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr1->grupa.' - '.$row_gr1->web_b2c_prikazi.'</option>';
        //         }else{
        //             $render .= '<option value="'.$row_gr1->grupa_pr_id.'" >|&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr1->grupa.' - '.$row_gr1->web_b2c_prikazi.'</option>';
        //         }
        //         foreach(self::getLevelGroups($row_gr1->grupa_pr_id) as $row_gr2){
        //             if($row_gr2->grupa_pr_id == $grupa_pr_id){
        //                 $render .= '<option value="'.$row_gr2->grupa_pr_id.'" selected >|&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr2->grupa.' - '.$row_gr2->web_b2c_prikazi.'</option>';
        //             }else{
        //                 $render .= '<option value="'.$row_gr2->grupa_pr_id.'" >|&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr2->grupa.' - '.$row_gr2->web_b2c_prikazi.'</option>';
        //             }
        //             foreach(self::getLevelGroups($row_gr2->grupa_pr_id) as $row_gr3){
        //                 if($row_gr3->grupa_pr_id == $grupa_pr_id){
        //                     $render .= '<option value="'.$row_gr3->grupa_pr_id.'" selected>|&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr3->grupa.' - '.$row_gr3->web_b2c_prikazi.'</option>';
        //                 }else{
        //                     $render .= '<option value="'.$row_gr3->grupa_pr_id.'">|&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr3->grupa.' - '.$row_gr3->web_b2c_prikazi.'</option>';
        //                 }
        //             }
        //         }
        //     }
        // }
        // return $render;
    }

    public function renderBrands() {
        echo View::make('admin/partials/ajax/proizvodjaci_import')->render();
    }

    public function renderBrandsMultiple() {
        $input = Input::get();
        $criteria = $input['criteria'];
        // echo View::make('admin/partials/ajax/proizvodjaci_import_multiple', array('criteria' => $criteria))->render();
        return AdminSupport::selectProizvodjaci($criteria);
    }

    public function renderTarifna() {
        echo View::make('admin/partials/ajax/tarifna_grupa')->render();
    }    
}