<?php
 
class AdminTroskoviIsporukeController extends Controller {

	public function index() {
		$troskoviIsporuke = DB::table('web_troskovi_isporuke')->orderBy('web_troskovi_isporuke','asc')->get();
		$troskoviIsporuke[] = (object) array('web_troskovi_isporuke_id'=>(DB::table('web_troskovi_isporuke')->max('web_troskovi_isporuke_id')+1),'tezina_gr'=>'','cena_do'=>'','cena'=>'');

		$data = array(
	                'strana'=>'troskovi_isporuke',
	                'title'=> 'Troškovi isporuke',
	                'troskovi_isporuke'=>$troskoviIsporuke
	            );
		return View::make('admin/page', $data);
	}

	public function save() {
		$data = Input::get();

		$previous = DB::table('web_troskovi_isporuke')->where('web_troskovi_isporuke_id','<',$data['web_troskovi_isporuke_id'])->orderBy('web_troskovi_isporuke_id','desc')->first();
		$last = DB::table('web_troskovi_isporuke')->where('web_troskovi_isporuke_id','>',$data['web_troskovi_isporuke_id'])->orderBy('web_troskovi_isporuke_id','asc')->first();

		$validator_messages = array('required'=>'Polje nesme biti prazno!','numeric'=>'Polje sme da sadrzi samo brojeve!','digits_between'=>'Duzina unetih cifara je predugačka!','min'=>'Minimalna vrednost polja je neodgovarajuća!','max'=>'Prekoračili ste maksimalnu vrednost polja!');

		$validation = array('cena' => 'required|numeric|digits_between:0,10');
		if(AdminOptions::web_options(133)==1){
			$validation['tezina_gr'] = 'required|numeric|digits_between:0,20'.(!is_null($previous) ? '|min:'.$previous->tezina_gr : '').(!is_null($last) ? '|max:'.$last->tezina_gr : '');
		}
		if(AdminOptions::web_options(150)==1){
			$validation['cena_do'] = 'required|numeric|digits_between:0,20'.(!is_null($previous) ? '|max:'.$previous->cena_do : '').(!is_null($last) ? '|min:'.$last->cena_do : '');
		}

    	$validator = Validator::make(
    		array(
    			'tezina_gr'=>$data['tezina_gr'],
    			'cena_do'=>$data['cena_do'],
    			'cena'=>$data['cena']
    		), 
    		$validation,
    		$validator_messages);

        if($validator->fails()){
        	return Redirect::to(AdminOptions::base_url().'admin/troskovi-isporuke')->withInput()->withErrors($validator->messages());
        }else{
        	if(is_null(DB::table('web_troskovi_isporuke')->where('web_troskovi_isporuke_id',$data['web_troskovi_isporuke_id'])->first())){

        		DB::table('web_troskovi_isporuke')->insert(
        			array(
        				'web_troskovi_isporuke_id'=>$data['web_troskovi_isporuke_id'],
        				'tezina_gr'=>$data['tezina_gr'],
        				'cena_do'=>$data['cena_do'],
        				'cena'=>$data['cena']
        			)
        		);
        		AdminSupport::saveLog('SIFARNIK_TROSKOVI_ISPORUKE_DODAJ', array(DB::table('web_troskovi_isporuke')->max('web_troskovi_isporuke_id')));
        	}else{
        		DB::table('web_troskovi_isporuke')->where('web_troskovi_isporuke_id',$data['web_troskovi_isporuke_id'])->update(
        			array(
        				'tezina_gr'=>$data['tezina_gr'],
        				'cena_do'=>$data['cena_do'],
        				'cena'=>$data['cena']
        			)
        		);
        		AdminSupport::saveLog('SIFARNIK_TROSKOVI_ISPORUKE_IZMENI', array($data['web_troskovi_isporuke_id']));
        	}
        	return Redirect::to(AdminOptions::base_url().'admin/troskovi-isporuke')->with('success',true);
        }
	}

	public function delete($id) {
		AdminSupport::saveLog('SIFARNIK_TROSKOVI_ISPORUKE_OBRISI', array($id));
		DB::table('web_troskovi_isporuke')->where('web_troskovi_isporuke_id',$id)->delete();
		return Redirect::to(AdminOptions::base_url().'admin/troskovi-isporuke')->with('success-delete',true);
	}

	
	public function cena() {

		$cena_isporuke =DB::table('cena_isporuke')->pluck('cena');
		
		$data = array(
	                'strana'=>'troskovi_isporuke',
	                'title'=> 'Troškovi isporuke',
	                'cena_isporuke'=>$cena_isporuke
	            );

		return View::make('admin/troskovi_isporuke', $data);
	}

	public function cena_save() {
		
		$inputs = Input::get();
		$validator = Validator::make($inputs, array('cena' => 'numeric' ));
		if($validator->fails()){
            return Redirect::back()->withInput()->withErrors($validator->messages());
        }else{
		 $general_data = $inputs;

            if($inputs['cena_id'] != 0){
                DB::table('cena_isporuke')->where('cena_id',$inputs['cena_id'])->update($general_data);
               AdminSupport::saveLog('CENA_IZMENI', array($inputs['cena_id']));
            }else{
                DB::table('cena_isporuke')->insert($general_data);
                AdminSupport::saveLog('CENA_DODAJ', array(DB::table('cena_isporuke')->max('cena_isporuke_id')));

            }
		return Redirect::to(AdminOptions::base_url().'admin/troskovi-isporuke')->with('success',true);
        }
	}

	public function cena_delete($id) {
		AdminSupport::saveLog('CENA_OBRISI', array($id));
		DB::table('cena_isporuke')->where('cena_isporuke_id',$id)->delete();

		return Redirect::to(AdminOptions::base_url().'admin/troskovi-isporuke')->with('success-delete',true);
	}




}