<?php
use Service\TranslatorService;

class AdminCRMController extends Controller {
      
      
    public function index($crm_id=null,$partner_id=null,$crm_akcija_id=null,$extension_id=null){
        $crm= DB::table('crm')->orderBy('crm_id','dsc')->get();       
        $page = Input::get('page');
        $partner_vrsta_id=DB::table('partner_je')->where('partner_id',$partner_id)->pluck('partner_vrsta_id');
        $partner_je=DB::table('partner_je')->where('partner_vrsta_id', 258)->select('partner_id')->get();
        $page = !is_null($page) ? $page : 1;
        $partneri=AdminKupci::getPartneriSearch(null,258,null,null,null,null,null);
        $crm_akcija_tip_id=DB::table('crm_akcija')->where('crm_id',$crm_id)->pluck('crm_akcija_tip_id');
        $limit=15;       
        $aktivni=Input::get('aktivni');
        $crm_status_id=Input::get('crm_status_id');
        $crm_tip_id=Input::get('crm_tip_id');
        $datum_pocetka=Input::get('datum_pocetka');
        $datum_kraja=Input::get('datum_kraja');
        $search=Input::get('search');
        $sort_column = Input::get('sort_column') != null && Input::get('sort_column') != '' ? Input::get('sort_column') : 'cr.datum_kreiranja';
        $sort_direction = Input::get('sort_direction') != null && Input::get('sort_direction') != '' ? Input::get('sort_direction') : 'ASC';

      //buduce akcije ulogovanog korisnika i administratora  
        if(AdminCrm::check_administrator()){
            
               $akcije_danas=DB::table('crm_akcija')->select('crm_akcija.opis','crm_akcija.datum','crm_akcija_tip.naziv','crm_akcija.flag_zavrseno','crm_akcija.crm_id', 'crm_akcija.datum_zavrsetka','partner.naziv as partner_ime')
                ->where('datum','>=',date('Y-m-d'))
                ->join('crm', 'crm_akcija.crm_id', '=', 'crm.crm_id')
                ->join('partner', 'crm.partner_id', '=', 'partner.partner_id')
                ->join('crm_akcija_tip', 'crm_akcija.crm_akcija_tip_id', '=', 'crm_akcija_tip.crm_akcija_tip_id')->orderBy('datum','asc')->get();   
       }
        else{
               $akcije_danas=DB::table('crm_akcija')->select('crm_akcija.opis','crm_akcija.datum','crm_akcija_tip.naziv','crm_akcija.flag_zavrseno','crm_akcija.crm_id', 'crm_akcija.datum_zavrsetka','partner.naziv as partner_ime')
                ->where('datum','>=',date('Y-m-d'))
                ->join('crm', 'crm_akcija.crm_id', '=', 'crm.crm_id')
                ->join('imenik_crm', 'imenik_crm.crm_id', '=', 'crm.crm_id')
                ->join('partner', 'crm.partner_id', '=', 'partner.partner_id')
                ->join('crm_akcija_tip', 'crm_akcija.crm_akcija_tip_id', '=', 'crm_akcija_tip.crm_akcija_tip_id')->orderBy('datum','asc')->get();
        }
        
        $crm_filtered=AdminCrm::getCrmFilteri($aktivni,$datum_pocetka,$datum_kraja,$search,$crm_status_id,$crm_tip_id,$sort_column,$sort_direction,$limit,!is_null($page) ? $page : 1);
        if(is_null($datum_pocetka) || $datum_pocetka=='' || $datum_pocetka=='null'){
            $datum_pocetka=null;
        }
        if(is_null($datum_kraja) || $datum_kraja=='' || $datum_kraja=='null'){
            $datum_kraja=null;
        }
        if(is_null($search) || $search=='' || $search=='null'){
            $search=null;
        }
        if(is_null($aktivni) || $aktivni=='' || $aktivni=='null'){
            $aktivniS=null;
        }
        if(is_null($crm_status_id) || $crm_status_id==''){
            $status_ids=array();
        }else{
            $status_ids=explode('-',$crm_status_id);
        }
        if(is_null($crm_tip_id) || $crm_tip_id==''){
            $tip_ids=array();
        }else{
            $tip_ids=explode('-',$crm_tip_id);
        }
         $pagination = array(
                'limit' => $limit,
                'offset' => Input::get('page') ? (Input::get('page')-1)*$limit : 0
                ); 
        $data=array(
                "strana"=>'crm_home',
                "title"=> 'CRM Sistem',    
                "crm"=>$crm_filtered->rows,
                "cerm_id"=> $crm_id,
                "limit"=>$limit,
                "crm_id"=>DB::table('crm')->where('partner_id',$partner_id)->pluck('crm_id'),
                "partner_vrsta_id" => strval($partner_vrsta_id),
                "partneri" => $partneri,
                "count" => count($crm_filtered->rows),              
                "akcije" => DB::table('crm_akcija')->where('crm_id',$crm_id)->join('crm_akcija_tip', 'crm_akcija.crm_akcija_tip_id', '=', 'crm_akcija_tip.crm_akcija_tip_id')->orderBy('datum','dsc')->get(),
                "akcije_danas"=> $akcije_danas,
                "crm_akcija" =>DB::table('crm_akcija')->where('crm_id',$crm_id)->pluck('opis'),
                "crm_akcija_tip"=> DB::table('crm_akcija_tip')->where('crm_akcija_tip_id',$crm_akcija_tip_id)->pluck('naziv'),
                "crm_tip"=> DB::table('crm_tip')->orderBy('crm_tip_id','asc')->get(),
                "crm_datum"=> DB::table('crm_akcija')->where('crm_id',$crm_id)->pluck('datum'),
                "opis_akcije"=>DB::table('crm_akcija')->where('crm_id',$crm_id)->pluck('opis'),
                "opis"=>DB::table('crm')->where('crm_id',$crm_id)->pluck('opis'),
                "datum_zavrsetka_crma"=>DB::table('crm')->where('crm_id',$crm_id)->pluck('datum_zavrsetka'),
                "datum_pocetka_crma"=>DB::table('crm')->where('crm_id',$crm_id)->pluck('datum_kreiranja'),
                "status_ids" => $status_ids,
                "tip_ids" => $tip_ids,
                "aktivni" => $aktivni,
                "datum_pocetka" => $datum_pocetka,
                "datum_kraja" => $datum_kraja,
                "search" => !is_null($search) ? $search : '',
                "vrsta_kontakta" =>  DB::table('vrsta_kontakta')->orderBy('vrsta_kontakta_id', 'asc')->get(),
                "nmb"=>0               
            );
      

        
            return View::make('crm/page', $data);
    }


    
//slog crm lead
    public function lead($crm_id=null){
        
        $partneri=AdminKupci::getPartneriSearch(null,258,null,null,20,null,null);
        $crm_status= DB::table('crm_status')->orderBy('crm_status_id','asc')->get();
        $crm_tip= DB::table('crm_tip')->orderBy('crm_tip_id','asc')->get();
        $data=array(
                "strana"=>'crm_lead',
                "title"=> 'CRM lead',
                "crm_tip"=>$crm_tip,
                "crm_status" =>$crm_status,
                "partneri"=>$partneri
                
            );
    

            return View::make('crm/page', $data);
        
    }
    public function leadSave($crm_id){
        $zavrseno= Input::get('flag_zavrseno');
        $vrednost= Input::get('vrednost');
        if($zavrseno==NULL){
                $zavrseno = '0';
            }
        if($vrednost==NULL){
            $vrednost=0;
        }
        $datum_zavrsetka=Input::get('datum_zavrsetka');
        $roba= Input::get('roba_id');
        $ponuda= Input::get('ponuda_id');
        $inputs = Input::get();
        $validator_messages = array(
            'required'=>'Polje ne sme biti prazno!',
            'max'=>'Prekoračili ste maksimalnu vrednost polja!'
                               
                                );

        $validator_rules = array(
                'opis' =>  'max:800',
                'datum_kreiranja'=> 'required'
               
            );
      
        $validator = Validator::make($inputs, $validator_rules, $validator_messages);

            if($validator->fails()){
            return Redirect::to(AdminOptions::base_url().'crm/crm_home/0')->withInput()->withErrors($validator->messages());
        }
        else
        {
            if(AdminCrm::check_administrator()){


                    if($datum_zavrsetka==NULL)
                    {
                        $datum_zavrsetka=date('Y-m-d');
                        DB::table('crm')->insert(array('partner_id'=>$inputs['partner_id'],'crm_tip_id'=>$inputs['crm_tip_id'],'crm_status_id'=>$inputs['crm_status_id'] ,'opis'=>$inputs['opis'],'flag_zavrseno'=>$zavrseno,'datum_kreiranja'=>$inputs['datum_kreiranja'],'datum_zavrsetka'=>$datum_zavrsetka,'vrednost'=>$vrednost));
                         
                    }
                    elseif($roba==NULL || $ponuda==NULL)
                    {
                        DB::table('crm')->insert(array('partner_id'=>$inputs['partner_id'],'crm_tip_id'=>$inputs['crm_tip_id'],'crm_status_id'=>$inputs['crm_status_id'] ,'opis'=>$inputs['opis'],'flag_zavrseno'=>$zavrseno,'datum_kreiranja'=>$inputs['datum_kreiranja'],'datum_zavrsetka'=>$inputs['datum_zavrsetka'],'vrednost'=>$vrednost));

                    }
                    else
                    {
                        DB::table('crm')->insert(array('partner_id'=>$inputs['partner_id'],'crm_tip_id'=>$inputs['crm_tip_id'],'crm_status_id'=>$inputs['crm_status_id'],'roba_id'=>$inputs['roba_id'], 'ponuda_id'=>$inputs['ponuda_id'],'opis'=>$inputs['opis'],'flag_zavrseno'=>$zavrseno,'datum_kreiranja'=>$inputs['datum_kreiranja'],'datum_zavrsetka'=>$inputs['datum_zavrsetka'],'vrednost'=>$vrednost));
                    }
                    

            }
            else{
                if($datum_zavrsetka==NULL)
                    {
                        $datum_zavrsetka=date('Y-m-d');
                        DB::table('crm')->insert(array('partner_id'=>$inputs['partner_id'],'crm_tip_id'=>$inputs['crm_tip_id'],'crm_status_id'=>$inputs['crm_status_id'] ,'opis'=>$inputs['opis'],'flag_zavrseno'=>$zavrseno,'datum_kreiranja'=>$inputs['datum_kreiranja'],'datum_zavrsetka'=>$datum_zavrsetka,'vrednost'=>$vrednost));
                         
                    }
                    elseif($roba==NULL || $ponuda==NULL)
                    {
                        DB::table('crm')->insert(array('partner_id'=>$inputs['partner_id'],'crm_tip_id'=>$inputs['crm_tip_id'],'crm_status_id'=>$inputs['crm_status_id'] ,'opis'=>$inputs['opis'],'flag_zavrseno'=>$zavrseno,'datum_kreiranja'=>$inputs['datum_kreiranja'],'datum_zavrsetka'=>$inputs['datum_zavrsetka'],'vrednost'=>$vrednost));

                    }
                    else
                    {
                        DB::table('crm')->insert(array('partner_id'=>$inputs['partner_id'],'crm_tip_id'=>$inputs['crm_tip_id'],'crm_status_id'=>$inputs['crm_status_id'],'roba_id'=>$inputs['roba_id'], 'ponuda_id'=>$inputs['ponuda_id'],'opis'=>$inputs['opis'],'flag_zavrseno'=>$zavrseno,'datum_kreiranja'=>$inputs['datum_kreiranja'],'datum_zavrsetka'=>$inputs['datum_zavrsetka'],'vrednost'=>$vrednost));
                    }
                    $imenik_id = Session::get('b2c_admin'.AdminOptions::server());
                    $cirimiri = DB::table('crm')->max('crm_id');
                    //$cirimiri=$cirimiri +1;
                    //All::dd($imenik_id);
                    DB::table("imenik_crm")->insert(array('imenik_id'=>$imenik_id,'crm_id'=>$cirimiri));
                    

            }
                    $akcija= 'Kreiran lead';
                    $date=date('Y-m-d H:m:s');
                    $novi=json_encode($inputs);
                    $imenik_id = Session::get('b2c_admin'.AdminOptions::server());
                DB::table('crm_log')->insert(array('datum_izmene'=>$date, 'akcija'=>$akcija,'nova_vrednost'=>$novi,'imenik_id'=>$imenik_id));
                    

        return Redirect::to(AdminOptions::base_url().'crm/crm_home/')->with('message','Uspešno ste dodali lead.');
        }
    }
    public function leadEdit($crm_id){
        $datum= date('Y-m-d');
        $inputs = Input::get();
        //All::dd($inputs);
        $vrednost= Input::get('vrednost');
        $stari=json_encode(DB::table('crm')->where('crm_id', $crm_id)->get());
        $query_result=false;
        if($vrednost==NULL){
            $vrednost=0;
        }
        $validator_messages = array(
            'required'=>'Polje ne sme biti prazno!',
            'max'=>'Prekoračili ste maksimalnu vrednost polja!'
                               
                                );

        $validator_rules = array(
                'opis' =>  'max:200'
            );
        
        $validator = Validator::make($inputs, $validator_rules, $validator_messages);

        if($validator->fails()){
            return Redirect::to(AdminOptions::base_url().'crm/crm_home/0')->withInput()->withErrors($validator->messages());
        }
        else
        {  
            DB::table('crm')->where('crm_id',$crm_id)->update(array('crm_tip_id'=>$inputs['crm_tip_id'],'crm_status_id'=>$inputs['crm_status_id'],'opis'=>$inputs['opis'],'datum_kreiranja'=>$datum,'vrednost'=>$vrednost));
            $query_result=true;

         if(AdminCrm::check_administrator()){

                if(isset($inputs['imenik_id'])){
                    $idS_imenik = $inputs['imenik_id'];
                }else{
                    $idS_imenik = array();
                }

                $menadzeri = array_map('current',DB::table('imenik_crm')
                    ->select('imenik_id')            
                    ->where('crm_id', $crm_id)
                    ->get());

                foreach($menadzeri as $man){
                    if(!in_array($man,$idS_imenik)){
                        DB::table('imenik_crm')->where(array('crm_id'=>$crm_id,'imenik_id'=>$man))->delete();
                    }
                }

                foreach($idS_imenik as $idk){
                    if(!in_array($idk,$menadzeri)){
                        DB::table('imenik_crm')->insert(array('imenik_id'=>$idk,'crm_id'=>$crm_id));
                    }
                }
            }

        else{

            if($query_result==true){
                $akcija= 'Izmena LEAD-a sa id  '.$crm_id;
                $date=date('Y-m-d H:m:s');
                $imenik_id = Session::get('b2c_admin'.AdminOptions::server());
                $novi=json_encode($inputs); 
                DB::table('crm_log')->insert(array('datum_izmene'=>$date, 'akcija'=>$akcija,'stara_vrednost'=>$stari,'nova_vrednost'=>$novi,'imenik_id'=>$imenik_id));
                }
    
        
        }
            
    return Redirect::to(AdminOptions::base_url().'crm/crm_home/')->with('message','Uspešno ste sačuvali podatke.');


            
    }
}

    function crm_delete($crm_id)
    { 
        $akcija= 'Brisanje LEAD-a sa id  '.$crm_id;
        $date=date('Y-m-d H:m:s');
        $imenik_id = Session::get('b2c_admin'.AdminOptions::server());
        $stari=json_encode(DB::table('crm')->where('crm_id',$crm_id)->get());
        DB::table('crm_log')->insert(array('datum_izmene'=>$date, 'akcija'=>$akcija,'stara_vrednost'=>$stari,'imenik_id'=>$imenik_id));
        DB::table('crm')->where('crm_id',$crm_id)->delete();
         DB::table('crm_akcija')->where('crm_id',$crm_id)->delete();
       return Redirect::to(AdminOptions::base_url().'crm/crm_home/')->with('message','Uspešno ste obrisali lead.');
    }
 
    public function zavrseno($crm_id){
         $datum_zavrsetka = date('Y-m-d');
         DB::table('crm')->where('crm_id',$crm_id)->update(array('flag_zavrseno'=>1,'datum_zavrsetka'=> $datum_zavrsetka));
            $akcija= 'Zavrsen lead sa id  '.$crm_id;
            $date=date('Y-m-d H:m:s');
            $imenik_id = Session::get('b2c_admin'.AdminOptions::server());
            $stari=json_encode(DB::table('crm')->where('crm_id',$crm_id)->get());
        DB::table('crm_log')->insert(array('datum_izmene'=>$date, 'akcija'=>$akcija,'stara_vrednost'=>$stari,'imenik_id'=>$imenik_id));
         return Redirect::back()->with('message','Uspešno ste završili lead.');
    }
    public function vrati($crm_id){
         $datum_kreiranja = date('Y-m-d');
         DB::table('crm')->where('crm_id',$crm_id)->update(array('flag_zavrseno'=>0,'datum_kreiranja'=> $datum_kreiranja));
          $akcija= 'Ponovo aktiviran lead sa id  '.$crm_id;
            $date=date('Y-m-d H:m:s');
            $imenik_id = Session::get('b2c_admin'.AdminOptions::server());
            $stari=json_encode(DB::table('crm')->where('crm_id',$crm_id)->get());
        DB::table('crm_log')->insert(array('datum_izmene'=>$date, 'akcija'=>$akcija,'stara_vrednost'=>$stari,'imenik_id'=>$imenik_id));
         return Redirect::back()->with('message','Uspešno ste aktivirali lead.');
    }


//pregled svih akcija vezanih za partnera
    public function ispis(){
        $crm_id=Input::get('CrmId');
      
        //$crm_id=DB::table('crm')->where('partner_id',$partnerid)->pluck('crm_id');
        $akcije=DB::table('crm')->select('crm.crm_id','crm.datum_kreiranja', 'crm.datum_zavrsetka','crm.flag_zavrseno','crm_akcija_tip.naziv','crm_akcija.datum','crm_akcija.datum_zavrsetka as akcija_zavrsetak','crm_akcija.opis')
        ->join('partner', 'crm.partner_id', '=', 'partner.partner_id')
        ->join('crm_akcija', 'crm.crm_id', '=', 'crm_akcija.crm_id')
        ->join('crm_akcija_tip', 'crm_akcija.crm_akcija_tip_id', '=', 'crm_akcija_tip.crm_akcija_tip_id')
        
        ->where('crm.crm_id', '=',$crm_id)
        ->orderBy('crm_akcija.datum','dsc')
        ->get();
        
        
        foreach($akcije as $id){ 
            $map[$id->crm_id] = array(); 
            $kveri= DB::table('crm')->select('crm.crm_id','crm.datum_kreiranja', 'crm.datum_zavrsetka','crm.flag_zavrseno','crm_akcija_tip.naziv','crm_akcija.datum','crm_akcija.datum_zavrsetka as akcija_zavrsetak','crm_akcija.opis')
        ->join('partner', 'crm.partner_id', '=', 'partner.partner_id')
        ->join('crm_akcija', 'crm.crm_id', '=', 'crm_akcija.crm_id')
        ->join('crm_akcija_tip', 'crm_akcija.crm_akcija_tip_id', '=', 'crm_akcija_tip.crm_akcija_tip_id')
        ->where('crm.crm_id','=',$id->crm_id)
        ->orderBy('crm_akcija.datum','dsc')
        ->get();
        array_push($map[$id->crm_id],$kveri);
            

        }

        
        $data=array(
                "strana"=>'crm_ispis',
                "title"=> 'CRM akcije',
                //"partner"=>DB::table('partner')->where('partner_id',$partnerid)->pluck('naziv'),
                "crm_id"=>$crm_id,         
                //"crm"=> DB::table('crm')->where('partner_id',$partnerid)->get(),
                "akcije"=> $akcije
                     
            );

        return View::make('crm/crm_ispis', $data);
        
    }
    
    
   
    public function partnerDetails()
    {   
        $partnerid=Input::get('myPartnerId');
        $dodatni_podaci=DB::table('partner_kontakt')->where('partner_id',$partnerid )->get();
        $all=DB::table('partner_kontakt')->join('vrsta_kontakta', function($join)
        {$join->on('partner_kontakt.vrsta_kontakta_id', '=', 'vrsta_kontakta.vrsta_kontakta_id');
        })->get();
        //All::dd($all);

        $data=array (
            "partner"=>DB::table('partner')->where('partner_id',$partnerid )->first(),
            "vrsta_kontakta"=> DB::table('vrsta_kontakta')->orderBy('vrsta_kontakta_id','asc')->get(),
            "dodatni_podaci"=>$dodatni_podaci,
            "all"=>$all,
            "partnerid"=>$partnerid
        );
        
        return View::make('crm/partials/partner_details_modal_crm', $data);

    }
    
    public function partner_kontakt_delete($partner_kontakt_id)
    {   
         DB::table('partner_kontakt')->where('partner_kontakt_id',$partner_kontakt_id)->delete();
       return Redirect::to(AdminOptions::base_url().'crm/crm_home/')->with('message','Uspešno ste obrisali kontakt.');
    }

       public function crm_partner(){

        $crm_id = Input::get('crm_id');
        $manager_id = Input::get('manager_id');
        $status = DB::table('imenik_crm')->where('imenik_id',$manager_id)->where('crm_id',$crm_id)->first();
        if( !isset($status->crm_id) AND !isset($status->imenik_id) AND $manager_id != 0) {          
            DB::table('imenik_crm')->insert(array('imenik_id'=>$manager_id,'crm_id'=>$crm_id));
        }
        if($manager_id == 0){
            DB::table('imenik_crm')->where('crm_id',$crm_id)->delete();
        }
    return Redirect::to(AdminOptions::base_url().'crm/crm_home/')->with('message','Uspešno ste dodelili komercijalistu.');    

    }


    public function crmDocuments()
    {   
        $crmID=Input::get('crmID');
        
        $data=array (
            "crm"=>DB::table('crm')->where('crm_id',$crmID )->first(),
            'crm_id'=>$crmID,
            'vrste_fajlova'=> $crmID != 0 ? AdminCrm::crmFajlovi($crmID) : null,
            // 'extension_id'=> $crmID != 0 ? $extension_id : null,
            // 'fajlovi' => $crmID != 0 ? $fajlovi : null,
            "crmID"=>$crmID
        );
        
        return View::make('crm/partials/partner_document_modal_crm', $data);

    }
    public function dokumenta_crm_save(){
        $inputs = Input::get();
        //All::dd($inputs);
        $fileUpl = Input::file('upl_file');
        $akcija= 'Dodavanje dokumenata/fajlova';
        $date=date('Y-m-d H:m:s');
        $imenik_id = Session::get('b2c_admin'.AdminOptions::server());
        $query_result=false;
        $data_arr = array(
            'vrsta_fajla_id' => $inputs['vrsta_fajla_id'],
            'crm_id' => $inputs['crm_id'],
            'putanja_http' => $inputs['putanja'],
            'naziv' => $inputs['naziv'],
            'file' => $fileUpl,
            'datum_kreiranja'=> $inputs['datum_kreiranja']
        );
        if(empty($inputs['putanja'])) {
                $size = $fileUpl->getSize();
                if($size > 1000000000000000000){
                    return Redirect::to(AdminOptions::base_url().'crm/crm_home/'.$inputs['crm_id'])->with('mess',true);
                }
                $originalName = $fileUpl->getClientOriginalName();
                $fileUpl->move('images/files/', $originalName);

                unset($data_arr['putanja_http']);
                unset($data_arr['file']);
                $data_arr['putanja'] = 'images/files/'.$originalName;

                DB::table('crm_dokumenta')->insert($data_arr);
                $new_values = DB::table('crm_dokumenta')->max('crm_dokumenta_id');
                $query_result=true;
           }
        else{
                unset($data_arr['file']);
                //All::dd($inputs);
                DB::table('crm_dokumenta')->insert($data_arr);
                $new_values = DB::table('crm_dokumenta')->max('crm_dokumenta_id');
                $query_result=true;                
            }
            if($query_result==true){
                DB::table('crm_log')->insert(array('datum_izmene'=>$date, 'akcija'=>$akcija,'nova_vrednost'=>json_encode($inputs),'imenik_id'=>$imenik_id));
            }           
            return Redirect::to(AdminOptions::base_url().'crm/crm_home/'.$inputs['crm_id']);
        

    }
      public function dokumenta_crm_delete($crm_dokumenta_id){
        $akcija= 'Brisanje dokumanata sa id  '.$crm_dokumenta_id;
        $date=date('Y-m-d H:m:s');
        $imenik_id = Session::get('b2c_admin'.AdminOptions::server());
        $stari=json_encode(DB::table('crm_dokumenta')->where('crm_dokumenta_id',$crm_dokumenta_id)->get());
            DB::table('crm_log')->insert(array('datum_izmene'=>$date, 'akcija'=>$akcija,'stara_vrednost'=>$stari,'imenik_id'=>$imenik_id));
            DB::table('crm_dokumenta')->where('crm_dokumenta_id',$crm_dokumenta_id)->delete();
         $crm_id=DB::table('crm_dokumenta')->where('crm_dokumenta_id',$crm_dokumenta_id)->pluck('crm_id');
       return Redirect::to(AdminOptions::base_url().'crm/crm_home/'.$crm_id)->with('message','Uspešno ste obrisali dokument.');

      }

       public function preracunaj($crm_id){
        $vrednost=DB::table('crm')->where('crm_id',$crm_id)->pluck('vrednost');
       
        try { 
        $nbs='https://nbs.rs/kursnaListaModul/zaDevize.faces'; 
        $content = file_get_contents($nbs);
        } catch (Exception $e) {
            return Redirect::to(AdminOptions::base_url().'crm/crm_fakture/')->with('message','Došlo je do greške, kurs nije preuzet!');
        }
        $exploded = explode(' ', $content);
        $jos = explode('AUD', $exploded[101], 2);
        $euri = htmlentities(strip_tags(substr($jos[0], -26)));
       // All::dd($euri);die;

        // $kursevi = array(
        //     'EUR' => array('kupovni' => '','kupovni' => floatval(substr($exploded[2], 0, 6)),'srednji' => (floatval(substr($exploded[1], 0, 6))+floatval(substr($exploded[2], 0, 6)))/2,'prodajni' => floatval(substr($exploded[1], 0, 6)))
        //     );
        
        // $evro = $kursevi['EUR']['prodajni'];
        $preracunato=round($vrednost*$euri);
    
        //var_dump($evro);die;
         DB::table('crm')->where('crm_id',$crm_id)->update(array('vrednost'=>$preracunato, 'kursna_lista_id'=>1));
               
       return Redirect::to(AdminOptions::base_url().'crm/crm_home/')->with('message','Uspešno ste konvertovali vrednost u evre.');

      }
       public function dinari($crm_id){
        $vrednost=DB::table('crm')->where('crm_id',$crm_id)->pluck('vrednost');
       
        try { 
        $nbs='https://nbs.rs/kursnaListaModul/zaDevize.faces'; 
        $content = file_get_contents($nbs);
        } catch (Exception $e) {
            return Redirect::to(AdminOptions::base_url().'crm/crm_fakture/')->with('message','Došlo je do greške, kurs nije preuzet!');
        }
        $exploded = explode(' ', $content);
        $jos = explode('AUD', $exploded[101], 2);
        $euri = htmlentities(strip_tags(substr($jos[0], -26)));
       // All::dd($euri);die;

        // $kursevi = array(
        //     'EUR' => array('kupovni' => '','kupovni' => floatval(substr($exploded[2], 0, 6)),'srednji' => (floatval(substr($exploded[1], 0, 6))+floatval(substr($exploded[2], 0, 6)))/2,'prodajni' => floatval(substr($exploded[1], 0, 6)))
        //     );
        
        // $evro = $kursevi['EUR']['prodajni'];
        
        $preracunato=round($vrednost/$euri);
        //var_dump($evro);die;
         DB::table('crm')->where('crm_id',$crm_id)->update(array('vrednost'=>$preracunato,'kursna_lista_id'=>null));
        
       return Redirect::to(AdminOptions::base_url().'crm/crm_home/')->with('message','Uspešno ste konvertovali vrednost u dinare.');

      }

     // AJAX artikli koji su trenutno zakomentarisani 
    public function artikliCRMsearch() {

        $rec = trim(Input::get('articles'));
        $artikli = DB::table('roba')->select('roba_id')->get();
        if(AdminOptions::vodjenje_lagera() == 1){
            $lager_join = 'INNER JOIN lager l ON r.roba_id = l.roba_id ';
            $lager_where = 'l.kolicina <> 0 AND ';
        }else{
            $lager_join = '';
            $lager_where = '';
        }

        is_numeric($rec) ? $robaIdFormatiran = "r.roba_id = '" . $rec . "' OR " : $robaIdFormatiran = "";
        $nazivFormatiran = "naziv_web ILIKE '%" . $rec . "%' ";
        $skuFormatiran = "sku ILIKE '%" . $rec . "%' ";
        $grupaFormatiran = "grupa ILIKE '%" . $rec . "%' ";
        $proizvodjacFormatiran = "p.naziv ILIKE '%" . $rec . "%' ";

        $articles = DB::select("SELECT r.roba_id, naziv_web, grupa,sku FROM roba r ".$lager_join."INNER JOIN grupa_pr g ON r.grupa_pr_id = g.grupa_pr_id INNER JOIN proizvodjac p ON r.proizvodjac_id = p.proizvodjac_id WHERE ".$lager_where."flag_aktivan=1 AND flag_prikazi_u_cenovniku=1 AND (" . $robaIdFormatiran . "" . $nazivFormatiran . " OR " . $grupaFormatiran . " OR " . $proizvodjacFormatiran . " OR " . $skuFormatiran . ") ORDER BY grupa ASC");
        header('Content-type: text/plain; charset=utf-8');          
        $list = "<ul class='articles_list'>";
        foreach ($articles as $article) {
            $list .= "
                <li class='articles_list__item'>
                    <a class='articles_list__item__link' href='" . AdminOptions::base_url() . "crm/crm_home/". $article->roba_id . "'>"
                        ."<span class='articles_list__item__link__small'>" . $article->sku . "</span>"
                        ."<span class='articles_list__item__link__small'>" . $article->roba_id . "</span>"
                        ."<span class='articles_list__item__link__text'>" . $article->naziv_web . "</span>"
                        ."<span class='articles_list__item__link__cat'>" . $article->grupa . "</span>
                    </a>
                </li>";
        }

        $list .= "</ul>";
        echo $list; 
    }

    public function analitika($datum_od_crm=null, $datum_do_crm=null, $crm_status_id=null){
        $search='sklopljen';
        $search2='uspesno';
        $search3='uspešno';
        $id_statusa= DB::table('crm_status')->where('naziv','ILIKE', '%'.$search.'%')->orWhere('naziv' ,'ILIKE', '%'.$search2.'%')->orWhere('naziv' ,'ILIKE', '%'.$search3.'%')->pluck('crm_status_id');
        $whereBetween='';
        if(isset($datum_od_crm) && isset($datum_do_crm)) {
            $whereBetween .= " AND c.datum_kreiranja BETWEEN '".$datum_od_crm."' AND '".$datum_do_crm."'";
        }
        $uspesno_zavrsen=DB::select("
            SELECT COUNT(c.crm_id) as count 
            FROM crm c           
            WHERE c.crm_status_id = $id_statusa
            ".$whereBetween."
            ");
    
        $ukupno = DB::select("
            SELECT COUNT(c.crm_id) as count 
            FROM crm c           
            WHERE c.crm_id != -1
            ".$whereBetween."
            ");
        $zavrsen_lead = DB::select("
            SELECT COUNT(c.crm_id) as count 
            FROM crm c            
            WHERE c.flag_zavrseno = 1
            ".$whereBetween."
            ");

        $ukupno_akcija = DB::select("
            SELECT COUNT(ca.crm_akcija_id) as count 
            FROM crm_akcija ca
            JOIN crm c
            ON c.crm_id = ca.crm_id           
            WHERE ca.crm_akcija_id != -1
            ".$whereBetween."
            ");

        
        $zavrsene_akcije = DB::select("
            SELECT COUNT(ca.crm_akcija_id) as count 
            FROM crm_akcija ca  
            JOIN crm c 
            ON c.crm_id = ca.crm_id       
            WHERE ca.flag_zavrseno = 1
            ".$whereBetween."
            ");
        
        $crm= DB::select("
            SELECT COUNT(ca.crm_id) as count,ca.crm_id,c.crm_tip_id 
            FROM crm_akcija ca LEFT JOIN crm c
            ON c.crm_id = ca.crm_id
            ".$whereBetween." 
            GROUP BY ca.crm_id,c.crm_tip_id 
            ORDER BY count DESC LIMIT 10");


         $akcije= DB::select("
            SELECT COUNT(ca.crm_akcija_tip_id) as count,ca.crm_akcija_tip_id
            FROM crm_akcija ca
            JOIN crm c
            ON c.crm_id = ca.crm_id
            ".$whereBetween." 
            GROUP BY ca.crm_akcija_tip_id
            ORDER BY count DESC LIMIT 10");

          $tipovi= DB::select("
            SELECT COUNT(c.crm_tip_id) as count,c.crm_tip_id
            FROM crm c LEFT JOIN crm_tip ct
            ON c.crm_tip_id = ct.crm_tip_id
            ".$whereBetween." 
            GROUP BY c.crm_tip_id
            ORDER BY count DESC LIMIT 10");

           $status= DB::select("
            SELECT COUNT(c.crm_status_id) as count,c.crm_status_id,cs.naziv
            FROM crm c LEFT JOIN crm_status cs
            ON c.crm_status_id = cs.crm_status_id
            ".$whereBetween." 
            GROUP BY c.crm_status_id,cs.naziv
            ORDER BY count DESC LIMIT 10");

          $brojKomercijalista= DB::select("
            SELECT COUNT(ic.crm_id) as count,ic.imenik_id 
            FROM imenik_crm ic
            JOIN crm c
            ON c.crm_id = ic.crm_id
            ".$whereBetween." 
            GROUP BY ic.imenik_id
            ORDER BY count DESC LIMIT 10");
    //All::dd($brojKomercijalista);

         $komercijaliste= DB::select("
            SELECT COUNT(ic.crm_id) as count,ic.imenik_id,i.ime,c.vrednost,p.naziv,c.kursna_lista_id,c.crm_tip_id,c.crm_id
            FROM imenik_crm ic LEFT JOIN crm c
            ON c.crm_id = ic.crm_id
            LEFT JOIN imenik i
            ON ic.imenik_id = i.imenik_id
            LEFT JOIN partner p
            ON p.partner_id = c.partner_id
            ".$whereBetween." 
            GROUP BY ic.crm_id,ic.imenik_id,i.ime,c.vrednost,p.naziv,c.kursna_lista_id,c.crm_tip_id,c.crm_id
            ORDER BY c.vrednost DESC LIMIT 10");

          $komercUkupanPrihod= DB::select("
            SELECT SUM(c.vrednost) as vrednost,i.ime
            FROM crm c
            JOIN imenik_crm ic
            ON c.crm_id = ic.crm_id
            JOIN imenik i
            ON i.imenik_id = ic.imenik_id
            ".$whereBetween." 
            GROUP BY ic.imenik_id,c.vrednost,i.ime
            ORDER BY c.vrednost DESC LIMIT 10");

            

        $data=array(
                "strana"=>'crm_analitika',
                "title"=>'Analitika CRM',
                "ukupno"=> $ukupno[0]->count,
                "ukupno_akcija"=> $ukupno_akcija[0]->count,
                "zavrsen_lead"=> $zavrsen_lead[0]->count,
                "zavrsene_akcije"=> $zavrsene_akcije[0]->count,
                'datum_od' => '',
                'datum_do' => '',
                'od' => isset($datum_od_crm) ? $datum_od_crm : '',
                'do' =>  isset($datum_do_crm) ? $datum_do_crm : '',
                "crm" => $crm,
                "akcije"=>$akcije,
                "uspesno_zavrsen"=>$uspesno_zavrsen[0]->count,
                "komercijaliste"=>$komercijaliste,
                "brojKomercijalista"=>$brojKomercijalista,
                "tipovi"=>$tipovi,
                "status"=>$status,
                "komercUkupanPrihod"=>$komercUkupanPrihod
               
            );
  // All::dd($data);
            return View::make('crm/page', $data);

    }
  public function crm_logovi(){
        $logovi =DB::table('imenik')->get();
        $search=Input::get('search');
        $page = Input::get('page');
        $datum_pocetka=Input::get('datum_pocetka');
        $datum_kraja= Input::get('datum_kraja');
        $crm_logovi_akcija=Input::get('crm_logovi_akcija');
        $korisnik = Input::get('imenik_id');
        $limit=15;  
        $sort_column = Input::get('sort_column') != null && Input::get('sort_column') != '' ? Input::get('sort_column') : 'cl.datum_izmene';
        $sort_direction = Input::get('sort_direction') != null && Input::get('sort_direction') != '' ? Input::get('sort_direction') : 'ASC';
        $logovi_filtered=AdminCrm::getLogoviFiltered($search,$crm_logovi_akcija,$korisnik,$sort_column,$sort_direction,$datum_pocetka,$datum_kraja,$limit,!is_null($page) ? $page : 1);
        if(is_null($datum_pocetka) || $datum_pocetka=='' || $datum_pocetka=='null'){
            $datum_pocetka=null;
        }
        if(is_null($datum_kraja) || $datum_kraja=='' || $datum_kraja=='null'){
            $datum_kraja=null;
        }
        if(is_null($search) || $search=='' || $search=='null'){
            $search=null;
        }
        if(is_null($crm_logovi_akcija) || $crm_logovi_akcija=='' || $crm_logovi_akcija=='null'){
            $crm_logovi_akcija==null;
        }
        if(is_null($korisnik) || $korisnik=='' || $korisnik=='null'){
            $korisnik==null;
        }
        
        $pagination = array(
                'limit' => $limit,
                'offset' => Input::get('page') ? (Input::get('page')-1)*$limit : 0
                );
         $data=array(
                "strana"=>'crm_logovi',
                "title"=> 'CRM logovi',            
                "search" => !is_null($search) ? $search : '',
                "datum_pocetka" => !is_null($datum_pocetka) ? $datum_pocetka : '',
                "datum_kraja" => !is_null($datum_kraja) ? $datum_kraja : '',
                "crm_logovi_akcija" => !is_null($crm_logovi_akcija) ? $crm_logovi_akcija : '',  
                "logovi"=>$logovi_filtered->rows,
                "korisnici"=>$logovi,
                "korisnik" => !is_null($korisnik) ? $korisnik : '', 
                "nmb"=>0               
            );
         //All::dd($data);
      

         return View::make('crm/page', $data);
     }

     

      


}