<?php
use Service\TranslatorService;

class CRMFaktureController extends Controller {

//fakture
 public function fakturisanje(){
    $search=Input::get('search');
    $page = Input::get('page');
    $crm_tip_id=Input::get('crm_tip_id');
    $datum_pocetka=Input::get('datum_pocetka');
    $datum_kraja=Input::get('datum_kraja');
    //All::dd($search);
    $limit= 10;
    $fakture_filtered=AdminCrm::getCrmFakture($search,$crm_tip_id,!is_null($page) ? $page : 1);

        if(is_null($search) || $search=='' || $search=='null'){
            $search=null;
        }
        if(is_null($datum_pocetka) || $datum_pocetka=='' || $datum_pocetka=='null'){
            $datum_pocetka=null;
        }
        if(is_null($datum_kraja) || $datum_kraja=='' || $datum_kraja=='null'){
            $datum_kraja=null;
        }
        if(is_null($crm_tip_id) || $crm_tip_id==''){
            $tip_ids=array();
        }else{
            $tip_ids=explode('-',$crm_tip_id);
        }

        if(Input::get('export') == 1){
             $fakture = DB::table("crm_fakture")->select("partner.naziv as partner", "crm_tip.naziv as tip", "crm_fakture.datum_fakturisanja", "crm_fakture.opis", "crm_fakture.vrednost", "crm_fakture.kursna_lista_id")
        ->join("partner", "partner.partner_id","=","crm_fakture.partner_id")
        ->join("crm_tip", "crm_tip.crm_tip_id","=","crm_fakture.crm_tip_id")
        ->get();
        //All::dd($fakture);
        $imena_kolona = Input::get('imena_kolona') ? Input::get('imena_kolona') : 'null';
        $vrsta_fajla = 'xls';

        if($imena_kolona != 'null'){
                $kolone = explode('-',$imena_kolona);
            }else{
                $kolone = array('partner','datum','vrednost');
            }
        if($vrsta_fajla == 'xls'){
                header("Content-Type: application/vnd.ms-excel");
                header("Content-disposition: attachment; filename=export_fakture.xls");
                $printPartner="";
                $printTip="";
                $printDatum="";
                $printOpis="";
                $printVrednost="";   
                $printValuta="";         
                    if(in_array('partner',$kolone)){                      
                        $printPartner="PARTNER";
                    }
                    if(in_array('tip',$kolone)){
                        $printTip="TIP";
                    }
                    if(in_array('datum',$kolone)){
                        $printDatum="DATUM";
                    }
                    if(in_array('opis',$kolone)){
                        $printOpis="OPIS";
                    }
                    if(in_array('vrednost',$kolone)){
                        $printVrednost="VREDNOST";
                    }
                    if(in_array('valuta',$kolone)){
                        $printValuta="VALUTA";
                    }
                echo $printPartner."\t".$printTip."\t".$printDatum."\t".$printOpis."\t".$printVrednost."\t".$printValuta."\t\n";
                foreach($fakture as $row){             
                    $partner="";
                    $tip="";
                    $datum="";
                    $opis="";
                    $vrednost="";
                    $valuta= "";
                     if($row->kursna_lista_id == 1){
                        $valuta= "RSD";
                     }
                     else{
                        $valuta="EUR";
                     }
                    if(in_array('partner',$kolone)){                      
                        $partner=$row->partner;
                    }
                    if (in_array('tip',$kolone)) {
                        $tip=$row->tip;
                    }
                    if (in_array('datum',$kolone)) {
                        $datum=$row->datum_fakturisanja;
                    }
                    if (in_array('opis',$kolone)) {
                        $opis=$row->opis;
                    }
                    if (in_array('vrednost',$kolone)) {
                        $vrednost=$row->vrednost;
                    }
                    if (in_array('valuta',$kolone)) {
                        $valuta=$valuta;
                    }
                    echo $partner."\t".$tip."\t".$datum."\t".$opis."\t".$vrednost."\t".$valuta."\t\n";
                }
                die;
            }else{
                return Redirect::to(AdminOptions::base_url().'/crm/crm_fakture');
            }

        }
       
        $data=array(
                "strana"=>'crm_fakture',
                "title"=> 'CRM fakture',
                "search" => !is_null($search) ? $search : '',
                "datum_pocetka" => $datum_pocetka,
                "datum_kraja" => $datum_kraja,
                "fakture"=>$fakture_filtered->rows,
                "cekirane"=>DB::table('crm_fakture')->where('flag_poslato',1)->get(),
                "sve"=>DB::table('crm_fakture')->get(),
                "tip_ids" => $tip_ids

                // "fakture"=>DB::table('crm_fakture')
                // ->join('partner', 'crm_fakture.partner_id', '=', 'partner.partner_id')
                // ->join('crm_tip', 'crm_fakture.crm_tip_id', '=', 'crm_tip.crm_tip_id')
                // ->select('partner.partner_id', 'partner.mail', 'partner.kontakt_osoba','partner.naziv as ime', 'crm_tip.crm_tip_id', 'crm_tip.naziv','crm_fakture.datum_fakturisanja', 'crm_fakture.crm_fakture_id','crm_fakture.opis','crm_fakture.kursna_lista_id', 'crm_fakture.vrednost')
                // ->orderBy('partner.naziv','asc')
                // ->get()
            );

            return View::make('crm/page', $data);
        
    }

    public function fakturisanje_save($crm_fakture_id=null)
        {
            $inputs = Input::get();
            //All::dd($inputs);

            if($inputs['email_fakture']){
                $vrsta_kontakta_id =  DB::table('vrsta_kontakta')->where('naziv','ILIKE', '%fakturisanje%')->pluck('vrsta_kontakta_id');
                DB::table('partner_kontakt')->insert(array('partner_id'=>$inputs['partner_id'],'vrsta_kontakta_id'=>$vrsta_kontakta_id,'osoba'=> '','vrednost'=>$inputs['email_fakture'], 'napomena'=>''));
                  DB::table('partner_kontakt')->where('partner_id',$inputs['partner_id'])->update(array('vrsta_kontakta_id'=>$vrsta_kontakta_id,'osoba'=> '','vrednost'=>$inputs['email_fakture'], 'napomena'=>''));
            }
            unset($inputs['email_fakture']);
           
            $akcija= 'Izmenjena faktura sa  id  '.$crm_fakture_id;
            $date=date('Y-m-d H:m:s');
            $imenik_id = Session::get('b2c_admin'.AdminOptions::server());
            $stari= json_encode(DB::table('crm_fakture')->where('crm_fakture_id', $inputs['crm_fakture_id'])->get());  
            $novi = json_encode($inputs);
            $datum_fakturisanja= date('Y-m-d H:m:s', strtotime($inputs['datum_fakturisanja']));
            $query_result=false;


            $validator_messages = array(
                'required'=>'Polje ne sme biti prazno!',
                'max'=>'Prekoračili ste maksimalnu vrednost polja!',
                'numeric'=> 'Polje mora sadrzati samo brojeve'
                                   
                                    );

            $validator_rules = array(
                    'partner_id' =>  'required',
                    'crm_tip_id'=> 'required' ,
                    'vrednost' => 'numeric'
                );
            
            $validator = Validator::make($inputs, $validator_rules, $validator_messages);

            if($validator->fails()){
                return Redirect::to(AdminOptions::base_url().'crm/crm_fakture')->withInput()->withErrors($validator->messages());
            }
            else
            {  
            DB::table('crm_fakture')->where('crm_fakture_id',$inputs['crm_fakture_id'])->update(array('partner_id'=>$inputs['partner_id'],'crm_tip_id'=>$inputs['crm_tip_id'],'datum_fakturisanja'=>$datum_fakturisanja,'opis'=>$inputs['opis'],'popust'=>$inputs['popust'],'vrednost'=>$inputs['vrednost']));
             $query_result=true;
             
            }
            if($query_result==true){
             DB::table('crm_log')->insert(array('datum_izmene'=>$date, 'akcija'=>$akcija,'stara_vrednost'=>$stari,'nova_vrednost'=>$novi,'imenik_id'=>$imenik_id));
            }
            return Redirect::to(AdminOptions::base_url().'crm/crm_fakture/')->with('message','Izmene uspesno izvrsene.');
            
        }

    public function preracunaj_faktura($crm_fakture_id){
         $vrednost=DB::table('crm_fakture')->where('crm_fakture_id',$crm_fakture_id)->pluck('vrednost');
       
        try { 
        $nbs='https://nbs.rs/kursnaListaModul/zaDevize.faces'; 
        $content = file_get_contents($nbs);
        } catch (Exception $e) {
            return Redirect::to(AdminOptions::base_url().'crm/crm_fakture/')->with('message','Došlo je do greške, kurs nije preuzet!');
        }
        $exploded = explode(' ', $content);
        $jos = explode('AUD', $exploded[101], 2);
        $euri = htmlentities(strip_tags(substr($jos[0], -26)));
       // All::dd($euri);die;

        // $kursevi = array(
        //     'EUR' => array('kupovni' => '','kupovni' => floatval(substr($exploded[2], 0, 6)),'srednji' => (floatval(substr($exploded[1], 0, 6))+floatval(substr($exploded[2], 0, 6)))/2,'prodajni' => floatval(substr($exploded[1], 0, 6)))
        //     );
        
        // $evro = $kursevi['EUR']['prodajni'];
        $preracunato=number_format(floatval($vrednost*$euri), 2, '.', '');

        //var_dump($evro);die;
         DB::table('crm_fakture')->where('crm_fakture_id',$crm_fakture_id)->update(array('vrednost'=>$preracunato, 'kursna_lista_id'=>1));
               
       return Redirect::to(AdminOptions::base_url().'crm/crm_fakture/')->with('message','Uspešno ste konvertovali vrednost u evre.');

      }
    public function dinari_faktura($crm_fakture_id){
        $vrednost=DB::table('crm_fakture')->where('crm_fakture_id',$crm_fakture_id)->pluck('vrednost');
        try { 
        $nbs='https://nbs.rs/kursnaListaModul/zaDevize.faces'; 
        $content = file_get_contents($nbs);
        } catch (Exception $e) {
            return Redirect::to(AdminOptions::base_url().'crm/crm_fakture/')->with('message','Došlo je do greške, kurs nije preuzet!');
        }
        $exploded = explode(' ', $content);
        $jos = explode('AUD', $exploded[101], 2);
        $euri = htmlentities(strip_tags(substr($jos[0], -26)));
        
        $preracunato=number_format(floatval($vrednost/$euri), 2, '.', '');
        //var_dump($evro);die;
         DB::table('crm_fakture')->where('crm_fakture_id',$crm_fakture_id)->update(array('vrednost'=>$preracunato,'kursna_lista_id'=>null));
        
       return Redirect::to(AdminOptions::base_url().'crm/crm_fakture/')->with('message','Uspešno ste konvertovali vrednost u dinare.');

      } 
      public function crm_fakturisi($partner_id=null){
        $query_result=false;
        $crm_tip=DB::table('crm')->where('partner_id',$partner_id)->pluck('crm_tip_id');
        $akcija= 'Prevacivanje CRM u fakturisanje partnera sa ID - '.$partner_id;
        $date=date('Y-m-d H:m:s');
        $imenik_id = Session::get('b2c_admin'.AdminOptions::server());
       
        DB::table('crm_fakture')->insert(['partner_id' => $partner_id,'opis'=>'na pocetku','vrednost'=>0,'kursna_lista_id'=>1,'crm_tip_id'=>$crm_tip,'datum_fakturisanja'=>date('Y-m-d')]);
        $query_result=true;
        if($query_result==true){
             DB::table('crm_log')->insert(array('datum_izmene'=>$date, 'akcija'=>$akcija,'imenik_id'=>$imenik_id));
        }        
       return Redirect::to(AdminOptions::base_url().'crm/crm_fakture/')->with('message','Uspešno ste prebacili crm u fakturisanje.');

      }
      public function fakturisanje_delete($crm_fakture_id){
        $akcija= 'Brisanje fakture sa id  '.$crm_fakture_id;
        $date=date('Y-m-d H:m:s');
        $imenik_id = Session::get('b2c_admin'.AdminOptions::server());
        $stari=json_encode(DB::table('crm_fakture')->where('crm_fakture_id',$crm_fakture_id)->get());
        DB::table('crm_log')->insert(array('datum_izmene'=>$date, 'akcija'=>$akcija,'stara_vrednost'=>$stari,'imenik_id'=>$imenik_id));
        DB::table('crm_fakture')->where('crm_fakture_id',$crm_fakture_id)->delete();      
       return Redirect::back()->with('message','Uspešno ste obrisali fakturu.');

      }
        public function crm_fakture_dokumenta(){
        $partnerID=Input::get('partnerID');
        //var_dump($partnerID);die;
       $racuni = DB::table('racun')->where('racun.partner_id',$partnerID)
       ->join("partner", "partner.partner_id","=","racun.partner_id")
       ->select('racun.datum_racuna as racun_datum', 'racun.broj_dokumenta as dokument_broj','racun.racun_id as racun_id', 'partner.partner_id as id_partnera', 'partner.kontakt_osoba as osoba')
        ->orderBy('racun.datum_racuna','dsc')->get();
        $data=array (
             "racuni"=>$racuni,
             "partner_id"=>$partnerID
        );
        
        return View::make('crm/partials/crm_fakture_dokumenta', $data);


      }
      public function faktura_poslata($crm_fakture_id){
         
         DB::table('crm_fakture')->where('crm_fakture_id',$crm_fakture_id)->update(array('flag_poslato'=>1));
            $akcija= 'Poslata faktura za mesec '.date('m');
            $date=date('Y-m-d H:m:s');
            $imenik_id = Session::get('b2c_admin'.AdminOptions::server());
            $stari=json_encode(DB::table('crm_fakture')->where('crm_fakture_id',$crm_fakture_id)->get());
        DB::table('crm_log')->insert(array('datum_izmene'=>$date, 'akcija'=>$akcija,'stara_vrednost'=>$stari,'imenik_id'=>$imenik_id));
         return Redirect::back()->with('message','Faktura oznacena kao poslata.');

      }
      public function faktura_nije_poslata($crm_fakture_id){
         
         DB::table('crm_fakture')->where('crm_fakture_id',$crm_fakture_id)->update(array('flag_poslato'=>0));
            $akcija= 'Poslata faktura za mesec '.date('m');
            $date=date('Y-m-d H:m:s');
            $imenik_id = Session::get('b2c_admin'.AdminOptions::server());
            $stari=json_encode(DB::table('crm_fakture')->where('crm_fakture_id',$crm_fakture_id)->get());
        DB::table('crm_log')->insert(array('datum_izmene'=>$date, 'akcija'=>$akcija,'stara_vrednost'=>$stari,'imenik_id'=>$imenik_id));
         return Redirect::back()->with('message','Faktura oznacena kao poslata.');

      }

      public function faktura_cekiraj(){
        $cekirani= DB::table('crm_fakture')->where('flag_poslato',1)->get();
        $svi=DB::table('crm_fakture')->get();
        if(count($cekirani)==count($svi)){
            DB::table('crm_fakture')->where('crm_fakture_id', '>','0')->update(array('flag_poslato'=>0));
             return Redirect::back()->with('message','Sve fakture vracene u neposlate');
        }
        else{
            DB::table('crm_fakture')->where('crm_fakture_id', '>','0')->update(array('flag_poslato'=>1));
             return Redirect::back()->with('message','Sve fakture poslate');
        }

      }

}