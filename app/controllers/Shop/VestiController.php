<?php

class VestiController extends Controller {


	public function getNews(){
        $offset = Language::segment_offset();
        $lang = Language::multi() ? Request::segment(1) : null;
        $url = Request::segment(5+$offset);
        $karakteristike = Request::get('ch');

		$strana = 'blog';
		$check_page = DB::table('web_b2c_seo')->where('naziv_stranice',$strana)->count();
		if($check_page==0){
			return Redirect::to(Options::base_url());
		}
		$seo = Seo::page($strana);
        $web_b2c_seo_id = Seo::get_page_id($strana);
        
        $stranica=DB::table('web_b2c_seo')->where(array('web_b2c_seo_id'=>$web_b2c_seo_id))->first();
        $data['slajder_id'] = !is_null($stranica->slajder_id) ? $stranica->slajder_id : 0;
        if($karakteristike=='novo')
            $news = DB::table('web_vest_b2c')->join('web_vest_b2c_jezik','web_vest_b2c_jezik.web_vest_b2c_id','=','web_vest_b2c.web_vest_b2c_id')->select('*','web_vest_b2c_jezik.sadrzaj as tekst')->where(array('novo'=> 1,'aktuelno'=>1,'jezik_id'=>Language::lang_id()))->orderBy('rbr','desc')->paginate(30);
        elseif($karakteristike=='tehnika'){
            $news = DB::table('web_vest_b2c')->join('web_vest_b2c_jezik','web_vest_b2c_jezik.web_vest_b2c_id','=','web_vest_b2c.web_vest_b2c_id')->select('*','web_vest_b2c_jezik.sadrzaj as tekst')->where(array('tehnika'=>1,'aktuelno'=>1,'jezik_id'=>Language::lang_id()))->orderBy('rbr','desc')->paginate(30);
        }
        else{
            $news = DB::table('web_vest_b2c')->join('web_vest_b2c_jezik','web_vest_b2c_jezik.web_vest_b2c_id','=','web_vest_b2c.web_vest_b2c_id')->select('*','web_vest_b2c_jezik.sadrzaj as tekst')->where(array('aktuelno'=>1,'jezik_id'=>Language::lang_id()))->orderBy('rbr','desc')->paginate(30);
        }
		$data=array(
                "strana"=> $strana,
                "org_strana"=>$strana,
                "url"=> $url,

	            "title"=>$seo->title,
	            "description"=>$seo->description,
                "news"=> $news,
	            "keywords"=>$seo->keywords           
	            );
		return View::make('shop/themes/'.Support::theme_path().'pages/news', $data);
	} 

	public function oneNew(){
        $offset = Language::segment_offset();
        $lang = Language::multi() ? Request::segment(1) : null;
        $blog_slug = Request::segment(2+$offset);
        $url = Request::segment(5+$offset);

        $jezik_id = DB::table('jezik')->where(array('aktivan'=>1, 'kod'=>Language::lang()))->pluck('jezik_id');
        $lang_blogs = DB::table('web_vest_b2c_jezik')->where('jezik_id',$jezik_id)->get();
        $id = null;

        foreach($lang_blogs as $lang_blog){
            if($blog_slug == Url_mod::slugify($lang_blog->naslov)){
                $id = $lang_blog->web_vest_b2c_id;
            }
        }

        if(is_null($id)){
            return Redirect::to(Options::base_url());
        }

        $vest = DB::table('web_vest_b2c')->where('web_vest_b2c_id',$id)->first();
        $previous_vest = DB::table('web_vest_b2c')->where('web_vest_b2c_id',$id-1)->first();
        $next_vest = DB::table('web_vest_b2c')->where('web_vest_b2c_id',$id+1)->first();

        if(is_null($vest)){
        	return Redirect::to(Options::base_url());
        }
		$vesti_jezik=DB::table('web_vest_b2c_jezik')->where(array('web_vest_b2c_id'=>$id, 'jezik_id'=>Language::lang_id()))->first();
        $seo = Seo::vest($id);


        $previous = DB::table('web_vest_b2c')->where('rbr','<',$vest->rbr)->orderBy('rbr','desc')->first();
        $next = DB::table('web_vest_b2c')->where('rbr','>',$vest->rbr)->orderBy('rbr','asc')->first();
        if(!is_null($previous)){
			$previous_vest = DB::table('web_vest_b2c')->join('web_vest_b2c_jezik','web_vest_b2c_jezik.web_vest_b2c_id','=','web_vest_b2c.web_vest_b2c_id')->select('web_vest_b2c.web_vest_b2c_id as web_vest_b2c_id','web_vest_b2c_jezik.naslov as naslov')->where(array('web_vest_b2c.web_vest_b2c_id'=>$previous->web_vest_b2c_id,'aktuelno'=>1,'jezik_id'=>Language::lang_id()))->first();
		}
		if(!is_null($next)){
			$next_vest = DB::table('web_vest_b2c')->join('web_vest_b2c_jezik','web_vest_b2c_jezik.web_vest_b2c_id','=','web_vest_b2c.web_vest_b2c_id')->select('web_vest_b2c.web_vest_b2c_id as web_vest_b2c_id','web_vest_b2c_jezik.naslov as naslov')->where(array('web_vest_b2c.web_vest_b2c_id'=>$next->web_vest_b2c_id,'aktuelno'=>1,'jezik_id'=>Language::lang_id()))->first();
		}
        $strana = 'blog';
        $seo = Seo::page($strana);
        $web_b2c_seo_id = Seo::get_page_id($strana);
        $stranica=DB::table('web_b2c_seo')->where(array('web_b2c_seo_id'=>$web_b2c_seo_id))->first();
        $data['slajder_id'] = !is_null($stranica->slajder_id) ? $stranica->slajder_id : 0;

		$data=array(
                "strana"=> "vest",
                "url"=> $url,
                "org_strana"=> "vest",
                "title"=>$seo->title,
                "web_vest_b2c_id"=>$id,
                "description"=>$seo->description,
                "keywords"=>$seo->keywords,
                "web_vest_b2c_komentar" =>DB::table('web_vest_b2c_komentar')->join('web_vest_b2c', 'web_vest_b2c_komentar.web_vest_b2c_id', '=', 'web_vest_b2c.web_vest_b2c_id')->where('web_vest_b2c_komentar.komentar_odobren', 1)->where('web_vest_b2c_komentar.web_vest_b2c_id', $id)->get(),
                "datum_komentara"=>DB::table('web_vest_b2c_komentar')->join('web_vest_b2c', 'web_vest_b2c_komentar.web_vest_b2c_id', '=', 'web_vest_b2c.web_vest_b2c_id')->where('web_vest_b2c_komentar.komentar_odobren', 1)->where('web_vest_b2c_komentar.web_vest_b2c_id', $id)->first(),
                "og_image"=> isset($vest->slika) ? $vest->slika : null,
                "id" => $id,
                'slika' => $vest && $vest->slika ? $vest->slika : '',
                'datum' => $vest && $vest->datum ? $vest->datum : '',
                'naslov' => $vesti_jezik && $vesti_jezik->naslov ? $vesti_jezik->naslov : '',
                'sadrzaj' => $vesti_jezik && $vesti_jezik->sadrzaj ? $vesti_jezik->sadrzaj : '',
                'previous_vest' => !is_null($previous) ? $previous_vest : null,
                'next_vest' => !is_null($next) ? $next_vest : null,
                'slajder_id' => !is_null($stranica->slajder_id) ? $stranica->slajder_id : 0
	            );
		return View::make('shop/themes/'.Support::theme_path().'pages/new', $data);
	}	
    public function addComment(){   
        $data=Input::get();
        $validator_arr = array(
                'ime_osobe' => 'required|regex:'.Support::regex().'|max:200',
                'komentar' => 'required|regex:'.Support::regex().'|max:1000'
                );
        $translate_mess = Language::validator_messages();
        $vesti_jezik=DB::table('web_vest_b2c_jezik')->where(array('web_vest_b2c_id'=>$data['web_vest_b2c_id'], 'jezik_id'=>Language::lang_id()))->first();
        $naslov= DB::table('web_vest_b2c')->join('web_vest_b2c_jezik','web_vest_b2c_jezik.web_vest_b2c_id','=','web_vest_b2c.web_vest_b2c_id')->select('web_vest_b2c_jezik.naslov as naslov')->where(array('web_vest_b2c.web_vest_b2c_id'=>$data['web_vest_b2c_id']))->first();

        $validator = Validator::make($data,$validator_arr,$translate_mess);
        if($validator->fails() ){
                return Redirect::to(URL::previous().'blog')->withInput()->withErrors($validator->messages())->with('contactError',true);
        }
        else {          
                $ip=All::ip_adress();
                $data=array(
                'web_vest_b2c_id'=>$data['web_vest_b2c_id'],
                'ip_adresa'=>$ip,
                'ime_osobe'=>$data['ime_osobe'],
                'email'=> $data['email'],
                'web_site'=>$data['web_site'],
                'komentar_odobren'=>0,
                'komentar'=>$data['komentar'],
                'web_kupac_id'=> $data['web_kupac_id'],
                'ocena'=> $data['ocena'],
                'datum'=>date("Y-m-d")   
            );
            
                
                DB::table('web_vest_b2c_komentar')->insert($data);
                return Redirect::to(Options::base_url().Url_mod::slug_trans('blog').'/'.$vesti_jezik->naslov);
                
            }
        }

                

   }