<?php
use Service\MSUApi;

class MSUBankingController extends BaseController{
    public function msu(){
        $offset = Language::segment_offset();
        $lang = Language::multi() ? Request::segment(1) : null;
        $web_b2c_narudzbina_id = Request::segment(2+$offset);
        $session = Request::segment(3+$offset);

        $web_b2c_narudzbina = DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->first();
        if(is_null($web_b2c_narudzbina)){
            return Redirect::to(Options::base_url());
        }
        $web_kupac = DB::table('web_kupac')->where('web_kupac_id',$web_b2c_narudzbina->web_kupac_id)->first();
        if(is_null($web_kupac)){
            return Redirect::to(Options::base_url());
        }
        $preduzece = DB::table('preduzece')->where('preduzece_id',1)->first();
        if(is_null($preduzece)){
            return Redirect::to(Options::base_url());
        }

        $data = array(
            'title' => 'Msu',
            'description' => 'Msu',
            'keywords' => 'msu',
            'strana' => 'msu',
            "org_strana"=>'msu',
            'link' => 'https://entegrasyon.asseco-see.com.tr/chipcard/pay3d/'.$session,
            'web_kupac' => $web_kupac,
            'preduzece' => $preduzece
            );

        return View::make('shop/themes/'.Support::theme_path().'pages/msu',$data);
    }
    public function msu_response(){
        $request = Request::all();

        $web_b2c_narudzbina_id=$request['merchantPaymentId'];
        $web_b2c_narudzbina = DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->first();
        if(is_null($web_b2c_narudzbina)){
            return Redirect::to(Options::base_url().Url_mod::slug_trans('korpa'))->with('failure-message',Language::trans('Došlo je do greške. Račun platne kartice nije zadužen').'.');
        }

        $bank_response = array(
            'realized' => $request['responseCode'] == "00" ? 1 : 0,
            'payment_id' => isset($request['pgOrderId']) ? $request['pgOrderId'] : '',
            'oid' => isset($request['apiMerchantId']) ? $request['apiMerchantId'] : '',
            'trans_id' => isset($request['pgTranId']) ? $request['pgTranId'] : '',
            'track_id' => isset($request['pgTranRefId']) ? $request['pgTranRefId'] : '',
            'post_date' => isset($request['pgTranDate']) ? $request['pgTranDate'] : '',
            'result_code' => isset($request['responseCode']) ? $request['responseCode'] : '',
            'auth_code' => isset($request['sessionToken']) ? $request['sessionToken'] : '',
            'response' => isset($request['responseMsg']) ? $request['responseMsg'] : '',
            'md_status' => isset($request['installment']) ? $request['installment'] : ''
            );

        $data = array('result_code'=> json_encode($bank_response));
        if($request['responseCode'] == "00" && isset($request['responseMsg']) && $request['responseMsg'] == "Approved"){
            $data['stornirano'] = 0;
            DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->update($data);
            return Redirect::to(Options::base_url().Url_mod::slug_trans('narudzbina').'/'.$web_b2c_narudzbina_id);
        }

        DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->update($data);
        return Redirect::to(Options::base_url().Url_mod::slug_trans('banka-greska').'/'.$web_b2c_narudzbina_id);
    }

    public function banka_greska(){
        $offset = Language::segment_offset();
        $lang = Language::multi() ? Request::segment(1) : null;
        $web_b2c_narudzbina_id = Request::segment(2+$offset);

        $web_b2c_narudzbina = DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->first();
        if(is_null($web_b2c_narudzbina) && $web_b2c_narudzbina->web_nacin_placanja_id!=3 && $web_b2c_narudzbina->stornirano!=0){
            return Redirect::to(Options::base_url());
        }
        $kupac = DB::table('web_kupac')->where('web_kupac_id',$web_b2c_narudzbina->web_kupac_id)->first();
        if(is_null($kupac)){
            return Redirect::to(Options::base_url());
        }
        if(!Session::has('b2c_korpa')){
            return Redirect::to(Options::base_url());
        }
        Session::forget('b2c_korpa');


        $subject=Language::trans('Banka greška')." ".Order::broj_dokumenta($web_b2c_narudzbina_id);
        
        if($kupac->flag_vrsta_kupca == 0){
            $name = $kupac->ime.' '.$kupac->prezime;
        }else{
            $name = $kupac->naziv.' '.$kupac->pib;
        }
        $data=array(
            "strana"=>'order',
            "org_strana"=>'order',
            "title"=> $subject,
            "description"=>"",
            "keywords"=>"",
            "web_b2c_narudzbina_id"=>$web_b2c_narudzbina_id,
            "bank_result"=> json_decode($web_b2c_narudzbina->result_code),
            "kupac"=>$kupac
            );
        $body = View::make('shop/themes/'.Support::theme_path().'partials/order_details',$data)->render();
        WebKupac::send_email_to_client($body,$kupac->email,$subject);

        return View::make('shop/themes/'.Support::theme_path().'pages/order',$data); 
    }

}