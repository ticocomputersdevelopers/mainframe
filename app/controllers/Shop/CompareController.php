<?php 

class CompareController extends Controller {
	public function compare($grupa_pr_id){
		$lang = Language::multi() ? Request::segment(1) : null;
        $strana = Url_mod::slug_convert_page('uporedi');

		if(empty($strana) || !is_numeric($grupa_pr_id) || is_null($grupa = DB::table('grupa_pr')->where('grupa_pr_id',$grupa_pr_id)->first())){
	        $data=array(
	            "strana"=>'Not found',
	            "org_strana"=>'Not found',
	            "title"=>'Not found',
	            "description"=>'Not found',
	            "keywords"=>'Not found'
	        );
	        $content = View::make('shop/themes/'.Support::theme_path().'pages/not_found', $data)->render();
	        return Response::make($content, 404);
		}



        $seo = Seo::page($strana);
        $data=array(
            "strana"=>$strana,
            "org_strana" => 'uporedi',
            "naziv"=>str_replace(' - '.Options::company_name(),'',$seo->title),
            "title"=>$seo->title,
            "description"=>$seo->description,
            "keywords"=>$seo->keywords,
            'grupa' => $grupa,
            "ids" => Session::has('compare_ids') ? array_map('current',DB::table('roba')->select('roba_id')->whereIn('roba_id',Session::get('compare_ids'))->where(['flag_aktivan'=>1, 'flag_prikazi_u_cenovniku'=>1, 'grupa_pr_id'=>$grupa->grupa_pr_id])->get()) : []
        );

		return View::make('shop/themes/'.Support::theme_path().'pages/group_compare',$data);
	}

	public function clearCompare($grupa_pr_id=null){
		$lang = Language::multi() ? Request::segment(1) : null;

		if(!is_null($grupa_pr_id) && is_numeric($grupa_pr_id) && !is_null($grupa = DB::table('grupa_pr')->where('grupa_pr_id',$grupa_pr_id)->first()) && Session::has('compare_ids')){
			$ids = array_map('current',DB::table('roba')->select('roba_id')->whereIn('roba_id',Session::get('compare_ids'))->where(['flag_aktivan'=>1, 'flag_prikazi_u_cenovniku'=>1])->where('grupa_pr_id','!=',$grupa->grupa_pr_id)->get());
			Session::put('compare_ids',$ids);
			return Redirect::to(Options::base_url().Url_mod::slug_trans('uporedi'));
		}else{
			Session::forget('compare_ids');
			return Redirect::back();
		}
	}

	public function compareajax(){
		$lang = Language::multi() ? Request::segment(1) : null;

		$compareIds = [];
	  	if(Session::has('compare_ids')){
	  		Session::regenerate();
		  	$compareIds = Session::pull('compare_ids');
		}

		if(Input::get('id')){
			$id = strval(Input::get('id'));
		  	if(!in_array($id,$compareIds)){
		  		$compareIds[] = $id;
		    }
		}
		else if(Input::get('clear_id')){
	        foreach($compareIds as $key => $id){
	            if(strval(Input::get('clear_id')) == $id){
	                unset($compareIds[$key]);
	            }
	        }		
		}
        Session::put('compare_ids',$compareIds);


		$html = View::make('shop/themes/'.Support::theme_path().'partials/mini_compare_list')->render();

		return Response::json(array('compare_ids' => $compareIds, 'count_compare'=>count($compareIds), 'html' => $html));
	  }
	public function clear_compare(){
		$lang = Language::multi() ? Request::segment(1) : null;

		if(Session::has('compare_ids')){
			$ids = Session::get('compare_ids');
	        foreach($ids as $key => $id){
	            if(Input::get('clear_id') == $id){
	                unset($ids[$key]);
	            }
	        }
	     	Session::forget('compare_ids');
	     	foreach($ids as $id){
	     		Session::push('compare_ids', $id);
	     	}	        
		 }
		 return Response::json(array('count_compare'=>count(Session::get('compare_ids'))));
	} 

      public function compare_old(){
      	$lang = Language::multi() ? Request::segment(1) : null;

      	if(Session::has('compare_ids')){
	      	$ids = Session::get('compare_ids');
	     	if(Input::get('clear_id')!=null){
		        foreach($ids as $key => $id){
		            if(Input::get('clear_id') == $id){
		                unset($ids[$key]);
		            }
		        }
	     	}
	     	Session::forget('compare_ids');
	     	foreach($ids as $id){
	     		Session::push('compare_ids', $id);
	     	}
	     	$ids = Session::get('compare_ids');

	     	if(is_array($ids) && count($ids)>0){

		        $table = '<table><td></td>';
		        foreach($ids as $id){
		            $table .= '<td><span class="JSclearCompare" data-id="'.$id.'">'.Language::trans('Obriši').'</span></td>';
		        }
		        $table .= '<tr><td class="compare-table-info">'.Language::trans('naziv').'</td>';
		        foreach($ids as $id){
		            $table .= '<td>'.Language::trans(All::getCompare($id)[0]->naziv_web).'</td>';
		        }
		        $table .= '<tr><td class="compare-table-info">'.Language::trans('slika').'</td>';
		        foreach($ids as $id){
		            $table .= '<td><img src="'.Options::base_url().Product::web_slika($id).'"></td>';
		        }
		        $table .= '</tr><tr><td class="compare-table-info">'.Language::trans('cena').'</td>';
		        foreach($ids as $id){
		        	$akcijska_cena = Cart::cena(All::getCompare($id)[0]->akcijska_cena);
		        	$web_cena = Cart::cena(Options::checkCena()=='web_cena'?All::getCompare($id)[0]->web_cena:All::getCompare($id)[0]->mpcena);
		        	if(All::getCompare($id)[0]->akcija_flag_primeni==1 && $akcijska_cena<$web_cena && $akcijska_cena!=0){
		        		$table .= '<td>'.$akcijska_cena.'</td>';
		        	}else{
		            	$table .= '<td>'.$web_cena.'</td>';
		        	}
		        }       
		        $table .= '</tr><tr><td class="compare-table-info">'.Language::trans('proizvođač').'</td>';
		        foreach($ids as $id){
		            $table .= '<td>'.All::getCompare($id)[0]->proizvodjac.'</td>';
		        }
		        $table .= '</tr>';

	            foreach(All::getCompare($ids) as $karak){
	                $table .= '<tr><td>'.Language::trans($karak->naziv).'</td>';
	                
	                foreach($ids as $id){
	                    if(All::getVrednost($id,$karak->naziv)){
	                        $table .= '<td>'.All::getVrednost($id,$karak->naziv).'</td>';
	                    }else{
	                        $table .= '<td></td>';
	                    }
	                }
	                $table .= '</tr>';
	            }
		        $table .= '</table>';
		        $content = $table;
	     	}else{
	     		$content = Language::trans('Najmanje dva artikla mogu biti upoređena').'!';
	     	}
     	}else{
     		$content = Language::trans('Najmanje dva artikla mogu biti upoređena').'!';
     	}
     	return Response::json(array('content'=>$content, 'count_compare'=>count(Session::get('compare_ids'))));
     } 


} 