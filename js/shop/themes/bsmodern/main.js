
$(document).ready(function () {
// LAZY LOAD IMAGES
	$(function() {
		// change data-src to src to disable plugin
		// 1.product_on_grid
		// 2.product_on_list
		// 3.footer
        $('.JSlazy_load').lazy({
        	scrollDirection: 'vertical',
			effect: 'fadeIn',
			visibleOnly: true,
			delay: 1000,
			combined: true,
        });
    });

	  // IF THERE IS NO IMAGE
	$('img').on('error', function(){ 
		$(this).attr('src', '/images/no-image.jpg');
	});

 // if($('body').is('#start-page') || $('body').is('#artical-page')){		// START PAGE ==============

   $('.shop-product-card').on('mouseenter', function() {
	   $(this).removeClass('no-shadow');
   })
   
   $('.shop-product-card').on('mouseleave', function() {
	$(this).addClass('no-shadow');
})
 
	// SLICK SLIDER INCLUDE
	if($('.JSmain-slider')[0]) {  
		if($('#admin_id').val()){
			$('.JSmain-slider').slick({
				autoplay: false,
				draggable: false
			});
		}else{
			$('.JSmain-slider').slick({
				autoplay: true
			});
		} 
	}

	 if ($('.JScategory-slider')[0]) { 
		$('.JScategory-slider').slick({ 
			autoplay: true, 
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: true,
			draggable: true
		}); 
	 }
 
// PRODUCTS SLICK SLIDER 
	 if ($('.JSproducts_slick')[0]) { 
		$('.JSproducts_slick').slick({ 
			autoplay: true, 
			slidesToShow: 4,
			slidesToScroll: 1, 
			responsive: [
				{
				  breakpoint: 1160,
				  settings: {
					slidesToShow: 3,
					slidesToScroll: 1
				  }
				},
				{
				  breakpoint: 880,
				  settings: {
					slidesToShow: 2,
					slidesToScroll: 1
				  }
				},
				{
				  breakpoint: 480,
				  settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				  }
				}
			]
		}); 
	 }
  
  
	// if ($('.JSBrandSlider')[0]){
	// 	$('.JSBrandSlider').slick({
	// 		autoplay: true,
	// 	    infinite: true,
	// 	    speed: 600,
	// 	    arrows: true,
	// 		slidesToShow: 5,
	// 		slidesToScroll: 3, 
	// 		responsive: [
	// 			{
	// 			  breakpoint: 1100,
	// 			  settings: {
	// 				slidesToShow: 3,
	// 				slidesToScroll: 3,
	// 				infinite: true,
	// 				dots: false
	// 			  }
	// 			},
	// 			{
	// 			  breakpoint: 800,
	// 			  settings: {
	// 				slidesToShow: 2,
	// 				slidesToScroll: 2
	// 			  }
	// 			},
	// 			{
	// 			  breakpoint: 480,
	// 			  settings: {
	// 				slidesToShow: 2,
	// 				slidesToScroll: 2
	// 			  }
	// 			}
	// 		]
	// 	});
	// }
	
	if ($('.JSblog-slick').length){
		$('.JSblog-slick').slick({
			autoplay: true,
		    infinite: true,
		    speed: 600, 
			slidesToShow: 3,
			slidesToScroll: 1,
 			responsive: [
				{
				  breakpoint: 1100,
				  settings: {
					slidesToShow: 2,
					slidesToScroll: 1,
					infinite: true,
					dots: false
				  }
				},
				{
				  breakpoint: 991,
				  settings: { 
				  	slidesToShow: 1,
					slidesToScroll: 1,
					infinite: true
				  }
				}
			]
		});
	}

	if ($('.JSgallery_slick')[0]) { 
		$('.JSgallery_slick').slick({ 
			autoplay: true, 
			slidesToShow: 4,
			slidesToScroll: 1, 
			arrows: true,
			draggable: true,
			responsive: [
				{
				  breakpoint: 991,
				  settings: {
					slidesToShow: 2,
					slidesToScroll: 1,
					arrows: true,
					draggable: true
				  }
				// },
				// {
				//   breakpoint: 480,
				//   settings: {
				//   	arrows: false,
				// 	slidesToShow: 3,
				// 	slidesToScroll: 1,
				// 	arrows: true,
				// 	draggable: true
				//   }
				}
			]
			
		}); 
	 }

	 if(! $('body').is('#start-page')) {
	 	$('JStip-slider').addClass('JSproducts-slick');
	 }

// } 	// START PAGE END ==============

	// BR TAG AFTER THREE STAR REVIEW
		if ($(window).width() < 520 ) {
			$("#JSstar3").after("<br>");
		}
 
	// CATEGORIES - MOBILE
	// if ($(window).width() < 991 ) {
		$('.categories-title').click(function () { 
	    	$('.JSlevel-1').slideToggle();
		});
	// }
	if($(window).width() > 991) {
		$('.JSlevel-1 > li').each(function() {
			$(this).on("mouseenter", function () {
		    	$(this).find('.JSlevel-2').stop().animate({left: "100%" , opacity:"show"}, 400);
			});
			$(this).on("mouseleave", function () {
		    	$(this).find('.JSlevel-2').stop().animate({left: "110%" , opacity:"hide"}, 400);
			});
		});
	}

	$('.JSlevel-1 li').each(function(i, li){
		if($(li).children('ul').length > 0){
			$(li).append('<span class="JSsubcat-toggle"><i class="fas fa-chevron-down"></i></span>');
		}
	}); 

	$('.JSsubcat-toggle').on('click', function() {
	 	$(this).toggleClass('rotate');
	    $(this).siblings('ul').slideToggle();
		$(this).parent().siblings().children('ul').slideUp();
		$(this).parent().siblings().find('.JSsubcat-toggle').removeClass('rotate');
	}); 
 
 
 // SOCIAL ICONS
	$('.social-icons .facebook').append('<i class="fab fa-facebook-f"></i>');
	$('.social-icons .twitter').append('<i class="fab fa-twitter"></i>');
	$('.social-icons .google-plus').append('<i class="fab fa-google-plus-g"></i>');
	$('.social-icons .skype').append('<i class="fab fa-skype"></i>');
	$('.social-icons .instagram').append('<i class="fab fa-instagram"></i>');
 	$('.social-icons .linkedin').append('<i class="fab fa-linkedin"></i>');
	$('.social-icons .youtube').append('<i class="fab fa-youtube"></i>'); 

// PRODUCT PREVIEW IMAGE
    if ($(".JSproduct-preview-image")[0]){
		jQuery(".JSzoom_03").elevateZoom({
			gallery:'gallery_01', 
			cursor: 'pointer',  
			imageCrossfade: true,  
			zoomType: 'lens',
			lensShape: 'round', 
			lensSize: 200, 
			borderSize: 1, 
			containLensZoom: true
		}); 
		jQuery(".JSzoom_03").bind("click", function(e) { var ez = $('.JSzoom_03').data('elevateZoom');	$.fancybox(ez.getGalleryList()); return false; });
    } 

// GALLERY SLIDER
    (function(){
 		var img_gallery_href = $('.JSimg-gallery'),
 			img_gallery_img = img_gallery_href.children('img'),
 			main_img = $('.JSmain_img'),
 			modal_img = $('#JSmodal_img'),
 			modal = $('.JSmodal'),

 			img_arr = [], 
			next_prev = 1;


		img_gallery_href.eq(0).addClass('galery_Active');

		img_gallery_href.on('click', function(){
			var img = $(this).children('img');
			main_img.attr({ src: img.attr('src'), alt: img.attr('alt') });
			$(this).addClass('galery_Active').siblings().removeClass('galery_Active');
		});

		main_img.on('click', function(){
			var src = $(this).attr('src');
			if (src.toLowerCase().indexOf('no-image') <= 0){
				modal_img.attr('src', src);
				$('.JSmodal').show();
			} 
		});

		$('.JSclose-modal').on('click', function(){
			modal.hide();
		});

		$(document).on('click', function(){
			if (modal.css('display') == 'block')  {
				$('body').css('overflow', 'hidden');
			} else {
				$('body').css('overflow', '');  
			} 
		});

		modal.on('click', function(e){
			if ( ! $('.JSmodal img, .JSmodal .btn, .JSmodal .btn > *').is(e.target)) {
				modal.hide();
			} 
		}); 

		img_gallery_img.each(function(i, img){
			img_arr.push($(img).attr('src'));  
		});

		$('.JSleft_btn').on('click', function(){ 
			modal_img.attr('src', img_arr[next_prev]); 
			 
			if (next_prev == img_gallery_img.length - 1) {
				next_prev = 0; 
			} else {
				next_prev++;
			}	  
		}); 

		$('.JSright_btn').on('click', function(){ 
			modal_img.attr('src', img_arr[next_prev]);
	 		next_prev--;

	 		if (next_prev < 0 ) {
	 			next_prev = img_gallery_img.length - 1;
	 		}   
		});  

		if (img_gallery_href.length > 1) {
			$('.modal-cont .btn').show();
		}
 
	}());

// ======== SEARCH ============
 
	var searchTime;
	$('#JSsearch2').on("keyup", function(event) {
		if(event.keyCode == 13 && $('#JSsearch2').val() != ''){
        	$(".JSsearch-button2").trigger("click");
    	}
    	if($('#elasticsearch').val() == 1){
			timer2();
    	}else{	
			clearTimeout(searchTime);
			searchTime = setTimeout(timer2, 300);
    	}
	});
 
	$('.JSSearchGroup2').change(function(){	timer2(); });

	function timer2(){
		var search_length = 2;
		if($('#elasticsearch').val() == 1){
			search_length = 0;
		}
		var search = $('#JSsearch2').val();
		if (search != '' && search.length > search_length) {
			$.post(base_url + 'livesearch', {search:search, grupa_pr_id: $('.JSSearchGroup2').val()}, function (response){
				if(response != ''){
					$('.JSsearch_list').remove();
					$('.JSsearchContent2').append(response);
				}
			});
		} 
	}

	$('html :not(.JSsearch_list)').on("click", function() {
		$('.JSsearch_list').remove();
	});
		 
// RATING STARS
	$('.JSrev-star i').each(function(i, el){
		var val = 0;
		$(el).on('click', function(){    
			$(el).addClass('fas').removeClass('far'); 
		    $(el).prevAll('i').addClass('fas').removeClass('far'); 
			$(el).nextAll('i').removeClass('fas').addClass('far'); 
		 	if (i == 0) { val = 1; }
		 	else if(i == 1){ val = 2; }
		 	else if(i == 2){ val = 3; }
		 	else if(i == 3){ val = 4; }
		 	else if(i == 4){ val = 5; } 
		 	$('#JSreview-number').val(val); 
		}); 
	}); 
 
  
	$('.JStoggle-btn').on('click', function(){
		$(this).closest('div').find('.JStoggle-content').toggleClass('hidden-small'); 
	});
	 
 // RESPONSIVE NAVIGATON
 	$(".resp-nav-btn").on("click", function(){  
		$("body").toggleClass("openMe");		 
	});
	$(".JSclose-nav").on("click", function(){  
		$("body").removeClass("openMe");		 
	});
 
// SCROLL TO TOP 
 	$(window).scroll(function () {
        if ($(this).scrollTop() > 150) {
            $('.JSscroll-top').css('right', '20px');
        } else {
            $('.JSscroll-top').css('right', '-70px');
        }
    });
 
    $('.JSscroll-top').click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 600);
        return false;
    }); 
    
// filters slide down
	$(".JSfilters-slide-toggle").click(function(){
	    $(this).next(".JSfilters-slide-toggle-content").slideToggle('fast');
	});

	if ($('.selected-filters li').length) {
		$('.JShidden-if-no-filters').show();
	} 
 
 // SELECT ARROW - FIREFOX FIX
	// $('select').wrap('<span class="select-wrapper"></span>');
	
 
 /*======= MAIN CONTENT HEIGHT ========*/
	if ($(window).width() > 1024 ) {
		$('.JScategories').on('mouseover', function(){
			if ($('.d-content').height() < $('.JSlevel-1').height()) { 
				$('.d-content').height($('.JSlevel-1').height());
			}
		}).on('mouseleave', function(){
			$('.d-content').height('');
		});
	} 
 

// POPUP BANNER
	if($('body').is('#start-page') && !(sessionStorage.getItem('popup')) ){   
	 	setTimeout(function(){ 
		 	$('.JSfirst-popup').animate({ top: '50%' }, 700);
		}, 1500);   

		 $(document).on('click', function(e){
		 	var target = $(e.target);
		 	if(!target.is($('.first-popup-inner img'))) { 
		 		$('.JSfirst-popup').hide();
		 	} 
		 }); 
		 if ($('.JSfirst-popup').length) {
	    	sessionStorage.setItem('popup','1');
		 }
	}


if ($(window).width() > 991 ) {

	// CATEGORIES WRAP
		$('.JSlevel-1 li').each(function () {	
			var subitems = $(this).find('.JSlevel-2-wrap > li'),
				menuitems = $(this).find('.main-menu > li'),
				colwidth = 'col-md-6';
				headcount = 1;

			if($(this).find('.header-banner').length) {
				colwidth = 'col-md-6';
				headcount = 2;
			} else {
				$(this).find('.JSlevel-2-wrap').removeClass('col-xs-8').addClass('col-xs-12');
				colwidth = 'col-md-4';
				headcount = 3;
			}
				// alert(colwidth)

			for(var i = 0; i < subitems.length; i+=headcount) {
				subitems.slice(i, i+headcount).wrapAll("<div class='clearfix'></div>");
				subitems.addClass(colwidth + ' ' + 'col-sm-12 col-xs-12');
				menuitems.removeClass(colwidth).addClass('col-md-6');
				// colwidth needs to be softcoded, based on if category banners are active | col-md-4 | col-md-6 |
			}
		});

	// FOOTER COLS CLEARFIX
	  	(function(){
	  		var cols = $('.JSfooter-cols > div');
	  		for(var i = 0; i < cols.length; i+=4){
	  			cols.slice(i, i+4).wrapAll('<div class="clearfix JSfooter-secs"></div>');
	  		}
  		}); 
}


// LEFT & RIGHT BODY LINK
	if ($(window).width() > 1024 ) {  
		var right_link = $('.JSright-body-link'),
			left_link = $('.JSleft-body-link'),
			main = $('.JSmain'); 

		setTimeout(function(){ 
			main.before((left_link).css('top', main.position().top + 'px'));
			main.after((right_link).css('top', main.position().top + 'px'));
			left_link.width(($(window).outerWidth() - $('.container').outerWidth())/2).height(main.height()); 
			right_link.width(($(window).outerWidth() - $('.container').outerWidth())/2).height(main.height()); 
		}, 1000); 
  	} 
 

// SEARCH BY GROUP TEXT
	function selectTxt(){    
		var txt = $('.JSSearchGroup2 option:first').text(trans('Sve')); 
		// if ($(window).width() > 1024 ){ 
		// 	txt.text('Pretraga po kategorijama');  
		// }else{
		// 	txt.text('po kategorijama');  
		// }
	}
	selectTxt();


	// GENERIC CAR TITLE
	$('.generic_car li').each(function(i, li){
		if ($(li).text().trim().length > 20) {
			$(li).attr('title', $(li).text().trim());
		} 
	});

	//order poll modal
	$('.JSOrderPoll').click(function() {
		var order_id = $(this).data('id');
		
		$.ajax({
			type: "POST",
			url: base_url + 'poll-content',
			data: { order_id: order_id },
			success: function(response) {
				$('#JSOrderPollContent').html(response);
				$('#JSOrderPollModal').modal('show');
			}
		});
	});
 	
 	// COOKIES
 	$(function(){
		function getCookie(cookieParamName){
			// if(document.cookie.split('; ').find(row => row.startsWith(cookieParamName))) {
			var row = function(row) { 
				return row.startsWith(cookieParamName) 
			}
			if(document.cookie.split('; ').find(row)) {
				return document.cookie
				  .split('; ')
				  .find(row)
				  .split('=')[1];
			}else{
				return undefined;
			}
			console.log(row)
		}
		function setCookie(cookieParamName,cookieParamValue,expireDays){
			var now = new Date();
			var time = now.getTime();
			var expireTime = time + 1000*86400*expireDays;
			now.setTime(expireTime);
			document.cookie = cookieParamName+"="+cookieParamValue+";expires="+now.toUTCString()+"; path=/;";
		}

		$("#alertCookiePolicy").hide()
		if (getCookie('acceptedCookiesPolicy') === undefined){
			//console.log('accepted policy', chk);
			$("#alertCookiePolicy").show(); 
		}
		$('#btnAcceptCookiePolicy').on('click',function(){
			//console.log('btn: accept');
			setCookie('acceptedCookiesPolicy','yes',30);
		});
		$('#btnDeclineCookiePolicy').on('click',function(){
			//console.log('btn: decline');
			setCookie('acceptedCookiesPolicy','no',30);
		});
 	});

 	// QUICK VIEW
	$(document).on('click', '.JSQuickViewButton', function() {
		$('#JSQuickView').modal('show');
		$.post(base_url + 'quick-view', { roba_id: $(this).data('roba_id') }, function(response) {
			$('#JSQuickViewContent').html(response);
		});	
	});
	$(document).on('click', '#JSQuickViewCloseButton', function() { 
		$('#JSQuickView').modal('hide');
	});
 
 	// COOKIES TEXT
 	(function(){
	 	var cookiesBtn = $('.JScookiesInfo_btn');
		cookiesBtn.text('Prikaži detalje');
		cookiesBtn.on('click', function(){
			cookiesBtn.next().slideToggle('fast');
			cookiesBtn.text(function(i, text){
		   		return text == 'Prikaži detalje' ? 'Sakrij detalje' : 'Prikaži detalje'
			}); 
		});
	}());

 	// SHOW COOKIES
	setTimeout(function(){ 
	 	$('.JScookies-part').animate({ bottom: '0' }, 700);
	}, 1500);   


	// HOW TO BUY VIDEO ARTICLE DETAILS
	(function(){
		var video_instuctions = $('.JSvideo_how_to_buy'),
			video_instuctions_src = video_instuctions.find('iframe').attr('src');

		$('.JSopen_video').on('click', function(){
	  		video_instuctions.addClass('flex').removeClass('hidden');
	    	video_instuctions.find('iframe').attr('src', video_instuctions_src + '?autoplay=1')  
		});
		video_instuctions.on('click', function(e){
			if ( !$('JSvideo_how_to_buy *').is(e.target)) {
				$('.JSvideo_how_to_buy').addClass('hidden').removeClass('flex');
				video_instuctions.find('iframe').attr('src', video_instuctions_src)  
			}  
		});
	}())
	
	// MAIN BANNER FIX IF LEFT BANNER ACTIVE
	if($('body').is('#start-page') && $(window).width() > 991 && $('.side-banners').length) {
		$('.JSmain-banners').each(function(i) {
			if(i == 0) {
				$(this).find('.col-md-4').removeClass('col-md-4').addClass('col-md-6');
				$(this).find('.col-md-6').wrapAll("<div class='col-md-9 col-sm-12 col-xs-12 no-padding'></div>");
			}
		});
		$('.JSmain-slider').each(function(i) {
			if(i == 0) {
				$(this).addClass('col-md-9 no-padding slider-border');
			}
		});

	};

	// LINKS APPENDED FROM TOP MENU TO RESPONSIVE NAV
	if($(window).width() < 991) {
		$('#responsive-nav').append($('.JS-site-links'));
	}

	// FAQ
	$('.faq-single').each(function(i, item) {
	    $(this).find('.faq-header').on('click', function() {
	      	$(this).parent().find('.faq-question').slideToggle();
	      	$(this).find('.faq-closed').toggle();
	      	$(this).find('.faq-opened').toggle();
	    });
	    if(i == 0) {
	    	$(this).parent().find('.faq-question').slideToggle();
	    	$(this).find('.faq-closed').toggle();
	      	$(this).find('.faq-opened').toggle();
	    }
  	});

	// PRODUCT LIST META WIDTH
	if($(window).width() > 768) {
		$('.shop-product-card-list-meta').css('width', $('.shop-product-card-list-meta').parent().width() - 270);
	}

	// REVIEW STARS COLORS
	$('.JSrev-star-group').each(function() {
		$(this).on('click', function() {
			$(this).removeClass('star-review-empty');
			$(this).siblings().addClass('star-review-empty');
		});
	});

	// REVIEW STAR CHECKBOX CHANGE HANDLE
	$('input.JSstars-checkbox').on('change', function() {
	    $('input.JSstars-checkbox').not(this).prop('checked', false);  
	});

	// HEADER MINI CART SCROLL REVEAL
	$('.header-cart-container').on('mouseenter', function() {
		$('.JSheader-cart-content').stop().slideDown();
	});
	
	$('.JSheader-cart-content').on('mouseleave', function() {
		$('.JSheader-cart-content').stop().slideUp();
	});

	// SITEMAP ROW CLEARFIX
	$('.category-listing').each(function(i, item) {
  		var rows = $(this).find('.JSsitemap-box');
  		console.log(i)
  		for(var i = 0; i < rows.length; i+=2){
  			rows.slice(i, i+2).wrapAll('<li class="clearfix JSsitemap-secs"></li>');
  		}
	});

	// AUTO GENERATED CHARACTERISTICS CLEARFIX
	$('.genererated-chars').find('.features-list').each(function(i, item) {
  		var rows = $(this).find('li');
  		console.log(i)
  		for(var i = 0; i < rows.length; i+=2){
  			rows.slice(i, i+2).wrapAll('<span class="clearfix no-padding"></span>');
  		}
	});

	// SIDE CONTENT RESPONSIVE ORDER CHANGE
	if($(window).width() < 768) {
		$('.side-content').parent().addClass('flex');
	}

	// TIP SLICK TABS
	// THIS WAS USED FOR THE OLD MODULAR TIP TABS
	// if($('body').is('#start-page') && $('.JStip-tabs-box').length) {
	// 	$('.JStip-slider').each(function(i) {
	// 		$(this).parent().parent().parent().hide();
	// 		$('.JStip-tabs-box').append($(this));

	// 		$(this).attr('id', 'tip' + '-' + i + '-content');
	// 		$('.tip-' + i + '-header').on('click', function() {
	// 			console.log(i)
	// 			$('#tip-' + i + '-content').show();
	// 			$('#tip-' + i + '-content').siblings().hide();
	// 		});
	// 	});
	// }
	//-----------NEW TIP SLICK TABS-----------------
	if($('body').is('#start-page') && $('.JStip-tabs-box').length) {
		$('.JStip-tabs-box .nav-tabs li').each(function(i) {
			$('.tip-' + 0 + '-content').show();
			$('.tip-' + i + '-header').on('click', function() {
				$('.tip-' + i + '-content').fadeIn();
				$('.tip-' + i + '-content').siblings().hide();
			});
		});
	}


	// PRODUCT TAGS
	
	// REMOVES EMPTY AUTO GENERETAED DIVS IN TAGS
	// $('.JSside-tags').children().each(function(i, item) {
	// 	if(!$(this).children().length) {
	// 		$(this).remove();
	// 	}
	// });

	// TRASHCODE
	var tagsGroup = {};
	var fontSize = 0;

	$('.JSside-tags').children().each(function(i, item) {
		if($(this).children().length) {
			var tag = $(this).text().toLowerCase().replace(/\s/g,'');
			$(this).addClass(tag);
			var classTag = $(this).attr('class');

			if (tagsGroup[classTag]) {
			   $(this).remove();
			} else {
			   tagsGroup[classTag] = true;
			}
				
		} else {
			$(this).remove();
		}
	});
	
	// ARTICLE DETAILS ICONS
	if($(window).width() > 991) {
		$('.side-icon-box').each(function() {
			$(this).on("mouseenter", function () {
		    	$(this).find('.icon-content').stop().animate({right: "100%" , opacity:"show"}, 400);
			});
			$(this).on("mouseleave", function () {
		    	$(this).find('.icon-content').stop().animate({right: "150%" , opacity:"hide"}, 400);
			});
		});
	}

}); // DOCUMENT READY END

//change ammount whishlist

 $(document).on("change", "#JSWishAmmount", function () {
	   
	     var ammount =$('input[name="ammount"]').val();
	     var robaID =$('input[name="robaID"]').val();
			$.post(base_url + 'ammount-add-wishlist', {ammount:ammount,robaID:robaID}, function (response){
				alertSuccess(trans('Količina je promenjena') + '.');

		});
		
	   
	});


//add all products from whislist

	 $(document).on("click", ".JSaddAlltoCart", function () {
	     var web_kupac_id = $(this).data('id');  
	     var cart_id =$('input[name="cart_id"]').val();
			$.post(base_url + 'add-to-cart-all', {web_kupac_id:web_kupac_id,cart_id:cart_id}, function (response){
				location.reload(true);
		});
		
	   
	});
 

// ZOOMER 
document.onreadystatechange = function() {
	// SIDE BANNER TO MAIN SLIDER & BANNER
	$('.JSside-banner-to-banners').each(function(i) {
		if(i == 0) {
			$(this).append($('.side-banners').find('.side-banner:nth-child(2)'));
		}
	});

	$('.JSside-banner-to-slider').each(function(i) {
		if(i == 0) {
			$(this).append($('.side-banners').find('.side-banner:nth-child(1)'));
		}
	});

	// $('.side-banners').each(function(i) {
	// 	if(i == 0) {
	// 		$('.JSside-banner-to-banners').append($(this).find('.side-banner:nth-child(2)'));
	// 		$('.JSside-banner-to-slider').append($(this).find('.side-banner:nth-child(1)'));
	// 	}
	// });


	// $('.JSside-banner').each(function(i) {
	// 	if(i == 0) {
	// 		$('.side-banners').each(function(i) {
	// 			$('.JSside-banner').append($(this));
	// 		});
	// 		$('.side-banners > div').removeClass('col-md-4 col-sm-4 col-xs-4');
	// 		if($('.side-banners').length && $(window).width() > 991) {
	// 			$('.JSmain-slider').parent().addClass('col-md-9 sm-no-padd p-right');
	// 			$('.JSside-banner .center-block').css('max-height', $('.JSmain-slider').height());
	// 		};
	// 	};
	// });
	
		

	// LEFT BANNER TO MAIN BANNERS
	// if($('.JSmain-banners').length && $(window).width() > 991) {
	// 	$('.left-banner').each(function(i) {
	// 		$('.JSleft-banner').append($(this).find('div'));
	// 	});
	// 	$('.JSleft-banner').find('div').removeClass('col-md-3 col-sm-3 col-xs-3');
	// };

	

    if (document.readyState === "complete") { 

		if($('body').is('#artical-page')){	
			$('.zoomContainer').css({
				width: $('.zoomWrapper').width(),
				height: $('.zoomWrapper').height()
			}); 
				
			var art_img =  $('#art-img').attr('src');
			if (art_img.toLowerCase().indexOf('no-image') >= 0){
				$('.zoomContainer').hide();
			} 
		// ARTICLE ZOOMER		
			$('.zoomContainer').each(function(i, el){
				if ( i != 0) {
					$(el).remove();
				}
			});
		}  

		$('.product-image').each(function(i, img){
			if ($(img).attr('src') != 'quick_view_loader.gif') {
				$(img).attr('src', '/images/no-image.jpg');
			}
		});  

// OVERLAPING CATEGORIES
		// (function(){
		//   	var categ = $('.JSlevel-1'),
		//   		categ_offset_bottom = categ.offset().top + categ.outerHeight(true); 
 
	 //  		$('.JSproducts_slick').each(function(i, el){
	 //  			if (categ_offset_bottom >= $(el).offset().top) {
	 //  				$(el).closest('.JSaction_offset').css('marginLeft', categ.width() + 'px');
	 //  			}  
	 //  		});

		// }());

		//filter vesti
		
		$('.JSfilter-categories').click(function() {
			
			var element = $(this).data("element");
			var url = $("#JScharacteristic-url").val();
		
			//alert(base_url+'filter/'+url+'/'+proizvodjaci+'/'+karakteristike);
			window.location.href = base_url + 'blog/' + '?ch=' + element;
		});

 
 
// SECTION OFFSET
		if($('body').is('#start-page') && $(window).width() > 991){		
			if($('.JSd_content').length) {
				$('.JSd_content').each(function(i, el){  
		 			var lvl_1 = $('.JSlevel-1'),
		 				main_slider = $('.JSmain-slider'),
		 				parent_cont = $('.parent-container');

		 			if ($(el).offset().top <= lvl_1.offset().top + lvl_1.outerHeight()) { 

		 				if((lvl_1).children().length){
							$(el).css('margin-left', lvl_1.outerWidth()); 
						}	 

						if ($(this).closest(parent_cont).hasClass('container-fluid')) {

							if (!$(el).find(main_slider).length) {
					 			$(this).closest(parent_cont).removeClass('container-fluid no-padding').addClass('container');

					 		} else {
					 			$(el).css('margin-left', ''); 	 
					 		}

				 		}
					}  

	 
					$(el).css('width', 'auto');  
				 
					$('.JSloader').hide();
				}); 
			} 
			else {
				$('.JSloader').hide();
			}


			// $('.JSmain-slider .slick-slide').css('max-height', $('.JSlevel-1').outerHeight() - 25);
		}
 
	} 
}
 
 