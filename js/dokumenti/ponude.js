$(document).ready(function(){
	//ponude
	$('.JSSort').click(function(){
		var params = getUrlVars();
		params['sort_column'] = $(this).data('sort_column');
		params['sort_direction'] = $(this).data('sort_direction');
		delete params['page'];

		window.location.href = window.location.pathname+(Object.keys(params).length > 0 ? '?'+$.param(params) : '');
	});
	$('.JSVrstaDokumenta').click(function(){
		var redirect = '';
		if($(this).val() == 'predracun'){
			redirect = "/dokumenti/predracuni";
		}else if($(this).val() == 'racun'){
			redirect = "/dokumenti/racuni";
		}else if($(this).val() == 'avansni_racun'){
			redirect = "/dokumenti/racuni?vrsta_dokumenta=avansni_racun";
		}else{
			var params = { vrsta_dokumenta: $(this).val() };

			redirect = window.location.pathname+(Object.keys(params).length > 0 ? '?'+$.param(params) : '');
		}

		window.location.href = redirect;
	});
	$('#JSParnerSearch').change(function(){
		var params = getUrlVars();
		params['partner_id'] = $(this).val();
		delete params['page'];
		window.location.href = window.location.pathname+(Object.keys(params).length > 0 ? '?'+$.param(params) : '');
	});
	$('#JSStatusSearch').change(function(){
		var params = getUrlVars();
		params['dokumenti_status_id'] = $(this).val();
		delete params['page'];
		window.location.href = window.location.pathname+(Object.keys(params).length > 0 ? '?'+$.param(params) : '');
	});

	//ponuda
	var kupciTime;
	$(document).on("keyup", '#JSPartnerNaziv', function() {
		clearTimeout(kupciTime);
		kupciTime = setTimeout(function(){
			$('#JSPartnerId').val('0');

			$('.JSPartnerSearchList').remove();
			var partner = $('#JSPartnerNaziv').val();
			if (partner != '' && partner.length > 2) {
				$.post(base_url + 'dokumenti/ajax/partner-pretraga', { partner: partner }, function (response){
					$('#JSPartnerSearchContent').html(response);
				});
			} 				
		}, 500);
	});
	$(document).on("click", ".JSPartnerSearchItem", function() {
		$('#JSPartnerNaziv').val($(this).find('.JSPartnerSearchNaziv').text());
		$('#JSPartnerId').val($(this).data('partner_id'));

		$.post(base_url + 'dokumenti/ajax/partner-podaci', { partner_id: $(this).data('partner_id') }, function (response){
			var result = $.parseJSON(response);
			$('#JSPartnerPib').val(result.pib.trim());
			$('#JSPartnerAdresa').val(result.adresa);
			$('#JSPartnerMesto').val(result.mesto);
			$('#JSPartnerKontaktOsoba').val(result.kontakt_osoba);
			$('#JSPartnerMail').val(result.mail);
			$('#JSPartnerTelefon').val(result.telefon);
		});

		$('.JSPartnerSearchList').remove();
	});
	$('html :not(.JSPartnerSearchList)').on("click", function() {
		$('.JSPartnerSearchList').remove();
	});

	$('input[name="datum_ponude"]').datepicker({
		dateFormat: 'dd-mm-yy'
	});
	$('input[name="vazi_do"]').datepicker({
		dateFormat: 'dd-mm-yy'
	});
	$('input[name="rok_isporuke"]').datepicker({
		dateFormat: 'dd-mm-yy'
	});

	$('input[name="datum_ponude"]').change(function(){
		$('#JSPonudaSubmit').css('background-color','red');
	});
	$('input[name="vazi_do"]').change(function(){
		$('#JSPonudaSubmit').css('background-color','red');
	});
	$('input[name="rok_isporuke"]').change(function(){
		$('#JSPonudaSubmit').css('background-color','red');
	});
	$('input[name="predracun"]').click(function(){
		$('#JSPonudaSubmit').css('background-color','red');
	});
	$('input[name="racun"]').click(function(){
		$('#JSPonudaSubmit').css('background-color','red');
	});
	$('input[name="nacin_placanja"]').keyup(function(){
		$('#JSPonudaSubmit').css('background-color','red');
	});
	$('#JSPonudaStatus').on('change',function(){
		$('#JSPonudaSubmit').css('background-color','red');
	});
	$('input[name="narudzbina"]').click(function(){
		$('#JSPonudaSubmit').css('background-color','red');
	});

	//stavke
	$('.JSRobaId').select2({placeholder: 'Izaberi stavku'});
	$('.JSRobaId').change(function(){
		var roba_id = $(this).val();
		var stavka_row = $(this).closest('.JSStavkaRow');
		var ponuda_stavka_id = stavka_row.attr('data-ponuda_stavka_id');
		var data = { change: 'roba', narudzbina: ($('#JSNarudzbina').is(':checked') ? 1 : 0), ponuda_stavka_id: ponuda_stavka_id, roba_id: roba_id };
		if(ponuda_stavka_id == 0){
			data['ponuda_id'] = stavka_row.attr('data-ponuda_id');
		}else{
			data['ponuda_id'] = $('input[name="ponuda_id"]').val();
		}

		$.post(base_url + 'dokumenti/ajax/ponuda-stavka-save', data, function (response){
			var result = $.parseJSON(response);
			if(!result.success){
				location.reload(true);
			}else{
				alertify.success('Stavka je sačuvana.');
				if(ponuda_stavka_id == 0){
					setTimeout(function(){ window.location.href = window.location.pathname+'#JSStavke'; },500);
				}
				stavka_row.find('.JSRobaIdText').text(result.roba_id);
				stavka_row.find('.JSNsbCena').val(parseFloat(result.nab_cena).toFixed(2));
				stavka_row.find('.JSRabat').val(parseFloat(result.rabat));
				stavka_row.find('.JSRabatCena').val(parseFloat(result.rabat_cena).toFixed(2));
				stavka_row.find('.JSUkupno').val(parseFloat(result.ukupna_cena).toFixed(2));
				$('#JSPonudaUkupanIznos').text(parseFloat(result.iznos).toFixed(2));
			}
		});

	});

	var stavkaNazivTime;
	$(document).on("keyup", '.JSStavkaNaziv', function() {
		var naziv_stavke = $(this).val();
		var stavka_row = $(this).closest('.JSStavkaRow');
		var ponuda_stavka_id = stavka_row.attr('data-ponuda_stavka_id');
		
		clearTimeout(stavkaNazivTime);
		stavkaNazivTime = setTimeout(function(){
			$.post(base_url + 'dokumenti/ajax/ponuda-stavka-save', { change: 'naziv_stavke', ponuda_id: $('input[name="ponuda_id"]').val(), narudzbina: ($('#JSNarudzbina').is(':checked') ? 1 : 0), ponuda_stavka_id: ponuda_stavka_id, naziv_stavke: naziv_stavke }, function (response){
				var result = $.parseJSON(response);
				if(!result.success){
					location.reload(true);
				}else{
					stavka_row.find('.JSStavkaNaziv').text(result.naziv_stavke);
					$('#JSPonudaUkupanIznos').text(parseFloat(result.iznos).toFixed(2));
					alertify.success('Stavka je sačuvana.');
				}
			});			
		}, 500);
	});

	$('.JSKolicina').keyup(function(){
		var kolicina = $(this).val();
		if(!isNaN(kolicina) && kolicina > 0){
			$(this).css('border-color', '');
			var stavka_row = $(this).closest('.JSStavkaRow');
			var ponuda_stavka_id = stavka_row.attr('data-ponuda_stavka_id');

			if(ponuda_stavka_id > 0){
				$('.select2-container').css('border','');
				$.post(base_url + 'dokumenti/ajax/ponuda-stavka-save', { change: 'kolicina', ponuda_id: $('input[name="ponuda_id"]').val(), narudzbina: ($('#JSNarudzbina').is(':checked') ? 1 : 0), ponuda_stavka_id: ponuda_stavka_id, kolicina: kolicina }, function (response){
					var result = $.parseJSON(response);
					if(!result.success){
						location.reload(true);
					}else{
						stavka_row.find('.JSKolicina').val(result.kolicina);
						stavka_row.find('.JSUkupno').val(parseFloat(result.ukupna_cena).toFixed(2));
						$('#JSPonudaUkupanIznos').text(parseFloat(result.iznos).toFixed(2));
						alertify.success('Stavka je sačuvana.');
					}
				});
			}else{
				stavka_row.find('.select2-container').css('border','1px solid red');
			}
		}else{
			$(this).css('border-color', 'red');
		}
	});

	var stavkaRabatTime;
	$('.JSRabat').keyup(function(){
		var rabat = $(this).val();
		if(!isNaN(rabat)){
			$(this).css('border-color', '');
			var stavka_row = $(this).closest('.JSStavkaRow');
			var ponuda_stavka_id = stavka_row.attr('data-ponuda_stavka_id');
			if(ponuda_stavka_id > 0){
				$('.select2-container').css('border','');
				clearTimeout(stavkaRabatTime);
				stavkaRabatTime = setTimeout(function(){
					$.post(base_url + 'dokumenti/ajax/ponuda-stavka-save', { change: 'rabat', ponuda_id: $('input[name="ponuda_id"]').val(), narudzbina: ($('#JSNarudzbina').is(':checked') ? 1 : 0), ponuda_stavka_id: ponuda_stavka_id, rabat: rabat }, function (response){
						var result = $.parseJSON(response);
						if(!result.success){
							location.reload(true);
						}else{
							// stavka_row.find('.JSRabat').val(result.rabat);
							stavka_row.find('.JSRabatCena').val(parseFloat(result.rabat_cena).toFixed(2));
							stavka_row.find('.JSUkupno').val(parseFloat(result.ukupna_cena).toFixed(2));
							$('#JSPonudaUkupanIznos').text(parseFloat(result.iznos).toFixed(2));
							alertify.success('Stavka je sačuvana.');
						}
					});
				},500);
			}else{
				stavka_row.find('.select2-container').css('border','1px solid red');
			}
		}else{
			stavka_row.find('.select2-container').css('border','1px solid red');
		}
	});

	var stavkaAddedRabatTime;
	$('.JSAddedRabat').keyup(function(){
		var added_rabat = $(this).val();
		if(!isNaN(added_rabat)){
			$(this).css('border-color', '');
			var stavka_row = $(this).closest('.JSStavkaRow');
			var ponuda_stavka_id = stavka_row.attr('data-ponuda_stavka_id');
			if(ponuda_stavka_id > 0){
				$('.select2-container').css('border','');
				clearTimeout(stavkaAddedRabatTime);
				stavkaAddedRabatTime = setTimeout(function(){
					$.post(base_url + 'dokumenti/ajax/ponuda-stavka-save', { change: 'added_rabat', ponuda_id: $('input[name="ponuda_id"]').val(), narudzbina: ($('#JSNarudzbina').is(':checked') ? 1 : 0), ponuda_stavka_id: ponuda_stavka_id, added_rabat: added_rabat }, function (response){
						var result = $.parseJSON(response);
						if(!result.success){
							location.reload(true);
						}else{
							// stavka_row.find('.JSRabat').val(result.rabat);
							stavka_row.find('.JSRabatCena').val(parseFloat(result.rabat_cena).toFixed(2));
							stavka_row.find('.JSUkupno').val(parseFloat(result.ukupna_cena).toFixed(2));
							$('#JSPonudaUkupanIznos').text(parseFloat(result.iznos).toFixed(2));
							alertify.success('Stavka je sačuvana.');
						}
					});
				},500);
			}else{
				stavka_row.find('.select2-container').css('border','1px solid red');
			}
		}else{
			stavka_row.find('.select2-container').css('border','1px solid red');
		}
	});

	$('.JSPdv').keyup(function(){
		var pdv = $(this).val();
		if(!isNaN(pdv)){
			$(this).css('border-color', '');
			var stavka_row = $(this).closest('.JSStavkaRow');
			var ponuda_stavka_id = stavka_row.attr('data-ponuda_stavka_id');
			if(ponuda_stavka_id > 0){
				$('.select2-container').css('border','');

				$.post(base_url + 'dokumenti/ajax/ponuda-stavka-save', { change: 'pdv', ponuda_id: $('input[name="ponuda_id"]').val(), narudzbina: ($('#JSNarudzbina').is(':checked') ? 1 : 0), ponuda_stavka_id: ponuda_stavka_id, pdv: pdv }, function (response){
					var result = $.parseJSON(response);
					if(!result.success){
						location.reload(true);
					}else{
						stavka_row.find('.JSPdv').val(result.pdv);
						stavka_row.find('.JSUkupno').val(parseFloat(result.ukupna_cena).toFixed(2));
						$('#JSPonudaUkupanIznos').text(parseFloat(result.iznos).toFixed(2));
						alertify.success('Stavka je sačuvana.');
					}
				});
			}else{
				stavka_row.find('.select2-container').css('border','1px solid red');
			}
		}else{
			stavka_row.find('.select2-container').css('border','1px solid red');
		}
	});

	var stavkaCenaTime;
	$('.JSNsbCena').keyup(function(){
		var nab_cena = $(this).val();
		if(!isNaN(nab_cena) && nab_cena > 0){
			$(this).css('border-color', '');
			var stavka_row = $(this).closest('.JSStavkaRow');
			var ponuda_stavka_id = stavka_row.attr('data-ponuda_stavka_id');
			if(ponuda_stavka_id > 0){
				$('.select2-container').css('border','');

				clearTimeout(stavkaCenaTime);
				stavkaCenaTime = setTimeout(function(){
					$.post(base_url + 'dokumenti/ajax/ponuda-stavka-save', { change: 'nab_cena', ponuda_id: $('input[name="ponuda_id"]').val(), narudzbina: ($('#JSNarudzbina').is(':checked') ? 1 : 0), ponuda_stavka_id: ponuda_stavka_id, nab_cena: nab_cena }, function (response){
					var result = $.parseJSON(response);
						if(!result.success){
							location.reload(true);
						}else{
							// stavka_row.find('.JSNsbCena').val(parseFloat(result.nab_cena).toFixed(2));
							stavka_row.find('.JSRabatCena').val(parseFloat(result.rabat_cena).toFixed(2));
							stavka_row.find('.JSUkupno').val(parseFloat(result.ukupna_cena).toFixed(2));
							$('#JSPonudaUkupanIznos').text(parseFloat(result.iznos).toFixed(2));
							alertify.success('Stavka je sačuvana.');
						}
					});
				},500);
			}else{
				stavka_row.find('.select2-container').css('border','1px solid red');
			}
		}else{
			$(this).css('border-color', 'red');
		}
	});

	$(document).on('keyup', 'input.select2-search__field', function(e) {
		var code = e.keyCode || e.which;
		if(code == 13 && $(this).val() != ''){
			var naziv_stavke = $(this).val();
			// $('#JSStavkaNaziv').attr('type','text');
			// $('#JSStavkaNaziv').val(text);
			// $('.select2-dropdown').hide();
			// $('.JSRobaId[data-roba_id="0"]').closest('td').find('.select2-container').hide();
			var data = { change: 'naziv_stavke', ponuda_stavka_id: 0, naziv_stavke: naziv_stavke, ponuda_id: $('input[name="ponuda_id"]').val()};

			$.post(base_url + 'dokumenti/ajax/ponuda-stavka-save', data, function (response){
				alertify.success('Stavka je sačuvana.');
				setTimeout(function(){ window.location.href = window.location.pathname+'#JSStavke'; },500);
			});
		}
	});	
	$('#JSPonudaSabloniLink').click(function(){
		if($('#JSPonudaSabloni').attr("hidden")){
			$('#JSPonudaSabloni').removeAttr('hidden');
		}else{
			$('#JSPonudaSabloni').attr('hidden','hidden');
		}
	});


	$('#JSNarudzbina').on('click',function(){
		var ponuda_id = $('input[name="ponuda_id"]').val();
		if($(this).is(':checked')){
			$.post(base_url + 'dokumenti/ajax/ponuda-kolicina-narudzbine', { ponuda_id: ponuda_id }, function (response){ });
		}else{
			location.reload(true);
		}
	});

	$('#JSBezPdv').on('click',function(){
		var ponuda_id = $('input[name="ponuda_id"]').val();
		if($(this).is(':checked')){
			$.post(base_url + 'dokumenti/ajax/ponuda-bez-pdv', { ponuda_id: ponuda_id }, function (response){ location.reload(true); });
		}
	});

	if($('#submit_order').val() == 1){
		alertify.confirm('Već ste poslali ponudu u korpu, da li želite da ponovite proces?', function(e){
			if(e){
				$('input[name="u_korpu"]').val(1);
				$('#JSPonudaSubmitOrder').trigger('click');
			}
		});
	}
	if($('#template_change').val() == 1){
		alertify.confirm('Šablon je izmenjen. Da li želite da ga sačuvate kao podrazumevani?', function(e){
			if(e){
				var url_arr = window.location.href.split('/');
				var isb2b = parseInt($('#isb2b').val());
				var kind = url_arr[4+isb2b];
				var id = url_arr[5+isb2b];

				$.post(base_url + 'dokumenti/ajax/change-sablon', { kind: kind, id: id }, function (response){
					alertify.success('Uspešno ste izmenili šablon.');
				});
			}
		});
	}

});