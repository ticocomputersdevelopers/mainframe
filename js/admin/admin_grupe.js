$('.nova-karak_li').hide();
$('.nova-vred_li').hide();

$(document).ready(function () {
  
    $('#nova-kar').click(function(){
        
        $('.nova-karak_li').toggle('hide');

    });

    $('#nova-vred').click(function(){

        $('.nova-vred_li').toggle('hide');
       
    });

    // NAZIVI KARAKTERISTIKA
    $('.JSCharNew').on("keyup",function(event){  

        $this = $(this);
        var naziv = $this.parent().find(".naziv").val();       
        var grupa_pr_id = $(this).data('id');
        var karak_rbr = $this.parent().find(".karak_rbr").val();
        if(event.keyCode == 13 ){
        $('#info').html(''); 
        if(naziv != '') {
        
            $.post(base_url+'admin/ajax/groups', {action: 'sacuvaj-novu-karak', grupa_pr_id: grupa_pr_id, naziv: naziv, karak_rbr:karak_rbr}, function (response){ location.reload(true); });   
            $this.css('border', '');
            alertify.success(translate(translate('Uspešno ste uneli vrednost.')));  
        } else {

            $this.css('border', 'solid red 2px').attr('placeholder','');         
            alertify.error(translate('Niste popunili polje.'))
       
        }
        }       
    });        

    $('.JSChar').on("keyup",function(event){  
        $this = $(this);
        var naziv = $this.parent().find(".naziv").val();
        var grupa_pr_naziv_id = $(this).data('id');
         if(event.keyCode == 13 ){
        $('#info').html(''); 
        if(naziv != '') {
            $.post(base_url+'admin/ajax/groups', {action: 'sacuvaj', naziv: naziv, grupa_pr_naziv_id: grupa_pr_naziv_id}, function (response){
                $this.css('border', '');
                alertify.success(translate('Uspešno ste sačuvali vrednost.'));   
            });
         
        } else {
            
                $this.css('border', 'solid red 2px').attr('placeholder','');
                alertify.error(translate('Niste popunili polje.'));
         
        }
        }

    });

    $('.activeKarak').click(function(){
            if($(this).attr('checked')){
                $(this).attr('checked', false);
            }else{
                $(this).attr('checked', true);
            }
            var grupa_pr_naziv_id = $(this).data('id');
             $.post(base_url+'admin/ajax/groups', {action: 'aktivnaKarak', grupa_pr_naziv_id: grupa_pr_naziv_id}, function (response){ });
    });

    $('.obrisi-karak').click(function(){

        var grupa_pr_naziv_id = $(this).data('id');

        if (confirm('Da li želite da izbrišete karakteristiku? Sve vrednosti ove karakteristike biće izbrisane!') == true) {
            $.post(base_url+'admin/ajax/groups', {action: 'obrisi-karak', grupa_pr_naziv_id: grupa_pr_naziv_id}, function (response){ });
        
        $('#info').html(translate('Uspešno ste obrisali karakteristiku i njene vrednosti.')).css('color', 'green');

        $(this).parent().hide();
        location.reload(true)
        }

       
    });


    // VREDNOSTI
    $('.JSCharNewValue').on("keyup",function(event){            
       
        $this = $(this);
        var vrednost = $this.val();
        var grupa_pr_naziv_id = $(this).data('id');
        if(event.keyCode == 13 ){
            $('#info').html(''); 
            if( vrednost != ''){            
                $.post(base_url+'admin/ajax/groups', {action: 'nova-vred', vrednost: vrednost, grupa_pr_naziv_id: grupa_pr_naziv_id}, function (response){location.reload(true) });
                $this.css('border', '');
                alertify.success(translate(translate('Uspešno ste uneli vrednost.')));  
            } else {

                $this.css('border', 'solid red 2px').attr('placeholder','');         
                alertify.error(translate('Niste popunili polje.'));
           }
        }

    });

    $('.JSCharValue').on("keyup",function(event){
         
        $this = $(this);
        var vrednost = $this.val();
        var grupa_pr_vrednost_id = $(this).data('id');
        if(event.keyCode == 13 ){
            $('#info').html(''); 
            if(vrednost != '') {
            $.post(base_url+'admin/ajax/groups', {action: 'sacuvaj-vrednost', vrednost: vrednost, grupa_pr_vrednost_id: grupa_pr_vrednost_id}, function (response){ 
                $this.css('border', '');
                alertify.success(translate('Uspešno ste sačuvali vrednost.'));                             
            });             
                      
            } else {
                $this.css('border', 'solid red 2px').attr('placeholder','');
                alertify.error(translate('Niste popunili polje.'));
            }
        }
    });

    $('.activeVred').click(function(){
            if($(this).attr('checked')){
                $(this).attr('checked', false);
            }else{
                $(this).attr('checked', true);
            }
            var grupa_pr_vrednost_id = $(this).data('id');
             $.post(base_url+'admin/ajax/groups', {action: 'aktivnaVred', grupa_pr_vrednost_id: grupa_pr_vrednost_id}, function (response){ });
    });

    $('.obrisi-vred').click(function(){


        var grupa_pr_vrednost_id = $(this).data('id');

        if (confirm(translate('Da li želite da izbrišete vrednost karakteristike?')) == true) {

            $.post(base_url+'admin/ajax/groups', {action: 'obrisi-vred', grupa_pr_vrednost_id: grupa_pr_vrednost_id}, function (response){ });
            
            $('#info').html(translate('Uspešno ste obrisali vrednost.')).css('color', 'green');
            
            $(this).parent().hide();
        }
    });

    //CHARACTERS COUNT//
    
    var text_max = 163 - $('#seo_opis').val().length;
    $('#seo_opis_label').html('SEO Opis (max 163 karaktera, ostalo - '+ text_max + ')' );

    $('#seo_opis').keyup(function() {
        
        var text_length = $('#seo_opis').val().length;
        console.log(text_length);
        var text_remaining = text_max - text_length;

        $('#seo_opis_label').html('SEO Opis (max 163 karaktera, ostalo - '+ text_remaining + ')' );
    });
    
    //var text_max = 159 - $('#seo_keywords').val().length;
    $('#seo_keywords_label').html('SEO Keywords (max 159 karaktera, ostalo - '+ text_max + ')' );

    $('#seo_keywords').keyup(function() {
        
        var text_length = $('#seo_keywords').val().length;
        console.log(text_length);
        var text_remaining = text_max - text_length;

        $('#seo_keywords_label').html('SEO keywords (max 159 karaktera, ostalo - '+ text_remaining + ')' );
    });

    $(".grupa_select").select2({
        width: '100%',
        language: {
            noResults: function (params) {
            return translate("Nema rezultata");
            }
        }
    });

});

$(function() {
     
    $('#JSListValues').sortable({
        //observe the update event...
        update: function(event, ui) {
            //create the array that hold the positions...
            var moved = ui.item[0].id;
            var order = []; 
            //loop trought each li...
            $('#JSListValues li').each( function(e) {

            //add each li position to the array...     
            // the +1 is for make it start from 1 instead of 0
            order.push( $(this).attr('id') );                
            });
            $.ajax({
                type: "POST",
                url: base_url+'admin/position-value',
                data: {order:order, moved:moved},
                success: function(msg) {
                }
            });
        }
    });                    
    $( "#JSListValues").disableSelection();
                        
});  

$(function() {
     
    $('#JSListChar').sortable({
        //observe the update event...
        update: function(event, ui) {
            //create the array that hold the positions...
            var moved = ui.item[0].id;
            var order = []; 
            //loop trought each li...
            $('#JSListChar li').each( function(e) {

            //add each li position to the array...     
            // the +1 is for make it start from 1 instead of 0
            order.push( $(this).attr('id') );                
            });
            $.ajax({
                type: "POST",
                url: base_url+'admin/position-char',
                data: {order:order, moved:moved},
                success: function(msg) {
                }
            });
        }
    });                    
    $( "#JSListChar").disableSelection();
                        
});  


$(function () {
    var target_id = null;
    $('#jstree').jstree({
       "core" : {
        "animation" : 0,
        "check_callback" : true,
        "themes" : { "stripes" : true },
      },
      "types" : {
        "#" : {
          "max_children" : 1,
          "max_depth" : 5,
          "valid_children" : ["root"]
        },
        "root" : {
          "icon" : "/static/3.3.3/assets/images/tree_icon.png",
          "valid_children" : ["default"]
        },
        "default" : {
          "valid_children" : ["default","file"]
        },
        "file" : {
          "icon" : "glyphicon glyphicon-file",
          "valid_children" : []
        }
      },
      "plugins" : [
            "dnd", "search", "types", "wholerow"
        ]
    })
    .on("mousedown", "a",
        function() {
            target_id = $(this).closest('li').data('id');
        }
    )
    .on("move_node.jstree", function (e, data) {

        if(target_id == null){
            location.reload(true);
        }
        var parent_id = $('#'+data.parent).data('id');
        var childs_ids = [];

        var childs = $('#jstree').find( "li[data-id='"+parent_id+"']" ).find('ul').children();
        
        childs.each(function(i,obj){
            childs_ids.push($(obj).data('id'));
        });

        var data = {action: 'grupa_rbr', target_id: target_id, parent_id: parent_id, childs_ids: childs_ids};
        $.ajax({
            type: "POST",
            url: base_url+'admin/ajax/groups',
            data: data,
            success:function(response){ },
            error: function(response){ location.reload(true); },
        }); 
    })
    .on("click", "a",
        function() {
            document.location.href = this;
        }
    );
});




// $(document).ready(function () {

//     /* JS TREE VIEW CATEGORIES */
//     $('#jstree').jstree({
//        "core" : {
//         "animation" : 0,
//         "check_callback" : true,
//         "themes" : { "stripes" : true },
//       },
//       "types" : {
//         "#" : {
//           "max_children" : 1,
//           "max_depth" : 4,
//           "valid_children" : ["root"]
//         },
//         "root" : {
//           "icon" : "/static/3.3.3/assets/images/tree_icon.png",
//           "valid_children" : ["default"]
//         },
//         "default" : {
//           "valid_children" : ["default","file"]
//         },
//         "file" : {
//           "icon" : "glyphicon glyphicon-file",
//           "valid_children" : []
//         }
//       },
//       "plugins" : [
//             "contextmenu", "dnd", "search",
//             "state", "types", "wholerow"
//         ]
//     });

//     $("#jstree li").on("click", "a",
//         function() {
//             document.location.href = this;
//         }
//     );

// });



// $(document)
// // .bind("dnd_start.vakata", function(e, data) {
// //     console.log(data);
// // })
// // .bind("dnd_move.vakata", function(e, data) {
// //     console.log(data);
// // })

// .bind("dnd_stop.vakata", function(e, data) {
//     var target = $(data.element);
//     var target_id = target.closest('li').data('id');
//     var parent_id = target.closest('ul').closest('li').data('id');

//     // var parent = $( "li[data-id='"+target_id+"']" ).closest('ul').closest('li').data('id');
//     console.log(target.closest('li').attr('id'));
// });

// // $(document).bind("dnd_start.vakata", function(e, data) {
// //     console.log(data);
// // })
// // .bind("move_node.jstree", function(e, data) {
// //    console.log("Drop node " + data.node.id + " to " + data.parent);
// // });

// $('#jstree').on("move_node.jstree", function (e, data) {

//         //item being moved                      
//         var moveitemID = $('#' + data.node.id).find('a')[0].id;            

//         //new parent
//         var newParentID = $('#' + data.parent).find('a')[0].id;

//         //old parent
//         var oldParentID = $('#' + data.old_parent).find('a')[0].id;

//         //position index point in group
//         //old position
//         var oldPostionIndex = data.old_position;
//         //new position
//         var newPostionIndex = data.position;

//         console.log(newParentID);
//     });