$(document).ready(function () {
	$('#JSArticleSubmit').click(function(){
		$(this).attr('disabled',true);
		$('#JSArticleForm').submit();
	});
	   // PRODUCT
	  $("#roba_grupe").select2({
	  	placeholder: translate('Izaberite grupe')
	  	});
	   // product promena magacina
	  $("#orgj_id").change(function(){
	  	var roba_id = $("input[name='roba_id']").val();
	  	var magacin_id = $(this).val();
	  	
	  	$.post(base_url+'admin/ajax/articles', {action: 'magacin_change', roba_id: roba_id, magacin_id: magacin_id}, function (response){
	  		$("input[name='kolicina']").val(Math.round(response));
	  	});
	  });

	  // product promena poreza
	  $("#tarifna_grupa_id").change(function(){
	  	if($("#JSIWebCena").is(':checked')){
		  	var tarifna_grupa_id = $(this).val();
		  	var nabavna_cena = $("input[name='racunska_cena_nc']").val();
		  	var web_marza = $("input[name='web_marza']").val();
		  	var mp_marza = $("input[name='mp_marza']").val();

		  	if($.isNumeric(nabavna_cena) && $.isNumeric(web_marza) && $.isNumeric(mp_marza)){		  		
			  	$.post(base_url+'admin/ajax/articles', {action: 'vrednost_poreza', tarifna_grupa_id: tarifna_grupa_id}, function (response){
				  	var web_cena = nabavna_cena*(1+web_marza/100)*(1+response/100);
				  	var mpcena = nabavna_cena*(1+mp_marza/100)*(1+response/100);
				  	$("input[name='web_cena']").val(parseFloat(web_cena).toFixed(2));
				  	$("input[name='mpcena']").val(parseFloat(mpcena).toFixed(2));		  		
			  	});
		  	}
	  	}
	  });
		// product provera menjanja i web cene pri promeni poreza
		if(!$("#JSIWebCena").is(':checked')){
			$("#JSIWebCena").click(function(){
				
		  	var nabavna_cena = $("input[name='racunska_cena_nc']").val();
		  	var web_marza = $("input[name='web_marza']").val();
			if($.isNumeric(web_marza) && $.isNumeric(nabavna_cena)){	
			  	var tarifna_grupa_id = $("#tarifna_grupa_id").val();
			  	$.post(base_url+'admin/ajax/articles', {action: 'vrednost_poreza', tarifna_grupa_id: tarifna_grupa_id}, function (response){
				  	var web_cena = nabavna_cena*(1+web_marza/100)*(1+response/100);
				  	$("input[name='web_cena']").val(parseFloat(web_cena).toFixed(2));	  		
			  	});
			}

			});
		}
		$(".search_select").select2({
		width: '90%',
        language: {
            noResults: function () {
            return translate("Nema rezultata");
            }
        }
    });

	 //  // product promena nabavne cene
	 //  $("#racunska_cena_nc").keyup(function(){
	 //  	var nabavna_cena = $(this).val();
	 //  	var web_marza = $("input[name='web_marza']").val();
	 //  	var mp_marza = $("input[name='mp_marza']").val();
	 //  	if($.isNumeric(nabavna_cena) && $.isNumeric(web_marza) && $.isNumeric(mp_marza)){
		//   	var tarifna_grupa_id = $("#tarifna_grupa_id").val();

		//   	$.post(base_url+'admin/ajax/articles', {action: 'vrednost_poreza', tarifna_grupa_id: tarifna_grupa_id}, function (response){
		// 	  	var web_cena = nabavna_cena*(1+web_marza/100)*(1+response/100);
		// 	  	var mpcena = nabavna_cena*(1+mp_marza/100)*(1+response/100);
		// 	  	$("input[name='web_cena']").val(Math.round(web_cena)+'.00');
		// 	  	$("input[name='mpcena']").val(Math.round(mpcena)+'.00');		  		
		//   	});
		// }
	 //  });

	  // product promena web marze
	  $("#web_marza").keyup(function(){
	  	if($("#web_marza_check").is(':checked')){
		  	var web_marza = $(this).val();
	  		var nabavna_cena = $("input[name='racunska_cena_nc']").val();
		  	if($.isNumeric(web_marza) && $.isNumeric(nabavna_cena)){
			  	var tarifna_grupa_id = $("#tarifna_grupa_id").val();

			  	$.post(base_url+'admin/ajax/articles', {action: 'vrednost_poreza', tarifna_grupa_id: tarifna_grupa_id}, function (response){
				  	var web_cena = nabavna_cena*(1+web_marza/100)*(1+response/100);
				  	$("input[name='web_cena']").val(parseFloat(web_cena).toFixed(2));		  		
			  	});
			}
		}
	  });
		$("#web_marza_check").click(function(){
			if($("#web_marza_check").is(':checked')){
			  	var web_marza = $("#web_marza").val();
		  		var nabavna_cena = $("input[name='racunska_cena_nc']").val();
			  	if($.isNumeric(web_marza) && $.isNumeric(nabavna_cena)){
				  	var tarifna_grupa_id = $("#tarifna_grupa_id").val();

				  	$.post(base_url+'admin/ajax/articles', {action: 'vrednost_poreza', tarifna_grupa_id: tarifna_grupa_id}, function (response){
					  	var web_cena = nabavna_cena*(1+web_marza/100)*(1+response/100);
					  	$("input[name='web_cena']").val(parseFloat(web_cena).toFixed(2));		  		
				  	});
				}
			}
  		});

	  // product promena mp marze
	  $("#mp_marza").keyup(function(){
	  	if($("#mp_marza_check").is(':checked')){
		  	var mp_marza = $(this).val();
	  		var nabavna_cena = $("input[name='racunska_cena_nc']").val();
		  	if($.isNumeric(mp_marza) && $.isNumeric(nabavna_cena)){
			  	var tarifna_grupa_id = $("#tarifna_grupa_id").val();

			  	$.post(base_url+'admin/ajax/articles', {action: 'vrednost_poreza', tarifna_grupa_id: tarifna_grupa_id}, function (response){
				  	var mpcena = nabavna_cena*(1+mp_marza/100)*(1+response/100);
				  	$("input[name='mpcena']").val(parseFloat(mpcena).toFixed(2));		  		
			  	});
			}
		}
	  });
		$("#mp_marza_check").click(function(){
			if($("#mp_marza_check").is(':checked')){
			  	var mp_marza = $("#mp_marza").val();
		  		var nabavna_cena = $("input[name='racunska_cena_nc']").val();
			  	if($.isNumeric(mp_marza) && $.isNumeric(nabavna_cena)){
				  	var tarifna_grupa_id = $("#tarifna_grupa_id").val();

				  	$.post(base_url+'admin/ajax/articles', {action: 'vrednost_poreza', tarifna_grupa_id: tarifna_grupa_id}, function (response){
					  	var mpcena = nabavna_cena*(1+mp_marza/100)*(1+response/100);
					  	$("input[name='mpcena']").val(parseFloat(mpcena).toFixed(2));		  		
				  	});
				}
			}
  		});
 	

 	 // product promena END marze
	  $("#end_marza").keyup(function(){
	  	if($("#end_marza_check").is(':checked')){
		  	var end_marza = $(this).val();
	  		var nabavna_cena = $("input[name='racunska_cena_nc']").val();
		  	if($.isNumeric(end_marza) && $.isNumeric(nabavna_cena)){
			  	var tarifna_grupa_id = $("#tarifna_grupa_id").val();

			  	$.post(base_url+'admin/ajax/articles', {action: 'vrednost_poreza', tarifna_grupa_id: tarifna_grupa_id}, function (response){
				  	var racunska_cena_end = nabavna_cena*(1+end_marza/100);
				  	$("input[name='racunska_cena_end']").val(parseFloat(racunska_cena_end).toFixed(2));		  		
			  	});
			}
		}
	  });
		$("#end_marza_check").click(function(){
			if($("#end_marza_check").is(':checked')){
			  	var end_marza = $("#end_marza").val();
		  		var nabavna_cena = $("input[name='racunska_cena_nc']").val();
			  	if($.isNumeric(end_marza) && $.isNumeric(nabavna_cena)){
				  	var tarifna_grupa_id = $("#tarifna_grupa_id").val();

				  	$.post(base_url+'admin/ajax/articles', {action: 'vrednost_poreza', tarifna_grupa_id: tarifna_grupa_id}, function (response){
					  	var racunska_cena_end = nabavna_cena*(1+end_marza/100);
					  	$("input[name='racunska_cena_end']").val(parseFloat(racunska_cena_end).toFixed(2));		  		
				  	});
				}
			}
  		});





	  // product promena akcijske cene
	  $("#JSAkcijskaCena").keyup(function(){
		  	var akcijska_cena = $(this).val();
	  		var web_cena = $("input[name='web_cena']").val();
		  	if($.isNumeric(akcijska_cena) && $.isNumeric(web_cena)){
		  		var akcija_popust = 100-((100*akcijska_cena)/web_cena);
		  		$("input[name='akcija_popust']").val(parseFloat(akcija_popust).toFixed(2));
			}
	  });
	  // product promena akcijskog popusta
	  $("#JSAkcijaPopust").keyup(function(){
		  	var akcija_popust = $(this).val();
	  		var web_cena = $("input[name='web_cena']").val();
		  	if($.isNumeric(akcija_popust) && $.isNumeric(web_cena)){
		  		var akcijska_cena = web_cena*(1-(akcija_popust/100));
		  		$("input[name='akcijska_cena']").val(parseFloat(akcijska_cena).toFixed(2));
			}
	  });
	 //  // product promena web cene
	 //  $("input[name='web_cena']").keyup(function(){
	 //  	var web_cena = $(this).val();
  // 		var nabavna_cena = $("input[name='racunska_cena_nc']").val();
	 //  	if($.isNumeric(web_cena) && $.isNumeric(nabavna_cena) && nabavna_cena != 0){
		//   	var tarifna_grupa_id = $("#tarifna_grupa_id").val();

		//   	$.post(base_url+'admin/ajax/articles', {action: 'vrednost_poreza', tarifna_grupa_id: tarifna_grupa_id}, function (response){

		// 	  	var web_marza = ((web_cena / (nabavna_cena*(1+response/100)))-1) * 100;

		// 	  	$("input[name='web_marza']").val(round(web_marza,2));		  		
		//   	});
		// }
	 //  });

	 //  // product promena mp cene
	 //  $("input[name='mpcena']").keyup(function(){
	 //  	var mpcena = $(this).val();
  // 		var nabavna_cena = $("input[name='racunska_cena_nc']").val();
	 //  	if($.isNumeric(mpcena) && $.isNumeric(nabavna_cena)){
		//   	var tarifna_grupa_id = $("#tarifna_grupa_id").val();

		//   	$.post(base_url+'admin/ajax/articles', {action: 'vrednost_poreza', tarifna_grupa_id: tarifna_grupa_id}, function (response){

		// 	  	var mp_marza = ((mpcena / (nabavna_cena*(1+response/100)))-1) * 100;

		// 	  	$("input[name='mp_marza']").val(round(mp_marza,2));		  		
		//   	});
		// }
	 //  });

	  	// product osvezi
		$("#JSArtikalRefresh").click(function(){
			location.reload(true)
		});

		//GRUPE
	  // redni brojevi za grupe
	   $( ".rbrg" ).focusout(function(){
	  		var grupa_pr_id = $(this).parent().data('id');
	  		var rbr_old = $(this).parent().data('old');
	  		var rbr = $(this).val();

	  		if(rbr != '' && $.isNumeric(rbr) && rbr != rbr_old){
	  			$(this).parent().data('old',rbr);
	  			$.post(base_url+'admin/ajax/groups', {action: 'grupa_rbr', grupa_pr_id: grupa_pr_id, rbr: rbr}, function (response){});
	  		}else{
	  			$(this).val(rbr_old);
	  		}
	  });

	   // KARAKTERISTIKE
	   //generisane karakteristike
		$( "#add_naziv_karak" ).click(function(){
			window.open(base_url+'admin/grupe/'+$(this).data('grupa'), '_blank');
		});

		$( "#add_all_karak" ).click(function(){
			var roba_id = $('#roba_id_obj').val();

			$.post(base_url+'admin/ajax/generisane', {action: 'add_all_karak', roba_id: roba_id}, function (response){ location.reload(true); });
		});

		$( "#add_karak" ).click(function(){
			var roba_id = $('#roba_id_obj').val();
			var grupa_pr_naziv_id = $(this).closest('.row').find('#grupa_pr_naziv_id').val();

			$.post(base_url+'admin/ajax/generisane', {action: 'add_karak', roba_id: roba_id, grupa_pr_naziv_id: grupa_pr_naziv_id}, function (response){ location.reload(true); });
		});

		$( ".save-gener" ).change(function(){
			var roba_id = $('#roba_id_obj').val();
			var grupa_pr_naziv_id = $(this).data('id');
			var grupa_pr_vrednost_id = $(this).val();
			var rbr = $(this).closest('.inline-list').find('input').val();

			$.post(base_url+'admin/ajax/generisane', {action: 'save-gener', roba_id: roba_id, grupa_pr_naziv_id: grupa_pr_naziv_id, grupa_pr_vrednost_id: grupa_pr_vrednost_id, rbr: rbr}, function (response){ location.reload(true); });
		});
		$( ".save-gener-value" ).click(function(){
			var roba_id = $('#roba_id_obj').val();
			var naziv = $(this).prev('.JSnaziv').val(); 
			//console.log(naziv);
			var grupa_pr_naziv_id = $(this).data('id');	
			var grupa_pr_vrednost_id = $(this).data('vrednost'); 
		
			var rbr = $(this).closest('.inline-list').find('input').val();
			if(naziv != ''){
			$.post(base_url+'admin/ajax/generisane', {action: 'save-gener-val', roba_id: roba_id, naziv:naziv, grupa_pr_naziv_id: grupa_pr_naziv_id, grupa_pr_vrednost_id: grupa_pr_vrednost_id, rbr: rbr}, function (response){ location.reload(true); });
			alertify.success(translate('Uspešno ste uneli novu karakteristiku! Vrednost nece biti uneta ukoliko vec postoji'))
			}else{
			alertify.alert(translate('Polje za unos karakteristike ne sme biti prazno!!!'))	
			}
		});

		$( ".delete-gener" ).click(function(){
			var roba_id = $('#roba_id_obj').val();
			var grupa_pr_naziv_id = $(this).data('id');

			$.post(base_url+'admin/ajax/generisane', {action: 'delete-gener', roba_id: roba_id, grupa_pr_naziv_id: grupa_pr_naziv_id}, function (response){ location.reload(true); });
		});

		$( "#edit-proizvodjac" ).change(function(){
			var url = $(this).val();
			if(url != ''){
				location.href = url;
			}
		});

		//od dobavljaca
		$( ".JSKarak" ).click(function(){
			var karakteristika_id = $(this).data('id');
			var naziv = $(this).closest('tr').find('.JSKarakNaziv').val();
			var vrednost = $(this).closest('tr').find('.JSKarakVrednost').val();
			var data = {
				action: 'save_naziv',
				karakteristika_id: karakteristika_id, 
				naziv: naziv, 
				vrednost: vrednost
			}

			if(karakteristika_id == 'new'){
				data.roba_id = $('#roba_id_obj').val();
				data.grupa = $(this).closest('.JSGrupaNazivData').find('.JSGrupaNaziv').val();
			}

			if(naziv!='' && vrednost!=''){
				$.post(base_url+'admin/ajax/od_dobavljaca', data, function (response){ location.reload(true); });
			}
		});

		$( ".JSKarakDelete" ).click(function(){
			var karakteristika_id = $(this).data('id');

			alertify.confirm(translate("Da li ste sigurni da želite da izvršite akciju brisanja?"),
            function(e){
                if(e){
			$.post(base_url+'admin/ajax/od_dobavljaca', {action: 'delete_naziv', karakteristika_id: karakteristika_id}, function (response){ location.reload(true); });
				}
			});
		});

		$( ".JSGrupa" ).click(function(){
			var roba_id = $('#roba_id_obj').val();
			var grupa_old = $(this).data('old');
			var grupa = $(this).closest('tr').find('.JSGrupaNaziv').val();

			if(grupa!=''){
				if(grupa_old == 'new'){
					$.post(base_url+'admin/ajax/od_dobavljaca', {action: 'save_grupa', roba_id: roba_id, grupa: grupa}, function (response){ location.reload(true); });
				}else{
					// var rbr = $(this).closest('tr').find('.JSGrupaRbr').val();		
					// if($.isNumeric(rbr)){
						$.post(base_url+'admin/ajax/od_dobavljaca', {action: 'save_grupa', roba_id: roba_id, grupa: grupa, grupa_old: grupa_old}, function (response){ location.reload(true); });
					// }
				}
			}
		});

		$( ".JSGrupaDelete" ).click(function(){
			var roba_id = $('#roba_id_obj').val();
			var grupa_old = $(this).data('old');
			alertify.confirm(translate("Da li ste sigurni da želite da izvršite akciju brisanja?"),
            function(e){
                if(e){
			$.post(base_url+'admin/ajax/od_dobavljaca', {action: 'delete_grupa', roba_id: roba_id, grupa_old: grupa_old}, function (response){ location.reload(true); });
				}
          	});
		});

		// PRETRAGA ZA ARTIKLE
		var articlesTime;
		$('#articles').on("keyup", function() {
			clearTimeout(articlesTime);
			articlesTime = setTimeout(function(){
				$('.articles_list').remove();
				var articles = $('#articles').val();
				var general_id = $('#articles').data("id");
				if (articles != '' && articles.length > 2) {
					$.post(base_url + 'admin/ajax/articles_search', {articles:articles,general_id:general_id}, function (response){
						$('#search_content').html(response);
					});
				} 				
			}, 500);
		});
		$('html :not(.articles_list)').on("click", function() {
			$('.articles_list').remove();
		});

		// PRETRAGA ZA ARTIKLE
			//SRODNI ARTIKLI
			var articlesTime;
			$('#articles1').on("keyup", function() {
				clearTimeout(articlesTime);
				articlesTime = setTimeout(function(){
					$('.articles_list').remove();
					var articles = $('#articles1').val();
					var general_id = $('#articles1').data("id");
					if (articles != '' && articles.length > 2) {
						$.post(base_url + 'admin/ajax/srodni_search', {articles:articles,general_id:general_id}, function (response){
							$('#search_content1').html(response);
						});
					} 				
				}, 500);
			});
			$('html :not(.articles_list)').on("click", function() {
				$('.articles_list').remove();
			});

			$('.SrodniVrednost').on("change", function() {
				if ($(this).val() != -1) {
					var store = $(this).data('store');
					var grupa_pr_vrednost_id = $(this).val();
					$.post(base_url+'admin/ajax/povezani', {action: 'izmeni_vrednost', grupa_pr_vrednost_id: grupa_pr_vrednost_id, store: store}, function (response){
						location.href = base_url+'admin/srodni_artikli/'+response;
					});
				}
			});

			$( ".JSFlagCenaVezanSrodni" ).click(function(){
	        var aktivan;
	        if($(this).attr('checked')){
	            aktivan = 0;
	            $(this).removeAttr("checked");
	        }else{
	            aktivan = 1;
	            $(this).attr('checked', '');
	        }
			var store = $(this).data('store');
			$.post(base_url+'admin/ajax/povezani', {action: 'flag_cena_srodni', aktivan: aktivan, store: store}, function (response){ });
		});
			$( ".JSDeleteSrodni" ).click(function(){
				var store = $(this).data('store');
				$.post(base_url+'admin/ajax/povezani', {action: 'delete_srodni', store: store}, function (response){
					location.href = base_url+'admin/srodni_artikli/'+response;
				});
			});


			// VEZANI ARTIKLI
			$( ".JSsacuvajCenu" ).click(function(){
				var store = $(this).data('store');
				var cena = $(this).closest('tr').find('.JSCenaVezan').val();
				$.post(base_url+'admin/ajax/povezani', {action: 'sacuvaj_cenu', cena: cena, store: store}, function (response){
					location.href = base_url+'admin/vezani_artikli/'+response;
				});
			});
			$( ".JSDeleteVezan" ).click(function(){
				var store = $(this).data('store');
				$.post(base_url+'admin/ajax/povezani', {action: 'delete', store: store}, function (response){
					location.href = base_url+'admin/vezani_artikli/'+response;
				});
			});
		$( ".JSFlagCenaVezan" ).click(function(){
	        var aktivan;
	        if($(this).attr('checked')){
	            aktivan = 0;
	            $(this).removeAttr("checked");
	        }else{
	            aktivan = 1;
	            $(this).attr('checked', '');
	        }
			var store = $(this).data('store');
			$.post(base_url+'admin/ajax/povezani', {action: 'flag_cena', aktivan: aktivan, store: store}, function (response){ });
		});



		// zatvaranje prozora
		$('.fa-times').click(function() {
			window.close();
		});
		// UPLOAD SLIKE
		$('#JSSlika').on('change',function() {
			$("#JSUploadForm").trigger("submit");
		});

		// dodatni fajlovi
		$('.JSCheckFilter').click(function(){

			var action = $(this).data('action');
	        if($(this).attr('checked')){
	            location.href = base_url + 'admin/dodatni_fajlovi/'+action.roba_id;
	        }else{
				location.href = base_url + 'admin/dodatni_fajlovi/'+action.roba_id+'/'+action.vrsta_id;
	        }			
		});

		// $('#filePutanja').hide();

		$("#check_link").change(function(){

			var action = $(this).val();
			if(action == 1){
				$('#filePutanja').show();
				$('#fileFile').hide();
			}else{
				$('#filePutanja').hide();
				$('#fileFile').show();				
			}		
		});



		$(document).click(function(event){
			if(
				!$(event.target).hasClass('JSListaGrupa') 
				&& $(event.target).attr('name') != 'grupa_pr_grupa'
			){
				$('#JSListaGrupe').attr('hidden','hidden');	
			}
			if(
				!$(event.target).hasClass('JSListaProizvodjac') 
				&& $(event.target).attr('name') != 'proizvodjac'
			){
				$('#JSListaProizvodjaca').attr('hidden','hidden');	
			}
			if(
				!$(event.target).hasClass('JSListaJedinicaMere') 
				&& $(event.target).attr('name') != 'jedinica_mere'
			){
				$('#JSListaJedinicaMera').attr('hidden','hidden');	
			}
			if(
				!$(event.target).hasClass('JSListaFlagCena') 
				&& $(event.target).attr('name') != 'roba_flag_cene'
			){
				$('#JSListaFlagCene').attr('hidden','hidden');	
			}
			if(
				!$(event.target).hasClass('JSListaTipArtikla') 
				&& $(event.target).attr('name') != 'tip_artikla'
			){
				$('#JSListaTipaArtikla').attr('hidden','hidden');	
			}
		});
		//grupe
		$('input[name="grupa_pr_grupa"]').click(function(){
			if($('#JSListaGrupe').attr('hidden') == 'hidden'){
				$('#JSListaGrupe').removeAttr('hidden');
			}else{
				$('#JSListaGrupe').attr('hidden','hidden');	
			}	
		});
		$('.JSListaGrupa').click(function(){
			var grupa = $(this).data('grupa');
			$('input[name="grupa_pr_grupa"]').val(grupa);

			// var grupa_pr_id = $(this).data('grupa_pr_id');
			// var roba_id = $("input[name='roba_id']").val();
			// $.post(base_url + 'admin/grupa-karakteristike', { roba_id: roba_id, grupa_pr_id: grupa_pr_id }, function(response) {
			// 	$('#JSGrupaKarakteristike').html(response);
			// });

			$('#JSListaGrupe').attr('hidden','hidden');	
		});

		//proizvodjaci
		$('input[name="proizvodjac"]').click(function(){
			if($('#JSListaProizvodjaca').attr('hidden') == 'hidden'){
				$('#JSListaProizvodjaca').removeAttr('hidden');
			}else{
				$('#JSListaProizvodjaca').attr('hidden','hidden');	
			}	
		});
		$('.JSListaProizvodjac').click(function(){
			var grupa = $(this).data('proizvodjac');
			$('input[name="proizvodjac"]').val(grupa);
			$('#JSListaProizvodjaca').attr('hidden','hidden');	
		});

		//jedinice_mere
		$('input[name="jedinica_mere"]').click(function(){
			if($('#JSListaJedinicaMera').attr('hidden') == 'hidden'){
				$('#JSListaJedinicaMera').removeAttr('hidden');
			}else{
				$('#JSListaJedinicaMera').attr('hidden','hidden');	
			}	
		});
		$('.JSListaJedinicaMere').click(function(){
			var grupa = $(this).data('jedinica_mere');
			$('input[name="jedinica_mere"]').val(grupa);
			$('#JSListaJedinicaMera').attr('hidden','hidden');	
		});

		//jedinice_mere
		$('input[name="roba_flag_cene"]').click(function(){
			if($('#JSListaFlagCene').attr('hidden') == 'hidden'){
				$('#JSListaFlagCene').removeAttr('hidden');
			}else{
				$('#JSListaFlagCene').attr('hidden','hidden');	
			}	
		});
		$('.JSListaFlagCena').click(function(){
			var grupa = $(this).data('roba_flag_cene');
			$('input[name="roba_flag_cene"]').val(grupa);
			$('#JSListaFlagCene').attr('hidden','hidden');	
		});

		//jedinice_mere
		$('input[name="tip_artikla"]').click(function(){
			if($('#JSListaTipaArtikla').attr('hidden') == 'hidden'){
				$('#JSListaTipaArtikla').removeAttr('hidden');
			}else{
				$('#JSListaTipaArtikla').attr('hidden','hidden');	
			}	
		});
		$('.JSListaTipArtikla').click(function(){
			var grupa = $(this).data('tip_artikla');
			$('input[name="tip_artikla"]').val(grupa);
			$('#JSListaTipaArtikla').attr('hidden','hidden');	
		});

		$(".JSSlikaDeleteShort").click(function(){
			var link = $(this).data('link');
	        alertify.confirm("Da li želite da obrišete sliku?",
	            function(e){
	            if(e){
	                location.href = link;
	            }
	        }); 			
		});
		$("input[name='slike[]']").on("change", function(){
			$('.JSUploadImage').remove();

			for (var i = this.files.length - 1; i >= 0; i--) {
		        var extension = this.files[i].name.split('.').pop().toLowerCase();
		        if(['png','jpg','jpeg'].includes(extension)){
			        var reader = new FileReader();
			        reader.onload = function (e) {
			            $('#JSUploadImages').prepend('<img src="'+e.target.result+'" class="JSUploadImage">');
			        }
			        reader.readAsDataURL(this.files[i]);
			    }
			}
		});

		$(document).on("click",".JSGrupaNazivRemove",function(){
			$(this).closest('li').find("input[type='radio']").removeAttr('checked');
		});

		$(function() {
			$( "#datum_akcije_od, #datum_akcije_do" ).datepicker();
		});
		$('#datum_akcije_od').keydown(false);
		$('#datum_akcije_do').keydown(false);
		$('#datum_od_delete').click(function(){
			$('#datum_akcije_od').val('');
		});
		$('#datum_do_delete').click(function(){
			$('#datum_akcije_do').val('');
		});

		$('#JSAkcijaFlagPrimeni').change(function(){
			if($(this).val() == 1){
				$('#JSAkcijaDatum').removeAttr('hidden');
			}else{
				$('#JSAkcijaDatum').attr('hidden','hidden');
			}
		});

});

$(".Deleting1").click(function(e, ){
						
				alertify.confirm(translate("Da li ste sigurni da želite obrisati artikal?"),
			function(e){
	            if(e){
				

				var niz=location.href;
				var niz1= niz.split("/");
				niz1[niz1.length-2]='product-delete';
				location.href=niz1.join("/");	

				
			}
		});
			
		});

function round(value, exp) {
  if (typeof exp === 'undefined' || +exp === 0)
    return Math.round(value);

  value = +value;
  exp = +exp;

  if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0))
    return NaN;

  // Shift
  value = value.toString().split('e');
  value = Math.round(+(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp)));

  // Shift back
  value = value.toString().split('e');
  return +(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp));
}




$(function() {
    $('#sortable_grupa').sortable({
        update: function(event, ui) {
        	
        	var moved = ui.item.attr('grupa');

        	
    		var roba_id = $('#roba_id_obj').val();
            var order = []; 
            $('#sortable_grupa tr.JSGrupaNazivData').each( function(index,value) {
            	if($(value).attr('grupa') && $(value).attr('grupa') != ''){
          			order.push($(value).attr('grupa') );                
            	}
            });
            $.ajax({
                type: "POST",
                url: base_url+'admin/position-naziv',
                data: {is_grupa: 1,roba_id: roba_id,order:order,moved:moved},
                success: function(msg) {
                	// location.reload(true);
                }
            });
        }
    });                    
    $( "#sortable_grupa").disableSelection();         
});

$(function() {
    $('#sortable_naziv').sortable({
        update: function(event, ui) {

        	var grupa = ui.item.attr('grupa');
        	var naziv = ui.item.attr('naziv');
        	var vrednost = ui.item.attr('vrednost');

    		var roba_id = $('#roba_id_obj').val();
            var order = []; 
            $('#sortable_naziv tr').each( function(index,value) {
            	if($(value).attr('naziv') && $(value).attr('naziv') != ''){
          			order.push($(value).attr('naziv') );                
            	}
            });
            $.ajax({
                type: "POST",
                url: base_url+'admin/position-naziv',
                data: {is_grupa: 0,roba_id: roba_id,order:order,grupa:grupa,naziv:naziv,vrednost:vrednost},
                success: function(msg) {
                	location.reload(true);
                }
            });
        }
    });                    
    $( "#sortable_naziv").disableSelection();         
});
	//$('.JSRbrInput').hide();

		// $('.JSEditRbrBtn').on('click', function(){
		// 	$(this).parent().children('.JSRbrInput').show();
		// 	$(this).parent().children('.JSRbrInput').focus();
		// });

		// $('.JSRbrInput').focusout(function(){
		// 	$(this).css('display', 'none');
		// });

		$('.JSRbrInput').on("keyup", function(event) {
			var val = $(this).val();
			var art_id = $(this).data('id');			

			if(event.keyCode == 13){
				//$(this).closest('.rbr').children('.JSRbrVrednost').html(val);
		    	//$(this).focusout();

		    	$.post(base_url+'admin/ajax/dc_articles', {action:'promena_rbr', art_id: art_id, val: val}, function (response){
		    		alertify.success('Uspešno ste promenili redni broj')
		    	});
		   	}
		});

$(function() {
    $('.JSSortableNaziv').sortable({
        update: function(event, ui) {

        	var grupa = ui.item.attr('grupa');
        	var naziv = ui.item.attr('naziv');
        	var vrednost = ui.item.attr('vrednost');

    		var roba_id = $('#roba_id_obj').val();
    		var grupa = $(this).attr('grupa');
            var order = []; 
            $(this).find('tr').each( function(index,value) {
            	if($(value).attr('naziv') && $(value).attr('naziv') != ''){
          			order.push($(value).attr('naziv') );                
            	}
            });

            $.ajax({
                type: "POST",
                url: base_url+'admin/position-naziv',
                data: {is_grupa: 0,roba_id: roba_id, grupa: grupa,order:order,grupa:grupa,naziv:naziv,vrednost:vrednost},
                success: function(msg) {
                	// location.reload(true);
                }
            });
        }
    });                    
    $( "#sortable_podgrupa").disableSelection();         
});